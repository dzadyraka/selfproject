<!DOCTYPE html>
<html>
<head>
    <title>Reset Password Email</title>
</head>

<body>
<h2>Dear User,</h2>
<br/>
<a href="{{url('').'/login/reset/password?token=' .$password_reset['token'] }}">Link to reset password page</a>
</body>

</html>