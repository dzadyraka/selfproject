<!DOCTYPE html>
<html>
<head>
    <title>Welcome Email</title>
</head>

<body>
<h2>Dear User,</h2>
<br/>
Please input the validation code below into field "Validation code" on the Registration Page
<br/>
CODE : {{$user->verifyUser->token}}
<br/>
<a href="{{url('').'/registration?email='.$user->email.'&code='.$user->verifyUser->token}}">Link to registration page</a>
</body>

</html>