<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')->group(function () {


    Route::prefix('auth')->group(function () {
        Route::post('', 'Auth\AuthController@login')->name('api-login');
        Route::prefix('password')->group(function () {
            Route::post('forgot', 'Auth\AuthController@sendResetPassword')->name('forgot-password');
            Route::post('reset', 'Auth\AuthController@resetPassword')->name('reset-password');
        });

    });

    Route::prefix('locations')->group(function () {
        Route::prefix('countries')->group(function () {
            Route::get('', 'Location\LocationController@getCountriesList')->name('countries');
            Route::prefix('{id}')->group(function () {
                Route::get('partners', 'Location\LocationController@getPartnerCountriesList')->name('country-partners');
                Route::get('turnover', 'Location\LocationController@getTopTurnover')->name('country-turnover');
                Route::prefix('regions')->group(function () {
                    Route::get('', 'Location\LocationController@getCountryRegionsList')->name('country-regions');
                    Route::prefix('{region_id}')->group(function () {
                        Route::get('cities', 'Location\LocationController@getCountryRegionCitiesList')->name('country-region-cities');
                    });
                });
                Route::prefix('cities')->group(function () {
                    Route::get('', 'Location\LocationController@getCountryCitiesList')->name('country-cities');
                    Route::prefix('{city_id}')->group(function () {
                        Route::get('', 'Location\LocationController@getCity');
                        Route::get('ports', 'Ports\PortsController@getClosestPortsForCity');
                    });
                });
            });
        });
        Route::get('detect-user-country', 'Location\LocationController@detectUser')->name('detect-user');
    });

    Route::post('files/upload', 'Files\FilesController@upload')
        ->name('upload-file')
        ->middleware('jwt.auth');

    Route::post('files', 'Files\FilesController@uploadFiles')
        ->name('upload-files')
        ->middleware('jwt.auth');

    Route::prefix('transactions')->middleware('jwt.auth')->group(function () {
        Route::get('', 'Balance\BalanceController@getTransactionsList')->name('transactions-list');
        Route::post('', 'Balance\BalanceController@createTransaction')->name('transactions-create');
    });

    Route::prefix('blog')->group(function () {
        Route::get('', 'Blog\BlogController@getPostsList')->name('blog-list');
        Route::post('', 'Blog\BlogController@getPostsList')->name('blog-list');
        Route::put('', 'Blog\BlogController@createPost')->name('posts-create');

        Route::prefix('sections')->group(function () {
            Route::get('', 'Blog\BlogController@getSectionsList')->name('blog-sections-list');
            Route::put('', 'Blog\BlogController@createSection')->name('blog-sections-create');
            Route::post('', 'Blog\BlogController@createSection')->name('blog-sections-create');
            Route::get('{id}', 'Blog\BlogController@getSection')->name('blog-sections-get');
            Route::post('{id}', 'Blog\BlogController@updateSection')->name('blog-sections-update');
            Route::patch('{id}', 'Blog\BlogController@updateSection')->name('blog-sections-update');
            Route::delete('{id}', 'Blog\BlogController@deleteSection')->name('blog-sections-delete');
        });

        Route::get('{id}', 'Blog\BlogController@getPost')->name('posts-get');
        Route::post('{id}', 'Blog\BlogController@updatePost')->name('posts-update');
        Route::patch('{id}', 'Blog\BlogController@updatePost')->name('posts-update');
        Route::delete('{id}', 'Blog\BlogController@deletePost')->name('posts-delete');
        Route::get('{id}/recommendations', 'Blog\BlogController@getPostRecommendations')->name('posts-recommendations');
    });

    Route::prefix('faq')->group(function () {
        Route::get('', 'Faq\FaqController@getList')->name('faq-list');
        Route::post('', 'Faq\FaqController@getList')->name('faq-list-post');
        Route::put('', 'Faq\FaqController@create')->name('faq-create');

        Route::prefix('sections')->group(function () {
            Route::get('', 'Faq\FaqController@getSectionsList')->name('faq-sections-list');
            Route::put('', 'Faq\FaqController@createSection')->name('faq-sections-create');
            Route::post('', 'Faq\FaqController@createSection')->name('faq-sections-create-post');
            Route::get('{id}', 'Faq\FaqController@getSection')->name('faq-sections-get');
            Route::post('{id}', 'Faq\FaqController@updateSection')->name('faq-sections-update');
            Route::patch('{id}', 'Faq\FaqController@updateSection')->name('faq-sections-update-patch');
            Route::delete('{id}', 'Faq\FaqController@deleteSection')->name('faq-sections-delete');
        });

        Route::get('{id}', 'Faq\FaqController@get')->name('faq-get');
        Route::post('{id}', 'Faq\FaqController@edit')->name('faq-update');
        Route::patch('{id}', 'Faq\FaqController@edit')->name('faq-update');
        Route::delete('{id}', 'Faq\FaqController@delete')->name('faq-delete');
        Route::get('{id}/recommendations', 'Faq\FaqController@getRecommendations')->name('faq-recommendations');
        Route::get('{id}/prev-next', 'Faq\FaqController@getNext')->name('faq-next');
        Route::post('{id}/prev-next', 'Faq\FaqController@getNext')->name('faq-next');
    });

    Route::prefix('knowledge-base')->group(function () {
        Route::get('', 'KnowledgeBase\KnowledgeBaseController@getArticlesList')->name('knowledge-base-list');
        Route::post('', 'KnowledgeBase\KnowledgeBaseController@getArticlesList')->name('knowledge-base-list-post');
        Route::put('', 'KnowledgeBase\KnowledgeBaseController@createArticle')->name('knowledge-base-create');

        Route::prefix('sections')->group(function () {
            Route::get('', 'KnowledgeBase\KnowledgeBaseController@getSectionsList')->name('knowledge-base-sections-list');
            Route::put('', 'KnowledgeBase\KnowledgeBaseController@createSection')->name('knowledge-base-sections-create');
            Route::post('', 'KnowledgeBase\KnowledgeBaseController@createSection')->name('knowledge-base-sections-create-post');

            Route::prefix('{section}')->group(function () {
                Route::get('', 'KnowledgeBase\KnowledgeBaseController@getSection')->name('knowledge-base-sections-get');
                Route::post('', 'KnowledgeBase\KnowledgeBaseController@updateSection')->name('knowledge-base-sections-update');
                Route::patch('', 'KnowledgeBase\KnowledgeBaseController@updateSection')->name('knowledge-base-sections-update-patch');
                Route::delete('', 'KnowledgeBase\KnowledgeBaseController@deleteSection')->name('knowledge-base-sections-delete');

                Route::prefix('sub-sections')->group(function () {
                    Route::get('', 'KnowledgeBase\KnowledgeBaseController@getSubSectionsList')->name('knowledge-base-sub-sections-list');
                    Route::put('', 'KnowledgeBase\KnowledgeBaseController@createSubSection')->name('knowledge-base-sub-sections-create');
                    Route::post('', 'KnowledgeBase\KnowledgeBaseController@createSubSection')->name('knowledge-base-sub-sections-create-post');
                    Route::get('{id}', 'KnowledgeBase\KnowledgeBaseController@getSubSection')->name('knowledge-base-sub-sections-get');
                    Route::post('{id}', 'KnowledgeBase\KnowledgeBaseController@updateSubSection')->name('knowledge-base-sub-sections-update');
                    Route::patch('{id}', 'KnowledgeBase\KnowledgeBaseController@updateSubSection')->name('knowledge-base-sub-sections-update-patch');
                    Route::delete('{id}', 'KnowledgeBase\KnowledgeBaseController@deleteSubSection')->name('knowledge-base-sub-sections-delete');
                });

                Route::prefix('for-whom')->group(function () {
                    Route::get('', 'KnowledgeBase\KnowledgeBaseController@getForWhomsList')->name('knowledge-base-for-whom-list');
                    Route::put('', 'KnowledgeBase\KnowledgeBaseController@createForWhom')->name('knowledge-base-for-whom-create');
                    Route::post('', 'KnowledgeBase\KnowledgeBaseController@createForWhom')->name('knowledge-base-for-whom-create-post');
                    Route::get('{id}', 'KnowledgeBase\KnowledgeBaseController@getForWhom')->name('knowledge-base-for-whom-get');
                    Route::post('{id}', 'KnowledgeBase\KnowledgeBaseController@updateForWhom')->name('knowledge-base-for-whom-update');
                    Route::patch('{id}', 'KnowledgeBase\KnowledgeBaseController@updateForWhom')->name('knowledge-base-for-whom-update-patch');
                    Route::delete('{id}', 'KnowledgeBase\KnowledgeBaseController@deleteForWhom')->name('knowledge-base-for-whom-delete');
                });
            });
        });

        Route::get('{id}', 'KnowledgeBase\KnowledgeBaseController@getArticle')->name('knowledge-base-get');
        Route::post('{id}', 'KnowledgeBase\KnowledgeBaseController@updateArticle')->name('knowledge-base-update');
        Route::patch('{id}', 'KnowledgeBase\KnowledgeBaseController@updateArticle')->name('knowledge-base-update-patch');
        Route::delete('{id}', 'KnowledgeBase\KnowledgeBaseController@deleteArticle')->name('knowledge-base-delete');
        Route::get('{id}/recommendations', 'KnowledgeBase\KnowledgeBaseController@getArticleRecommendations')->name('knowledge-base-recommendations');
        Route::get('{id}/prev-next', 'KnowledgeBase\KnowledgeBaseController@getNextArticle')->name('knowledge-base-next');
        Route::post('{id}/prev-next', 'KnowledgeBase\KnowledgeBaseController@getNextArticle')->name('knowledge-base-next-post');
    });

    Route::prefix('settings')->group(function () {
        Route::prefix('{module_name}')->group(function () {
            Route::get('', 'Settings\SettingsController@getList')->name('settings-list');
            Route::put('', 'Settings\SettingsController@create')->name('create-settings');
            Route::prefix('{id}')->group(function () {
                Route::get('', 'Settings\SettingsController@view')->name('view-settings');
                Route::post('', 'Settings\SettingsController@edit')->name('update-settings');
                Route::patch('', 'Settings\SettingsController@edit')->name('update-settings-patch');
                Route::delete('', 'Settings\SettingsController@delete')->name('delete-settings');
            });
        });
    });

    Route::prefix('messages')->group(function () {
        Route::put('', 'Messages\MessagesController@sendMessage')->name('send-message')->middleware(['transaction']);
        Route::get('', 'Messages\MessagesController@getConversations')->name('conversations-list');
        Route::delete('{id}', 'Messages\MessagesController@deleteMessage')->name('delete-message');
        Route::prefix('conversation')->group(function () {
            Route::prefix('{id}')->group(function () {
                Route::post('', 'Messages\MessagesController@setSeenStatus')->name('set-seen-status');
                Route::patch('', 'Messages\MessagesController@setSeenStatus')->name('set-seen-status-patch');
                Route::get('', 'Messages\MessagesController@getConversation')->name('get-conversation');
                Route::delete('', 'Messages\MessagesController@deleteConversation')->name('delete-conversation');
                Route::prefix('messages')->group(function () {
                    Route::get('', 'Messages\MessagesController@getConversationMessages')->name('get-conversation-messages');
                });
            });
        });
    });

    Route::prefix('currencies')->group(function () {
        Route::get('', 'Currency\CurrencyController@getCurrenciesList')->name('currencies-list');
        Route::get('rate', 'Currency\CurrencyController@getRate')->name('currencies-rate');
    });

    Route::prefix('certificates')->group(function () {
        Route::get('', 'Certificates\CertificateController@getCertificates')->name('get-certificates-list');
        Route::post('', 'Certificates\CertificateController@createCertificate')->name('create-certificate');
        Route::get('{certificateId}', 'Certificates\CertificateController@retrieveCertificate')->name('retrieve-certificate');
    });

    Route::prefix('languages')->group(function () {
        Route::get('', 'Language\LanguageController@getList')->name('languages-list');
        Route::get('code/{code}', 'Language\LanguageController@getInfoByCode')->name('languages-by-code');
        Route::get('{id}', 'Language\LanguageController@getInfoById')->name('languages-by-id');
    });

    Route::prefix('social')->group(function () {
        Route::get('linkedin/oauth', 'Social\LinkedInController@getOAuthUrl')->name('get-linkedin-oauth');
        Route::get('linkedin/oauth/callback', 'Social\LinkedInController@oAuthCallback')->name('get-linkedin-oauth');
        Route::get('google/userinfo', 'Social\SocialController@googleUserInfo')->name('google-user-info');
        Route::post('{integration}', 'Social\SocialController@addIntegration')->name('social-add');
        Route::delete('{integration}', 'Social\SocialController@deleteIntegration')->name('social-delete');
        Route::post('{integration}/login', 'Social\SocialController@login')->name('social-login');
    });

    Route::prefix('companies')->group(function () {
        Route::get('', 'Companies\CompaniesController@getList')->name('companies-list');
        Route::put('', 'Companies\CompaniesController@create')->name('create-company')->middleware(['transaction']);
        Route::get('draft', 'Companies\CompaniesController@getFirstDraftCompany')->name('get-company-draft');
        Route::get('payments', 'Companies\Payment\PaymentController@companiesPaymentList')->name('companies-payment-list');
        Route::post('check-domain', 'Companies\CompaniesController@checkSubDomainAvailability')->name('companies-check-domain');
        Route::prefix('{id}')->group(function () {
            Route::get('', 'Companies\CompaniesController@get')->name('view-company');
            Route::post('', 'Companies\CompaniesController@edit')->name('edit-company');
            Route::patch('', 'Companies\CompaniesController@edit')->name('edit-company-patch');
            Route::delete('', 'Companies\CompaniesController@delete')->name('delete-company');
            Route::post('logo', 'Companies\CompaniesController@uploadLogo')->name('upload-logo-company');
            Route::get('products', 'Products\ProductsController@companyProducts')->name('company-products');

            Route::prefix('certificates')->group(function () {
                Route::get('', 'Certificates\CertificateController@getCompanyCertificates')->name('get-company-certificates');
                Route::post('', 'Certificates\CertificateController@addCompanyCertificate')->name('add-company-certificate');
                Route::delete('{certificateId}', 'Certificates\CertificateController@deleteCompanyCertificate')->name('delete-company-certificate');
                Route::post('{certificateId}/verify', 'Certificates\CertificateController@verifyCompanyCertificate')->name('verify-company-certificate');
            });

            Route::prefix('users')->group(function () {
                Route::get('', 'Companies\CompaniesController@getCompanyUsers')->name('get-company-users');
                Route::put('', 'Companies\CompaniesController@addCompanyUser')->name('add-user-to-company');
                Route::delete('{user_id}', 'Companies\CompaniesController@removeCompanyUser')->name('delete-user-from-company');
                Route::post('{user_id}', 'Companies\CompaniesController@modifyCompanyUser')->name('edit-user-in-company');
                Route::patch('{user_id}', 'Companies\CompaniesController@modifyCompanyUser')->name('edit-user-in-company-patch');
            });
            Route::prefix('address')->group(function () {
                Route::get('', 'Companies\CompaniesController@listAddresses')->name('get-list-addresses');
                Route::put('', 'Companies\CompaniesController@addAddress')->name('add-company-address');
                Route::prefix('{address_id}')->group(function () {
                    Route::post('', 'Companies\CompaniesController@updateAddress')->name('edit-company-address');
                    Route::patch('', 'Companies\CompaniesController@updateAddress')->name('edit-company-address-patch');
                    Route::delete('', 'Companies\CompaniesController@deleteAddress')->name('delete-company-address');
                });
            });
            Route::prefix('delivery-and-payment')->group(function () {
                Route::get('', 'Companies\CompaniesController@listDeliveryPayment')->name('get-list-delivery-and-payment');
                Route::put('', 'Companies\CompaniesController@createDeliveryPayment')->name('add-delivery-and-payment');
                Route::prefix('{delivery_and_payment_id}')->group(function () {
                    Route::get('', 'Companies\CompaniesController@viewDeliveryPayment')->name('view-delivery-and-payment');
                    Route::post('', 'Companies\CompaniesController@editDeliveryPayment')->name('edit-delivery-and-payment');
                    Route::patch('', 'Companies\CompaniesController@editDeliveryPayment')->name('edit-delivery-and-payment-patch');
                    Route::delete('', 'Companies\CompaniesController@deleteDeliveryPayment')->name('delete-delivery-and-payment');
                });
            });
            Route::prefix('export-info')->group(function () {
                Route::get('', 'Companies\CompaniesController@listExportInfo')->name('get-list-export-info');
                Route::put('', 'Companies\CompaniesController@createExportInfo')->name('add-export-info');
                Route::prefix('{export_info_id}')->group(function () {
                    Route::get('', 'Companies\CompaniesController@viewExportInfo')->name('view-export-info');
                    Route::post('', 'Companies\CompaniesController@editExportInfo')->name('edit-export-info');
                    Route::patch('', 'Companies\CompaniesController@editExportInfo')->name('edit-export-info-patch');
                    Route::delete('', 'Companies\CompaniesController@deleteExportInfo')->name('delete-export-info');
                });
            });;
            Route::prefix('orders')->group(function () {
                Route::post('', 'Orders\OrdersController@createOrderByCompany')->name('create-order-by-company');
            });
            Route::prefix('payment')->group(function () {
                Route::get('', 'Companies\Payment\PaymentController@paymentList')->name('get-payment');
                Route::prefix('{payment_type}')->group(function () {
                    Route::post('', 'Companies\Payment\PaymentController@createPayment');
                    Route::get('', 'Companies\Payment\PaymentController@getPayment');
                    Route::put('', 'Companies\Payment\PaymentController@editPayment');
                    Route::delete('', 'Companies\Payment\PaymentController@deletePayment');
                });
            });
        });
    });

    Route::prefix('users')->group(function () {
        Route::get('', 'Users\UsersController@getList')->name('get-users');
        Route::post('', 'Users\UsersController@getList')->name('get-users-post');
        Route::put('', 'Auth\AuthController@register')->name('register-user');
        Route::prefix('{id}')->group(function () {
            Route::get('', 'Users\UsersController@get')->name('get-user');
            Route::post('', 'Users\UsersController@edit')->name('edit-user');
            Route::patch('', 'Users\UsersController@edit')->name('edit-user-patch');
            Route::delete('', 'Users\UsersController@delete')->name('delete-user');
            Route::get('companies', 'Companies\CompaniesController@getUserCompanies')->name('get-user-companies');
            Route::get('transactions', 'Balance\BalanceController@getTransactionsList')->name('get-user-transactions');
            Route::get('orders', 'Users\UsersController@ordersList')->name('get-user-orders');
            Route::post('orders', 'Users\UsersController@ordersList')->name('get-user-orders-post');
            Route::post('password', 'Users\UsersController@changePassword')->name('change-user-password');
            Route::prefix('favorite')->group(function () {
                Route::get('tenders', 'Users\UsersController@getFavoriteTenders')->name('get-user-favorite-tenders');
                Route::get('products', 'Users\UsersController@getBookmarksProducts')->name('get-user-favorite-products');
            });

            Route::prefix('customers')->group(function () {
                Route::get('people', 'Companies\CompaniesController@getCustomerPeopleList')->name('customers-people-list-get');
                Route::post('people', 'Companies\CompaniesController@getCustomerPeopleList')->name('customers-people-list-post');
                Route::get('people/{customer_id}', 'Companies\CompaniesController@getCustomerPeopleInfo')->name('customers-people-info');

                Route::get('companies', 'Companies\CompaniesController@getCustomerCompaniesList')->name('customers-companies-list-get');
                Route::post('companies', 'Companies\CompaniesController@getCustomerCompaniesList')->name('customers-companies-list-post');
                Route::get('companies/{customer_id}', 'Companies\CompaniesController@getCustomerCompanyInfo')->name('customers-companies-info');
            });
        });
    });

    Route::prefix('ports')->group(function () {
        Route::get('', 'Ports\PortsController@get')->name('get-ports');
    });

    Route::prefix('products')->group(function () {
        Route::get('', 'Products\ProductsController@productsList')->name('get-products');
        Route::post('', 'Products\ProductsController@productsList')->name('get-products-post');
        Route::delete('', 'Products\ProductsController@deleteProductsList')->name('delete-products-list');
        Route::get('list', 'Products\ProductsController@getPaginatedProductsList')->name('get-products-paginated');
        Route::post('list', 'Products\ProductsController@getPaginatedProductsList')->name('get-products-paginated');
        Route::post('bulk/set', 'Products\ProductsController@productsBulkSet')->name('set-bulk-products');
        Route::post('bulk', 'Products\ProductsController@bulkEditProductsList')->name('bulk-edit-products');
        Route::post('import', 'Products\ProductsController@import')->name('import-products');
        Route::get('export/{type}', 'Products\ProductsController@export')->name('export-products');
        Route::post('import/fields', 'Products\ProductsController@importFirstRow')->name('import-products-row');
        Route::put('', 'Products\ProductsController@create')->name('create-product');
        Route::prefix('compare')->middleware(['check-user-id'])->group(function () {
            Route::get('', 'Products\Compare\CompareController@listCompare')->name('get-product-compare-list');
            Route::put('', 'Products\Compare\CompareController@create')->name('create-product-compare');
            Route::delete('', 'Products\Compare\CompareController@deleteAll')->name('delete-all-product-compare');
            Route::prefix('{id}')->group(function () {
                Route::delete('', 'Products\Compare\CompareController@delete')->name('delete-product-compare');
            });
        });
        Route::prefix('attributes')->group(function () {
            Route::get('', 'Products\Attributes\AttributesController@listAttributes')->name('get-product-attributes');
            Route::put('', 'Products\Attributes\AttributesController@create')->name('create-product-attributes');
            Route::prefix('search')->group(function () {
                Route::post('', 'Products\Attributes\AttributesController@searchAttributes')->name('search-product-attribute');
            });
            Route::prefix('options')->group(function () {
                Route::get('', 'Products\AttributeOptions\AttributeOptionsController@searchAttributes')->name('get-product-attribute-options');
                Route::put('', 'Products\AttributeOptions\AttributeOptionsController@create')->name('create-product-attribute-option');
                Route::prefix('search')->group(function () {
                    Route::post('', 'Products\AttributeOptions\AttributeOptionsController@searchAttributeOptions')->name('search-product-attribute-options');
                });
                Route::prefix('{option_id}')->group(function () {
                    Route::get('', 'Products\AttributeOptions\AttributeOptionsController@view')->name('view-product-attribute-options');
                    Route::post('', 'Products\AttributeOptions\AttributeOptionsController@edit')->name('edit-product-attribute-options');
                    Route::patch('', 'Products\AttributeOptions\AttributeOptionsController@edit')->name('edit-product-attribute-options-patch');
                    Route::delete('', 'Products\AttributeOptions\AttributeOptionsController@delete')->name('delete-product-attribute-options');
                });
            });
            Route::prefix('{id}')->group(function () {
                Route::post('options', 'Products\AttributeOptions\AttributeOptionsController@getAttributeOptions')->name('get-attribute-options');
                Route::get('options', 'Products\AttributeOptions\AttributeOptionsController@getAttributeOptions')->name('get-attribute-options');
                Route::put('options', 'Products\AttributeOptions\AttributeOptionsController@createAttributeOption')->name('create-attribute-options');
                Route::get('', 'Products\Attributes\AttributesController@view')->name('view-product-attribute');
                Route::post('', 'Products\Attributes\AttributesController@edit')->name('edit-product-attribute');
                Route::patch('', 'Products\Attributes\AttributesController@edit')->name('edit-product-attribute-patch');
                Route::delete('', 'Products\Attributes\AttributesController@delete')->name('delete-product-attribute');
            });
        });
        // Filtering routes
        Route::prefix('filters')->group(function () {
            Route::get('', 'Filters\FiltersController@getList')->name('filters-list')->defaults('module_name', 'products');
            Route::put('', 'Filters\FiltersController@create')->name('create-filter')->defaults('module_name', 'products');
            Route::get('{id}', 'Filters\FiltersController@view')->name('view-filter')->defaults('module_name', 'products');
            Route::post('{id}', 'Filters\FiltersController@edit')->name('update-filter')->defaults('module_name', 'products');
            Route::patch('{id}', 'Filters\FiltersController@edit')->name('update-filter-patch')->defaults('module_name', 'products');
            Route::delete('{id}', 'Filters\FiltersController@delete')->name('delete-filter')->defaults('module_name', 'products');
        });
        Route::get('fields', 'Products\ProductsController@getProductTableFields')->name('get-product-fields');
        Route::prefix('{id}')->group(function () {
            Route::get('', 'Products\ProductsController@view')->name('get-product');
            Route::post('', 'Products\ProductsController@update')->name('update-product');
            Route::patch('', 'Products\ProductsController@update')->name('update-product-patch');
            Route::delete('', 'Products\ProductsController@delete')->name('delete-product');
            Route::prefix('recommendations')->group(function () {
                Route::get('', 'Products\Recommendations\RecommendationsController@listRecommendations')->name('get-product-recommendation');
                Route::prefix('category')->group(function () {
                    Route::get('', 'Products\Recommendations\RecommendationsController@recommendationByCategory')->name('get-product-recommendation-category');
                });
            });
            Route::prefix('views')->group(function () {
                Route::get('', 'Products\Views\ViewsController@listViews')->name('product-views-list');
                Route::put('', 'Products\Views\ViewsController@createViews')->name('create-product-views');
            });
            Route::prefix('bookmarks')->group(function () {
                Route::put('', 'Products\ProductsController@addToBookmarks')->name('add-product-to-bookmarks');
                Route::delete('', 'Products\ProductsController@deleteFromBookmarks')->name('delete-product-to-bookmarks');
            });
            Route::prefix('feedback')->group(function () {
                Route::put('', 'Products\Feedback\FeedbackController@create')->name('create-product-feedback')->middleware(['transaction']);
                Route::get('', 'Products\Feedback\FeedbackController@getList')->name('get-product-feedback');
                Route::prefix('{feedback_id}')->group(function () {
                    Route::get('', 'Products\Feedback\FeedbackController@view')->name('view-product-feedback');
                    Route::post('', 'Products\Feedback\FeedbackController@update')->name('edit-product-feedback')->middleware(['transaction']);
                    Route::patch('', 'Products\Feedback\FeedbackController@update')->name('edit-product-feedback-patch')->middleware(['transaction']);
                    Route::delete('', 'Products\Feedback\FeedbackController@delete')->name('delete-product-feedback');
                });
            });
            // Questions routes
            Route::prefix('questions')->group(function () {
                Route::get('', 'Question\QuestionController@getList')->name('question-list')->defaults('module_name', 'products');
                Route::put('', 'Question\QuestionController@create')->name('create-question')->defaults('module_name', 'products');
                Route::get('{question_id}', 'Question\QuestionController@view')->name('view-question')->defaults('module_name', 'products');
                Route::post('{question_id}', 'Question\QuestionController@edit')->name('update-question')->defaults('module_name', 'products');
                Route::patch('{question_id}', 'Question\QuestionController@edit')->name('update-question-patch')->defaults('module_name', 'products');
                Route::delete('{question_id}', 'Question\QuestionController@delete')->name('delete-question')->defaults('module_name', 'products');
            });
            Route::delete('variations/{variation_id}', 'Products\ProductsController@deleteProductVariation')->name('delete-product-variation');
        });
    });

    Route::prefix('categories')->group(function () {
        Route::get('', 'Categories\CategoriesController@categoriesList')->name('categories-list');
        Route::post('', 'Categories\CategoriesController@categoriesList')->name('categories-list-post');
        Route::put('', 'Categories\CategoriesController@create')->name('create-category');
        Route::prefix('{id}')->group(function () {
            Route::get('', 'Categories\CategoriesController@getCategory')->name('get-category');
            Route::post('', 'Categories\CategoriesController@update')->name('edit-category');
            Route::patch('', 'Categories\CategoriesController@update')->name('edit-category-patch');
            Route::delete('', 'Categories\CategoriesController@delete')->name('delete-category');
            Route::get('view', 'Categories\CategoriesController@viewCategory')->name('view-category');
        });
    });

    Route::prefix('orders')->group(function () {
        Route::get('', 'Orders\OrdersController@orderList')->name('orders-list');
        Route::post('', 'Orders\OrdersController@orderList')->name('orders-list-post');
        Route::put('', 'Orders\OrdersController@create')->name('create-order');
        Route::prefix('customers')->group(function () {
            Route::get('', 'Orders\OrdersController@customers')->name('order-customers-list');
            Route::post('', 'Orders\OrdersController@customers')->name('order-customers-list-post');
        });
        Route::prefix('seller')->group(function () {
            Route::get('', 'Orders\OrdersController@orderList')->name('order-seller-list')->defaults('type', 'seller');
            Route::post('', 'Orders\OrdersController@orderList')->name('order-seller-list-post')->defaults('type', 'seller');
            Route::get('stats', 'Orders\OrdersController@stats')->name('order-seller-stats')->defaults('type', 'seller');
        });
        Route::prefix('buyer')->group(function () {
            Route::get('', 'Orders\OrdersController@orderList')->name('order-buyer-list')->defaults('type', 'buyer');
            Route::post('', 'Orders\OrdersController@orderList')->name('order-buyer-list-post')->defaults('type', 'buyer');
            Route::get('stats', 'Orders\OrdersController@stats')->name('order-buyer-stats')->defaults('type', 'buyer');
        });
        Route::prefix('{id}')->where(['id' => '[0-9]+'])->group(function () {
            Route::get('', 'Orders\OrdersController@view')->name('get-order');
            Route::post('', 'Orders\OrdersController@update')->name('edit-order');
            Route::patch('', 'Orders\OrdersController@update')->name('edit-order-patch');
            Route::put('add-product', 'Orders\OrdersController@addNewProduct')->name('add-product-to-order');
            Route::prefix('edit-product')->group(function () {
                Route::prefix('{order_product_id}')->group(function () {
                    Route::post('', 'Orders\OrdersController@')->name('edit-order-product');
                    Route::patch('', 'Orders\OrdersController@')->name('edit-order-product-patch');
                    Route::delete('', 'Orders\OrdersController@')->name('delete-order-product');
                });
            });
            Route::prefix('negotiates')->group(function () {
                Route::get('', 'Orders\Negotiates\NegotiatesController@listNegotiates')->name('get-order-negotiates');
                Route::put('', 'Orders\Negotiates\NegotiatesController@create')->name('add-order-negotiates');
                Route::prefix('{negotiate_id}')->group(function () {
                    Route::get('approve', 'Orders\Negotiates\NegotiatesController@approveNegotiate')->name('approve-order-request-negotiates');
                });
            });
//            Route::prefix('requests')->group(function () {
//                Route::get('', 'Orders\OrdersController@')->name('get-order-requests');
//                Route::prefix('negotiates')->group(function () {
//                    Route::get('', 'Orders\OrdersController@')->name('get-order-request-negotiates');
//
//                });
//                Route::prefix('{request_id}')->group(function () {
//                    Route::get('approved', 'Orders\OrdersController@')->name('approved-order-request');
//                });
//            });
        });
//        Route::prefix('')->group(function () {
//            Route::get('', 'Orders\OrdersController@orderList')->name('order-seller-list');
//            Route::post('', 'Orders\OrdersController@orderList')->name('order-seller-list-post');
////            Route::get('stats', 'Orders\OrdersController@')->name('order-seller-stats');
//        });
    });

    Route::prefix('classifiers')->group(function () {
        Route::get('', 'Classifiers\ClassifiersController@getAllFirstLevel')->name('classifiers-list');
        Route::post('', 'Classifiers\ClassifiersController@search')->name('classifiers-search');
        Route::post('search', 'Classifiers\ClassifiersController@search2')->name('classifiers-search-2');
        Route::put('', 'Classifiers\ClassifiersController@create')->name('classifier-create')->middleware(['transaction']);
        Route::post('reindex', 'Classifiers\ClassifiersController@reindex')->name('classifiers-reindex');
        Route::post('find', 'Classifiers\ClassifiersController@getById')->name('classifiers-get-by-id');
        Route::prefix('isic')->group(function () {
            Route::get('', 'Classifiers\ISICController@getList')->name('classifiers-isic-list');
            Route::post('', 'Classifiers\ISICController@getList')->name('classifiers-isic-list-post');
            Route::prefix('{id}')->group(function () {
                Route::get('', 'Classifiers\ISICController@getChildrenByParentId')->name('classifiers-isic-children-list-by-parent-id');
            });
            Route::get('parse', 'Classifiers\ISICController@getChildrenByParentId')->name('classifiers-isic-parse')->middleware(['transaction']);
        });
        Route::prefix('hs')->group(function () {
            Route::get('', 'Classifiers\HSController@getList')->name('classifiers-hs-list');
            Route::post('', 'Classifiers\HSController@getList')->name('classifiers-hs-list-post');
            Route::get('export', 'Classifiers\HSController@getExportImportList')->name('classifiers-hs-list-export')->defaults('type_name', 'export');
            Route::get('import', 'Classifiers\HSController@getExportImportList')->name('classifiers-hs-list-import')->defaults('type_name', 'import');
            Route::get('export/{parent_id}', 'Classifiers\HSController@getImportExportParentList')->name('classifiers-hs-list-export')->defaults('type_name', 'export');
            Route::get('import/{parent_id}', 'Classifiers\HSController@getImportExportParentList')->name('classifiers-hs-list-import')->defaults('type_name', 'import');
            Route::prefix('{id}')->group(function () {
                Route::get('', 'Classifiers\HSController@getChildrenByParentId')->name('classifiers-hs-children-list-by-parent-id');
            });
            Route::get('first-level', 'Classifiers\HSController@getFistLevel')->name('classifiers-hs-get-first-level');
            Route::get('search', 'Classifiers\HSController@searchByName')->name('classifiers-hs-search');
        });
        Route::prefix('uspsc')->group(function () {
            Route::get('', 'Classifiers\USPSCController@getList')->name('classifiers-uspsc-list');
            Route::post('', 'Classifiers\USPSCController@getList')->name('classifiers-uspsc-list-post');
            Route::prefix('{id}')->group(function () {
                Route::get('', 'Classifiers\USPSCController@getChildrenByParentId')->name('classifiers-uspsc-children-list-by-parent-id');
            });
        });
        Route::prefix('cpv')->group(function () {
            Route::get('', 'Classifiers\CPVController@getList')->name('classifiers-cpv-list');
            Route::post('', 'Classifiers\CPVController@getList')->name('classifiers-cpv-list-post');
            Route::prefix('{id}')->group(function () {
                Route::get('', 'Classifiers\CPVController@getChildrenByParentId')->name('classifiers-cpv-children-list-by-parent-id');
            });
        });
    });

    Route::prefix('rta')->group(function () {
        Route::get('', 'Rta\RtaController@getList')->name('rta-list');
        Route::post('', 'Rta\RtaController@getList')->name('rta-list-post');
        Route::get('country/{country}/partner/{partner}', 'Rta\RtaController@getCountryRta')->name('get-rta');
        Route::get('{id}', 'Rta\RtaController@getRta')->name('get-rta');
    });

    Route::prefix('tenders')->group(function () {
        Route::get('', 'Tenders\TenderController@getList')->name('tenders-list');
        Route::post('', 'Tenders\TenderController@getList')->name('tenders-list');
        Route::put('', 'Tenders\TenderController@create')->name('tenders-create');

        Route::prefix('{id}')->group(function () {
            Route::get('', 'Tenders\TenderController@get')->name('tenders-get');
            Route::post('', 'Tenders\TenderController@edit')->name('tenders-edit');
            Route::patch('', 'Tenders\TenderController@edit')->name('tenders-edit');
            Route::delete('', 'Tenders\TenderController@delete')->name('tenders-delete');

            Route::put('contacts/{contact_id}/conversation', 'Tenders\TenderController@startConversation')->name('tenders-start-conversation');

            Route::put('favorite', 'Tenders\TenderController@addToFavorites')->name('tenders-favorites-add');
            Route::delete('favorite', 'Tenders\TenderController@removeFromFavorites')->name('tenders-favorites-remove');

            Route::get('stage/{step}/calendar/{service}', 'Tenders\TenderController@getCalendar')->name('tenders-calendar');

            Route::prefix('proposals')->group(function () {
                Route::get('', 'Tenders\ProposalController@getList')->name('proposals-list');
                Route::put('', 'Tenders\ProposalController@create')->name('proposals-create');

                Route::prefix('{proposal}')->group(function () {
                    Route::get('', 'Tenders\ProposalController@get')->name('proposals-get');
                    Route::post('', 'Tenders\ProposalController@edit')->name('proposals-edit');
                    Route::patch('', 'Tenders\ProposalController@edit')->name('proposals-edit');
                    Route::delete('', 'Tenders\ProposalController@delete')->name('proposals-delete');
                });
            });

            Route::prefix('auction')->group(function () {
                Route::get('', 'Tenders\AuctionController@get')->name('auction-get');

                Route::prefix('{proposal}')->group(function () {
                    Route::post('', 'Tenders\AuctionController@edit')->name('auction-edit');
                    Route::patch('', 'Tenders\AuctionController@edit')->name('auction-edit');
                    Route::get('', 'Tenders\AuctionController@getProposal')->name('auction-proposal');
                });
            });

            // Questions routes
            Route::prefix('questions')->group(function () {
                Route::get('', 'Question\QuestionController@getList')->name('question-list')->defaults('module_name', 'tenders');
                Route::put('', 'Question\QuestionController@create')->name('create-question')->defaults('module_name', 'tenders');
                Route::get('{question_id}', 'Question\QuestionController@view')->name('view-question')->defaults('module_name', 'tenders');
                Route::post('{question_id}', 'Question\QuestionController@edit')->name('update-question')->defaults('module_name', 'tenders');
                Route::patch('{question_id}', 'Question\QuestionController@edit')->name('update-question-patch')->defaults('module_name', 'tenders');
                Route::delete('{question_id}', 'Question\QuestionController@delete')->name('delete-question')->defaults('module_name', 'tenders');
            });
        });

        Route::get('stages', 'Tenders\StageController@getList')->name('stages-list');

        // Filtering routes
        Route::prefix('filters')->group(function () {
            Route::get('', 'Filters\FiltersController@getList')->name('filters-list')->defaults('module_name', 'tenders');
            Route::put('', 'Filters\FiltersController@create')->name('create-filter')->defaults('module_name', 'tenders');
            Route::get('{id}', 'Filters\FiltersController@view')->name('view-filter')->defaults('module_name', 'tenders');
            Route::post('{id}', 'Filters\FiltersController@edit')->name('update-filter')->defaults('module_name', 'tenders');
            Route::patch('{id}', 'Filters\FiltersController@edit')->name('update-filter-patch')->defaults('module_name', 'tenders');
            Route::delete('{id}', 'Filters\FiltersController@delete')->name('delete-filter')->defaults('module_name', 'tenders');
        });
    });

    Route::prefix('integrations')->middleware('jwt.auth')->group(function () {
        Route::put('', 'Integrations\IntegrationsController@create')->name('create-integration');
        Route::get('', 'Integrations\IntegrationsController@getList')->name('integrations-list');
        Route::prefix('magento')->group(function () {
            Route::post('check', 'Integrations\Magento\MagentoController@checkModule')->name('check-magento-module');
            Route::put('products', 'Integrations\Magento\MagentoController@parseProducts')->name('parse-products-from-magento');
            Route::get('fields', 'Integrations\Magento\MagentoController@getFields');
            Route::get('fields-integration', 'Integrations\Magento\MagentoController@getFieldsWithIntegration');
        });
        Route::prefix('woocommerce')->group(function () {
            Route::post('check', 'Integrations\Woocommerce\WoocommerceController@checkModule')->name('check-magento-module');
            Route::put('products', 'Integrations\Woocommerce\WoocommerceController@parseProducts')->name('woocommerce-list');
            Route::get('fields', 'Integrations\Woocommerce\WoocommerceController@getFields')->name('woocommerce-product-fields-list');
        });
        Route::prefix('{id}')->group(function () {
            Route::get('', 'Integrations\IntegrationsController@view')->name('view-integration');
            Route::post('', 'Integrations\IntegrationsController@edit')->name('edit-integration');
            Route::delete('', 'Integrations\IntegrationsController@delete')->name('delete-integration');
        });
    });

    Route::prefix('registration')->group(function () {
        Route::prefix('email')->group(function () {
            Route::post('', 'Auth\AuthController@registerEmail')->name('registration-email');
            Route::post('confirm', 'Auth\AuthController@confirmEmail')->name('confirm-email');
        });
        Route::post('code/resend', 'Auth\AuthController@resentCode')->name('resent-code');
    });

    Route::prefix('comtrade')->group(function () {
        Route::get('trends', 'Comtrade\ComtradeController@trends')->name('trade-trends');
    });

    Route::prefix('support')->group(function () {
        Route::prefix('ticket')->group(function () {
            Route::post('', 'Support\Tickets\TicketsController@createTicket')->name('create-zend-desk-ticket');
        });
    });
});
