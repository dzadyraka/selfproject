<?php

namespace Tests\Feature;

use App\Currency\Currency;
use App\Currency\CurrencyRate;
use Tests\TestCase;

class CurrencyTest extends TestCase {
	/**
	 * A basic test example.
	 *
	 * @return void
	 */
	public function testCurrenciesList() {
		$response = $this->get('/api/v1/currencies');

		$response->assertJsonCount(Currency::whereActive(true)->count());
		$response->assertStatus(200);
	}

	public function testRatesList() {
		$response = $this->get('/api/v1/currencies/rate');

		$response->assertJsonCount(CurrencyRate::whereNull('date_to')->get()->count());
		$response->assertStatus(200);
	}
}
