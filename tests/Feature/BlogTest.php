<?php

namespace Tests\Feature;

use Tests\TestCase;

class BlogTest extends TestCase {

	public function testCategoryCreate() {
		$response = $this->post('/api/v1/blog/sections', [
			"alias" => "section-1",
			"name" => "Section #1"
		]);
		$response->assertStatus(201);
		$this->assertEquals($response->json('name'), 'Section #1');

		return $response->json('id');
	}

	/**
	 * @depends testCategoryCreate
	 * @param int $id
	 */
	public function testCategoryUpdate(int $id) {
		$response = $this->post('/api/v1/blog/sections/' . $id, [
			"alias" => "section-1",
			"name" => "Section #1 edited"
		]);
		$response->assertStatus(201);

		$this->assertEquals($response->json('name'), 'Section #1 edited');
	}

	/**
	 * @depends testCategoryCreate
	 * @param int $id
	 */
	public function testCategoriesList(int $id) {
		$response = $this->get('/api/v1/blog/sections');
		$response->assertStatus(200);
		$response->assertJsonCount(1);
		$this->assertTrue(in_array($id, array_column($response->json(), 'id')));
	}

	public function testCreatePost() {
		$response = $this->put('/api/v1/blog', [
			"is_published" => false,
		    "is_featured" => false,
		    "name" => "Test New #1",
		    "content" => "Test, test, test..."
		]);
		$response->assertStatus(201);
	}

	/**
	 * @depends testCategoryCreate
	 * @param int $id
	 */
	public function testDeleteCategory(int $id) {
		$response = $this->delete('/api/v1/blog/sections/' . $id);
		$response->assertStatus(204);
	}
}