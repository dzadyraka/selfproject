<?php

namespace Tests\Feature;

use Tests\TestCase;

class BalanceTest extends TestCase {
	public function testTransactionsList() {
		$response = $this->get('/api/v1/transactions');
		$response->assertStatus(200);
		$response->assertJsonCount(0);
	}

	public function testAddTransaction() {
		$response = $this->put('/api/v1/transactions', [
			'value' => 10,
			'currency_id' => 1
		]);
		print_r($response->content());
		$response->assertStatus(201);
	}
}