<?php

namespace Tests\Feature;

use App\Language\Models\Language;
use Tests\TestCase;

class LanguageTest extends TestCase {
	/**
	 * A basic test example.
	 *
	 * @return void
	 */
	public function testGetList() {
		$response = $this->get('/api/v1/languages');
		$response->assertJsonCount(Language::get()->count());
		$response->assertStatus(200);
	}

	public function testGetByCode() {
		$response = $this->get('/api/v1/languages/code/en');
		$response->assertJson(Language::whereCode('en')->first()->toArray());
		$response->assertStatus(200);
	}

	public function testGetById() {
		$response = $this->get('/api/v1/languages/1');
		$response->assertJson(Language::find(1)->toArray());
		$response->assertStatus(200);
	}
}
