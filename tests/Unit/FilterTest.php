<?php

namespace Tests\Unit;

use App\Filters\Models\TendersFilter;
use App\Users\Models\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class FilterTest extends TestCase {

    protected $_data = [
        'name' => 'Test filter',
        'filter' => [
            'classifier_uspsc' => [25, 75],
            'classifier_hs' => [155, 1783],
            "classifier_cpv" => [251, 7225],
            "classifier_isic" => [255, 7895],
            "expected_cost" => [0, 100],
            "procurement_category" => 1,
            "procedure_type" => 1,
            "q" => "String search keyword in name or procurement_description",
            "bidder" => true,
            "favorite" => true,
            "companies" => true,
        ]
    ];

    protected $_data_2 = [
        'name' => 'Test filter 2',
        'filter' => [
            'classifier_uspsc' => [1212, 7215],
            'classifier_hs' => [55, 178],
            "classifier_cpv" => [2251, 7225],
            "classifier_isic" => [25, 785],
            "expected_cost" => [0, 10],
            "procurement_category" => 2,
            "procedure_type" => 3,
            "q" => "String search keyword in name or procurement_description",
            "bidder" => true,
            "favorite" => false,
            "companies" => true,
        ]
    ];

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCreateTendersFilter() {
//        $user = factory(User::class)->create();

        $tenders_response = $this->json('PUT', '/api/tenders/filters', $this->_data);
        $tenders_response_2 = $this->json('PUT', '/api/tenders/filters', $this->_data_2);

        $tenders_response->assertStatus(201);
        $tenders_response_2->assertStatus(201);
    }

    public function testCreateProductsFilter() {
//        $user = factory(User::class)->create();

        $products_response = $this->json('PUT', '/api/products/filters', $this->_data);
        $products_response_2 = $this->json('PUT', '/api/products/filters', $this->_data_2);

        $products_response->assertStatus(201);
        $products_response_2->assertStatus(201);
    }


    public function testAllTendersFilters() {
//        $user = factory(User::class)->create();

        $response = $this->json('GET', '/api/tenders/filters');
        $response->assertStatus(200);

    }


    public function testAllProductsFilters() {
//        $user = factory(User::class)->create();

        $response = $this->json('GET', '/api/products/filters');
        $response->assertStatus(200);
    }

    public function testUpdateProductFilter() {
        $response = $this->json('GET', '/api/products/filters');
        $response->assertStatus(200);

        $filter = $response->getData()[0];

//        $user = factory(User::class)->create();
        $update = $this->json('POST', '/api/products/filters/' . $filter->id, ['name' => "Changed for test"]);
        $update->assertStatus(200);
    }

    public function testUpdateTendersFilter() {
        $response = $this->json('GET', '/api/tenders/filters');
        $response->assertStatus(200);

        $filter = $response->getData()[0];

//        $user = factory(User::class)->create();
        $update = $this->json('POST', '/api/tenders/filters/' . $filter->id, ['name' => "Changed for test"]);
        $update->assertStatus(200);
    }

    public function testDeleteProductFilter() {
        $response = $this->json('GET', '/api/products/filters');
        $response->assertStatus(200);

        $filter = $response->getData()[0];

//        $user = factory(User::class)->create();
        $update = $this->json('DELETE', '/api/products/filters/' . $filter->id);
        $update->assertStatus(204);
    }

    public function testDeleteTendersFilter() {
        $response = $this->json('GET', '/api/tenders/filters');
        $response->assertStatus(200);

        $filter = $response->getData()[0];

//        $user = factory(User::class)->create();
        $update = $this->json('DELETE', '/api/tenders/filters/' . $filter->id);
        $update->assertStatus(204);
    }
}
