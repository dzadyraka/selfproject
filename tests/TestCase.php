<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Tests\Concerns\AttachJwtToken;

abstract class TestCase extends BaseTestCase {
	use CreatesApplication;
	use AttachJwtToken;

	protected $baseUrl = 'http://tradalaxy.test';
}
