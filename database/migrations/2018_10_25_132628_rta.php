<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Rta extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        Schema::create('rta', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('rta_id');
            $table->string('name', 500);
            $table->string('coverage', 500)->nullable();
            $table->string('status', 500)->nullable();
            $table->string('notification_under', 500)->nullable();
            $table->date('signature_date')->nullable();
            $table->date('notification_date')->nullable();
            $table->date('entry_force_date')->nullable();
            $table->jsonb('current_signatories')->default("[]");
            $table->jsonb('original_signatories')->default("[]");
            $table->jsonb('rta_composition')->default("[]");
            $table->jsonb('region')->default("[]");
            $table->boolean('wto_members')->default(false);
            $table->string('type', 500)->nullable();
            $table->jsonb('web_addresses')->default("[]");
            $table->jsonb('agreement')->default("[]");
            $table->jsonb('countries_first')->default("[]");
            $table->jsonb('countries_second')->default("[]");
        });

        Schema::create('rta_countries', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('rta_id')->unsigned()->index();
            $table->integer('country_id')->unsigned()->index();

            $table->foreign('rta_id')->references('id')->on('rta')->onDelete('cascade');
            $table->foreign('country_id')->references('id')->on('countries')->onDelete('cascade');
        });

        Schema::create('rta_countries_first', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('rta_id')->unsigned()->index();
            $table->integer('country_id')->unsigned()->index();

            $table->foreign('rta_id')->references('id')->on('rta')->onDelete('cascade');
            $table->foreign('country_id')->references('id')->on('countries')->onDelete('cascade');
        });

        Schema::create('rta_countries_second', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('rta_id')->unsigned()->index();
            $table->integer('country_id')->unsigned()->index();

            $table->foreign('rta_id')->references('id')->on('rta')->onDelete('cascade');
            $table->foreign('country_id')->references('id')->on('countries')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('rta_countries');
        Schema::dropIfExists('rta_countries_first');
        Schema::dropIfExists('rta_countries_second');
        Schema::dropIfExists('rta');
    }
}
