<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messages', function (Blueprint $table) {
            $table->increments('id');
            $table->text('content');
            $table->string('status', 10);
            $table->timestamp('sending_date')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->integer('from_user_id');
            $table->jsonb('seen')->default('[]');
            $table->integer('conversation_id');
            $table->jsonb('hide')->default('[]');
            $table->jsonb('attachments')->default('[]');

            $table->index('hide');
            $table->foreign('from_user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
            $table->foreign('conversation_id')
                ->references('id')->on('conversations')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messages');
    }
}
