<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Certificates extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('certificates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('logo');
            $table->text('description');
            $table->string('address');
            $table->string('website');
            $table->string('email');
            $table->string('phone');

            $table->string('responsibility');
            $table->string('category');
            $table->boolean('for_sme')->default(false);
            $table->string('area');
            $table->integer('country_id')->unsigned();
            $table->string('sector');

            $table->timestamps();

            $table->foreign('country_id')
                ->references('id')
                ->on('countries')
                ->onDelete('cascade');
        });

        Schema::create('company_certificates', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('certificate_id')->unsigned();
            $table->integer('company_id')->unsigned();

            $table->string('file_url');
            $table->string('site_url');

            $table->boolean('is_verified')->default(false);

            $table->timestamps();

            $table->foreign('certificate_id')
                ->references('id')
                ->on('certificates')
                ->onDelete('cascade');

            $table->foreign('company_id')
                ->references('id')
                ->on('companies')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('company_certificates');
        Schema::dropIfExists('certificates');
    }
}
