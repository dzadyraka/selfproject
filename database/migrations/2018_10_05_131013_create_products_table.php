<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->float('rating')->default(0);
            $table->float('price');
            $table->integer('currency_id')->unsigned()->nullable();
            $table->timestamp('earliest_shipping_date')->nullable();
            $table->integer('quantity_available')->nullable();
            $table->integer('minimum_ordery')->nullable();
            $table->integer('company_id')->unsigned();
            $table->integer('created_by')->unsigned();
            $table->string('unit')->nullable();
            $table->jsonb('bulk_prices')->default('[]');
            $table->jsonb('certifications')->default('[]');
            $table->float('discount_percent')->nullable();
            $table->float('discount_cash')->nullable();
            $table->integer('location_city')->nullable();
            $table->integer('location_country')->nullable();
            $table->string('port')->nullable();
            $table->string('external_id')->nullable();
            $table->string('integration_name')->nullable();
            $table->jsonb('attachments')->default('[]');
            $table->integer('parent_id')->nullable();
            $table->boolean('negotiate')->default(false);
            $table->boolean('is_draft')->default(false);
            $table->jsonb('images')->default('[]');
            $table->integer('hs_classifier')->nullable();
            $table->integer('paid_order')->nullable();
            $table->float('weight')->default(0);
            $table->float('dim_weight')->default(0);
            $table->float('width')->default(0);
            $table->float('height')->default(0);
            $table->float('depth')->default(0);
            $table->jsonb('shipping')->default('[]');
            $table->integer('place_of_origin')->nullable();
            $table->timestamp('created_date');
            $table->timestamp('edited_date');

            $table->foreign('currency_id')
                ->references('id')
                ->on('currencies')
			    ->onDelete('cascade');

            $table->foreign('company_id')
                ->references('id')
                ->on('companies')
                ->onDelete('cascade');

            $table->foreign('created_by')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->foreign('parent_id')
                ->references('id')->on('products')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
