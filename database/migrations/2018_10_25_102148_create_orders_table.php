<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('description')->nullable();
            $table->jsonb('attachments')->default('[]');
            $table->float('subtotal')->nullable();
            $table->bigInteger('creator_id')->unsigned()->index();
            $table->integer('customer_company_id')->nullable()->unsigned()->index();
            $table->integer('company_id')->nullable()->unsigned()->index();
            $table->string('delivery_service')->nullable();
            $table->float('shipping_cost')->nullable();
            $table->string('order_status')->nullable();
            $table->boolean('need_approve_request')->default(false);
            $table->boolean('is_completed')->default(false);
            $table->boolean('is_cancelled')->default(false);
            $table->boolean('is_draft')->default(false);
            $table->string('order_url')->nullable();
            $table->jsonb('billing_address')->default('[]');
            $table->jsonb('shipping_address')->default('[]');
            $table->jsonb('shipping_details')->default('[]');
            $table->bigInteger('editor_id')->unsigned()->index();
            $table->float('shipping')->default(0);
            $table->float('taxes')->default(0);
            $table->float('total')->default(0);
            $table->boolean('invoice_sent')->default(false);
            $table->timestamp('invoice_date')->nullable();
            $table->timestamp('created_date');
            $table->timestamp('edited_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
