<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CompaniesTableUpdate2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('companies', function (Blueprint $table) {
            $table->string('phone')->nullable()->change();
            $table->string('website')->nullable()->change();
            $table->string('email')->nullable()->change();
            $table->jsonb('import_trade_capacity')->default('{}');
            $table->jsonb('export_trade_capacity')->default('{}');
            $table->string('brand')->nullable();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('interested_in')->nullable();
            $table->string('reseller')->nullable();
            $table->string('business_entity')->nullable();
        });

        Schema::table('users', function (Blueprint $table) {
            $table->boolean('has_accepted_principles')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('companies', function (Blueprint $table) {
            $table->string('phone')->change();
            $table->string('website')->change();
            $table->string('email')->change();
            $table->dropColumn('import_trade_capacity');
            $table->dropColumn('export_trade_capacity');
            $table->dropColumn('brand');
            $table->dropColumn('first_name');
            $table->dropColumn('last_name');
            $table->dropColumn('interested_in');
            $table->dropColumn('reseller');
            $table->dropColumn('business_entity');
        });

        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('has_accepted_principles');
        });
    }
}
