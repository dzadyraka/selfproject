<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CertificateUpdate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('certificates', function (Blueprint $table) {
            $table->string('logo')->nullable()->change();
            $table->text('description')->nullable()->change();
            $table->string('address')->nullable()->change();
            $table->string('website')->nullable()->change();
            $table->string('email')->nullable()->change();
            $table->string('phone')->nullable()->change();
            $table->string('responsibility')->nullable()->change();
            $table->string('category')->nullable()->change();
            $table->string('area')->nullable()->change();
            $table->string('sector')->nullable()->change();
            $table->integer('country_id')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
