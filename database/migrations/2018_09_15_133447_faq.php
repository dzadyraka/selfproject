<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Faq extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('faqs', function (Blueprint $table) {
	        $table->increments('id');
			$table->timestamp('created_date');
			$table->timestamp('edited_date');
			$table->boolean('is_published')->default(false);
			$table->boolean('is_featured')->default(false);
        });

        Schema::create('faq_translations', function (Blueprint $table) {
	        $table->increments('id');
        	$table->integer('faq_id')->unsigned();
			$table->string('locale', 2);
			$table->string('name');
			$table->text('content');
			$table->text('description');
			$table->text('meta_title');
			$table->text('meta_keywords');
			$table->text('meta_description');

	        $table->unique(['faq_id', 'locale']);

	        $table->foreign('faq_id')->references('id')->on('faqs')
		        ->onDelete('cascade');
	        $table->foreign('locale')->references('code')->on('languages')
		        ->onDelete('cascade');
        });

        Schema::create('faq_categories', function (Blueprint $table) {
	        $table->increments('id');
			$table->string('alias')->unique();
			$table->integer('position')->default(1);
	        $table->timestamp('created_date');
	        $table->timestamp('edited_date');
	        $table->boolean('is_active')->default(false);
        });

        Schema::create('faq_category_translations', function (Blueprint $table) {
	        $table->increments('id');
	        $table->integer('faq_category_id')->unsigned();
	        $table->string('locale', 2);
	        $table->string('name');

	        $table->unique(['faq_category_id', 'locale']);

	        $table->foreign('faq_category_id')->references('id')->on('faq_categories')
		        ->onDelete('cascade');
	        $table->foreign('locale')->references('code')->on('languages')
		        ->onDelete('cascade');
        });

        Schema::create('faq_category_faq', function (Blueprint $table) {
        	$table->integer('faq_id')->unsigned();
        	$table->integer('faq_category_id')->unsigned();

	        $table->unique(['faq_category_id', 'faq_id']);

	        $table->foreign('faq_category_id')->references('id')->on('faq_categories')
		        ->onDelete('cascade');
	        $table->foreign('faq_id')->references('id')->on('faqs')
		        ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('faqs');
        Schema::drop('faq_translations');
        Schema::drop('faq_categories');
        Schema::drop('faq_category_translations');
        Schema::drop('faq_category_faq');
    }
}
