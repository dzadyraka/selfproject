<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FixTableNamings extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::table('languages', function (Blueprint $table) {
			$table->string('code', 2)->change();
			$table->unique('code');
		});

		Schema::table('countries_translations', function (Blueprint $table) {
			$table->dropForeign('countries_translations_language_id_foreign');
		});

		Schema::table('countries_translations', function (Blueprint $table) {
			$table->string('language_id', 2)->change();
			$table->foreign('language_id')->references('code')->on('languages')->onDelete('cascade');
			$table->renameColumn('language_id', 'locale');
		});

		Schema::table('users_translations', function (Blueprint $table) {
			$table->dropForeign('users_translations_language_id_foreign');
		});

		Schema::table('users_translations', function (Blueprint $table) {
			$table->string('language_id', 2)->change();
			$table->foreign('language_id')->references('code')->on('languages')->onDelete('cascade');
			$table->renameColumn('language_id', 'locale');
		});

		Schema::table('languages_translations', function (Blueprint $table) {
			$table->dropForeign('languages_translations_languages_id_foreign');
		});

		Schema::table('languages_translations', function (Blueprint $table) {
			$table->string('languages_id', 2)->change();
			$table->foreign('languages_id')->references('code')->on('languages')->onDelete('cascade');
			$table->renameColumn('languages_id', 'locale');
		});

		Schema::rename('countries_translations', 'country_translations');
		Schema::rename('languages_translations', 'language_translations');
		Schema::rename('users_translations', 'user_translations');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::table('languages', function (Blueprint $table) {
			$table->string('code', 3)->change();
			$table->dropUnique('code');
		});

		Schema::table('country_translations', function (Blueprint $table) {
			$table->dropForeign('locale');
			$table->renameColumn('locale', 'language_id');
			$table->integer( 'language_id')->unsigned()->change();
			$table->foreign('language_id')->references('language_id')->on('languages')->onDelete('cascade');
		});

		Schema::table('user_translations', function (Blueprint $table) {
			$table->dropForeign('locale');
			$table->renameColumn('locale', 'language_id');
			$table->integer( 'language_id')->unsigned()->change();
			$table->foreign('language_id')->references('language_id')->on('languages')->onDelete('cascade');
		});

		Schema::table('language_translations', function (Blueprint $table) {
			$table->dropForeign('locale');
			$table->renameColumn('locale', 'language_id');
			$table->integer( 'language_id')->unsigned()->change();
			$table->foreign('language_id')->references('language_id')->on('languages')->onDelete('cascade');
		});

		Schema::rename('country_translations', 'countries_translations');
		Schema::rename('language_translations', 'languages_translations');
		Schema::rename('user_translations', 'users_translations');
	}
}
