<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndex extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('comtrades', function(Blueprint $table)
        {
            $table->index(['country_id', 'partner_id', 'rg_desc', 'trade_value']);
        });

        Schema::table('rta_countries_first', function(Blueprint $table)
        {
            $table->index(['rta_id', 'country_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('comtrades', function (Blueprint $table)
        {
            $table->dropIndex(['country_id_partner_id_rg_desc_trade_value']);
        });

        Schema::table('rta_countries_first', function(Blueprint $table)
        {
            $table->dropIndex(['rta_id_country_id']);
        });
    }
}
