<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FixTranslatableTables extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::table('blog_category_translations', function (Blueprint $table) {
			$table->increments('id');
		});

		Schema::table('blog_post_translations', function (Blueprint $table) {
			$table->increments('id');
		});

		Schema::table('country_translations', function (Blueprint $table) {
			$table->increments('id');
		});

		Schema::table('language_translations', function (Blueprint $table) {
			$table->increments('id');
		});

		Schema::table('user_translations', function (Blueprint $table) {
			$table->increments('id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::table('blog_category_translations', function (Blueprint $table) {
			$table->dropColumn('id');
		});

		Schema::table('blog_post_translations', function (Blueprint $table) {
			$table->dropColumn('id');
		});

		Schema::table('country_translations', function (Blueprint $table) {
			$table->dropColumn('id');
		});

		Schema::table('language_translations', function (Blueprint $table) {
			$table->dropColumn('id');
		});

		Schema::table('user_translations', function (Blueprint $table) {
			$table->dropColumn('id');
		});
	}
}
