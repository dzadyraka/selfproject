<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CompanyUpdate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('companies', function (Blueprint $table) {
            $table->string('business_type_other')->default('');
            $table->string('business_entity_other')->default('');

            $table->json('social_links')->default('[]');
            $table->json('authorized_persons')->default('[]');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('companies', function (Blueprint $table) {
            $table->dropColumn('business_type_other');
            $table->dropColumn('business_entity_other');
            $table->dropColumn('social_links');
            $table->dropColumn('authorized_persons');
        });
    }
}
