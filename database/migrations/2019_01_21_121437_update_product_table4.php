<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateProductTable4 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->string('gtin')->nullable();
            $table->string('mpn')->nullable();
            $table->string('sale_price')->nullable();
            $table->jsonb('sale_period')->default('[]');
            $table->string('brand')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('gtin');
            $table->dropColumn('mpn');
            $table->dropColumn('sale_price');
            $table->dropColumn('sale_period');
            $table->dropColumn('brand');
        });
    }
}
