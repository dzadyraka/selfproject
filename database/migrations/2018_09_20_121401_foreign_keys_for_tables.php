<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ForeignKeysForTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('company_export_info', function (Blueprint $table) {
            $table->foreign('city_id')
                ->references('id')->on('cities');

            $table->foreign('region_id')
                ->references('id')->on('regions');
        });
        Schema::table('company_addresses', function (Blueprint $table) {
            $table->foreign('city_id')
                ->references('id')->on('cities');

            $table->foreign('region_id')
                ->references('id')->on('regions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('company_export_info', function (Blueprint $table) {
            $table->dropForeign('companies_city_id_foreign');

            $table->dropForeign('companies_region_id_foreign');
        });
        Schema::table('company_addresses', function (Blueprint $table) {
            $table->dropForeign('companies_city_id_foreign');

            $table->dropForeign('companies_region_id_foreign');
        });
    }
}
