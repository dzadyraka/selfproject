<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComtradesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comtrades', function (Blueprint $table) {
            $table->increments('id');
            $table->year('year');
            $table->integer('period');
            $table->string('period_desc', 125);
            $table->integer('aggr_level');
            $table->bigInteger('is_leaf');
            $table->bigInteger('rg_code');
            $table->string('rg_desc');
            $table->integer('country_id')->unsigned();
            $table->integer('partner_id')->unsigned();
            $table->integer('classifiers_hs_id')->unsigned();
            $table->string('cst_code')->nullable();
            $table->string('cst_desc')->nullable();
            $table->string('mot_code')->nullable();
            $table->string('mot_desc')->nullable();
            $table->bigInteger('qt_code');
            $table->string('qt_desc')->nullable();
            $table->bigInteger('qt_alt_code')->nullable();
            $table->string('qt_alt_desc')->nullable();
            $table->string('trade_quantity')->nullable();
            $table->string('alt_quantity')->nullable();
            $table->bigInteger('net_weight')->nullable();
            $table->string('gross_weight')->nullable();
            $table->bigInteger('trade_value')->nullable();
            $table->string('cif_value')->nullable();
            $table->string('fob_value')->nullable();
            $table->string('type');
            $table->bigInteger('est_code')->nullable();
            $table->timestamps();

            $table->foreign('country_id')
                ->references('id')->on('countries')
                ->onDelete('cascade');
            $table->foreign('partner_id')
                ->references('id')->on('countries')
                ->onDelete('cascade');
            $table->foreign('classifiers_hs_id')
                ->references('id')->on('classifiers_hs')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comtrades');
    }
}
