<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassifiersHSTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('classifiers_hs_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('classifiers_hs_id')->unsigned()->index();
            $table->string('locale', 2)->index();
            $table->text('name')->nullable(false)->index();

            $table->foreign('classifiers_hs_id')
                ->references('id')->on('classifiers_hs')
                ->onDelete('cascade');
            $table->foreign('locale')->references('code')->on('languages')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('classifiers_hs_translations');
    }
}
