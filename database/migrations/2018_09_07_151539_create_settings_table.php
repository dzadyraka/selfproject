<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type', 255);
            $table->integer('user_id')->unsigned();
            $table->integer('company_id')->unsigned();
            $table->string('site');
            $table->jsonb('fields')->default('[]');
            $table->boolean('active')->default(true);
            $table->string('language', 255);
            $table->jsonb('keys')->default('[]');
            $table->timestamp('created_date');
            $table->timestamp('updated_date');

            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products_settings');
    }
}
