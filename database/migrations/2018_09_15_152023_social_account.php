<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SocialAccount extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('social_accounts', function (Blueprint $table) {
	        $table->increments('id');
			$table->string('access_token');
			$table->integer('user_id')->unsigned();
			$table->string('integration_user_id');
			$table->string('integration');

			$table->unique(['user_id', 'integration_user_id', 'integration']);
			$table->foreign('user_id')
				->references('id')->on('users')
				->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('social_accounts');
    }
}
