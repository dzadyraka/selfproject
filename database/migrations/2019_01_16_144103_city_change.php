<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CityChange extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('DELETE FROM city_translations');
        DB::statement('DELETE FROM cities');

        Schema::table('cities', function (Blueprint $table) {
           $table->integer('region_id')->nullable()->change();
           $table->string('name');
           $table->float('coordinates_lat')->default(0);
           $table->float('coordinates_lng')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
