<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProductFieldsNew extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('sale_price');
        });

        Schema::create('units', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
        });

        Schema::table('products', function (Blueprint $table) {
            $table->float('sale_price')->default(0);
            $table->boolean('has_sale')->default(false);
            $table->integer('unit_id')->unsigned()->nullable();
            $table->string('availability')->default('in_stock');
            $table->integer('availability_date')->nullable();
            $table->boolean('sample_free')->default(false);
            $table->float('sample_length')->nullable();
            $table->string('sample_weight_unit', 4)->nullable();
            $table->string('sample_size_unit', 4)->nullable();

            $table->foreign('unit_id')
                ->references('id')
                ->on('units')
                ->onDelete('SET NULL');
        });

        Schema::create('product_packages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id')->unsigned();
            $table->string('description');
            $table->float('weight');
            $table->string('weight_unit', 4);
            $table->string('size_unit', 4);
            $table->float('length');
            $table->float('width');
            $table->float('height');
            $table->integer('quantity_in_package');

            $table->foreign('product_id')->references('id')
                ->on('products')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        //
    }
}
