<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OrderNegotiationOfferProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_negotiation_offer_products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_negotiation_offer_id')->unsigned()->index();
            $table->integer('product_id')->unsigned()->index();
            $table->float('price');
            $table->integer('quantity');
            $table->float('subtotal');

            $table->foreign('order_negotiation_offer_id')->references('id')->on('order_negotiation_offers')->onDelete('cascade');
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_negotiation_offer_products');
    }
}
