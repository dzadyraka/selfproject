<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassifiersISICTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('classifiers_isic_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('classifiers_isic_id')->unsigned()->index();
            $table->string('locale', 2)->index();
            $table->string('name', 500)->nullable(false)->index();

            $table->foreign('classifiers_isic_id')
                ->references('id')->on('classifiers_isic')
                ->onDelete('cascade');
            $table->foreign('locale')->references('code')->on('languages')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('classifiers_isic_translations');
    }
}
