<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Blog extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('blog_posts', function (Blueprint $table) {
			$table->increments('id');
			$table->string('image', 255);
			$table->timestamp('created_date');
			$table->timestamp('edited_date');
			$table->boolean('is_published')->default(true);
			$table->boolean('is_featured')->default(false)->index();
		});

		Schema::create('blog_post_translations', function (Blueprint $table) {
			$table->integer('blog_post_id')->unsigned();
			$table->string('locale', 2);
			$table->string('name');
			$table->text('content');
			$table->string('meta_title');
			$table->text('meta_keywords');
			$table->text('meta_description');
			$table->text('description');

			$table->unique(['blog_post_id', 'locale']);

			$table->foreign('blog_post_id')->references('id')->on('blog_posts')
				->onDelete('cascade');
			$table->foreign('locale')->references('code')->on('languages')
				->onDelete('cascade');
		});

		Schema::create('blog_categories', function (Blueprint $table) {
			$table->increments('id');
			$table->string('alias')->unique();
			$table->timestamp('created_date');
			$table->timestamp('edited_date');
			$table->boolean('is_active')->default(true);
			$table->integer('position');
		});

		Schema::create('blog_category_translations', function (Blueprint $table) {
			$table->integer('blog_category_id')->unsigned();
			$table->string('locale', 2);
			$table->string('name');

			$table->unique(['blog_category_id', 'locale']);

			$table->foreign('blog_category_id')->references('id')->on('blog_categories')
				->onDelete('cascade');
			$table->foreign('locale')->references('code')->on('languages')
				->onDelete('cascade');
		});

		Schema::create('blog_category_blog_post', function (Blueprint $table) {
			$table->integer('blog_category_id')->unsigned();
			$table->integer('blog_post_id')->unsigned();
			$table->unique(['blog_category_id', 'blog_post_id']);
			$table->foreign('blog_post_id')->references('id')->on('blog_posts')
				->onDelete('cascade');
			$table->foreign('blog_category_id')->references('id')->on('blog_categories')
				->onDelete('cascade');
		});

		Schema::table('country_translations', function (Blueprint $table) {
			$table->unique(['locale', 'country_id']);
		});

		Schema::table('user_translations', function (Blueprint $table) {
			$table->unique(['locale', 'user_id']);
		});

		Schema::table('language_translations', function (Blueprint $table) {
			$table->unique(['locale', 'language_id']);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::drop('blog_posts');
		Schema::drop('blog_post_translations');
		Schema::drop('blog_categories');
		Schema::drop('blog_category_translations');
		Schema::drop('blog_category_blog_post');

		Schema::table('country_translations', function (Blueprint $table) {
			$table->dropUnique(['locale', 'country_id']);
		});

		Schema::table('user_translations', function (Blueprint $table) {
			$table->dropUnique(['locale', 'user_id']);
		});

		Schema::table('language_translations', function (Blueprint $table) {
			$table->dropUnique(['locale', 'language_id']);
		});
	}
}
