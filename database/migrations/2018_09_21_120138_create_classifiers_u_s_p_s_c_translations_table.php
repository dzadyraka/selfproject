<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassifiersUSPSCTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('classifiers_uspsc_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('classifiers_uspsc_id')->unsigned()->index();
            $table->string('locale', 2)->index();
            $table->string('name', 500)->nullable(false)->index();

            $table->foreign('classifiers_uspsc_id')
                ->references('id')->on('classifiers_uspsc')
                ->onDelete('cascade');
            $table->foreign('locale')->references('code')->on('languages')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('classifiers_uspsc_translations');
    }
}
