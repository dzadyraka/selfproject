<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIntegrationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('integrations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('company_id')->unsigned()->index();
            $table->integer('company_delivery_and_payment_id')->unsigned()->index();
            $table->integer('company_export_info_id')->unsigned()->index();
            $table->string('type');
            $table->string('status');
            $table->jsonb('settings')->default('{}');
            $table->timestamps();

            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');
            $table->foreign('company_delivery_and_payment_id')->references('id')->on('company_delivery_and_payments')->onDelete('cascade');
            $table->foreign('company_export_info_id')->references('id')->on('company_export_info')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('integrations');
    }
}
