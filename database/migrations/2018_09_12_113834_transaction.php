<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Transaction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
        	$table->increments('id');
			$table->integer('user_id')->unsigned()->index();
			$table->double('value', 12, 2);
			$table->text('comment');
			$table->integer('currency_id')->unsigned();
			$table->integer('currency_code');
			$table->timestamp('timestamp');
			$table->string('payment_method', 25);
			$table->string('payment_system', 25);
			$table->jsonb('payment_details');

			$table->foreign('user_id')->references('id')->on('users')
				->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('transactions');
    }
}
