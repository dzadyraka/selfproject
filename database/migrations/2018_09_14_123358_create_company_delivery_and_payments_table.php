<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyDeliveryAndPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_delivery_and_payments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 255);
            $table->integer('company_id')->unsigned();
            $table->jsonb('delivery_type')->default('[]');
            $table->jsonb('shipping_services')->default('[]');
            $table->jsonb('payment_terms')->default('[]');

            $table->foreign('company_id')
                ->references('id')->on('companies')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_delivery_and_payments');
    }
}
