<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProductSample extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->string('sample_name')->nullable();
            $table->string('sample_description')->nullable();
            $table->string('sample_image')->nullable();
            $table->double('sample_discount')->nullable();
            $table->double('sample_width')->nullable();
            $table->double('sample_height')->nullable();
            $table->double('sample_depth')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
