<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Tenders extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        Schema::create('tender_stages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('tenders_type');
            $table->integer('step');
            $table->jsonb('option')->default('[]');
        });


        Schema::table('tenders', function (Blueprint $table) {
            $table->integer('procedure_type')->default(1);
            $table->integer('procurement_category')->default(1);
            $table->timestamp('notice_publication_date');
            $table->integer('publication_closing_period')->default(14);
            $table->integer('bid_security')->default(1);
            $table->integer('bid_security_period');
            $table->double('bid_security_sum');
            $table->integer('bid_security_currency')->unsigned();
            $table->integer('payment_security')->default(1);
            $table->integer('purchaser_id')->unsigned();
            $table->double('expected_cost');
            $table->boolean('reverse_auction')->default(false);
            $table->integer('reverse_auction_duration')->default(1);
            $table->double('reverse_auction_min_step_percent');
            $table->double('reverse_auction_min_step_cash');
            $table->integer('publication_prequalification_period');
            $table->integer('prequalification_closing_period');
            $table->integer('publication_type');
            $table->boolean('alternative_product_solution_allowed')->default(false);
            $table->timestamp('created_date');
            $table->timestamp('edited_date');
            $table->boolean('is_published')->default(false);
            $table->jsonb('classifier_uspsc')->default('[]');
            $table->jsonb('classifier_hs')->default('[]');
            $table->jsonb('classifier_cpv')->default('[]');
            $table->jsonb('classifier_isic')->default('[]');
            $table->integer('stage_id')->default(0)->unsigned();
            $table->jsonb('stage_option')->default('[]');
            $table->jsonb('stage_list')->default('[]');
            $table->boolean('reverse_auction_min_step_type')->default(false);
            $table->boolean('change_period')->default(true);
            $table->boolean('edit_requirements')->default(true);
            $table->jsonb('requirements')->default('[]');
            $table->boolean('is_draft')->default(true);
            $table->integer('created_by')->unsigned();
            $table->integer('edited_by')->unsigned();

            $table->foreign('created_by')
                ->references('id')->on('users')
                ->onDelete('cascade');

            $table->foreign('edited_by')
                ->references('id')->on('users')
                ->onDelete('cascade');

            $table->foreign('purchaser_id')
                ->references('id')->on('companies')
                ->onDelete('cascade');

            $table->foreign('stage_id')
                ->references('id')->on('tender_stages')
                ->onDelete('cascade');

            $table->foreign('bid_security_currency')
                ->references('id')->on('currencies')
                ->onDelete('cascade');
        });

        Schema::create('tenders_translations', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('tender_id')->unsigned();
            $table->string('locale', 2);

            $table->string('name');
            $table->text('procurement_description');
            $table->jsonb('non_price_criteria')->default('[]');
            $table->jsonb('procured_item_mandatory_requirements')->default('[]');
            $table->jsonb('procured_item_additional_requirements')->default('[]');
            $table->jsonb('bidder_mandatory_requirements')->default('[]');
            $table->jsonb('bidder_additional_requirements')->default('[]');
            $table->jsonb('lots')->default('[]');

            $table->unique(['tender_id', 'locale']);

            $table->foreign('tender_id')->references('id')->on('tenders')
                ->onDelete('cascade');
            $table->foreign('locale')->references('code')->on('languages')
                ->onDelete('cascade');
        });

        Schema::create('tenders_auction', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('tender_id')->index()->unsigned();
            $table->string('lot');
            $table->integer('company_id')->index()->unsigned();
            $table->float('value');
            $table->integer('currency')->unsigned();
            $table->boolean('rated')->default(false);
            $table->integer('round');
            $table->timestamp('rate_start');
            $table->timestamp('rate_end');

            $table->foreign('currency')
                ->references('id')->on('currencies')
                ->onDelete('cascade');

            $table->foreign('company_id')
                ->references('id')->on('companies')
                ->onDelete('cascade');

            $table->foreign('tender_id')
                ->references('id')->on('tenders')
                ->onDelete('cascade');
        });

        Schema::create('tender_proposals', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('tender_id')->index()->unsigned();
            $table->integer('company_id')->index()->unsigned();
            $table->integer('creator_id')->index()->unsigned();

            $table->jsonb('requirements')->default('[]');
            $table->jsonb('lots')->default('[]');

            $table->timestamp('created_date');
            $table->timestamp('edited_date');

            $table->boolean('is_approved')->default(false);
            $table->boolean('win')->default(false);

            $table->foreign('tender_id')
                ->references('id')->on('tenders')
                ->onDelete('cascade');

            $table->foreign('company_id')
                ->references('id')->on('companies')
                ->onDelete('cascade');

            $table->foreign('creator_id')
                ->references('id')->on('companies')
                ->onDelete('cascade');
        });

        Schema::create('tender_stage_dates', function (Blueprint $table) {

            $table->increments('id');

            $table->integer('tender_id')->index()->unsigned();
            $table->integer('stage_id')->index()->unsigned();

            $table->timestamp('date_from');
            $table->timestamp('date_to');

            $table->foreign('tender_id')
                ->references('id')->on('tenders')
                ->onDelete('cascade');

            $table->foreign('stage_id')
                ->references('id')->on('tender_stages')
                ->onDelete('cascade');

            $table->index(['tender_id', 'stage_id', 'date_from', 'date_to']);
        });

        Schema::create('tender_contacts', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('tender_id')->index()->unsigned();
            $table->integer('user_id')->index()->unsigned();

            $table->foreign('tender_id')
                ->references('id')->on('tenders')
                ->onDelete('cascade');

            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');

            $table->unique(['tender_id', 'user_id']);
        });

        Schema::create('tender_bidders', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('tender_id')->index()->unsigned();
            $table->integer('user_id')->index()->unsigned();

            $table->foreign('tender_id')
                ->references('id')->on('tenders')
                ->onDelete('cascade');

            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');

            $table->unique(['tender_id', 'user_id']);
        });

        Schema::create('tender_classifiers_cpv', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('tender_id')->index()->unsigned();
            $table->integer('classifier_cpv_id')->index()->unsigned();

            $table->foreign('tender_id')
                ->references('id')->on('tenders')
                ->onDelete('cascade');

            $table->foreign('classifier_cpv_id')
                ->references('id')->on('classifiers_cpv')
                ->onDelete('cascade');

            $table->unique(['tender_id', 'classifier_cpv_id']);
        });

        Schema::create('tender_classifiers_hs', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('tender_id')->index()->unsigned();
            $table->integer('classifier_hs_id')->index()->unsigned();

            $table->foreign('tender_id')
                ->references('id')->on('tenders')
                ->onDelete('cascade');

            $table->foreign('classifier_hs_id')
                ->references('id')->on('classifiers_hs')
                ->onDelete('cascade');

            $table->unique(['tender_id', 'classifier_hs_id']);
        });

        Schema::create('tender_classifiers_isic', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('tender_id')->index()->unsigned();
            $table->integer('classifier_isic_id')->index()->unsigned();

            $table->foreign('tender_id')
                ->references('id')->on('tenders')
                ->onDelete('cascade');

            $table->foreign('classifier_isic_id')
                ->references('id')->on('classifiers_isic')
                ->onDelete('cascade');

            $table->unique(['tender_id', 'classifier_isic_id']);
        });

        Schema::create('tender_classifiers_uspsc', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('tender_id')->index()->unsigned();
            $table->integer('classifier_uspsc_id')->index()->unsigned();

            $table->foreign('tender_id')
                ->references('id')->on('tenders')
                ->onDelete('cascade');

            $table->foreign('classifier_uspsc_id')
                ->references('id')->on('classifiers_uspsc')
                ->onDelete('cascade');

            $table->unique(['tender_id', 'classifier_uspsc_id']);
        });

        Schema::create('user_tender_favorite', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('company_id')->unsigned();
            $table->integer('user_id')->unsigned();

            $table->foreign('company_id')
                ->references('id')->on('companies')
                ->onDelete('cascade');

            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');

            $table->unique(['company_id', 'user_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('user_tender_favorite');
        Schema::drop('tenders_translations');
        Schema::drop('tenders_auction');
        Schema::drop('tender_proposals');
        Schema::drop('tender_stage_dates');
        Schema::drop('tender_contacts');
        Schema::drop('tender_bidders');
        Schema::drop('tender_classifiers_cpv');
        Schema::drop('tender_classifiers_hs');
        Schema::drop('tender_classifiers_isic');
        Schema::drop('tender_classifiers_uspsc');
        Schema::table('tenders', function (Blueprint $table) {
            $table->dropColumn('procedure_type');
            $table->dropColumn('procurement_category');
            $table->dropColumn('notice_publication_date');
            $table->dropColumn('publication_closing_period');
            $table->dropColumn('bid_security');
            $table->dropColumn('bid_security_period');
            $table->dropColumn('bid_security_sum');
            $table->dropColumn('bid_security_currency');
            $table->dropColumn('payment_security');
            $table->dropColumn('purchaser_id');
            $table->dropColumn('expected_cost');
            $table->dropColumn('reverse_auction');
            $table->dropColumn('reverse_auction_duration');
            $table->dropColumn('reverse_auction_min_step_percent');
            $table->dropColumn('reverse_auction_min_step_cash');
            $table->dropColumn('publication_prequalification_period');
            $table->dropColumn('prequalification_closing_period');
            $table->dropColumn('publication_type');
            $table->dropColumn('alternative_product_solution_allowed');
            $table->dropColumn('created_date');
            $table->dropColumn('edited_date');
            $table->dropColumn('is_published');
            $table->dropColumn('classifier_uspsc');
            $table->dropColumn('classifier_hs');
            $table->dropColumn('classifier_cpv');
            $table->dropColumn('classifier_isic');
            $table->dropColumn('stage_id');
            $table->dropColumn('stage_option');
            $table->dropColumn('stage_list');
            $table->dropColumn('reverse_auction_min_step_type');
            $table->dropColumn('change_period');
            $table->dropColumn('edit_requirements');
            $table->dropColumn('requirements');
            $table->dropColumn('is_draft');
            $table->dropColumn('created_by');
            $table->dropColumn('edited_by');

            $table->dropForeign('tenders_created_by_foreign');
            $table->dropForeign('tenders_edited_by_foreign');
            $table->dropForeign('tenders_purchaser_id_foreign');
            $table->dropForeign('tenders_stage_id_foreign');
            $table->dropForeign('tenders_bid_security_currency_foreign');
        });

        Schema::drop('tender_stages');
    }
}
