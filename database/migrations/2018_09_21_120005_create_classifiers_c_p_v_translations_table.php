<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassifiersCPVTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('classifiers_cpv_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('classifiers_cpv_id')->unsigned()->index();
            $table->string('locale', 2)->index();
            $table->string('name', 500)->nullable(false)->index();

            $table->foreign('classifiers_cpv_id')
                ->references('id')->on('classifiers_cpv')
                ->onDelete('cascade');
            $table->foreign('locale')->references('code')->on('languages')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('classifiers_cpv_translations');
    }
}
