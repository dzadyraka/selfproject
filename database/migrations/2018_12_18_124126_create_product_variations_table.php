<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductVariationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_variations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id')->unsigned()->index();
            $table->jsonb('images')->default('[]');
            $table->string('sku')->nullable();
            $table->float('price')->nullable();
            $table->float('minimum_bulk_price')->nullable();
            $table->integer('quantity_available')->nullable();
            $table->integer('minimum_ordery')->nullable();
            $table->boolean('negotiate')->default(false);
            $table->jsonb('bulk_prices')->default('[]');
            $table->boolean('has_sample')->default(false);
            $table->float('sample_price')->nullable();
            $table->timestamp('created_date');
            $table->timestamp('edited_date');

            $table->foreign('product_id')
                ->references('id')
                ->on('products')
                ->onDelete('cascade');
        });

        Schema::create('product_variation_attributes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_variation_id')->unsigned();
            $table->integer('attribute_id')->unsigned();
            $table->integer('attribute_option_id')->unsigned();

            $table->unique(['product_variation_id', 'attribute_id', 'attribute_option_id']);

            $table->foreign('product_variation_id')
                ->references('id')
                ->on('product_variations')
                ->onDelete('cascade');

            $table->foreign('attribute_id')
                ->references('id')
                ->on('attributes')
                ->onDelete('cascade');

            $table->foreign('attribute_option_id')
                ->references('id')
                ->on('attribute_options')
                ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_variation_attributes');
        Schema::dropIfExists('product_variations');
    }
}
