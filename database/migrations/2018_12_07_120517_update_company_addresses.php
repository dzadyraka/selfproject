<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCompanyAddresses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('company_addresses', function (Blueprint $table) {
            $table->integer('zip_code')->nullable()->change();
            $table->string('first_name')->nullable()->change();
            $table->string('last_name')->nullable()->change();
            $table->string('street_name')->nullable()->change();
            $table->string('street_data')->nullable()->change();
        });

        Schema::table('companies', function (Blueprint $table) {
            $table->string('address')->nullable();
            $table->string('postal_code')->nullable();
            $table->string('number_of_employees_distribution')->nullable();
            $table->string('annual_turnover')->nullable();
            $table->string('port')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('company_addresses', function (Blueprint $table) {
            $table->integer('zip_code')->change();
            $table->string('first_name')->change();
            $table->string('last_name')->change();
            $table->string('street_name')->change();
            $table->string('street_data')->change();
        });


        Schema::table('companies', function (Blueprint $table) {
            $table->dropColumn('address');
            $table->dropColumn('postal_code');
            $table->dropColumn('number_of_employees_distribution');
            $table->dropColumn('annual_turnover');
            $table->dropColumn('port');
        });
    }
}
