<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UsersUserFields extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {

		Schema::create('languages', function (Blueprint $table) {
			$table->increments('language_id');
			$table->string('name');
			$table->string('code', 3);
			$table->string('locale');
			$table->string('image');
			$table->boolean('is_active')->default(true);
		});

		Schema::create('languages_translations', function (Blueprint $table) {
			$table->integer('language_id')->unsigned()->index();
			$table->integer('languages_id')->unsigned()->index();
			$table->string('name');

			$table->unique(['language_id', 'languages_id']);
			$table->foreign('language_id')->references('language_id')->on('languages')->onDelete('cascade');
			$table->foreign('languages_id')->references('language_id')->on('languages')->onDelete('cascade');
		});

		Schema::create('countries', function (Blueprint $table) {
			$table->increments('id');
			$table->string('code', 2);
			$table->string('iso', 2);
			$table->string('iso3', 3);
			$table->string('phone_code', 15);
			$table->string('phone_length', 50);
			$table->string('latitude', 50)->nullable();
			$table->string('longitude', 50)->nullable();
		});

		Schema::create('countries_translations', function (Blueprint $table) {
			$table->integer('language_id')->unsigned()->index();
			$table->integer('country_id')->unsigned()->index();
			$table->string('name');

			$table->unique(['language_id', 'country_id']);
			$table->foreign('language_id')->references('language_id')->on('languages')->onDelete('cascade');
			$table->foreign('country_id')->references('id')->on('countries')->onDelete('cascade');
		});

		Schema::table('users', function (Blueprint $table) {
			$table->dropColumn('name');

			$table->string('phone');
			$table->integer( 'country_id')->unsigned();
			$table->integer( 'language_id')->unsigned();
			$table->renameColumn('created_at', 'created_date');
			$table->renameColumn('updated_at', 'updated_date');
			$table->boolean('is_active')->default(true);
			$table->boolean('is_approved')->default(false);
			$table->boolean('is_blocked')->default(false);
			$table->double( 'balance', 12, 5)->default(0);
			$table->string('avatar')->default('avatar.png');
			$table->jsonb('custom_settings')->default('{}');
			$table->integer( 'currency_id')->default(2)->unsigned();
			$table->string('time_zone')->default('UTC');

			$table->foreign('country_id')->references('id')->on('countries');
			$table->foreign('language_id')->references('language_id')->on('languages');
		});

		Schema::create('users_translations', function (Blueprint $table) {
			$table->string( 'first_name');
			$table->string( 'last_name');
			$table->integer('user_id')->unsigned();
			$table->integer('language_id')->unsigned()->index();

			$table->unique(['user_id', 'language_id']);
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
			$table->foreign('language_id')->references('language_id')->on('languages')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::drop('users_translations');
		Schema::drop('countries_translations');
		Schema::drop('languages_translations');
		Schema::drop('languages');
		Schema::drop('countries');

		Schema::table('users', function (Blueprint $table) {
			$table->dropColumn('phone');
			$table->dropColumn('country_id');
			$table->dropColumn('language_id');
			$table->dropColumn('is_active');
			$table->dropColumn('is_approved');
			$table->dropColumn('is_blocked');
			$table->dropColumn('balance');
			$table->dropColumn('avatar');
			$table->dropColumn('custom_settings');
			$table->dropColumn('currency_id');
			$table->dropColumn('time_zone');

//			$table->string('name');
			$table->renameColumn('created_date', 'created_at');
			$table->renameColumn('updated_date', 'updated_at');
		});
	}
}
