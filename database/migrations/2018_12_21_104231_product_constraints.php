<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProductConstraints extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
           $table->float('price')->default(0)->change();
           $table->string('sku')->default('')->change();
        });

        Schema::table('product_attribute', function (Blueprint $table) {
            $table->dropColumn('attribute_option_id');
        });

        Schema::create('product_attribute_options', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_attribute_id')->unsigned();
            $table->integer('attribute_option_id')->unsigned();

            $table->unique('product_attribute_id', 'attribute_option_id');

            $table->foreign('product_attribute_id')
                ->references('id')->on('product_attribute')
                ->onDelete('cascade');

            $table->foreign('attribute_option_id')
                ->references('id')->on('attribute_options')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
