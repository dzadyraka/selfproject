<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ComtradeExtend extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('comtrades', function (Blueprint $table) {
            $table->integer('hs_level')->nullable()->index();
            $table->integer('hs_code')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('comtrades', function (Blueprint $table) {
            $table->dropColumn('hs_level');
            $table->dropColumn('hs_code');
        });
    }
}
