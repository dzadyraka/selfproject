<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassifiersCPVsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('classifiers_cpv', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code')->index();
            $table->integer('level');
            $table->integer('parent_id')->unsigned()->nullable()->index();
            $table->boolean('has_child')->nullable();


            $table->foreign('parent_id')
                ->references('id')->on('classifiers_cpv')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('classifiers_cpv');
    }
}
