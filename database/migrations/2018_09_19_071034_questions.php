<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Questions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	Schema::create('tenders', function (Blueprint $table) {
    		$table->increments('id');
	    });

	    Schema::create('products', function (Blueprint $table) {
		    $table->increments('id');
	    });

	    Schema::create('tenders_questions', function (Blueprint $table) {
		    $table->increments('id');

		    $table->text('content');
		    $table->integer('user_id')->unsigned();
		    $table->integer('parent_id')->nullable()->unsigned();
		    $table->integer('company_id')->unsigned();
		    $table->integer('tender_id')->unsigned();

		    $table->timestamp('created_date');
		    $table->timestamp('updated_date');

		    $table->foreign('user_id')
			    ->references('id')->on('users')
			    ->onDelete('cascade');

		    $table->foreign('parent_id')
			    ->references('id')->on('tenders_questions')
			    ->onDelete('cascade');

		    $table->foreign('company_id')
			    ->references('id')->on('companies')
			    ->onDelete('cascade');

		    $table->foreign('tender_id')
			    ->references('id')->on('tenders')
			    ->onDelete('cascade');
	    });

	    Schema::create('products_questions', function (Blueprint $table) {
		    $table->increments('id');

		    $table->text('content');
		    $table->integer('user_id')->unsigned();
		    $table->integer('parent_id')->nullable()->unsigned();
		    $table->integer('company_id')->unsigned();
		    $table->integer('product_id')->unsigned();

		    $table->timestamp('created_date');
		    $table->timestamp('updated_date');

		    $table->foreign('user_id')
			    ->references('id')->on('users')
			    ->onDelete('cascade');

		    $table->foreign('parent_id')
			    ->references('id')->on('products_questions')
			    ->onDelete('cascade');

		    $table->foreign('company_id')
			    ->references('id')->on('companies')
			    ->onDelete('cascade');

		    $table->foreign('product_id')
			    ->references('id')->on('products')
			    ->onDelete('cascade');
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::dropIfExists('tenders_questions');
	    Schema::dropIfExists('products_questions');
	    Schema::dropIfExists('tenders');
	    Schema::dropIfExists('products');
	    Schema::dropIfExists('companies');
    }
}
