<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('regions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('country_id')->unsigned()->index();

            $table->foreign('country_id')
                ->references('id')->on('countries')
                ->onDelete('cascade');

        });

        Schema::create('cities', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('country_id')->unsigned()->index();
            $table->integer('region_id')->unsigned()->nullable()->index();

            $table->foreign('country_id')
                ->references('id')->on('countries')
                ->onDelete('cascade');

            $table->foreign('region_id')
                ->references('id')->on('regions')
                ->onDelete('cascade');
        });

        Schema::table('companies', function (Blueprint $table) {
            $table->foreign('city_id')
                ->references('id')->on('cities');

            $table->foreign('region_id')
                ->references('id')->on('regions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('companies', function (Blueprint $table) {
            $table->dropForeign('companies_city_id_foreign');

            $table->dropForeign('companies_region_id_foreign');
        });

        Schema::dropIfExists('regions');
        Schema::dropIfExists('cities');
    }
}
