<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Currency extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	Schema::create('currencies', function (Blueprint $table) {
    		$table->increments('id');
		    $table->string('code', 3);
			$table->string('symbol', 5);
			$table->string('thousand_separator', 10);
			$table->string('decimal_separator', 10);
			$table->boolean('active')->default(false);

			$table->unique('code');
	    });

    	Schema::create('currency_translations', function (Blueprint $table) {
    		$table->increments('id');
		    $table->integer('currency_id')->unsigned();
		    $table->string('locale', 2);
		    $table->string('name');

		    $table->unique(['currency_id', 'locale']);

		    $table->foreign('currency_id')->references('id')->on('currencies')
			    ->onDelete('cascade');
		    $table->foreign('locale')->references('code')->on('languages')
			    ->onDelete('cascade');
	    });

        Schema::create('currency_rates', function (Blueprint $table) {
			$table->increments('id');
	        $table->string('source', 3);
	        $table->integer('source_id')->unsigned();
	        $table->string('destination', 3);
	        $table->integer('destination_id')->unsigned();
			$table->double('rate', 12, 2)->default(1);
			$table->date('date_from');
			$table->date('date_to')->nullable();

	        $table->foreign('source')
		        ->references('code')->on('currencies')
		        ->onDelete('cascade');
	        $table->foreign('source_id')
		        ->references('id')->on('currencies')
		        ->onDelete('cascade');
	        $table->foreign('destination')
		        ->references('code')->on('currencies')
		        ->onDelete('cascade');
	        $table->foreign('destination_id')
		        ->references('id')->on('currencies')
		        ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('currency_rates');
        Schema::drop('currency_translations');
        Schema::drop('currencies');
    }
}
