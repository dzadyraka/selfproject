<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProposalBidders extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('proposal_bidders', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('user_id')->index()->unsigned();
            $table->integer('proposal_id')->index()->unsigned();

            $table->foreign('proposal_id')
                ->references('id')->on('tender_proposals')
                ->onDelete('cascade');

            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');

            $table->unique(['proposal_id', 'user_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('proposal_bidders');
    }
}
