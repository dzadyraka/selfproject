<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class KnowledgeBase extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('knowledge_base_categories', function (Blueprint $table) {
			$table->increments('id');
			$table->string('alias')->unique();
			$table->integer('position')->default(1);
			$table->timestamp('created_date');
			$table->timestamp('edited_date');
			$table->boolean('is_active')->default(false);
		});

		Schema::create('knowledge_base_category_translations', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('knowledge_base_category_id')->unsigned();
			$table->string('locale', 2);
			$table->string('name');

			$table->unique(['knowledge_base_category_id', 'locale']);

			$table->foreign('knowledge_base_category_id', 'kbcid')->references('id')->on('knowledge_base_categories')
				->onDelete('cascade');
			$table->foreign('locale')->references('code')->on('languages')
				->onDelete('cascade');
		});

		Schema::create('knowledge_base_for_whoms', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('section_id')->unsigned();
			$table->timestamp('created_date');
			$table->timestamp('edited_date');
			$table->boolean('is_active')->default(false);

			$table->foreign('section_id')->references('id')->on('knowledge_base_categories')
				->onDelete('cascade');
		});

		Schema::create('knowledge_base_for_whoms_translations', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('knowledge_base_for_whom_id')->unsigned();
			$table->string('locale', 2);
			$table->string('name');

			$table->unique(['knowledge_base_for_whom_id', 'locale']);

			$table->foreign('knowledge_base_for_whom_id', 'kbfwid')->references('id')->on('knowledge_base_for_whoms')
				->onDelete('cascade');
			$table->foreign('locale')->references('code')->on('languages')
				->onDelete('cascade');
		});

		Schema::create('knowledge_base_articles', function (Blueprint $table) {
			$table->increments('id');

			$table->integer('post_type');
			$table->integer('section_id')->unsigned();
			$table->timestamp('created_date');
			$table->timestamp('edited_date');
			$table->boolean('is_published')->default(true);
			$table->boolean('is_active')->default(false);

			$table->foreign('section_id')->references('id')->on('knowledge_base_categories')
				->onDelete('cascade');
		});

		Schema::create('knowledge_base_article_translations', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('knowledge_base_article_id')->unsigned();
			$table->string('locale', 2);
			$table->string('name');
			$table->text('content');
			$table->text('meta_title');
			$table->text('meta_keywords');
			$table->text('meta_description');
			$table->text('description');

			$table->unique(['knowledge_base_article_id', 'locale']);

			$table->foreign('knowledge_base_article_id', 'kbaid')->references('id')->on('knowledge_base_articles')
				->onDelete('cascade');
			$table->foreign('locale')->references('code')->on('languages')
				->onDelete('cascade');
		});

		Schema::create('knowledge_base_for_whom_knowledge_base_article', function (Blueprint $table) {
		    $table->integer('knowledge_base_for_whom_id')->unsigned();
		    $table->integer('knowledge_base_article_id')->unsigned();

			$table->foreign('knowledge_base_article_id')->references('id')->on('knowledge_base_articles')
				->onDelete('cascade');
			$table->foreign('knowledge_base_for_whom_id')->references('id')->on('knowledge_base_for_whoms')
				->onDelete('cascade');
		});

		Schema::create('knowledge_base_sub_categories', function (Blueprint $table) {
			$table->increments('id');
			$table->string('alias')->unique();
			$table->integer('section_id')->unsigned();
			$table->integer('position')->default(1);
			$table->timestamp('created_date');
			$table->timestamp('edited_date');
			$table->boolean('is_active')->default(false);

			$table->foreign('section_id')->references('id')->on('knowledge_base_categories')
				->onDelete('cascade');
		});

		Schema::create('knowledge_base_sub_category_knowledge_base_article', function (Blueprint $table) {
			$table->integer('knowledge_base_sub_category_id')->unsigned();
			$table->integer('knowledge_base_article_id')->unsigned();

			$table->foreign('knowledge_base_article_id', 'kbaid')->references('id')->on('knowledge_base_articles')
				->onDelete('cascade');
			$table->foreign('knowledge_base_sub_category_id', 'kbscid')->references('id')->on('knowledge_base_sub_categories')
				->onDelete('cascade');
		});

		Schema::create('knowledge_base_sub_category_translations', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('knowledge_base_sub_category_id')->unsigned();
			$table->string('locale', 2);
			$table->string('name');

			$table->unique(['knowledge_base_sub_category_id', 'locale']);

			$table->foreign('knowledge_base_sub_category_id', 'kbscid')->references('id')->on('knowledge_base_sub_categories')
				->onDelete('cascade');
			$table->foreign('locale')->references('code')->on('languages')
				->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::drop('knowledge_base_sub_category_translations');
		Schema::drop('knowledge_base_sub_category_knowledge_base_article');
		Schema::drop('knowledge_base_sub_categories');
		Schema::drop('knowledge_base_for_whom_knowledge_base_article');
		Schema::drop('knowledge_base_article_translations');
		Schema::drop('knowledge_base_articles');
		Schema::drop('knowledge_base_for_whoms_translations');
		Schema::drop('knowledge_base_for_whoms');
		Schema::drop('knowledge_base_category_translations');
		Schema::drop('knowledge_base_categories');
	}
}
