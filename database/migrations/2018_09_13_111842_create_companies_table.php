<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('country_id')->unsigned();
            $table->integer('region_id')->unsigned()->nullable();
            $table->integer('city_id')->unsigned()->nullable();
            $table->string('phone', 12)->nullable(false);
            $table->string('website', 100);
            $table->string('email', 100)->nullable(false);
            $table->string('logo')->nullable(true);
            $table->jsonb('cpv_codes')->default('[]');
            $table->boolean('is_approved')->default(false);
            $table->string('number_of_employers', 255)->nullable(true);
            $table->jsonb('attachments')->default('[]');
            $table->jsonb('certificates')->default('[]');
            $table->double('rating', 10, 1)->default(0);
            $table->string('registration_number', 255)->nullable(true);
            $table->string('business_type', 255)->nullable(true);
            $table->integer('year_established')->nullable(true);
            $table->jsonb('authorized_products')->default('[]');
            $table->jsonb('production_information')->default('[]');
            $table->jsonb('trade_capacity')->default('[]');
            $table->integer('annual_revenue')->nullable(true);
            $table->timestamp('created_date');
            $table->timestamp('updated_date');

            $table->foreign('country_id')
                ->references('id')->on('countries');
        });

        Schema::table('products_settings', function (Blueprint $table) {
            $table->foreign('company_id')
                ->references('id')->on('companies')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products_settings', function (Blueprint $table) {
            $table->dropForeign('products_settings_company_id_foreign');
        });
        Schema::dropIfExists('companies');
    }
}
