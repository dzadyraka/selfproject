<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(LanguagesTableSeeder::class);
         $this->call(CountriesTableSeeder::class);
         $this->call(UsersTableSeeder::class);
         $this->call(CurrenciesTableSeeder::class);
         $this->call(CategoriesTableSeeder::class);
         $this->call(RegionTableSeeder::class);
         $this->call(CitiesTableSeeder::class);
         $this->call(CityTranslationTableSeeder::class);
         $this->call(CPVTableSeeder::class);
         $this->call(CPVTranslationTableSeeder::class);
         $this->call(HSTableSeeder::class);
         $this->call(HSTranslationTableSeeder::class);
         $this->call(ISICTableSeeder::class);
         $this->call(ISICTranslationTableSeeder::class);
         $this->call(USPSCTableSeeder::class);
         $this->call(USPSCTranslationTableSeeder::class);
         $this->call(RtaTableSeeder::class);
    }
}
