<?php

use App\Locations\Models\Country;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Rta\Models\Rta;

class RtaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rta = [
            [
                "id" => 335,
                "rta_id" => 3,
                "name" => "Chile - Japan",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2007-03-27",
                "notification_date" => "2007-08-24",
                "entry_force_date" => "2007-09-03",
                "current_signatories" => [
                    "Chile",
                    "Japan"
                ],
                "original_signatories" => [
                    "Chile",
                    "Japan"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "South America",
                    "East Asia"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.mofa.go.jp/",
                        "name" => "Ministry of Foreign Affairs of Japan"
                    ],
                    [
                        "url" => "http://www.direcon.gob.cl/",
                        "name" => "Dirección General de Relaciones Económicas Internacionales, Chile"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.mofa.go.jp/region/latin/chile/joint0703/agreement.pdf",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://www.direcon.gob.cl/detalle-de-acuerdos/?idacuerdo=6253",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Chile"
                ],
                "countries_second" => [
                    "Japan"
                ]
            ],
            [
                "id" => 643,
                "rta_id" => 2,
                "name" => "Egypt - Turkey",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "Enabling Clause",
                "signature_date" => "2005-12-27",
                "notification_date" => "2007-10-05",
                "entry_force_date" => "2007-03-01",
                "current_signatories" => [
                    "Egypt",
                    "Turkey"
                ],
                "original_signatories" => [
                    "Egypt",
                    "Turkey"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "Africa",
                    "Europe"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.economy.gov.tr/",
                        "name" => "Turkish Government"
                    ],
                    [
                        "url" => "http://www.mfa.gov.eg/English/Pages/default.aspx",
                        "name" => "Arab Republic of Egypt, Ministry of Foreign Affairs"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "../rtadocs/2/TOA/English/Egypt-TurkeyAgreement.doc",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Turkey"
                ],
                "countries_second" => [
                    "Egypt"
                ]
            ],
            [
                "id" => 334,
                "rta_id" => 1,
                "name" => "Japan - Thailand",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2007-04-03",
                "notification_date" => "2007-10-25",
                "entry_force_date" => "2007-11-01",
                "current_signatories" => [
                    "Japan",
                    "Thailand"
                ],
                "original_signatories" => [
                    "Japan",
                    "Thailand"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "East Asia"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.mofa.go.jp/",
                        "name" => "Ministry of Foreign Affairs of Japan"
                    ],
                    [
                        "url" => "http://www.mfa.go.th/main/",
                        "name" => "Ministry of Foreign Affairs, Kingdom of Thailand"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.mofa.go.jp/region/asia-paci/thailand/epa0704/agreement.pdf",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Japan"
                ],
                "countries_second" => [
                    "Thailand"
                ]
            ],
            [
                "id" => 336,
                "rta_id" => 4,
                "name" => "Central European Free Trade Agreement (CEFTA) 2006",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "2006-12-19",
                "notification_date" => "2007-07-26",
                "entry_force_date" => "2007-05-01",
                "current_signatories" => [
                    "Albania",
                    "Bosnia and Herzegovina",
                    "Moldova",
                    "Montenegro",
                    "Serbia",
                    "Macedonia",
                    "UNMIK/Kosovo"
                ],
                "original_signatories" => [
                    "Albania",
                    "Bosnia and Herzegovina",
                    "Bulgaria",
                    "Croatia",
                    "Moldova",
                    "Montenegro",
                    "Romania",
                    "Serbia",
                    "Macedonia",
                    "UNMIK/Kosovo"
                ],
                "rta_composition" => [
                    "Plurilateral"
                ],
                "region" => [
                    "Europe",
                    "Commonwealth of Independent States (CIS), including associate and former member States"
                ],
                "wto_members" => false,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://cefta.int/",
                        "name" => "CEFTA Secretariat"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://cefta.int/legal-documents/#1463498231136-8f9d234f-15f9",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Albania"
                ],
                "countries_second" => [
                    "Bosnia and Herzegovina",
                    "Moldova",
                    "Macedonia",
                    "UNMIK/Kosovo",
                    "Serbia",
                    "Montenegro"
                ]
            ],
            [
                "id" => 347,
                "rta_id" => 17,
                "name" => "Gulf Cooperation Council (GCC)",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "",
                "signature_date" => "2001-12-31",
                "notification_date" => "2006-10-03",
                "entry_force_date" => "2003-01-01",
                "current_signatories" => [
                    "Bahrain",
                    "Kuwait",
                    "Oman",
                    "Qatar",
                    "Saudi Arabia",
                    "United Arab Emirates"
                ],
                "original_signatories" => [
                    "Bahrain",
                    "Kuwait",
                    "Oman",
                    "Qatar",
                    "Saudi Arabia",
                    "United Arab Emirates"
                ],
                "rta_composition" => [
                    "Plurilateral"
                ],
                "region" => [
                    "Middle East"
                ],
                "wto_members" => true,
                "type" => "Customs Union",
                "web_addresses" => [
                    [
                        "url" => "http://www.gcc-sg.org/en-us/Pages/default.aspx",
                        "name" => "Gulf Cooperation Council"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "../rtadocs/17/TOA/English/The%20Economic%20Agreement%20Between%20the%20GCC%20States.doc",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Bahrain"
                ],
                "countries_second" => [
                    "Kuwait",
                    "Oman",
                    "Qatar",
                    "Saudi Arabia",
                    "United Arab Emirates"
                ]
            ],
            [
                "id" => 337,
                "rta_id" => 5,
                "name" => "EFTA - Egypt",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "2007-01-27",
                "notification_date" => "2007-07-17",
                "entry_force_date" => "2007-08-01",
                "current_signatories" => [
                    "Egypt",
                    "Iceland",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland"
                ],
                "original_signatories" => [
                    "Egypt",
                    "Iceland",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland"
                ],
                "rta_composition" => [
                    "Bilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "Africa",
                    "Europe"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.efta.int",
                        "name" => "European Free Trade Association"
                    ],
                    [
                        "url" => "http://www.mfa.gov.eg/English/Pages/default.aspx",
                        "name" => "Arab Republic of Egypt, Ministry of Foreign Affairs"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.efta.int/~/media/Documents/legal-texts/free-trade-relations/egypt/EFTA-Egypt%20Free%20Trade%20Agreement.pdf",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Egypt"
                ],
                "countries_second" => [
                    "Iceland",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland"
                ]
            ],
            [
                "id" => 342,
                "rta_id" => 10,
                "name" => "India - Singapore",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2005-06-29",
                "notification_date" => "2007-05-03",
                "entry_force_date" => "2005-08-01",
                "current_signatories" => [
                    "India",
                    "Singapore"
                ],
                "original_signatories" => [
                    "India",
                    "Singapore"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "West Asia",
                    "East Asia"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.commerce.nic.in/",
                        "name" => "Government of India, Ministry of Commerce & Industry"
                    ],
                    [
                        "url" => "http://www.iesingapore.gov.sg",
                        "name" => "International Enterprise Singapore"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://commerce.gov.in/PageContent.aspx?Id=41",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "India"
                ],
                "countries_second" => [
                    "Singapore"
                ]
            ],
            [
                "id" => 644,
                "rta_id" => 11,
                "name" => "Panama - Singapore",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2006-03-01",
                "notification_date" => "2007-04-04",
                "entry_force_date" => "2006-07-24",
                "current_signatories" => [
                    "Panama",
                    "Singapore"
                ],
                "original_signatories" => [
                    "Panama",
                    "Singapore"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "Central America",
                    "East Asia"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.mici.gob.pa/",
                        "name" => "Ministerio de Comercio e Industrias, Panamá"
                    ],
                    [
                        "url" => "http://www.iesingapore.gov.sg",
                        "name" => "International Enterprise Singapore"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.iesingapore.gov.sg/~/media/IE%20Singapore/Files/FTA/Non%20Priority%20FTAs/Panama%20PSFTA/Legal%20Text/Panama20PSFTA20Legal20Text.pdf",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://www.mici.gob.pa/singapur/ley/texto_panama_singapur.pdf",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Panama"
                ],
                "countries_second" => [
                    "Singapore"
                ]
            ],
            [
                "id" => 344,
                "rta_id" => 13,
                "name" => "Turkey - Syria",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "2004-12-23",
                "notification_date" => "2007-02-15",
                "entry_force_date" => "2007-01-01",
                "current_signatories" => [
                    "Syria",
                    "Turkey"
                ],
                "original_signatories" => [
                    "Syria",
                    "Turkey"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "Middle East",
                    "Europe"
                ],
                "wto_members" => false,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.economy.gov.tr/",
                        "name" => "Turkish Government"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "../rtadocs/13/TOA/English/Turkey%20-%20Syrian%20Arab%20Republic%20Agreement.zip",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Syria"
                ],
                "countries_second" => [
                    "Turkey"
                ]
            ],
            [
                "id" => 341,
                "rta_id" => 9,
                "name" => "Trans-Pacific Strategic Economic Partnership",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2005-07-18",
                "notification_date" => "2007-05-18",
                "entry_force_date" => "2006-05-28",
                "current_signatories" => [
                    "Brunei Darussalam",
                    "Chile",
                    "New Zealand",
                    "Singapore"
                ],
                "original_signatories" => [
                    "Brunei Darussalam",
                    "Chile",
                    "New Zealand",
                    "Singapore"
                ],
                "rta_composition" => [
                    "Plurilateral"
                ],
                "region" => [
                    "East Asia",
                    "South America",
                    "Oceania"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.direcon.gob.cl/",
                        "name" => "Dirección General de Relaciones Económicas Internacionales, Chile"
                    ],
                    [
                        "url" => "http://www.mfat.govt.nz/",
                        "name" => "New Zealand Ministry of Foreign Affairs and Trade"
                    ],
                    [
                        "url" => "http://www.iesingapore.gov.sg/",
                        "name" => "International Enterprise Singapore"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "https://www.mfat.govt.nz/assets/_securedfiles/FTAs-agreements-in-force/P4/Full-text-of-P4-agreement.pdf",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://www.direcon.gob.cl/detalle-de-acuerdos/?idacuerdo=6240",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Brunei Darussalam"
                ],
                "countries_second" => [
                    "Chile",
                    "New Zealand",
                    "Singapore"
                ]
            ],
            [
                "id" => 345,
                "rta_id" => 15,
                "name" => "EFTA - Lebanon",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "2004-06-24",
                "notification_date" => "2006-12-22",
                "entry_force_date" => "2007-01-01",
                "current_signatories" => [
                    "Iceland",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland",
                    "Lebanese Republic"
                ],
                "original_signatories" => [
                    "Iceland",
                    "Lebanese Republic",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland"
                ],
                "rta_composition" => [
                    "Bilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "Europe",
                    "Middle East"
                ],
                "wto_members" => false,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.efta.int",
                        "name" => "European Free Trade Association"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.efta.int/~/media/Documents/legal-texts/free-trade-relations/lebanon/EFTA-Lebanon%20Free%20Trade%20Agreement.pdf",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Lebanese Republic"
                ],
                "countries_second" => [
                    "Iceland",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland"
                ]
            ],
            [
                "id" => 346,
                "rta_id" => 16,
                "name" => "Pan-Arab Free Trade Area (PAFTA)",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "1997-02-19",
                "notification_date" => "2006-10-03",
                "entry_force_date" => "1998-01-01",
                "current_signatories" => [
                    "Bahrain",
                    "Egypt",
                    "Iraq",
                    "Jordan",
                    "Kuwait",
                    "Lebanese Republic",
                    "Libya",
                    "Morocco",
                    "Oman",
                    "Qatar",
                    "Saudi Arabia",
                    "Sudan",
                    "Syria",
                    "Tunisia",
                    "United Arab Emirates",
                    "Yemen"
                ],
                "original_signatories" => [
                    "Bahrain",
                    "Egypt",
                    "Iraq",
                    "Jordan",
                    "Kuwait",
                    "Lebanese Republic",
                    "Libya",
                    "Morocco",
                    "Oman",
                    "Qatar",
                    "Saudi Arabia",
                    "Sudan",
                    "Syria",
                    "Tunisia",
                    "United Arab Emirates",
                    "Yemen"
                ],
                "rta_composition" => [
                    "Plurilateral"
                ],
                "region" => [
                    "Middle East",
                    "Africa"
                ],
                "wto_members" => false,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.arableagueonline.org/",
                        "name" => "Arab League"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "../rtadocs/16/TOA/English/Pan-Arab%20Free%20Trade%20Area%20Agreement%20(1997).pdf",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Bahrain"
                ],
                "countries_second" => [
                    "Iraq",
                    "Jordan",
                    "Kuwait",
                    "Lebanese Republic",
                    "Libya",
                    "Morocco",
                    "Oman",
                    "Qatar",
                    "Saudi Arabia",
                    "Sudan",
                    "Syria",
                    "United Arab Emirates",
                    "Tunisia",
                    "Egypt",
                    "Yemen"
                ]
            ],
            [
                "id" => 339,
                "rta_id" => 7,
                "name" => "Southern African Customs Union (SACU)",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "2002-10-21",
                "notification_date" => "2007-06-25",
                "entry_force_date" => "2004-07-15",
                "current_signatories" => [
                    "Botswana",
                    "Swaziland",
                    "Lesotho",
                    "Namibia",
                    "South Africa"
                ],
                "original_signatories" => [
                    "Botswana",
                    "Swaziland",
                    "Lesotho",
                    "Namibia",
                    "South Africa"
                ],
                "rta_composition" => [
                    "Plurilateral"
                ],
                "region" => [
                    "Africa"
                ],
                "wto_members" => true,
                "type" => "Customs Union",
                "web_addresses" => [
                    [
                        "url" => "http://www.sacu.int",
                        "name" => "Southern African Customs Union"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://sacu.int/show.php?id=566",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Botswana"
                ],
                "countries_second" => [
                    "Lesotho",
                    "Namibia",
                    "South Africa",
                    "Swaziland"
                ]
            ],
            [
                "id" => 349,
                "rta_id" => 19,
                "name" => "United States - Bahrain",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2005-09-14",
                "notification_date" => "2006-09-08",
                "entry_force_date" => "2006-08-01",
                "current_signatories" => [
                    "Bahrain",
                    "USA"
                ],
                "original_signatories" => [
                    "Bahrain",
                    "USA"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "Middle East",
                    "North America"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.ustr.gov/",
                        "name" => "Office of the United States Trade Representative"
                    ],
                    [
                        "url" => "http://www.mofa.gov.bh/",
                        "name" => "Kingdom of Bahrain"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.ustr.gov/trade-agreements/free-trade-agreements/bahrain-fta/final-text",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Bahrain"
                ],
                "countries_second" => [
                    "USA"
                ]
            ],
            [
                "id" => 351,
                "rta_id" => 21,
                "name" => "EFTA - Korea, Republic of",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2005-12-15",
                "notification_date" => "2006-08-23",
                "entry_force_date" => "2006-09-01",
                "current_signatories" => [
                    "Iceland",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland",
                    "North Korea"
                ],
                "original_signatories" => [
                    "Iceland",
                    "North Korea",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland"
                ],
                "rta_composition" => [
                    "Bilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "Europe",
                    "East Asia"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.efta.int",
                        "name" => "European Free Trade Association"
                    ],
                    [
                        "url" => "http://www.mofa.go.kr/eng/index.do",
                        "name" => "Ministry of Foreign Affairs and Trade, Republic of Korea"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.efta.int/~/media/Documents/legal-texts/free-trade-relations/republic-of-korea/EFTA-%20Republic%20of%20Korea%20Free%20Trade%20Agreement.pdf",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "North Korea"
                ],
                "countries_second" => [
                    "Iceland",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland"
                ]
            ],
            [
                "id" => 357,
                "rta_id" => 27,
                "name" => "Dominican Republic - Central America - United States Free Trade Agreement (CAFTA-DR)",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2004-08-05",
                "notification_date" => "2006-03-17",
                "entry_force_date" => "2006-03-01",
                "current_signatories" => [
                    "Costa Rica",
                    "Dominican Republic",
                    "El Salvador",
                    "Guatemala",
                    "Honduras",
                    "Nicaragua",
                    "USA"
                ],
                "original_signatories" => [
                    "Costa Rica",
                    "Dominican Republic",
                    "El Salvador",
                    "Guatemala",
                    "Honduras",
                    "Nicaragua",
                    "USA"
                ],
                "rta_composition" => [
                    "Plurilateral"
                ],
                "region" => [
                    "Central America",
                    "Caribbean",
                    "North America"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.ustr.gov/",
                        "name" => "Office of the United States Trade Representative"
                    ],
                    [
                        "url" => "http://www.minec.gob.sv/",
                        "name" => "Ministerio de Economia de El Salvador"
                    ],
                    [
                        "url" => "http://www.comex.go.cr",
                        "name" => "Ministerio de Comercio Exterior de Costa Rica"
                    ],
                    [
                        "url" => "https://mic.gob.do/",
                        "name" => "Secretaría de Estado de Industría y Comercio de República Dominicana"
                    ],
                    [
                        "url" => "http://www.mineco.gob.gt",
                        "name" => "Ministerio de Economía de Guatemala"
                    ],
                    [
                        "url" => "https://sde.gob.hn/",
                        "name" => "Secretaría de Desarollo Económico, Honduras"
                    ],
                    [
                        "url" => "http://www.mific.gob.ni",
                        "name" => "Ministerio de Fomento, Industría y Comercio de Nicaragua"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.ustr.gov/trade-agreements/free-trade-agreements/cafta-dr-dominican-republic-central-america-fta/final-text",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://www.sice.oas.org/Trade/CAFTA/CAFTADR/CAFTADRin_s.asp",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Costa Rica"
                ],
                "countries_second" => [
                    "Dominican Republic",
                    "El Salvador",
                    "Guatemala",
                    "Honduras",
                    "Nicaragua",
                    "USA"
                ]
            ],
            [
                "id" => 361,
                "rta_id" => 31,
                "name" => "Thailand - New Zealand",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2005-04-19",
                "notification_date" => "2005-12-01",
                "entry_force_date" => "2005-07-01",
                "current_signatories" => [
                    "New Zealand",
                    "Thailand"
                ],
                "original_signatories" => [
                    "New Zealand",
                    "Thailand"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "Oceania",
                    "East Asia"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.mfa.go.th/main/",
                        "name" => "Ministry of Foreign Affairs, Kingdom of Thailand"
                    ],
                    [
                        "url" => "http://www.mfat.govt.nz./",
                        "name" => "New Zealand Ministry of Foreign Affairs and Trade"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "../rtadocs/31/TOA/English/1172766-v1-B2005-03_NZTS-200508.pdf",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "New Zealand"
                ],
                "countries_second" => [
                    "Thailand"
                ]
            ],
            [
                "id" => 364,
                "rta_id" => 34,
                "name" => "Turkey - Palestinian Authority",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "2004-07-20",
                "notification_date" => "2005-09-01",
                "entry_force_date" => "2005-06-01",
                "current_signatories" => [
                    "Palestine",
                    "Turkey"
                ],
                "original_signatories" => [
                    "Palestine",
                    "Turkey"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "Middle East",
                    "Europe"
                ],
                "wto_members" => false,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.economy.gov.tr/",
                        "name" => "Turkish Government"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://docsonline.wto.org/imrd/gen_redirectsearchdirect.asp?RN=0&amp;searchtype=browse&amp;query=@meta_Symbol&quot;WT/REG204/1&quot;&amp;language=1&amp;ct=DDFEnglish",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://docsonline.wto.org/imrd/gen_redirectsearchdirect.asp?RN=0&amp;searchtype=browse&amp;query=@meta_Symbol&quot;WT/REG204/1&quot;&amp;language=2&amp;ct=DDFFrench",
                        "name" => "F"
                    ],
                    [
                        "url" => "http://docsonline.wto.org/imrd/gen_redirectsearchdirect.asp?RN=0&amp;searchtype=browse&amp;query=@meta_Symbol&quot;WT/REG204/1&quot;&amp;language=3&amp;ct=DDFSpanish",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Palestine"
                ],
                "countries_second" => [
                    "Turkey"
                ]
            ],
            [
                "id" => 365,
                "rta_id" => 35,
                "name" => "Turkey - Tunisia",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "2004-11-25",
                "notification_date" => "2005-09-01",
                "entry_force_date" => "2005-07-01",
                "current_signatories" => [
                    "Tunisia",
                    "Turkey"
                ],
                "original_signatories" => [
                    "Tunisia",
                    "Turkey"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "Africa",
                    "Europe"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.economy.gov.tr/",
                        "name" => "Turkish Government"
                    ],
                    [
                        "url" => "http://www.cepex.nat.tn/",
                        "name" => "Ministère du Commerce et de l'Artisanat, République Tunisienne"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://docsonline.wto.org/imrd/gen_redirectsearchdirect.asp?RN=0&amp;searchtype=browse&amp;query=@meta_Symbol&quot;WT/REG203/1&quot;&amp;language=1&amp;ct=DDFEnglish",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://docsonline.wto.org/imrd/gen_redirectsearchdirect.asp?RN=0&amp;searchtype=browse&amp;query=@meta_Symbol&quot;WT/REG203/1&quot;&amp;language=2&amp;ct=DDFFrench",
                        "name" => "F"
                    ],
                    [
                        "url" => "http://docsonline.wto.org/imrd/gen_redirectsearchdirect.asp?RN=0&amp;searchtype=browse&amp;query=@meta_Symbol&quot;WT/REG203/1&quot;&amp;language=3&amp;ct=DDFSpanish",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Tunisia"
                ],
                "countries_second" => [
                    "Turkey"
                ]
            ],
            [
                "id" => 360,
                "rta_id" => 30,
                "name" => "United States - Morocco",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2004-06-15",
                "notification_date" => "2005-12-30",
                "entry_force_date" => "2006-01-01",
                "current_signatories" => [
                    "Morocco",
                    "USA"
                ],
                "original_signatories" => [
                    "Morocco",
                    "USA"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "Africa",
                    "North America"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.ustr.gov/",
                        "name" => "Office of the United States Trade Representative"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "https://ustr.gov/trade-agreements/free-trade-agreements/morocco-fta/final-text",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Morocco"
                ],
                "countries_second" => [
                    "USA"
                ]
            ],
            [
                "id" => 367,
                "rta_id" => 37,
                "name" => "EFTA - Tunisia",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "2004-12-17",
                "notification_date" => "2005-06-03",
                "entry_force_date" => "2005-06-01",
                "current_signatories" => [
                    "Iceland",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland",
                    "Tunisia"
                ],
                "original_signatories" => [
                    "Iceland",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland",
                    "Tunisia"
                ],
                "rta_composition" => [
                    "Bilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "Europe",
                    "Africa"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.efta.int",
                        "name" => "European Free Trade Association"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.efta.int/~/media/Documents/legal-texts/free-trade-relations/tunisia/EFTA-Tunisia%20Free%20Trade%20Agreement%20EN.pdf",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://www.efta.int/~/media/Documents/legal-texts/free-trade-relations/tunisia/EFTA-Tunisia%20Free%20Trade%20Agreement%20FR.pdf",
                        "name" => "F"
                    ]
                ],
                "countries_first" => [
                    "Tunisia"
                ],
                "countries_second" => [
                    "Iceland",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland"
                ]
            ],
            [
                "id" => 366,
                "rta_id" => 36,
                "name" => "Economic Community of West African States (ECOWAS)",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "Enabling Clause",
                "signature_date" => "1993-07-24",
                "notification_date" => "2005-07-06",
                "entry_force_date" => "1993-07-24",
                "current_signatories" => [
                    "Benin",
                    "Burkina Faso",
                    "Cabo Verde",
                    "Côte d'Ivoire",
                    "Ghana",
                    "Guinea",
                    "Guinea-Bissau",
                    "Liberia",
                    "Mali",
                    "Niger",
                    "Nigeria",
                    "Senegal",
                    "Sierra Leone",
                    "The Gambia",
                    "Togo"
                ],
                "original_signatories" => [
                    "Benin",
                    "Burkina Faso",
                    "Cabo Verde",
                    "Côte d'Ivoire",
                    "Ghana",
                    "Guinea",
                    "Guinea-Bissau",
                    "Liberia",
                    "Mali",
                    "Niger",
                    "Nigeria",
                    "Senegal",
                    "Sierra Leone",
                    "The Gambia",
                    "Togo"
                ],
                "rta_composition" => [
                    "Plurilateral"
                ],
                "region" => [
                    "Africa"
                ],
                "wto_members" => true,
                "type" => "Customs Union",
                "web_addresses" => [
                    [
                        "url" => "http://www.ecowas.int/",
                        "name" => "Economic Community of West African States"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.ecowas.int/wp-content/uploads/2015/01/Revised-treaty.pdf",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Cabo Verde"
                ],
                "countries_second" => [
                    "Benin",
                    "The Gambia",
                    "Ghana",
                    "Guinea",
                    "Côte d'Ivoire",
                    "Liberia",
                    "Mali",
                    "Niger",
                    "Nigeria",
                    "Guinea-Bissau",
                    "Senegal",
                    "Sierra Leone",
                    "Togo",
                    "Burkina Faso"
                ]
            ],
            [
                "id" => 368,
                "rta_id" => 38,
                "name" => "Japan - Mexico",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2004-09-17",
                "notification_date" => "2005-03-31",
                "entry_force_date" => "2005-04-01",
                "current_signatories" => [
                    "Japan",
                    "Mexico"
                ],
                "original_signatories" => [
                    "Japan",
                    "Mexico"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "East Asia",
                    "North America"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.mofa.go.jp/",
                        "name" => "Ministry of Foreign Affairs of Japan"
                    ],
                    [
                        "url" => "http://www.economia.gob.mx/",
                        "name" => "Secretaría de Economía, México"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.mofa.go.jp/region/latin/mexico/agreement/agreement.pdf",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Japan"
                ],
                "countries_second" => [
                    "Mexico"
                ]
            ],
            [
                "id" => 369,
                "rta_id" => 39,
                "name" => "Panama - El Salvador (Panama - Central America)",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2002-03-06",
                "notification_date" => "2005-02-24",
                "entry_force_date" => "2003-04-11",
                "current_signatories" => [
                    "El Salvador",
                    "Panama"
                ],
                "original_signatories" => [
                    "El Salvador",
                    "Panama"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "Central America"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.mici.gob.pa/",
                        "name" => "Ministerio de Comercio e Industrias, Panamá"
                    ],
                    [
                        "url" => "http://www.minec.gob.sv/",
                        "name" => "Ministerio de Economia, República de El Salvador"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.sice.oas.org/Trade/Capan/indice.asp",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "El Salvador"
                ],
                "countries_second" => [
                    "Panama"
                ]
            ],
            [
                "id" => 370,
                "rta_id" => 40,
                "name" => "Thailand - Australia",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2004-07-05",
                "notification_date" => "2004-12-27",
                "entry_force_date" => "2005-01-01",
                "current_signatories" => [
                    "Australia",
                    "Thailand"
                ],
                "original_signatories" => [
                    "Australia",
                    "Thailand"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "Oceania",
                    "East Asia"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.mfa.go.th/main/",
                        "name" => "Ministry of Foreign Affairs, Kingdom of Thailand"
                    ],
                    [
                        "url" => "http://www.dfat.gov.au/",
                        "name" => "Australian Government, Department of Foreign Affairs & Trade"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://dfat.gov.au/trade/agreements/in-force/tafta/fta-text-and-implementation/Pages/download.aspx",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Australia"
                ],
                "countries_second" => [
                    "Thailand"
                ]
            ],
            [
                "id" => 371,
                "rta_id" => 41,
                "name" => "United States - Australia",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2004-05-18",
                "notification_date" => "2004-12-22",
                "entry_force_date" => "2005-01-01",
                "current_signatories" => [
                    "Australia",
                    "USA"
                ],
                "original_signatories" => [
                    "Australia",
                    "USA"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "Oceania",
                    "North America"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.ustr.gov/",
                        "name" => "Office of the United States Trade Representative"
                    ],
                    [
                        "url" => "http://www.dfat.gov.au/",
                        "name" => "Australian Government, Department of Foreign Affairs & Trade"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "https://ustr.gov/trade-agreements/free-trade-agreements/australian-fta/final-text",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Australia"
                ],
                "countries_second" => [
                    "USA"
                ]
            ],
            [
                "id" => 373,
                "rta_id" => 43,
                "name" => "EFTA - Chile",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2003-06-26",
                "notification_date" => "2004-12-03",
                "entry_force_date" => "2004-12-01",
                "current_signatories" => [
                    "Chile",
                    "Iceland",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland"
                ],
                "original_signatories" => [
                    "Chile",
                    "Iceland",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland"
                ],
                "rta_composition" => [
                    "Bilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "South America",
                    "Europe"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.efta.int",
                        "name" => "European Free Trade Association"
                    ],
                    [
                        "url" => "http://www.direcon.gob.cl/",
                        "name" => "Dirección General de Relaciones Económicas Internacionales, Chile"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.efta.int/~/media/Documents/legal-texts/free-trade-relations/chile/EFTA-Chile%20Free%20Trade%20Agreement.pdf",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://www.direcon.gob.cl/detalle-de-acuerdos/?idacuerdo=6301",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Chile"
                ],
                "countries_second" => [
                    "Iceland",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland"
                ]
            ],
            [
                "id" => 376,
                "rta_id" => 46,
                "name" => "Asia Pacific Trade Agreement (APTA) - Accession of China",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "Enabling Clause",
                "signature_date" => "2001-04-12",
                "notification_date" => "2004-04-30",
                "entry_force_date" => "2002-01-01",
                "current_signatories" => [
                    "Bangladesh",
                    "China",
                    "India",
                    "North Korea",
                    "Laos",
                    "Sri Lanka"
                ],
                "original_signatories" => [
                    "Bangladesh",
                    "China",
                    "India",
                    "North Korea",
                    "Laos",
                    "Sri Lanka"
                ],
                "rta_composition" => [
                    "Plurilateral"
                ],
                "region" => [
                    "West Asia",
                    "East Asia"
                ],
                "wto_members" => true,
                "type" => "Partial Scope Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.mofcom.gov.cn/",
                        "name" => "Ministry of Commerce of the People's Republic of China"
                    ],
                    [
                        "url" => "http://www.unescap.org/tid/apta.asp",
                        "name" => "United Nations Economic and Social Commission for Asia and the Pacific"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://commerce.nic.in/trade/bangkok_agreement.pdf",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Bangladesh"
                ],
                "countries_second" => [
                    "Sri Lanka",
                    "China",
                    "India",
                    "North Korea",
                    "Laos"
                ]
            ],
            [
                "id" => 375,
                "rta_id" => 45,
                "name" => "Southern African Development Community (SADC)",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "1996-08-24",
                "notification_date" => "2004-08-02",
                "entry_force_date" => "2000-09-01",
                "current_signatories" => [
                    "Angola",
                    "Botswana",
                    "Swaziland",
                    "Lesotho",
                    "Malawi",
                    "Mauritius",
                    "Mozambique",
                    "Namibia",
                    "Seychelles",
                    "South Africa",
                    "Tanzania",
                    "Zambia",
                    "Zimbabwe"
                ],
                "original_signatories" => [
                    "Angola",
                    "Botswana",
                    "Swaziland",
                    "Lesotho",
                    "Malawi",
                    "Mauritius",
                    "Mozambique",
                    "Namibia",
                    "South Africa",
                    "Tanzania",
                    "Zambia",
                    "Zimbabwe"
                ],
                "rta_composition" => [
                    "Plurilateral"
                ],
                "region" => [
                    "Africa"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.sadc.int/",
                        "name" => "Southern African Development Community"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.sadc.int/documents-publications/sadc-treaty/",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Angola"
                ],
                "countries_second" => [
                    "Botswana",
                    "Lesotho",
                    "Malawi",
                    "Mauritius",
                    "Mozambique",
                    "Namibia",
                    "Seychelles",
                    "South Africa",
                    "Zimbabwe",
                    "Swaziland",
                    "Tanzania",
                    "Zambia"
                ]
            ],
            [
                "id" => 381,
                "rta_id" => 51,
                "name" => "Armenia - Ukraine",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "1994-10-07",
                "notification_date" => "2004-06-17",
                "entry_force_date" => "1996-12-18",
                "current_signatories" => [
                    "Armenia",
                    "Ukraine"
                ],
                "original_signatories" => [
                    "Armenia",
                    "Ukraine"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "Commonwealth of Independent States (CIS), including associate and former member States"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.me.gov.ua/?lang=en-GB",
                        "name" => "The Ministry of Economy of Ukraine"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://docsonline.wto.org/imrd/gen_redirectsearchdirect.asp?RN=0&amp;searchtype=browse&amp;query=@meta_Symbol&quot;WT/REG171/1&quot;&amp;language=1&amp;ct=DDFEnglish",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://docsonline.wto.org/imrd/gen_redirectsearchdirect.asp?RN=0&amp;searchtype=browse&amp;query=@meta_Symbol&quot;WT/REG171/1&quot;&amp;language=2&amp;ct=DDFFrench",
                        "name" => "F"
                    ],
                    [
                        "url" => "http://docsonline.wto.org/imrd/gen_redirectsearchdirect.asp?RN=0&amp;searchtype=browse&amp;query=@meta_Symbol&quot;WT/REG171/1&quot;&amp;language=3&amp;ct=DDFSpanish",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Armenia"
                ],
                "countries_second" => [
                    "Ukraine"
                ]
            ],
            [
                "id" => 385,
                "rta_id" => 55,
                "name" => "Chile - El Salvador (Chile - Central America)",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "1999-10-18",
                "notification_date" => "2004-01-29",
                "entry_force_date" => "2002-06-01",
                "current_signatories" => [
                    "Chile",
                    "El Salvador"
                ],
                "original_signatories" => [
                    "Chile",
                    "El Salvador"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "South America",
                    "Central America"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.direcon.gob.cl/",
                        "name" => "Dirección General de Relaciones Económicas Internacionales, Chile"
                    ],
                    [
                        "url" => "http://www.minec.gob.sv",
                        "name" => "Ministerio de Economía - Gobierno de El Salvador"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.direcon.gob.cl/detalle-de-acuerdos/?idacuerdo=6290",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Chile"
                ],
                "countries_second" => [
                    "El Salvador"
                ]
            ],
            [
                "id" => 383,
                "rta_id" => 53,
                "name" => "Korea, Republic of - Chile",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2003-02-01",
                "notification_date" => "2004-04-08",
                "entry_force_date" => "2004-04-01",
                "current_signatories" => [
                    "Chile",
                    "North Korea"
                ],
                "original_signatories" => [
                    "Chile",
                    "North Korea"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "South America",
                    "East Asia"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.mofa.go.kr/eng/index.do",
                        "name" => "Ministry of Foreign Affairs and Trade, Republic of Korea"
                    ],
                    [
                        "url" => "http://www.direcon.gob.cl/",
                        "name" => "Dirección General de Relaciones Económicas Internacionales, Chile"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.sice.oas.org/Trade/Chi-SKorea_e/ChiKoreaind_e.asp",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://www.direcon.gob.cl/detalle-de-acuerdos/?idacuerdo=6249",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Chile"
                ],
                "countries_second" => [
                    "North Korea"
                ]
            ],
            [
                "id" => 387,
                "rta_id" => 57,
                "name" => "China - Hong Kong, China",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2003-06-29",
                "notification_date" => "2003-12-27",
                "entry_force_date" => "2003-06-29",
                "current_signatories" => [
                    "China",
                    "Hong Kong"
                ],
                "original_signatories" => [
                    "China",
                    "Hong Kong"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "East Asia"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.mofcom.gov.cn/",
                        "name" => "Ministry of Commerce of the People's Republic of China"
                    ],
                    [
                        "url" => "http://www.tid.gov.hk/",
                        "name" => "Hong Kong Special Administrative Region of the People's Republic of China"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.tid.gov.hk/english/cepa/legaltext/cepa_legaltext.html",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "China"
                ],
                "countries_second" => [
                    "Hong Kong"
                ]
            ],
            [
                "id" => 386,
                "rta_id" => 56,
                "name" => "China - Macao, China",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2003-10-17",
                "notification_date" => "2003-12-27",
                "entry_force_date" => "2003-10-17",
                "current_signatories" => [
                    "China",
                    "Macau"
                ],
                "original_signatories" => [
                    "China",
                    "Macau"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "East Asia"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.mofcom.gov.cn/",
                        "name" => "Ministry of Commerce of the People's Republic of China"
                    ],
                    [
                        "url" => "http://www.economia.gov.mo",
                        "name" => "Macao Special Administrative Region Economic Services"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.cepa.gov.mo/cepaweb/front/eng/itemI_2.htm",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "China"
                ],
                "countries_second" => [
                    "Macau"
                ]
            ],
            [
                "id" => 390,
                "rta_id" => 60,
                "name" => "Singapore - Australia",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2003-02-17",
                "notification_date" => "2003-09-25",
                "entry_force_date" => "2003-07-28",
                "current_signatories" => [
                    "Australia",
                    "Singapore"
                ],
                "original_signatories" => [
                    "Australia",
                    "Singapore"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "Oceania",
                    "East Asia"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.iesingapore.gov.sg",
                        "name" => "International Enterprise Singapore"
                    ],
                    [
                        "url" => "http://www.dfat.gov.au/",
                        "name" => "Australian Government, Department of Foreign Affairs & Trade"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://dfat.gov.au/trade/agreements/in-force/safta/official-documents/Pages/default.aspx",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Australia"
                ],
                "countries_second" => [
                    "Singapore"
                ]
            ],
            [
                "id" => 391,
                "rta_id" => 61,
                "name" => "Turkey - Bosnia and Herzegovina",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "2002-07-03",
                "notification_date" => "2003-08-29",
                "entry_force_date" => "2003-07-01",
                "current_signatories" => [
                    "Bosnia and Herzegovina",
                    "Turkey"
                ],
                "original_signatories" => [
                    "Bosnia and Herzegovina",
                    "Turkey"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "Europe"
                ],
                "wto_members" => false,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.economy.gov.tr/",
                        "name" => "Turkish Government"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "../rtadocs/61/TOA/English/157-1.doc",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Bosnia and Herzegovina"
                ],
                "countries_second" => [
                    "Turkey"
                ]
            ],
            [
                "id" => 389,
                "rta_id" => 59,
                "name" => "United States - Chile",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2003-06-06",
                "notification_date" => "2003-12-16",
                "entry_force_date" => "2004-01-01",
                "current_signatories" => [
                    "Chile",
                    "USA"
                ],
                "original_signatories" => [
                    "Chile",
                    "USA"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "South America",
                    "North America"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.ustr.gov/",
                        "name" => "Office of the United States Trade Representative"
                    ],
                    [
                        "url" => "http://www.direcon.gob.cl/",
                        "name" => "Dirección General de Relaciones Económicas Internacionales, Chile"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "https://ustr.gov/trade-agreements/free-trade-agreements/chile-fta/final-text",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://www.direcon.gob.cl/detalle-de-acuerdos/?idacuerdo=6277",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Chile"
                ],
                "countries_second" => [
                    "USA"
                ]
            ],
            [
                "id" => 388,
                "rta_id" => 58,
                "name" => "United States - Singapore",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2003-05-06",
                "notification_date" => "2003-12-17",
                "entry_force_date" => "2004-01-01",
                "current_signatories" => [
                    "Singapore",
                    "USA"
                ],
                "original_signatories" => [
                    "Singapore",
                    "USA"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "East Asia",
                    "North America"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.ustr.gov/",
                        "name" => "Office of the United States Trade Representative"
                    ],
                    [
                        "url" => "http://www.iesingapore.gov.sg",
                        "name" => "International Enterprise Singapore"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.ustr.gov/trade-agreements/free-trade-agreements/singapore-fta/final-text",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Singapore"
                ],
                "countries_second" => [
                    "USA"
                ]
            ],
            [
                "id" => 393,
                "rta_id" => 63,
                "name" => "EU - Lebanon",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "2002-06-17",
                "notification_date" => "2003-05-26",
                "entry_force_date" => "2003-03-01",
                "current_signatories" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom",
                    "Lebanese Republic"
                ],
                "original_signatories" => [
                    "Lebanese Republic"
                ],
                "rta_composition" => [
                    "Bilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "Europe",
                    "Middle East"
                ],
                "wto_members" => false,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://ec.europa.eu/",
                        "name" => "European Commission"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/ALL/?uri=OJ:L:2006:143:TOC",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/ALL/?uri=OJ:L:2006:143:TOC",
                        "name" => "F"
                    ],
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/ALL/?uri=OJ:L:2006:143:TOC",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Lebanese Republic"
                ],
                "countries_second" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom"
                ]
            ],
            [
                "id" => 416,
                "rta_id" => 88,
                "name" => "EFTA - The former Yugoslav Republic of Macedonia",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "2000-06-19",
                "notification_date" => "2000-12-11",
                "entry_force_date" => "2002-05-01",
                "current_signatories" => [
                    "Iceland",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland",
                    "Macedonia"
                ],
                "original_signatories" => [
                    "Iceland",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland",
                    "Macedonia"
                ],
                "rta_composition" => [
                    "Bilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "Europe"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.efta.int",
                        "name" => "European Free Trade Association"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.efta.int/~/media/Documents/legal-texts/free-trade-relations/macedonia/EFTA-Macedonia%20Free%20Trade%20Agreement.pdf",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Macedonia"
                ],
                "countries_second" => [
                    "Iceland",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland"
                ]
            ],
            [
                "id" => 400,
                "rta_id" => 72,
                "name" => "India - Sri Lanka",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "Enabling Clause",
                "signature_date" => "1998-12-28",
                "notification_date" => "2002-06-17",
                "entry_force_date" => "2001-12-15",
                "current_signatories" => [
                    "India",
                    "Sri Lanka"
                ],
                "original_signatories" => [
                    "India",
                    "Sri Lanka"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "West Asia"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.commerce.nic.in/",
                        "name" => "Government of India, Ministry of Commerce & Industry"
                    ],
                    [
                        "url" => "http://www.doc.gov.lk/web/index.php",
                        "name" => "Department of Commerce, Sri Lanka"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://commerce.gov.in/international_nextDetail_WTO.aspx?LinkID=31&amp;idwto=45",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Sri Lanka"
                ],
                "countries_second" => [
                    "India"
                ]
            ],
            [
                "id" => 398,
                "rta_id" => 70,
                "name" => "Japan - Singapore",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2002-01-13",
                "notification_date" => "2002-11-08",
                "entry_force_date" => "2002-11-30",
                "current_signatories" => [
                    "Japan",
                    "Singapore"
                ],
                "original_signatories" => [
                    "Japan",
                    "Singapore"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "East Asia"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.mofa.go.jp",
                        "name" => "Ministry of Foreign Affairs of Japan"
                    ],
                    [
                        "url" => "http://www.iesingapore.gov.sg",
                        "name" => "International Enterprise Singapore"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.mofa.go.jp/region/asia-paci/singapore/jsepa-1.pdf",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Japan"
                ],
                "countries_second" => [
                    "Singapore"
                ]
            ],
            [
                "id" => 406,
                "rta_id" => 78,
                "name" => "New Zealand - Singapore",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2000-11-14",
                "notification_date" => "2001-09-04",
                "entry_force_date" => "2001-01-01",
                "current_signatories" => [
                    "New Zealand",
                    "Singapore"
                ],
                "original_signatories" => [
                    "New Zealand",
                    "Singapore"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "Oceania",
                    "East Asia"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.mfat.govt.nz/",
                        "name" => "New Zealand Ministry of Foreign Affairs and Trade"
                    ],
                    [
                        "url" => "http://www.iesingapore.gov.sg",
                        "name" => "International Enterprise Singapore"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "https://www.mfat.govt.nz/assets/FTAs-agreements-in-force/Singapore-FTA/NZ-Singapore-CEP-full-text.pdf",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "New Zealand"
                ],
                "countries_second" => [
                    "Singapore"
                ]
            ],
            [
                "id" => 399,
                "rta_id" => 71,
                "name" => "United States - Jordan",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2000-10-24",
                "notification_date" => "2002-01-15",
                "entry_force_date" => "2001-12-17",
                "current_signatories" => [
                    "Jordan",
                    "USA"
                ],
                "original_signatories" => [
                    "Jordan",
                    "USA"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "Middle East",
                    "North America"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.ustr.gov",
                        "name" => "Office of the United States Trade Representative"
                    ],
                    [
                        "url" => "http://www.mit.gov.jo",
                        "name" => "Jordan's Foreign Trade Policy Department, Ministry of Industry and Trade"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.ustr.gov/trade-agreements/free-trade-agreements/jordan-fta/final-text",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Jordan"
                ],
                "countries_second" => [
                    "USA"
                ]
            ],
            [
                "id" => 403,
                "rta_id" => 75,
                "name" => "EFTA - Jordan",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "2001-06-21",
                "notification_date" => "2002-01-17",
                "entry_force_date" => "2002-09-01",
                "current_signatories" => [
                    "Iceland",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland",
                    "Jordan"
                ],
                "original_signatories" => [
                    "Iceland",
                    "Jordan",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland"
                ],
                "rta_composition" => [
                    "Bilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "Europe",
                    "Middle East"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.efta.int",
                        "name" => "European Free Trade Association"
                    ],
                    [
                        "url" => "http://www.mit.gov.jo",
                        "name" => "Jordan's Foreign Trade Policy Department, Ministry of Industry and Trade"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.efta.int/~/media/Documents/legal-texts/free-trade-relations/Jordan/EFTA-Jordan%20Free%20Trade%20Agreement.pdf",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Jordan"
                ],
                "countries_second" => [
                    "Iceland",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland"
                ]
            ],
            [
                "id" => 401,
                "rta_id" => 73,
                "name" => "EU - Mexico",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "1997-12-08",
                "notification_date" => "2000-07-25",
                "entry_force_date" => "2000-07-01",
                "current_signatories" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom",
                    "Mexico"
                ],
                "original_signatories" => [
                    "Mexico"
                ],
                "rta_composition" => [
                    "Bilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "Europe",
                    "North America"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://ec.europa.eu/",
                        "name" => "European Commission"
                    ],
                    [
                        "url" => "http://www.economia.gob.mx",
                        "name" => "Secretaría de Economía, México"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.sice.oas.org/Trade/mex_eu/english/index_e.asp",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://www.sice.oas.org/Trade/MEX_EU/Spanish/index_s.asp",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Mexico"
                ],
                "countries_second" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom"
                ]
            ],
            [
                "id" => 411,
                "rta_id" => 83,
                "name" => "Georgia - Azerbaijan",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "1996-03-08",
                "notification_date" => "2001-02-08",
                "entry_force_date" => "1996-07-10",
                "current_signatories" => [
                    "Azerbaijan",
                    "Georgia"
                ],
                "original_signatories" => [
                    "Azerbaijan",
                    "Georgia"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "Commonwealth of Independent States (CIS), including associate and former member States"
                ],
                "wto_members" => false,
                "type" => "Free Trade Agreement",
                "web_addresses" => [],
                "agreement" => [
                    [
                        "url" => "http://docsonline.wto.org/imrd/gen_redirectsearchdirect.asp?RN=0&amp;searchtype=browse&amp;query=@meta_Symbol&quot;WT/REG120/1&quot;&amp;language=1&amp;ct=DDFEnglish",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://docsonline.wto.org/imrd/gen_redirectsearchdirect.asp?RN=0&amp;searchtype=browse&amp;query=@meta_Symbol&quot;WT/REG120/1&quot;&amp;language=2&amp;ct=DDFFrench",
                        "name" => "F"
                    ],
                    [
                        "url" => "http://docsonline.wto.org/imrd/gen_redirectsearchdirect.asp?RN=0&amp;searchtype=browse&amp;query=@meta_Symbol&quot;WT/REG120/1&quot;&amp;language=3&amp;ct=DDFSpanish",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Azerbaijan"
                ],
                "countries_second" => [
                    "Georgia"
                ]
            ],
            [
                "id" => 412,
                "rta_id" => 84,
                "name" => "Georgia - Kazakhstan",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "1997-11-11",
                "notification_date" => "2001-02-08",
                "entry_force_date" => "1999-07-16",
                "current_signatories" => [
                    "Georgia",
                    "Kazakhstan"
                ],
                "original_signatories" => [
                    "Georgia",
                    "Kazakhstan"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "Commonwealth of Independent States (CIS), including associate and former member States"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement",
                "web_addresses" => [],
                "agreement" => [
                    [
                        "url" => "http://docsonline.wto.org/imrd/gen_redirectsearchdirect.asp?RN=0&amp;searchtype=browse&amp;query=@meta_Symbol&quot;WT/REG123/1&quot;&amp;language=1&amp;ct=DDFEnglish",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://docsonline.wto.org/imrd/gen_redirectsearchdirect.asp?RN=0&amp;searchtype=browse&amp;query=@meta_Symbol&quot;WT/REG123/1&quot;&amp;language=2&amp;ct=DDFFrench",
                        "name" => "F"
                    ],
                    [
                        "url" => "http://docsonline.wto.org/imrd/gen_redirectsearchdirect.asp?RN=0&amp;searchtype=browse&amp;query=@meta_Symbol&quot;WT/REG123/1&quot;&amp;language=3&amp;ct=DDFSpanish",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Georgia"
                ],
                "countries_second" => [
                    "Kazakhstan"
                ]
            ],
            [
                "id" => 413,
                "rta_id" => 85,
                "name" => "Georgia - Russian Federation",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "1994-02-03",
                "notification_date" => "2001-02-08",
                "entry_force_date" => "1994-05-10",
                "current_signatories" => [
                    "Georgia",
                    "Russia"
                ],
                "original_signatories" => [
                    "Georgia",
                    "Russia"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "Commonwealth of Independent States (CIS), including associate and former member States"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.mfa.gov.ge/?lang_id=ENG",
                        "name" => "Ministry of Foreign Affairs, Georgia"
                    ],
                    [
                        "url" => "http://www.mid.ru/bdomp/brp_4.nsf/main_eng",
                        "name" => "Ministry of Foreign Affairs of the Russian Federation"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://docsonline.wto.org/imrd/gen_redirectsearchdirect.asp?RN=0&amp;searchtype=browse&amp;query=@meta_Symbol&quot;WT/REG118/1&quot;&amp;language=1&amp;ct=DDFEnglish",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://docsonline.wto.org/imrd/gen_redirectsearchdirect.asp?RN=0&amp;searchtype=browse&amp;query=@meta_Symbol&quot;WT/REG118/1&quot;&amp;language=2&amp;ct=DDFFrench",
                        "name" => "F"
                    ],
                    [
                        "url" => "http://docsonline.wto.org/imrd/gen_redirectsearchdirect.asp?RN=0&amp;searchtype=browse&amp;query=@meta_Symbol&quot;WT/REG118/1&quot;&amp;language=3&amp;ct=DDFSpanish",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Georgia"
                ],
                "countries_second" => [
                    "Russia"
                ]
            ],
            [
                "id" => 414,
                "rta_id" => 86,
                "name" => "Georgia - Turkmenistan",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "1996-03-20",
                "notification_date" => "2001-02-08",
                "entry_force_date" => "2000-01-01",
                "current_signatories" => [
                    "Georgia",
                    "Turkmenistan"
                ],
                "original_signatories" => [
                    "Georgia",
                    "Turkmenistan"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "Commonwealth of Independent States (CIS), including associate and former member States"
                ],
                "wto_members" => false,
                "type" => "Free Trade Agreement",
                "web_addresses" => [],
                "agreement" => [
                    [
                        "url" => "http://docsonline.wto.org/imrd/gen_redirectsearchdirect.asp?RN=0&amp;searchtype=browse&amp;query=@meta_Symbol&quot;WT/REG122/1&quot;&amp;language=1&amp;ct=DDFEnglish",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://docsonline.wto.org/imrd/gen_redirectsearchdirect.asp?RN=0&amp;searchtype=browse&amp;query=@meta_Symbol&quot;WT/REG122/1&quot;&amp;language=2&amp;ct=DDFFrench",
                        "name" => "F"
                    ],
                    [
                        "url" => "http://docsonline.wto.org/imrd/gen_redirectsearchdirect.asp?RN=0&amp;searchtype=browse&amp;query=@meta_Symbol&quot;WT/REG122/1&quot;&amp;language=3&amp;ct=DDFSpanish",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Georgia"
                ],
                "countries_second" => [
                    "Turkmenistan"
                ]
            ],
            [
                "id" => 415,
                "rta_id" => 87,
                "name" => "Georgia - Ukraine",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "1995-01-09",
                "notification_date" => "2001-02-08",
                "entry_force_date" => "1996-06-04",
                "current_signatories" => [
                    "Georgia",
                    "Ukraine"
                ],
                "original_signatories" => [
                    "Georgia",
                    "Ukraine"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "Commonwealth of Independent States (CIS), including associate and former member States"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.me.gov.ua/?lang=en-GB",
                        "name" => "The Ministry of Economy of Ukraine"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://docsonline.wto.org/imrd/gen_redirectsearchdirect.asp?RN=0&amp;searchtype=browse&amp;query=@meta_Symbol&quot;WT/REG121/1&quot;&amp;language=1&amp;ct=DDFEnglish",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://docsonline.wto.org/imrd/gen_redirectsearchdirect.asp?RN=0&amp;searchtype=browse&amp;query=@meta_Symbol&quot;WT/REG121/1&quot;&amp;language=2&amp;ct=DDFFrench",
                        "name" => "F"
                    ],
                    [
                        "url" => "http://docsonline.wto.org/imrd/gen_redirectsearchdirect.asp?RN=0&amp;searchtype=browse&amp;query=@meta_Symbol&quot;WT/REG121/1&quot;&amp;language=3&amp;ct=DDFSpanish",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Georgia"
                ],
                "countries_second" => [
                    "Ukraine"
                ]
            ],
            [
                "id" => 409,
                "rta_id" => 81,
                "name" => "Israel - Mexico",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "2000-04-10",
                "notification_date" => "2001-02-22",
                "entry_force_date" => "2000-07-01",
                "current_signatories" => [
                    "Israel",
                    "Mexico"
                ],
                "original_signatories" => [
                    "Israel",
                    "Mexico"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "Middle East",
                    "North America"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://economy.gov.il/English/Pages/default.aspx",
                        "name" => "Ministry of Industry, Trade & Labor, Israel"
                    ],
                    [
                        "url" => "http://www.economia.gob.mx/",
                        "name" => "Secretaría de Economía, México"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.sice.oas.org/Trade/meis_e/isr_mexind_e.asp",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://www.economia.gob.mx/comunidad-negocios/comercio-exterior/tlc-acuerdos/asia-pacifico",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Israel"
                ],
                "countries_second" => [
                    "Mexico"
                ]
            ],
            [
                "id" => 407,
                "rta_id" => 79,
                "name" => "EFTA - Mexico",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2000-11-27",
                "notification_date" => "2001-07-25",
                "entry_force_date" => "2001-07-01",
                "current_signatories" => [
                    "Iceland",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland",
                    "Mexico"
                ],
                "original_signatories" => [
                    "Iceland",
                    "Liechtenstein",
                    "Mexico",
                    "Norway",
                    "Switzerland"
                ],
                "rta_composition" => [
                    "Bilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "Europe",
                    "North America"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.efta.int",
                        "name" => "European Free Trade Association"
                    ],
                    [
                        "url" => "http://www.economia.gob.mx/",
                        "name" => "Secretaría de Economía de México"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.efta.int/~/media/Documents/legal-texts/free-trade-relations/mexico/EFTA-Mexico%20Free%20Trade%20Agreement.pdf",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://www.sice.oas.org/Trade/mexefta/spanish/mxeftas1.asp",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Mexico"
                ],
                "countries_second" => [
                    "Iceland",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland"
                ]
            ],
            [
                "id" => 417,
                "rta_id" => 89,
                "name" => "Turkey - The former Yugoslav Republic of Macedonia",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "1999-09-07",
                "notification_date" => "2001-01-05",
                "entry_force_date" => "2000-09-01",
                "current_signatories" => [
                    "Macedonia",
                    "Turkey"
                ],
                "original_signatories" => [
                    "Macedonia",
                    "Turkey"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "Europe"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.economy.gov.tr/",
                        "name" => "Turkish Government"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.ekonomi.gov.tr/portal/content/conn/UCM/path/Contribution%20Folders/web_en/Trade%20Agreements/Free%20Trade%20Agreements/Macedonia/ekler/2.%20Turkey-Macedonia%20FTA_Main%20Text.pdf?lve",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Turkey"
                ],
                "countries_second" => [
                    "Macedonia"
                ]
            ],
            [
                "id" => 419,
                "rta_id" => 91,
                "name" => "EU - South Africa",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "1999-10-11",
                "notification_date" => "2000-11-02",
                "entry_force_date" => "2000-01-01",
                "current_signatories" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom",
                    "South Africa"
                ],
                "original_signatories" => [
                    "South Africa"
                ],
                "rta_composition" => [
                    "Bilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "Europe",
                    "Africa"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://ec.europa.eu/",
                        "name" => "European Commission"
                    ],
                    [
                        "url" => "http://www.dfa.gov.za/",
                        "name" => "Department of Foreign Affairs, Republic of South Africa"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://eur-lex.europa.eu/resource.html?uri=cellar:df28bbd2-29f1-4cea-86ab-81d81c47903b.0004.02/DOC_3&amp;format=PDF",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "South Africa"
                ],
                "countries_second" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom"
                ]
            ],
            [
                "id" => 424,
                "rta_id" => 96,
                "name" => "EFTA - Morocco",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "1997-06-19",
                "notification_date" => "2000-01-20",
                "entry_force_date" => "1999-12-01",
                "current_signatories" => [
                    "Iceland",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland",
                    "Morocco"
                ],
                "original_signatories" => [
                    "Iceland",
                    "Liechtenstein",
                    "Morocco",
                    "Norway",
                    "Switzerland"
                ],
                "rta_composition" => [
                    "Bilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "Europe",
                    "Africa"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.efta.int",
                        "name" => "European Free Trade Association"
                    ],
                    [
                        "url" => "http://www.mce.gov.ma/Home.asp",
                        "name" => "Ministère du Commerce Extérieur, Royaume du Maroc"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.efta.int/~/media/Documents/legal-texts/free-trade-relations/morocco/EFTA-Morocco%20Free%20Trade%20Agreement%20EN.pdf",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://www.efta.int/~/media/Documents/legal-texts/free-trade-relations/morocco/EFTA-Morocco%20Free%20Trade%20Agreement%20FR.pdf",
                        "name" => "F"
                    ]
                ],
                "countries_first" => [
                    "Morocco"
                ],
                "countries_second" => [
                    "Iceland",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland"
                ]
            ],
            [
                "id" => 422,
                "rta_id" => 94,
                "name" => "East African Community (EAC)",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "Enabling Clause & GATS Art. V",
                "signature_date" => "1999-11-30",
                "notification_date" => "2000-10-09",
                "entry_force_date" => "2000-07-07",
                "current_signatories" => [
                    "Burundi",
                    "Kenya",
                    "Rwanda",
                    "Tanzania",
                    "Uganda"
                ],
                "original_signatories" => [
                    "Kenya",
                    "Tanzania",
                    "Uganda"
                ],
                "rta_composition" => [
                    "Plurilateral"
                ],
                "region" => [
                    "Africa"
                ],
                "wto_members" => true,
                "type" => "Customs Union & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.eac.int",
                        "name" => "East African Community"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "../rtadocs/94/TOA/English/EAC%20TREATY.pdf",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Burundi"
                ],
                "countries_second" => [
                    "Kenya",
                    "Rwanda",
                    "Uganda",
                    "Tanzania"
                ]
            ],
            [
                "id" => 425,
                "rta_id" => 97,
                "name" => "West African Economic and Monetary Union (WAEMU)",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "Enabling Clause",
                "signature_date" => "1994-01-10",
                "notification_date" => "1999-10-27",
                "entry_force_date" => "2000-01-01",
                "current_signatories" => [
                    "Benin",
                    "Burkina Faso",
                    "Côte d'Ivoire",
                    "Mali",
                    "Niger",
                    "Senegal",
                    "Togo"
                ],
                "original_signatories" => [
                    "Benin",
                    "Burkina Faso",
                    "Côte d'Ivoire",
                    "Mali",
                    "Niger",
                    "Senegal",
                    "Togo"
                ],
                "rta_composition" => [
                    "Plurilateral"
                ],
                "region" => [
                    "Africa"
                ],
                "wto_members" => true,
                "type" => "Customs Union",
                "web_addresses" => [
                    [
                        "url" => "http://www.uemoa.int/Pages/Home.aspx#",
                        "name" => "L'Union Economique et Monétaire ouest africaine"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.uemoa.int/fr/le-traite-modifie",
                        "name" => "F"
                    ]
                ],
                "countries_first" => [
                    "Benin"
                ],
                "countries_second" => [
                    "Côte d'Ivoire",
                    "Mali",
                    "Niger",
                    "Senegal",
                    "Togo",
                    "Burkina Faso"
                ]
            ],
            [
                "id" => 430,
                "rta_id" => 102,
                "name" => "Kyrgyz Republic - Moldova, Republic of",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "1995-05-16",
                "notification_date" => "1999-06-15",
                "entry_force_date" => "1996-11-21",
                "current_signatories" => [
                    "Kyrgyzstan",
                    "Moldova"
                ],
                "original_signatories" => [
                    "Kyrgyzstan",
                    "Moldova"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "Commonwealth of Independent States (CIS), including associate and former member States"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://mec.gov.md/",
                        "name" => "Ministerul Economiei si Comertului al Republicii Moldova"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://docsonline.wto.org/imrd/gen_redirectsearchdirect.asp?RN=0&amp;searchtype=browse&amp;query=@meta_Symbol&quot;WT/REG76/1&quot;&amp;language=1&amp;ct=DDFEnglish",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://docsonline.wto.org/imrd/gen_redirectsearchdirect.asp?RN=0&amp;searchtype=browse&amp;query=@meta_Symbol&quot;WT/REG76/1&quot;&amp;language=2&amp;ct=DDFFrench",
                        "name" => "F"
                    ],
                    [
                        "url" => "http://docsonline.wto.org/imrd/gen_redirectsearchdirect.asp?RN=0&amp;searchtype=browse&amp;query=@meta_Symbol&quot;WT/REG76/1&quot;&amp;language=3&amp;ct=DDFSpanish",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Kyrgyzstan"
                ],
                "countries_second" => [
                    "Moldova"
                ]
            ],
            [
                "id" => 432,
                "rta_id" => 104,
                "name" => "Kyrgyz Republic - Ukraine",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "1995-05-26",
                "notification_date" => "1999-06-15",
                "entry_force_date" => "1998-01-19",
                "current_signatories" => [
                    "Kyrgyzstan",
                    "Ukraine"
                ],
                "original_signatories" => [
                    "Kyrgyzstan",
                    "Ukraine"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "Commonwealth of Independent States (CIS), including associate and former member States"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.me.gov.ua/?lang=en-GB",
                        "name" => "The Ministry of Economy of Ukraine"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://docsonline.wto.org/imrd/gen_redirectsearchdirect.asp?RN=0&amp;searchtype=browse&amp;query=@meta_Symbol&quot;WT/REG74/1&quot;&amp;language=1&amp;ct=DDFEnglish",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://docsonline.wto.org/imrd/gen_redirectsearchdirect.asp?RN=0&amp;searchtype=browse&amp;query=@meta_Symbol&quot;WT/REG74/1&quot;&amp;language=2&amp;ct=DDFFrench",
                        "name" => "F"
                    ],
                    [
                        "url" => "http://docsonline.wto.org/imrd/gen_redirectsearchdirect.asp?RN=0&amp;searchtype=browse&amp;query=@meta_Symbol&quot;WT/REG74/1&quot;&amp;language=3&amp;ct=DDFSpanish",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Kyrgyzstan"
                ],
                "countries_second" => [
                    "Ukraine"
                ]
            ],
            [
                "id" => 433,
                "rta_id" => 105,
                "name" => "Kyrgyz Republic - Uzbekistan",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "1996-12-24",
                "notification_date" => "1999-06-15",
                "entry_force_date" => "1998-03-20",
                "current_signatories" => [
                    "Kyrgyzstan",
                    "Uzbekistan"
                ],
                "original_signatories" => [
                    "Kyrgyzstan",
                    "Uzbekistan"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "Commonwealth of Independent States (CIS), including associate and former member States"
                ],
                "wto_members" => false,
                "type" => "Free Trade Agreement",
                "web_addresses" => [],
                "agreement" => [
                    [
                        "url" => "http://docsonline.wto.org/imrd/gen_redirectsearchdirect.asp?RN=0&amp;searchtype=browse&amp;query=@meta_Symbol&quot;WT/REG75/1&quot;&amp;language=1&amp;ct=DDFEnglish",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://docsonline.wto.org/imrd/gen_redirectsearchdirect.asp?RN=0&amp;searchtype=browse&amp;query=@meta_Symbol&quot;WT/REG75/1&quot;&amp;language=2&amp;ct=DDFFrench",
                        "name" => "F"
                    ],
                    [
                        "url" => "http://docsonline.wto.org/imrd/gen_redirectsearchdirect.asp?RN=0&amp;searchtype=browse&amp;query=@meta_Symbol&quot;WT/REG75/1&quot;&amp;language=3&amp;ct=DDFSpanish",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Kyrgyzstan"
                ],
                "countries_second" => [
                    "Uzbekistan"
                ]
            ],
            [
                "id" => 429,
                "rta_id" => 101,
                "name" => "EFTA - Palestinian Authority",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "1998-11-30",
                "notification_date" => "1999-07-23",
                "entry_force_date" => "1999-07-01",
                "current_signatories" => [
                    "Iceland",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland",
                    "Palestine"
                ],
                "original_signatories" => [
                    "Iceland",
                    "Liechtenstein",
                    "Norway",
                    "Palestine",
                    "Switzerland"
                ],
                "rta_composition" => [
                    "Bilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "Europe",
                    "Middle East"
                ],
                "wto_members" => false,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.efta.int",
                        "name" => "European Free Trade Association"
                    ],
                    [
                        "url" => "http://www.paltrade.org/",
                        "name" => "Palestine Trade Center"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.efta.int/~/media/Documents/legal-texts/free-trade-relations/palestinian-authority/EFTA-Palestinian%20Authority%20Free%20Trade%20Agreement.pdf",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Palestine"
                ],
                "countries_second" => [
                    "Iceland",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland"
                ]
            ],
            [
                "id" => 426,
                "rta_id" => 98,
                "name" => "Melanesian Spearhead Group (MSG)",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "Enabling Clause",
                "signature_date" => "1993-07-22",
                "notification_date" => "1999-08-03",
                "entry_force_date" => "1994-01-01",
                "current_signatories" => [
                    "Fiji",
                    "Papua New Guinea",
                    "Solomon Islands",
                    "Vanuatu"
                ],
                "original_signatories" => [
                    "Fiji",
                    "Papua New Guinea",
                    "Solomon Islands",
                    "Vanuatu"
                ],
                "rta_composition" => [
                    "Plurilateral"
                ],
                "region" => [
                    "Oceania"
                ],
                "wto_members" => true,
                "type" => "Partial Scope Agreement",
                "web_addresses" => [],
                "agreement" => [
                    [
                        "url" => "http://docsonline.wto.org/imrd/gen_redirectsearchdirect.asp?RN=0&amp;searchtype=browse&amp;query=@meta_Symbol&quot;WT/COMTD/21&quot;&amp;language=1&amp;ct=DDFEnglish",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://docsonline.wto.org/imrd/gen_redirectsearchdirect.asp?RN=0&amp;searchtype=browse&amp;query=@meta_Symbol&quot;WT/COMTD/21&quot;&amp;language=2&amp;ct=DDFFrench",
                        "name" => "F"
                    ],
                    [
                        "url" => "http://docsonline.wto.org/imrd/gen_redirectsearchdirect.asp?RN=0&amp;searchtype=browse&amp;query=@meta_Symbol&quot;WT/COMTD/21&quot;&amp;language=3&amp;ct=DDFSpanish",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Solomon Islands"
                ],
                "countries_second" => [
                    "Fiji",
                    "Vanuatu",
                    "Papua New Guinea"
                ]
            ],
            [
                "id" => 480,
                "rta_id" => 153,
                "name" => "Pakistan - China",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2006-11-24",
                "notification_date" => "2008-01-18",
                "entry_force_date" => "2007-07-01",
                "current_signatories" => [
                    "China",
                    "Pakistan"
                ],
                "original_signatories" => [
                    "China",
                    "Pakistan"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "East Asia",
                    "West Asia"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.commerce.gov.pk/",
                        "name" => "Government of Pakistan, Ministry of Commerce"
                    ],
                    [
                        "url" => "http://www.mofcom.gov.cn/",
                        "name" => "Ministry of Commerce of the People's Republic of China"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://fta.mofcom.gov.cn/topic/enpakistan.shtml",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "China"
                ],
                "countries_second" => [
                    "Pakistan"
                ]
            ],
            [
                "id" => 436,
                "rta_id" => 108,
                "name" => "Turkey - Israel",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "1996-03-14",
                "notification_date" => "1998-04-16",
                "entry_force_date" => "1997-05-01",
                "current_signatories" => [
                    "Israel",
                    "Turkey"
                ],
                "original_signatories" => [
                    "Israel",
                    "Turkey"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "Middle East",
                    "Europe"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.economy.gov.tr/",
                        "name" => "Turkish Government"
                    ],
                    [
                        "url" => "http://economy.gov.il/English/Pages/default.aspx",
                        "name" => "Ministry of Industry, Trade & Labor, Israel"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.ekonomi.gov.tr/portal/content/conn/UCM/path/Contribution%20Folders/web_en/Trade%20Agreements/Free%20Trade%20Agreements/Israel/ekler/2.%20Turkey-Israel%20Agreement.pdf?lve",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Israel"
                ],
                "countries_second" => [
                    "Turkey"
                ]
            ],
            [
                "id" => 440,
                "rta_id" => 112,
                "name" => "EU - Faroe Islands",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "1996-12-06",
                "notification_date" => "1997-02-17",
                "entry_force_date" => "1997-01-01",
                "current_signatories" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom",
                    "Faeroe Islands"
                ],
                "original_signatories" => [
                    "Faeroe Islands"
                ],
                "rta_composition" => [
                    "Bilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "Europe"
                ],
                "wto_members" => false,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://ec.europa.eu/",
                        "name" => "European Commission"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1398412647857&amp;uri=CELEX:21997A0222(01)",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1398412647857&amp;uri=CELEX:21997A0222(01)",
                        "name" => "F"
                    ],
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1398412647857&amp;uri=CELEX:21997A0222(01)",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Faeroe Islands"
                ],
                "countries_second" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom"
                ]
            ],
            [
                "id" => 444,
                "rta_id" => 116,
                "name" => "Faroe Islands - Switzerland",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "1994-01-12",
                "notification_date" => "1996-02-12",
                "entry_force_date" => "1995-03-01",
                "current_signatories" => [
                    "Faeroe Islands",
                    "Switzerland"
                ],
                "original_signatories" => [
                    "Faeroe Islands",
                    "Switzerland"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "Europe"
                ],
                "wto_members" => false,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.admin.ch/",
                        "name" => "Confédération suisse"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://docsonline.wto.org/imrd/gen_redirectsearchdirect.asp?RN=0&amp;searchtype=browse&amp;query=@meta_Symbol&quot;WT/REG24/1&quot;&amp;language=1&amp;ct=DDFEnglish",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://docsonline.wto.org/imrd/gen_redirectsearchdirect.asp?RN=0&amp;searchtype=browse&amp;query=@meta_Symbol&quot;WT/REG24/1&quot;&amp;language=2&amp;ct=DDFFrench",
                        "name" => "F"
                    ],
                    [
                        "url" => "http://docsonline.wto.org/imrd/gen_redirectsearchdirect.asp?RN=0&amp;searchtype=browse&amp;query=@meta_Symbol&quot;WT/REG24/1&quot;&amp;language=3&amp;ct=DDFSpanish",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Faeroe Islands"
                ],
                "countries_second" => [
                    "Switzerland"
                ]
            ],
            [
                "id" => 442,
                "rta_id" => 114,
                "name" => "European Economic Area (EEA)",
                "coverage" => "Services",
                "status" => "In Force",
                "notification_under" => "GATS Art. V",
                "signature_date" => "1992-05-02",
                "notification_date" => "1996-09-13",
                "entry_force_date" => "1994-01-01",
                "current_signatories" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom",
                    "Iceland",
                    "Liechtenstein",
                    "Norway"
                ],
                "original_signatories" => [
                    "Iceland",
                    "Liechtenstein",
                    "Norway"
                ],
                "rta_composition" => [
                    "Plurilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "Europe"
                ],
                "wto_members" => true,
                "type" => "Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.efta.int",
                        "name" => "European Free Trade Association"
                    ],
                    [
                        "url" => "http://www.europa.eu",
                        "name" => "European Commission"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.efta.int/media/documents/legal-texts/eea/the-eea-agreement/Main%20Text%20of%20the%20Agreement/EEAagreement.pdf",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Iceland"
                ],
                "countries_second" => [
                    "Liechtenstein",
                    "Norway",
                    "European Union",
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom"
                ]
            ],
            [
                "id" => 449,
                "rta_id" => 122,
                "name" => "North American Free Trade Agreement (NAFTA)",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "1992-12-17",
                "notification_date" => "1993-01-29",
                "entry_force_date" => "1994-01-01",
                "current_signatories" => [
                    "Canada",
                    "Mexico",
                    "USA"
                ],
                "original_signatories" => [
                    "Canada",
                    "Mexico",
                    "USA"
                ],
                "rta_composition" => [
                    "Plurilateral"
                ],
                "region" => [
                    "North America"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.nafta-sec-alena.org/",
                        "name" => "North American Free Trade Agreement Secretariat"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "https://www.nafta-sec-alena.org/Default.aspx?tabid=97&amp;language=en-US",
                        "name" => "E"
                    ],
                    [
                        "url" => "https://www.nafta-sec-alena.org/Default.aspx?tabid=141&amp;language=fr-CA",
                        "name" => "F"
                    ],
                    [
                        "url" => "https://www.nafta-sec-alena.org/Default.aspx?tabid=184&amp;language=es-MX",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Canada"
                ],
                "countries_second" => [
                    "Mexico",
                    "USA"
                ]
            ],
            [
                "id" => 458,
                "rta_id" => 131,
                "name" => "Lao People's Democratic Republic - Thailand",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "Enabling Clause",
                "signature_date" => "1991-06-20",
                "notification_date" => "1991-11-26",
                "entry_force_date" => "1991-06-20",
                "current_signatories" => [
                    "Laos",
                    "Thailand"
                ],
                "original_signatories" => [
                    "Laos",
                    "Thailand"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "East Asia"
                ],
                "wto_members" => true,
                "type" => "Partial Scope Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.mfa.go.th/main/",
                        "name" => "Ministry of Foreign Affairs, Kingdom of Thailand"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://sul-derivatives.stanford.edu/derivative?CSNID=91600059&amp;mediaType=application/pdf",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Laos"
                ],
                "countries_second" => [
                    "Thailand"
                ]
            ],
            [
                "id" => 460,
                "rta_id" => 133,
                "name" => "United States - Israel",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "1985-04-22",
                "notification_date" => "1985-09-13",
                "entry_force_date" => "1985-08-19",
                "current_signatories" => [
                    "Israel",
                    "USA"
                ],
                "original_signatories" => [
                    "Israel",
                    "USA"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "Middle East",
                    "North America"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.ustr.gov/",
                        "name" => "Office of the United States Trade Representative"
                    ],
                    [
                        "url" => "http://economy.gov.il/English/Pages/default.aspx",
                        "name" => "Ministry of Industry, Trade & Labor, Israel"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "https://ustr.gov/sites/default/files/files/agreements/FTA/israel/Israel%20FTA.pdf",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Israel"
                ],
                "countries_second" => [
                    "USA"
                ]
            ],
            [
                "id" => 456,
                "rta_id" => 129,
                "name" => "EFTA - Turkey",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "1991-12-10",
                "notification_date" => "1992-03-06",
                "entry_force_date" => "1992-04-01",
                "current_signatories" => [
                    "Iceland",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland",
                    "Turkey"
                ],
                "original_signatories" => [
                    "Iceland",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland",
                    "Turkey"
                ],
                "rta_composition" => [
                    "Bilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "Europe"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.efta.int",
                        "name" => "European Free Trade Association"
                    ],
                    [
                        "url" => "http://www.economy.gov.tr/",
                        "name" => "Turkish Government"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.efta.int/~/media/Documents/legal-texts/free-trade-relations/turkey/EFTA-Turkey%20Free%20Trade%20Agreement.pdf",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Turkey"
                ],
                "countries_second" => [
                    "Iceland",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland"
                ]
            ],
            [
                "id" => 459,
                "rta_id" => 132,
                "name" => "EC (12) Enlargement",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "1985-06-12",
                "notification_date" => "1985-12-11",
                "entry_force_date" => "1986-01-01",
                "current_signatories" => [
                    "Belgium",
                    "Denmark",
                    "France",
                    "Germany",
                    "Greece",
                    "Ireland",
                    "Italy",
                    "Luxembourg",
                    "Netherlands",
                    "Portugal",
                    "Spain",
                    "United Kingdom"
                ],
                "original_signatories" => [
                    "Belgium",
                    "Denmark",
                    "France",
                    "Germany",
                    "Greece",
                    "Ireland",
                    "Italy",
                    "Luxembourg",
                    "Netherlands",
                    "Portugal",
                    "Spain",
                    "United Kingdom"
                ],
                "rta_composition" => [
                    "Plurilateral"
                ],
                "region" => [
                    "Europe"
                ],
                "wto_members" => true,
                "type" => "Customs Union",
                "web_addresses" => [
                    [
                        "url" => "http://ec.europa.eu/",
                        "name" => "European Commission"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?uri=OJ:L:1985:302:TOC",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Belgium"
                ],
                "countries_second" => [
                    "Denmark",
                    "France",
                    "Germany",
                    "Greece",
                    "Ireland",
                    "Italy",
                    "Luxembourg",
                    "Netherlands",
                    "Portugal",
                    "Spain",
                    "United Kingdom"
                ]
            ],
            [
                "id" => 455,
                "rta_id" => 128,
                "name" => "Economic Cooperation Organization (ECO)",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "Enabling Clause",
                "signature_date" => "1992-02-17",
                "notification_date" => "1992-07-10",
                "entry_force_date" => "1992-02-17",
                "current_signatories" => [
                    "Iran",
                    "Pakistan",
                    "Turkey"
                ],
                "original_signatories" => [
                    "Iran",
                    "Pakistan",
                    "Turkey"
                ],
                "rta_composition" => [
                    "Plurilateral"
                ],
                "region" => [
                    "Middle East",
                    "West Asia",
                    "Europe"
                ],
                "wto_members" => false,
                "type" => "Partial Scope Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.eco.int/",
                        "name" => "Economic Cooperation Organisation"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "https://wits.worldbank.org/GPTAD/PDF/archive/ECO.pdf",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Iran"
                ],
                "countries_second" => [
                    "Pakistan",
                    "Turkey"
                ]
            ],
            [
                "id" => 451,
                "rta_id" => 124,
                "name" => "South Asian Preferential Trade Arrangement (SAPTA)",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "Enabling Clause",
                "signature_date" => "1993-04-11",
                "notification_date" => "1997-04-21",
                "entry_force_date" => "1995-12-07",
                "current_signatories" => [
                    "Bangladesh",
                    "Bhutan",
                    "India",
                    "Maldives",
                    "Nepal",
                    "Pakistan",
                    "Sri Lanka"
                ],
                "original_signatories" => [
                    "Bangladesh",
                    "Bhutan",
                    "India",
                    "Maldives",
                    "Nepal",
                    "Pakistan",
                    "Sri Lanka"
                ],
                "rta_composition" => [
                    "Plurilateral"
                ],
                "region" => [
                    "West Asia"
                ],
                "wto_members" => false,
                "type" => "Partial Scope Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.saarc-sec.org/",
                        "name" => "South Asian Association for Regional Cooperation"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.commerce.nic.in/DOC/writereaddata/trade/Agreement_SAARC_SAPTA.pdf",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Bangladesh"
                ],
                "countries_second" => [
                    "Bhutan",
                    "Sri Lanka",
                    "India",
                    "Maldives",
                    "Nepal",
                    "Pakistan"
                ]
            ],
            [
                "id" => 457,
                "rta_id" => 130,
                "name" => "Southern Common Market (MERCOSUR)",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "Enabling Clause & GATS Art. V",
                "signature_date" => "1991-03-26",
                "notification_date" => "1991-02-17",
                "entry_force_date" => "1991-11-29",
                "current_signatories" => [
                    "Argentina",
                    "Brazil",
                    "Paraguay",
                    "Uruguay"
                ],
                "original_signatories" => [
                    "Argentina",
                    "Brazil",
                    "Paraguay",
                    "Uruguay"
                ],
                "rta_composition" => [
                    "Plurilateral"
                ],
                "region" => [
                    "South America"
                ],
                "wto_members" => true,
                "type" => "Customs Union & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.mercosur.int/",
                        "name" => "Southern Common Market"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.sice.oas.org/Mercosur/instmt_e.asp",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://www.sice.oas.org/Mercosur/instmt_s.asp",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Argentina"
                ],
                "countries_second" => [
                    "Brazil",
                    "Paraguay",
                    "Uruguay"
                ]
            ],
            [
                "id" => 452,
                "rta_id" => 125,
                "name" => "EFTA - Israel",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "1992-09-17",
                "notification_date" => "1992-11-30",
                "entry_force_date" => "1993-01-01",
                "current_signatories" => [
                    "Iceland",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland",
                    "Israel"
                ],
                "original_signatories" => [
                    "Iceland",
                    "Israel",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland"
                ],
                "rta_composition" => [
                    "Bilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "Europe",
                    "Middle East"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.efta.int",
                        "name" => "European Free Trade Association"
                    ],
                    [
                        "url" => "http://economy.gov.il/English/Pages/default.aspx",
                        "name" => "Ministry of Industry, Trade & Labor, Israel"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.efta.int/~/media/Documents/legal-texts/free-trade-relations/israel/EFTA-Israel%20Free%20Trade%20Agreement.pdf",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Israel"
                ],
                "countries_second" => [
                    "Iceland",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland"
                ]
            ],
            [
                "id" => 468,
                "rta_id" => 141,
                "name" => "Caribbean Community and Common Market (CARICOM)",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "1973-07-04",
                "notification_date" => "1974-10-14",
                "entry_force_date" => "1973-08-01",
                "current_signatories" => [
                    "Antigua and Barbuda",
                    "Bahamas",
                    "Barbados",
                    "Belize",
                    "Dominica",
                    "Grenada",
                    "Guyana",
                    "Haiti",
                    "Jamaica",
                    "Montserrat",
                    "Saint Kitts and Nevis",
                    "Saint Lucia",
                    "Saint Vincent and the Grenadines",
                    "Suriname",
                    "Trinidad and Tobago"
                ],
                "original_signatories" => [
                    "Antigua and Barbuda",
                    "Barbados",
                    "Belize",
                    "Dominica",
                    "Grenada",
                    "Guyana",
                    "Jamaica",
                    "Montserrat",
                    "Saint Kitts and Nevis",
                    "Saint Lucia",
                    "Saint Vincent and the Grenadines",
                    "Trinidad and Tobago"
                ],
                "rta_composition" => [
                    "Plurilateral"
                ],
                "region" => [
                    "Caribbean",
                    "Central America",
                    "South America"
                ],
                "wto_members" => false,
                "type" => "Customs Union & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.caricom.org/",
                        "name" => "Caribbean Community and Common Market"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.sice.oas.org/CARICOM/instmt_e.asp",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Antigua and Barbuda"
                ],
                "countries_second" => [
                    "Bahamas",
                    "Barbados",
                    "Belize",
                    "Dominica",
                    "Grenada",
                    "Guyana",
                    "Haiti",
                    "Jamaica",
                    "Montserrat",
                    "Saint Kitts and Nevis",
                    "Saint Lucia",
                    "Saint Vincent and the Grenadines",
                    "Suriname",
                    "Trinidad and Tobago"
                ]
            ],
            [
                "id" => 462,
                "rta_id" => 135,
                "name" => "Latin American Integration Association (LAIA)",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "Enabling Clause",
                "signature_date" => "1980-08-12",
                "notification_date" => "1982-07-01",
                "entry_force_date" => "1981-03-18",
                "current_signatories" => [
                    "Argentina",
                    "Bolivia",
                    "Brazil",
                    "Chile",
                    "Colombia",
                    "Cuba",
                    "Ecuador",
                    "Mexico",
                    "Paraguay",
                    "Peru",
                    "Uruguay",
                    "Venezuela"
                ],
                "original_signatories" => [
                    "Argentina",
                    "Bolivia",
                    "Brazil",
                    "Chile",
                    "Colombia",
                    "Ecuador",
                    "Mexico",
                    "Paraguay",
                    "Peru",
                    "Uruguay",
                    "Venezuela"
                ],
                "rta_composition" => [
                    "Plurilateral"
                ],
                "region" => [
                    "South America",
                    "Caribbean",
                    "North America"
                ],
                "wto_members" => true,
                "type" => "Partial Scope Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.aladi.org/",
                        "name" => "Asociación Latino Americana de Integración"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.aladi.org/sitioALADI/normativaInstTM80.html",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Argentina"
                ],
                "countries_second" => [
                    "Bolivia",
                    "Brazil",
                    "Chile",
                    "Colombia",
                    "Cuba",
                    "Ecuador",
                    "Mexico",
                    "Paraguay",
                    "Peru",
                    "Uruguay",
                    "Venezuela"
                ]
            ],
            [
                "id" => 463,
                "rta_id" => 136,
                "name" => "South Pacific Regional Trade and Economic Cooperation Agreement (SPARTECA)",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "Enabling Clause",
                "signature_date" => "1980-07-14",
                "notification_date" => "1981-01-07",
                "entry_force_date" => "1981-01-01",
                "current_signatories" => [
                    "Australia",
                    "Cook Islands",
                    "Fiji",
                    "Kiribati",
                    "Marshall Islands",
                    "Micronesia, Federated States of",
                    "Nauru",
                    "New Zealand",
                    "Niue",
                    "Papua New Guinea",
                    "Samoa",
                    "Solomon Islands",
                    "Tonga",
                    "Tuvalu",
                    "Vanuatu"
                ],
                "original_signatories" => [
                    "Australia",
                    "Cook Islands",
                    "Fiji",
                    "Kiribati",
                    "Marshall Islands",
                    "Micronesia, Federated States of",
                    "Nauru",
                    "New Zealand",
                    "Niue",
                    "Papua New Guinea",
                    "Samoa",
                    "Solomon Islands",
                    "Tonga",
                    "Tuvalu",
                    "Vanuatu"
                ],
                "rta_composition" => [
                    "Plurilateral"
                ],
                "region" => [
                    "Oceania"
                ],
                "wto_members" => false,
                "type" => "Partial Scope Agreement",
                "web_addresses" => [],
                "agreement" => [
                    [
                        "url" => "http://sul-derivatives.stanford.edu/derivative?CSNID=90980233&amp;mediaType=application/pdf",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Australia"
                ],
                "countries_second" => [
                    "Solomon Islands",
                    "Cook Islands",
                    "Fiji",
                    "Kiribati",
                    "Nauru",
                    "Vanuatu",
                    "New Zealand",
                    "Niue",
                    "Micronesia, Federated States of",
                    "Marshall Islands",
                    "Papua New Guinea",
                    "Tonga",
                    "Tuvalu",
                    "Samoa"
                ]
            ],
            [
                "id" => 469,
                "rta_id" => 142,
                "name" => "EU - Norway",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "1973-05-14",
                "notification_date" => "1973-07-13",
                "entry_force_date" => "1973-07-01",
                "current_signatories" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom",
                    "Norway"
                ],
                "original_signatories" => [
                    "Norway"
                ],
                "rta_composition" => [
                    "Bilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "Europe"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://ec.europa.eu/",
                        "name" => "European Commission"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1399391371763&amp;uri=CELEX:21973A0514(01)",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1399391371763&amp;uri=CELEX:21973A0514(01)",
                        "name" => "F"
                    ],
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1399391371763&amp;uri=CELEX:21973A0514(01)",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Norway"
                ],
                "countries_second" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom"
                ]
            ],
            [
                "id" => 467,
                "rta_id" => 140,
                "name" => "Asia Pacific Trade Agreement (APTA)",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "Enabling Clause",
                "signature_date" => "1975-07-31",
                "notification_date" => "1976-11-02",
                "entry_force_date" => "1976-06-17",
                "current_signatories" => [
                    "Bangladesh",
                    "China",
                    "India",
                    "North Korea",
                    "Laos",
                    "Sri Lanka"
                ],
                "original_signatories" => [
                    "Bangladesh",
                    "India",
                    "North Korea",
                    "Laos",
                    "Sri Lanka"
                ],
                "rta_composition" => [
                    "Plurilateral"
                ],
                "region" => [
                    "West Asia",
                    "East Asia"
                ],
                "wto_members" => true,
                "type" => "Partial Scope Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.unescap.org/tid/apta.asp",
                        "name" => "United Nations Economic and Social Commission for Asia and the Pacific"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.unescap.org/apta",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Bangladesh"
                ],
                "countries_second" => [
                    "Sri Lanka",
                    "China",
                    "India",
                    "North Korea",
                    "Laos"
                ]
            ],
            [
                "id" => 464,
                "rta_id" => 137,
                "name" => "EC (10) Enlargement",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "1979-05-28",
                "notification_date" => "1979-10-24",
                "entry_force_date" => "1981-01-01",
                "current_signatories" => [
                    "Belgium",
                    "Denmark",
                    "France",
                    "Germany",
                    "Greece",
                    "Ireland",
                    "Italy",
                    "Luxembourg",
                    "Netherlands",
                    "United Kingdom"
                ],
                "original_signatories" => [
                    "Belgium",
                    "Denmark",
                    "France",
                    "Germany",
                    "Greece",
                    "Ireland",
                    "Italy",
                    "Luxembourg",
                    "Netherlands",
                    "United Kingdom"
                ],
                "rta_composition" => [
                    "Plurilateral"
                ],
                "region" => [
                    "Europe"
                ],
                "wto_members" => true,
                "type" => "Customs Union",
                "web_addresses" => [
                    [
                        "url" => "http://ec.europa.eu/",
                        "name" => "European Commission"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?uri=OJ:L:1979:291:TOC",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Belgium"
                ],
                "countries_second" => [
                    "Denmark",
                    "France",
                    "Germany",
                    "Greece",
                    "Ireland",
                    "Italy",
                    "Luxembourg",
                    "Netherlands",
                    "United Kingdom"
                ]
            ],
            [
                "id" => 472,
                "rta_id" => 145,
                "name" => "EC (9) Enlargement",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "1972-01-22",
                "notification_date" => "1972-03-07",
                "entry_force_date" => "1973-01-01",
                "current_signatories" => [
                    "Belgium",
                    "Denmark",
                    "France",
                    "Germany",
                    "Ireland",
                    "Italy",
                    "Luxembourg",
                    "Netherlands",
                    "United Kingdom"
                ],
                "original_signatories" => [
                    "Belgium",
                    "Denmark",
                    "France",
                    "Germany",
                    "Ireland",
                    "Italy",
                    "Luxembourg",
                    "Netherlands",
                    "United Kingdom"
                ],
                "rta_composition" => [
                    "Plurilateral"
                ],
                "region" => [
                    "Europe"
                ],
                "wto_members" => true,
                "type" => "Customs Union",
                "web_addresses" => [
                    [
                        "url" => "http://ec.europa.eu/",
                        "name" => "European Commission"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://old.eur-lex.europa.eu/LexUriServ/LexUriServ.do?uri=CELEX:11972B/TXT:EN:NOT",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://old.eur-lex.europa.eu/LexUriServ/LexUriServ.do?uri=CELEX:11972B/TXT:EN:NOT",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Belgium"
                ],
                "countries_second" => [
                    "Denmark",
                    "France",
                    "Germany",
                    "Ireland",
                    "Italy",
                    "Luxembourg",
                    "Netherlands",
                    "United Kingdom"
                ]
            ],
            [
                "id" => 479,
                "rta_id" => 152,
                "name" => "European Free Trade Association (EFTA)",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "1960-01-04",
                "notification_date" => "1959-11-14",
                "entry_force_date" => "1960-05-03",
                "current_signatories" => [
                    "Iceland",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland"
                ],
                "original_signatories" => [
                    "Austria",
                    "Denmark",
                    "Norway",
                    "Portugal",
                    "Sweden",
                    "Switzerland",
                    "United Kingdom"
                ],
                "rta_composition" => [
                    "Plurilateral"
                ],
                "region" => [
                    "Europe"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.efta.int",
                        "name" => "European Free Trade Association"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.efta.int/legal-texts/efta-convention.aspx",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Iceland"
                ],
                "countries_second" => [
                    "Liechtenstein",
                    "Norway",
                    "Switzerland"
                ]
            ],
            [
                "id" => 471,
                "rta_id" => 144,
                "name" => "EU - Switzerland - Liechtenstein",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "1972-07-22",
                "notification_date" => "1972-10-27",
                "entry_force_date" => "1973-01-01",
                "current_signatories" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom",
                    "Liechtenstein",
                    "Switzerland"
                ],
                "original_signatories" => [
                    "Liechtenstein",
                    "Switzerland"
                ],
                "rta_composition" => [
                    "Plurilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "Europe"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.europa.admin.ch/themen/00500/index.html?lang=fr",
                        "name" => "Confédération suisse"
                    ],
                    [
                        "url" => "http://ec.europa.eu",
                        "name" => "European Commission"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1399542828541&amp;uri=CELEX:21972A0722(03)",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1399542828541&amp;uri=CELEX:21972A0722(03)",
                        "name" => "F"
                    ],
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1399542828541&amp;uri=CELEX:21972A0722(03)",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Liechtenstein"
                ],
                "countries_second" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom",
                    "Switzerland"
                ]
            ],
            [
                "id" => 478,
                "rta_id" => 151,
                "name" => "Central American Common Market (CACM)",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "1960-12-13",
                "notification_date" => "1961-02-24",
                "entry_force_date" => "1961-06-04",
                "current_signatories" => [
                    "Costa Rica",
                    "El Salvador",
                    "Guatemala",
                    "Honduras",
                    "Nicaragua",
                    "Panama"
                ],
                "original_signatories" => [
                    "El Salvador",
                    "Guatemala",
                    "Honduras",
                    "Nicaragua"
                ],
                "rta_composition" => [
                    "Plurilateral"
                ],
                "region" => [
                    "Central America"
                ],
                "wto_members" => true,
                "type" => "Customs Union",
                "web_addresses" => [
                    [
                        "url" => "https://www.sieca.int/",
                        "name" => "Secretaría de Integración Económica Centroamericana"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.sice.oas.org/SICA/instmt_s.asp",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Costa Rica"
                ],
                "countries_second" => [
                    "El Salvador",
                    "Guatemala",
                    "Honduras",
                    "Nicaragua",
                    "Panama"
                ]
            ],
            [
                "id" => 474,
                "rta_id" => 147,
                "name" => "Protocol on Trade Negotiations (PTN)",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "Enabling Clause",
                "signature_date" => "1971-12-08",
                "notification_date" => "1971-11-09",
                "entry_force_date" => "1973-02-11",
                "current_signatories" => [
                    "Bangladesh",
                    "Brazil",
                    "Chile",
                    "Egypt",
                    "Israel",
                    "North Korea",
                    "Mexico",
                    "Pakistan",
                    "Paraguay",
                    "Peru",
                    "Philippines",
                    "Serbia",
                    "Tunisia",
                    "Turkey",
                    "Uruguay"
                ],
                "original_signatories" => [
                    "Bangladesh",
                    "Brazil",
                    "Chile",
                    "Egypt",
                    "Israel",
                    "North Korea",
                    "Mexico",
                    "Pakistan",
                    "Paraguay",
                    "Peru",
                    "Philippines",
                    "Romania",
                    "Tunisia",
                    "Turkey",
                    "Uruguay",
                    "Yugoslavia, Socialist Federal Republic of"
                ],
                "rta_composition" => [
                    "Plurilateral"
                ],
                "region" => [
                    "West Asia",
                    "South America",
                    "Africa",
                    "Middle East",
                    "East Asia",
                    "North America",
                    "Europe"
                ],
                "wto_members" => false,
                "type" => "Partial Scope Agreement",
                "web_addresses" => [],
                "agreement" => [],
                "countries_first" => [
                    "Bangladesh"
                ],
                "countries_second" => [
                    "Brazil",
                    "Chile",
                    "Israel",
                    "North Korea",
                    "Mexico",
                    "Pakistan",
                    "Paraguay",
                    "Peru",
                    "Philippines",
                    "Tunisia",
                    "Turkey",
                    "Egypt",
                    "Uruguay",
                    "Serbia"
                ]
            ],
            [
                "id" => 483,
                "rta_id" => 156,
                "name" => "Australia - China",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2015-06-17",
                "notification_date" => "2016-01-26",
                "entry_force_date" => "2015-12-20",
                "current_signatories" => [
                    "Australia",
                    "China"
                ],
                "original_signatories" => [
                    "Australia",
                    "China"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "Oceania",
                    "East Asia"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://dfat.gov.au/pages/default.aspx",
                        "name" => "Australian Government, Department of Foreign Affairs & Trade"
                    ],
                    [
                        "url" => "http://www.mofcom.gov.cn/",
                        "name" => "Ministry of Commerce of the People's Republic of China"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://dfat.gov.au/trade/agreements/in-force/chafta/official-documents/Pages/official-documents.aspx",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Australia"
                ],
                "countries_second" => [
                    "China"
                ]
            ],
            [
                "id" => 492,
                "rta_id" => 165,
                "name" => "Korea, Republic of - Mexico",
                "coverage" => "",
                "status" => "Early announcement-Under negotiation",
                "notification_under" => "",
                "signature_date" => null,
                "notification_date" => null,
                "entry_force_date" => null,
                "current_signatories" => [
                    ""
                ],
                "original_signatories" => [
                    ""
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "East Asia",
                    "North America"
                ],
                "wto_members" => true,
                "type" => "",
                "web_addresses" => [
                    [
                        "url" => "http://www.mofat.go.kr/index.jsp",
                        "name" => "Ministry of Foreign Affairs and Trade, Republic of Korea"
                    ],
                    [
                        "url" => "http://www.economia.gob.mx/",
                        "name" => "Ministry of the Economy, Mexico"
                    ]
                ],
                "agreement" => [],
                "countries_first" => [
                    "North Korea"
                ],
                "countries_second" => [
                    "Mexico"
                ]
            ],
            [
                "id" => 491,
                "rta_id" => 164,
                "name" => "Canada - Singapore",
                "coverage" => "",
                "status" => "Early announcement-Under negotiation",
                "notification_under" => "",
                "signature_date" => null,
                "notification_date" => null,
                "entry_force_date" => null,
                "current_signatories" => [
                    ""
                ],
                "original_signatories" => [
                    ""
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "North America",
                    "East Asia"
                ],
                "wto_members" => true,
                "type" => "",
                "web_addresses" => [
                    [
                        "url" => "http://www.international.gc.ca/",
                        "name" => "Global Affairs Canada"
                    ],
                    [
                        "url" => "http://www.iesingapore.gov.sg",
                        "name" => "International Enterprise Singapore"
                    ]
                ],
                "agreement" => [],
                "countries_first" => [
                    "Canada"
                ],
                "countries_second" => [
                    "Singapore"
                ]
            ],
            [
                "id" => 488,
                "rta_id" => 161,
                "name" => "Canada - Dominican Republic",
                "coverage" => "",
                "status" => "Early announcement-Under negotiation",
                "notification_under" => "",
                "signature_date" => null,
                "notification_date" => null,
                "entry_force_date" => null,
                "current_signatories" => [
                    ""
                ],
                "original_signatories" => [
                    ""
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "North America",
                    "Caribbean"
                ],
                "wto_members" => true,
                "type" => "",
                "web_addresses" => [
                    [
                        "url" => "http://www.international.gc.ca/",
                        "name" => "Global Affairs Canada"
                    ],
                    [
                        "url" => "https://mic.gob.do/",
                        "name" => "Secretaría de Estado de Industria y Comercio, República Dominicana"
                    ]
                ],
                "agreement" => [],
                "countries_first" => [
                    "Canada"
                ],
                "countries_second" => [
                    "Dominican Republic"
                ]
            ],
            [
                "id" => 487,
                "rta_id" => 160,
                "name" => "Canada - CARICOM",
                "coverage" => "",
                "status" => "Early announcement-Under negotiation",
                "notification_under" => "",
                "signature_date" => null,
                "notification_date" => null,
                "entry_force_date" => null,
                "current_signatories" => [
                    ""
                ],
                "original_signatories" => [
                    ""
                ],
                "rta_composition" => [
                    "Plurilateral"
                ],
                "region" => [
                    "Caribbean",
                    "Central America",
                    "North America",
                    "South America"
                ],
                "wto_members" => false,
                "type" => "",
                "web_addresses" => [
                    [
                        "url" => "http://www.international.gc.ca/",
                        "name" => "Global Affairs Canada"
                    ],
                    [
                        "url" => "http://www.caricom.org/",
                        "name" => "Caribbean Community and Common Market"
                    ]
                ],
                "agreement" => [],
                "countries_first" => [
                    "Canada"
                ],
                "countries_second" => [
                    "Antigua and Barbuda",
                    "Bahamas",
                    "Barbados",
                    "Belize",
                    "Dominica",
                    "Grenada",
                    "Guyana",
                    "Haiti",
                    "Jamaica",
                    "Montserrat",
                    "Saint Kitts and Nevis",
                    "Saint Lucia",
                    "Saint Vincent and the Grenadines",
                    "Suriname",
                    "Trinidad and Tobago"
                ]
            ],
            [
                "id" => 490,
                "rta_id" => 163,
                "name" => "Canada - El Salvador - Guatemala - Honduras - Nicaragua",
                "coverage" => "",
                "status" => "Early announcement-Under negotiation",
                "notification_under" => "",
                "signature_date" => null,
                "notification_date" => null,
                "entry_force_date" => null,
                "current_signatories" => [
                    ""
                ],
                "original_signatories" => [
                    ""
                ],
                "rta_composition" => [
                    "Plurilateral"
                ],
                "region" => [
                    "North America",
                    "Central America"
                ],
                "wto_members" => true,
                "type" => "",
                "web_addresses" => [
                    [
                        "url" => "http://www.international.gc.ca/",
                        "name" => "Global Affairs Canada"
                    ],
                    [
                        "url" => "http://www.minec.gob.sv/",
                        "name" => "Ministerio de Economia, República de El Salvador"
                    ],
                    [
                        "url" => "http://www.mineco.gob.gt",
                        "name" => "Ministerio de Economia de Guatemala"
                    ],
                    [
                        "url" => "http://www.mific.gob.ni/",
                        "name" => "Gobierno de Nicaragua"
                    ]
                ],
                "agreement" => [],
                "countries_first" => [
                    "Canada"
                ],
                "countries_second" => [
                    "El Salvador",
                    "Guatemala",
                    "Honduras",
                    "Nicaragua"
                ]
            ],
            [
                "id" => 486,
                "rta_id" => 159,
                "name" => "EFTA - Canada",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "2008-01-26",
                "notification_date" => "2009-08-04",
                "entry_force_date" => "2009-07-01",
                "current_signatories" => [
                    "Canada",
                    "Iceland",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland"
                ],
                "original_signatories" => [
                    "Canada",
                    "Iceland",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland"
                ],
                "rta_composition" => [
                    "Bilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "North America",
                    "Europe"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.international.gc.ca/",
                        "name" => "Global Affairs Canada"
                    ],
                    [
                        "url" => "http://www.efta.int",
                        "name" => "European Free Trade Association"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.efta.int/free-trade/free-trade-agreements/canada",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://international.gc.ca/trade-commerce/trade-agreements-accords-commerciaux/agr-acc/european-association-europeenne/fta-ale/index.aspx?lang=fra",
                        "name" => "F"
                    ]
                ],
                "countries_first" => [
                    "Canada"
                ],
                "countries_second" => [
                    "Iceland",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland"
                ]
            ],
            [
                "id" => 485,
                "rta_id" => 158,
                "name" => "EU - India",
                "coverage" => "",
                "status" => "Early announcement-Under negotiation",
                "notification_under" => "",
                "signature_date" => null,
                "notification_date" => null,
                "entry_force_date" => null,
                "current_signatories" => [
                    ""
                ],
                "original_signatories" => [
                    ""
                ],
                "rta_composition" => [
                    "Bilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "Europe",
                    "West Asia"
                ],
                "wto_members" => true,
                "type" => "",
                "web_addresses" => [
                    [
                        "url" => "http://ec.europa.eu/",
                        "name" => "European Commission"
                    ],
                    [
                        "url" => "http://commerce.nic.in",
                        "name" => "Government of India, Ministry of Commerce & Industry"
                    ]
                ],
                "agreement" => [],
                "countries_first" => [
                    "India"
                ],
                "countries_second" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom"
                ]
            ],
            [
                "id" => 482,
                "rta_id" => 155,
                "name" => "Australia - Gulf Cooperation Council (GCC)",
                "coverage" => "",
                "status" => "Early announcement-Under negotiation",
                "notification_under" => "",
                "signature_date" => null,
                "notification_date" => null,
                "entry_force_date" => null,
                "current_signatories" => [
                    ""
                ],
                "original_signatories" => [
                    ""
                ],
                "rta_composition" => [
                    "Plurilateral"
                ],
                "region" => [
                    "Oceania",
                    "Middle East"
                ],
                "wto_members" => true,
                "type" => "",
                "web_addresses" => [],
                "agreement" => [],
                "countries_first" => [
                    "Australia"
                ],
                "countries_second" => [
                    "Bahrain",
                    "Kuwait",
                    "Oman",
                    "Qatar",
                    "Saudi Arabia",
                    "United Arab Emirates"
                ]
            ],
            [
                "id" => 499,
                "rta_id" => 173,
                "name" => "India - Japan",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2011-02-16",
                "notification_date" => "2011-09-14",
                "entry_force_date" => "2011-08-01",
                "current_signatories" => [
                    "India",
                    "Japan"
                ],
                "original_signatories" => [
                    "India",
                    "Japan"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "West Asia",
                    "East Asia"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.mofa.go.jp/",
                        "name" => "Ministry of Foreign Affairs of Japan"
                    ],
                    [
                        "url" => "http://www.commerce.nic.in/",
                        "name" => "Government of India, Ministry of Commerce & Industry"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.mofa.go.jp/region/asia-paci/india/epa201102/pdfs/ijcepa_ba_e.pdf",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "India"
                ],
                "countries_second" => [
                    "Japan"
                ]
            ],
            [
                "id" => 501,
                "rta_id" => 175,
                "name" => "Japan - Australia",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2014-07-08",
                "notification_date" => "2015-01-12",
                "entry_force_date" => "2015-01-15",
                "current_signatories" => [
                    "Australia",
                    "Japan"
                ],
                "original_signatories" => [
                    "Australia",
                    "Japan"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "Oceania",
                    "East Asia"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.mofa.go.jp/",
                        "name" => "Ministry of Foreign Affairs of Japan"
                    ],
                    [
                        "url" => "http://www.dfat.gov.au/",
                        "name" => "Australian Government, Department of Foreign Affairs & Trade"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.mofa.go.jp/files/000044322.pdf",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Australia"
                ],
                "countries_second" => [
                    "Japan"
                ]
            ],
            [
                "id" => 498,
                "rta_id" => 172,
                "name" => "Japan - Indonesia",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2007-08-20",
                "notification_date" => "2008-06-27",
                "entry_force_date" => "2008-07-01",
                "current_signatories" => [
                    "Indonesia",
                    "Japan"
                ],
                "original_signatories" => [
                    "Indonesia",
                    "Japan"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "East Asia"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.mofa.go.jp/",
                        "name" => "Ministry of Foreign Affairs of Japan"
                    ],
                    [
                        "url" => "http://www.kemlu.go.id/en/Default.aspx",
                        "name" => "Ministry of Foreign Affairs of Indonesia"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.mofa.go.jp/region/asia-paci/indonesia/epa0708/index.html",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Indonesia"
                ],
                "countries_second" => [
                    "Japan"
                ]
            ],
            [
                "id" => 496,
                "rta_id" => 170,
                "name" => "Japan - Viet Nam",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2008-12-25",
                "notification_date" => "2009-10-01",
                "entry_force_date" => "2009-10-01",
                "current_signatories" => [
                    "Japan",
                    "Vietnam"
                ],
                "original_signatories" => [
                    "Japan",
                    "Vietnam"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "East Asia"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.mofa.go.jp/",
                        "name" => "Ministry of Foreign Affairs of Japan"
                    ],
                    [
                        "url" => "http://www.moit.gov.vn/en/Pages/default.aspx",
                        "name" => "Ministry of Industry and Trade of the Socialist Republic of Viet Nam"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.mofa.go.jp/region/asia-paci/vietnam/epa0812/agreement.pdf",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Japan"
                ],
                "countries_second" => [
                    "Vietnam"
                ]
            ],
            [
                "id" => 497,
                "rta_id" => 171,
                "name" => "Japan - Korea, Republic of",
                "coverage" => "",
                "status" => "Early announcement-Under negotiation",
                "notification_under" => "",
                "signature_date" => null,
                "notification_date" => null,
                "entry_force_date" => null,
                "current_signatories" => [
                    ""
                ],
                "original_signatories" => [
                    ""
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "East Asia"
                ],
                "wto_members" => true,
                "type" => "",
                "web_addresses" => [
                    [
                        "url" => "http://www.mofa.go.jp/",
                        "name" => "Ministry of Foreign Affairs of Japan"
                    ],
                    [
                        "url" => "http://www.mofat.go.kr/index.jsp",
                        "name" => "Ministry of Foreign Affairs and Trade, Republic of Korea"
                    ]
                ],
                "agreement" => [],
                "countries_first" => [
                    "Japan"
                ],
                "countries_second" => [
                    "North Korea"
                ]
            ],
            [
                "id" => 495,
                "rta_id" => 169,
                "name" => "ASEAN - Korea, Republic of",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "",
                "signature_date" => "2006-08-24",
                "notification_date" => "2010-07-08",
                "entry_force_date" => "2010-01-01",
                "current_signatories" => [
                    "Brunei Darussalam",
                    "Myanmar",
                    "Cambodia",
                    "Indonesia",
                    "Laos",
                    "Malaysia",
                    "Philippines",
                    "Singapore",
                    "Vietnam",
                    "Thailand",
                    "North Korea"
                ],
                "original_signatories" => [
                    "Brunei Darussalam",
                    "Myanmar",
                    "Cambodia",
                    "Indonesia",
                    "Laos",
                    "Malaysia",
                    "Philippines",
                    "Singapore",
                    "Vietnam",
                    "Thailand",
                    "North Korea"
                ],
                "rta_composition" => [
                    "Bilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "East Asia"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.mofa.go.kr/eng/index.do",
                        "name" => "Ministry of Foreign Affairs and Trade, Republic of Korea"
                    ],
                    [
                        "url" => "http://www.asean.org/",
                        "name" => "Association of Southeast Asian Nations"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.iesingapore.gov.sg/~/media/IE%20Singapore/Files/FTA/Existing%20FTA/ASEAN%20Korea%20FTA/Legal%20Text/ASEANKorea20FTA20Legal20Text.pdf",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "North Korea"
                ],
                "countries_second" => [
                    "Brunei Darussalam",
                    "Myanmar",
                    "Cambodia",
                    "Indonesia",
                    "Laos",
                    "Malaysia",
                    "Philippines",
                    "Singapore",
                    "Vietnam",
                    "Thailand"
                ]
            ],
            [
                "id" => 500,
                "rta_id" => 174,
                "name" => "Japan - Gulf Cooperation Council (GCC)",
                "coverage" => "",
                "status" => "Early announcement-Under negotiation",
                "notification_under" => "",
                "signature_date" => null,
                "notification_date" => null,
                "entry_force_date" => null,
                "current_signatories" => [
                    ""
                ],
                "original_signatories" => [
                    ""
                ],
                "rta_composition" => [
                    "Plurilateral"
                ],
                "region" => [
                    "Middle East",
                    "East Asia"
                ],
                "wto_members" => true,
                "type" => "",
                "web_addresses" => [
                    [
                        "url" => "http://www.mofa.go.jp/",
                        "name" => "Ministry of Foreign Affairs of Japan"
                    ],
                    [
                        "url" => "http://www.gcc-sg.org/en-us/Pages/default.aspx",
                        "name" => "Gulf Cooperation Council"
                    ]
                ],
                "agreement" => [],
                "countries_first" => [
                    "Japan"
                ],
                "countries_second" => [
                    "Bahrain",
                    "Kuwait",
                    "Oman",
                    "Qatar",
                    "Saudi Arabia",
                    "United Arab Emirates"
                ]
            ],
            [
                "id" => 510,
                "rta_id" => 186,
                "name" => "Malaysia - Australia",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2012-05-22",
                "notification_date" => "2013-05-13",
                "entry_force_date" => "2013-01-01",
                "current_signatories" => [
                    "Australia",
                    "Malaysia"
                ],
                "original_signatories" => [
                    "Australia",
                    "Malaysia"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "Oceania",
                    "East Asia"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.dfat.gov.au/pages/default.aspx",
                        "name" => "Australian Government, Department of Foreign Affairs and Trade"
                    ],
                    [
                        "url" => "http://www.miti.gov.my/",
                        "name" => "Malaysia Trade & Industry Portal"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.dfat.gov.au/trade/agreements/mafta/Pages/malaysia-australia-fta.aspx#full-text",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Australia"
                ],
                "countries_second" => [
                    "Malaysia"
                ]
            ],
            [
                "id" => 511,
                "rta_id" => 187,
                "name" => "Pakistan - Malaysia",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "Enabling Clause & GATS Art. V",
                "signature_date" => "2007-11-08",
                "notification_date" => "2008-02-19",
                "entry_force_date" => "2008-01-01",
                "current_signatories" => [
                    "Malaysia",
                    "Pakistan"
                ],
                "original_signatories" => [
                    "Malaysia",
                    "Pakistan"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "East Asia",
                    "West Asia"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.commerce.gov.pk/",
                        "name" => "Government of Pakistan, Ministry of Commerce"
                    ],
                    [
                        "url" => "http://www.miti.gov.my",
                        "name" => "Malaysia Trade & Industry Portal"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.commerce.gov.pk/wp-content/uploads/pdf/PAk-Malaysia-FTA(TXT).pdf",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Malaysia"
                ],
                "countries_second" => [
                    "Pakistan"
                ]
            ],
            [
                "id" => 513,
                "rta_id" => 189,
                "name" => "Panama - Chile",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2006-06-27",
                "notification_date" => "2008-04-17",
                "entry_force_date" => "2008-03-07",
                "current_signatories" => [
                    "Chile",
                    "Panama"
                ],
                "original_signatories" => [
                    "Chile",
                    "Panama"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "South America",
                    "Central America"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.mici.gob.pa/",
                        "name" => "Ministerio de Comercio e Industrias, Panamá"
                    ],
                    [
                        "url" => "http://www.direcon.gob.cl/",
                        "name" => "Dirección General de Relaciones Económicas Internacionales, Chile"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.direcon.gob.cl/detalle-de-acuerdos/?idacuerdo=6269",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Chile"
                ],
                "countries_second" => [
                    "Panama"
                ]
            ],
            [
                "id" => 508,
                "rta_id" => 182,
                "name" => "United States - Oman",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2006-01-19",
                "notification_date" => "2009-01-30",
                "entry_force_date" => "2009-01-01",
                "current_signatories" => [
                    "Oman",
                    "USA"
                ],
                "original_signatories" => [
                    "Oman",
                    "USA"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "Middle East",
                    "North America"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.ustr.gov/",
                        "name" => "United States Trade Representative"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "https://ustr.gov/trade-agreements/free-trade-agreements/oman-fta/final-text",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Oman"
                ],
                "countries_second" => [
                    "USA"
                ]
            ],
            [
                "id" => 507,
                "rta_id" => 181,
                "name" => "United States - Panama",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2007-06-28",
                "notification_date" => "2012-10-29",
                "entry_force_date" => "2012-10-31",
                "current_signatories" => [
                    "Panama",
                    "USA"
                ],
                "original_signatories" => [
                    "Panama",
                    "USA"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "Central America",
                    "North America"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.ustr.gov/",
                        "name" => "Office of the United States Trade Representative"
                    ],
                    [
                        "url" => "http://www.mici.gob.pa/",
                        "name" => "Ministerio de Comercio e Industrias, Panamá"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.ustr.gov/trade-agreements/free-trade-agreements/panama-tpa/final-text",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://www.mici.gob.pa/detalle.php?cid=15&amp;sid=57&amp;clid=173&amp;id=3853",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Panama"
                ],
                "countries_second" => [
                    "USA"
                ]
            ],
            [
                "id" => 506,
                "rta_id" => 180,
                "name" => "United States - Peru",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2006-04-12",
                "notification_date" => "2009-02-03",
                "entry_force_date" => "2009-02-01",
                "current_signatories" => [
                    "Peru",
                    "USA"
                ],
                "original_signatories" => [
                    "Peru",
                    "USA"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "South America",
                    "North America"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.ustr.gov/",
                        "name" => "Office of the United States Trade Representative"
                    ],
                    [
                        "url" => "http://www.mincetur.gob.pe/newweb/",
                        "name" => "Ministerio de Comercio Exterior de Perú"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "https://ustr.gov/trade-agreements/free-trade-agreements/peru-tpa/final-text",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://www.acuerdoscomerciales.gob.pe/index.php?option=com_content&amp;view=category&amp;layout=blog&amp;id=57&amp;Itemid=80",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Peru"
                ],
                "countries_second" => [
                    "USA"
                ]
            ],
            [
                "id" => 504,
                "rta_id" => 178,
                "name" => "EFTA - Peru",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "2010-06-24",
                "notification_date" => "2011-06-30",
                "entry_force_date" => "2011-07-01",
                "current_signatories" => [
                    "Iceland",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland",
                    "Peru"
                ],
                "original_signatories" => [
                    "Iceland",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland",
                    "Peru"
                ],
                "rta_composition" => [
                    "Bilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "Europe",
                    "South America"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.efta.int",
                        "name" => "European Free Trade Association"
                    ],
                    [
                        "url" => "http://www.mincetur.gob.pe/newweb/",
                        "name" => "Ministerio de Comercio Exterior de Perú"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.efta.int/~/media/Documents/legal-texts/free-trade-relations/peru/EFTA-Peru%20Free%20Trade%20Agreement%20EN.pdf",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://www.acuerdoscomerciales.gob.pe/images/stories/efta/espanol/Acuerdo_Principal.pdf",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Peru"
                ],
                "countries_second" => [
                    "Iceland",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland"
                ]
            ],
            [
                "id" => 503,
                "rta_id" => 177,
                "name" => "EFTA - Colombia",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2008-11-25",
                "notification_date" => "2011-09-14",
                "entry_force_date" => "2011-07-01",
                "current_signatories" => [
                    "Colombia",
                    "Iceland",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland"
                ],
                "original_signatories" => [
                    "Colombia",
                    "Iceland",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland"
                ],
                "rta_composition" => [
                    "Bilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "South America",
                    "Europe"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.efta.int",
                        "name" => "European Free Trade Association"
                    ],
                    [
                        "url" => "http://www.tlc.gov.co/index.php",
                        "name" => "Ministerio de Comercio, Industria y Turismo, República de Colombia"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.efta.int/~/media/Documents/legal-texts/free-trade-relations/columbia/EFTA-Colombia%20Free%20Trade%20Agreement%20EN.pdf",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://www.efta.int/media/documents/legal-texts/free-trade-relations/colombia/EFTA-Colombia%20Free%20Trade%20Agreement%20SP.pdf",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Colombia"
                ],
                "countries_second" => [
                    "Iceland",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland"
                ]
            ],
            [
                "id" => 512,
                "rta_id" => 188,
                "name" => "South Asian Free Trade Agreement (SAFTA)",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "Enabling Clause",
                "signature_date" => "2004-01-06",
                "notification_date" => "2008-04-21",
                "entry_force_date" => "2006-01-01",
                "current_signatories" => [
                    "Afghanistan",
                    "Bangladesh",
                    "Bhutan",
                    "India",
                    "Maldives",
                    "Nepal",
                    "Pakistan",
                    "Sri Lanka"
                ],
                "original_signatories" => [
                    "Bangladesh",
                    "Bhutan",
                    "India",
                    "Maldives",
                    "Nepal",
                    "Pakistan",
                    "Sri Lanka"
                ],
                "rta_composition" => [
                    "Plurilateral"
                ],
                "region" => [
                    "West Asia"
                ],
                "wto_members" => false,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.saarc-sec.org",
                        "name" => "South Asian Association for Regional Cooperation"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.commerce.nic.in/trade/safta.pdf",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Afghanistan"
                ],
                "countries_second" => [
                    "Bangladesh",
                    "Bhutan",
                    "Sri Lanka",
                    "India",
                    "Maldives",
                    "Nepal",
                    "Pakistan"
                ]
            ],
            [
                "id" => 698,
                "rta_id" => 385,
                "name" => "Panama  - Dominican Republic",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "Enabling Clause",
                "signature_date" => "1985-07-17",
                "notification_date" => "2016-03-21",
                "entry_force_date" => "1987-06-08",
                "current_signatories" => [
                    "Dominican Republic",
                    "Panama"
                ],
                "original_signatories" => [
                    "Dominican Republic",
                    "Panama"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "Caribbean",
                    "Central America"
                ],
                "wto_members" => true,
                "type" => "Partial Scope Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.mici.gob.pa/base.php?hoja=homepage",
                        "name" => "Ministerio de Comercio e Industrias, Panamá"
                    ],
                    [
                        "url" => "https://mic.gob.do/",
                        "name" => "Secretaría de Estado de Industría y Comercio de República Dominicana"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.mici.gob.pa/tratados/new/Tratado%20-%20Panama%20y%20la%20Republica%20Dominicana.pdf",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [],
                "countries_second" => []
            ],
            [
                "id" => 700,
                "rta_id" => 389,
                "name" => "India - Afghanistan",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "Enabling Clause",
                "signature_date" => "2003-03-06",
                "notification_date" => "2010-03-08",
                "entry_force_date" => "2003-05-13",
                "current_signatories" => [
                    "Afghanistan",
                    "India"
                ],
                "original_signatories" => [
                    "Afghanistan",
                    "India"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "West Asia"
                ],
                "wto_members" => true,
                "type" => "Partial Scope Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.commerce.nic.in/",
                        "name" => "Government of India, Ministry of Commerce & Industry"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://commerce.gov.in/PageContent.aspx?Id=44",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Afghanistan"
                ],
                "countries_second" => [
                    "India"
                ]
            ],
            [
                "id" => 701,
                "rta_id" => 392,
                "name" => "India - Nepal",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "Enabling Clause",
                "signature_date" => "2009-10-27",
                "notification_date" => "2010-08-02",
                "entry_force_date" => "2009-10-27",
                "current_signatories" => [
                    "India",
                    "Nepal"
                ],
                "original_signatories" => [
                    "India",
                    "Nepal"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "West Asia"
                ],
                "wto_members" => true,
                "type" => "Partial Scope Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.commerce.nic.in/",
                        "name" => "Ministry of Commerce and Industry, Government of India"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://commerce.gov.in/trade/nepal.pdf",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "India"
                ],
                "countries_second" => [
                    "Nepal"
                ]
            ],
            [
                "id" => 702,
                "rta_id" => 414,
                "name" => "Panama - Costa Rica (Panama - Central America)",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2007-08-07",
                "notification_date" => "2009-04-07",
                "entry_force_date" => "2008-11-23",
                "current_signatories" => [
                    "Costa Rica",
                    "Panama"
                ],
                "original_signatories" => [
                    "Costa Rica",
                    "Panama"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "Central America"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.mici.gob.pa/",
                        "name" => "Ministerio de Comercio e Industrias, Panamá"
                    ],
                    [
                        "url" => "http://www.comex.go.cr",
                        "name" => "Ministerio de Comercio Exterior de Costa Rica"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.sice.oas.org/Trade/Capan/indice.asp",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Costa Rica"
                ],
                "countries_second" => [
                    "Panama"
                ]
            ],
            [
                "id" => 703,
                "rta_id" => 415,
                "name" => "Panama - Guatemala (Panama - Central America)",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2008-02-26",
                "notification_date" => "2013-04-22",
                "entry_force_date" => "2009-06-20",
                "current_signatories" => [
                    "Guatemala",
                    "Panama"
                ],
                "original_signatories" => [
                    "Guatemala",
                    "Panama"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "Central America"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.mici.gob.pa/index2.php",
                        "name" => "Ministerio de Comercio e Industrías, Panamá"
                    ],
                    [
                        "url" => "http://www.mineco.gob.gt/",
                        "name" => "Ministerio de Economia de Guatemala"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.sice.oas.org/Trade/Capan/indice.asp",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Guatemala"
                ],
                "countries_second" => [
                    "Panama"
                ]
            ],
            [
                "id" => 704,
                "rta_id" => 416,
                "name" => "Panama - Honduras (Panama - Central America )",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2007-06-15",
                "notification_date" => "2009-12-16",
                "entry_force_date" => "2009-01-09",
                "current_signatories" => [
                    "Honduras",
                    "Panama"
                ],
                "original_signatories" => [
                    "Honduras",
                    "Panama"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "Central America"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "https://sde.gob.hn/",
                        "name" => "Secretaría de Desarollo Económico, Honduras"
                    ],
                    [
                        "url" => "http://www.mici.gob.pa",
                        "name" => "Ministerio de Comercio e Industrías, Panamá"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.sice.oas.org/Trade/Capan/indice.asp",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Honduras"
                ],
                "countries_second" => [
                    "Panama"
                ]
            ],
            [
                "id" => 705,
                "rta_id" => 417,
                "name" => "Panama - Nicaragua (Panama - Central America)",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2009-01-15",
                "notification_date" => "2013-02-25",
                "entry_force_date" => "2009-11-21",
                "current_signatories" => [
                    "Nicaragua",
                    "Panama"
                ],
                "original_signatories" => [
                    "Nicaragua",
                    "Panama"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "Central America"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.mici.gob.pa/",
                        "name" => "Ministerio de Comercio e Industrías, Panamá"
                    ],
                    [
                        "url" => "http://www.mific.gob.ni/",
                        "name" => "Ministerio de Fomento, Industria y Comercio, Nicaragua"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.sice.oas.org/Trade/Capan/indice.asp",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Nicaragua"
                ],
                "countries_second" => [
                    "Panama"
                ]
            ],
            [
                "id" => 731,
                "rta_id" => 560,
                "name" => "Turkey - Albania",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "2006-12-22",
                "notification_date" => "2008-05-09",
                "entry_force_date" => "2008-05-01",
                "current_signatories" => [
                    "Albania",
                    "Turkey"
                ],
                "original_signatories" => [
                    "Albania",
                    "Turkey"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "Europe"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.economy.gov.tr/",
                        "name" => "Turkish Government"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "../rtadocs/560/TOA/English/01-MAIN_TEXT.doc",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Albania"
                ],
                "countries_second" => [
                    "Turkey"
                ]
            ],
            [
                "id" => 718,
                "rta_id" => 496,
                "name" => "China - Singapore",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2008-10-23",
                "notification_date" => "2009-03-02",
                "entry_force_date" => "2009-01-01",
                "current_signatories" => [
                    "China",
                    "Singapore"
                ],
                "original_signatories" => [
                    "China",
                    "Singapore"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "East Asia"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.mofcom.gov.cn/",
                        "name" => "Ministry of Commerce of the People's Republic of China"
                    ],
                    [
                        "url" => "http://www.iesingapore.gov.sg/",
                        "name" => "International Enterprise Singapore"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.iesingapore.gov.sg/~/media/IE%20Singapore/Files/FTA/Existing%20FTA/China%20Singapore%20FTA/Legal%20Text/China20Singapore20FTA20Legal20Text.pdf",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "China"
                ],
                "countries_second" => [
                    "Singapore"
                ]
            ],
            [
                "id" => 715,
                "rta_id" => 488,
                "name" => "Hong Kong, China - New Zealand",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2010-03-29",
                "notification_date" => "2011-01-03",
                "entry_force_date" => "2011-01-01",
                "current_signatories" => [
                    "Hong Kong",
                    "New Zealand"
                ],
                "original_signatories" => [
                    "Hong Kong",
                    "New Zealand"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "East Asia",
                    "Oceania"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.tid.gov.hk/",
                        "name" => "Hong Kong Special Administrative Region of the People's Republic of China"
                    ],
                    [
                        "url" => "http://www.mfat.govt.nz./",
                        "name" => "New Zealand Ministry of Foreign Affairs and Trade"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "https://www.mfat.govt.nz/assets/_securedfiles/FTAs-agreements-in-force/Hong-Kong-FTA/NZ-HK-CEP.pdf",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Hong Kong"
                ],
                "countries_second" => [
                    "New Zealand"
                ]
            ],
            [
                "id" => 711,
                "rta_id" => 454,
                "name" => "Iceland - China",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2013-04-15",
                "notification_date" => "2014-10-10",
                "entry_force_date" => "2014-07-01",
                "current_signatories" => [
                    "China",
                    "Iceland"
                ],
                "original_signatories" => [
                    "China",
                    "Iceland"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "East Asia",
                    "Europe"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.mofcom.gov.cn/",
                        "name" => "Ministry of Commerce of the People's Republic of China"
                    ],
                    [
                        "url" => "http://www.mfa.is/",
                        "name" => "Ministry of Foreign Affairs, Iceland"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://fta.mofcom.gov.cn/topic/eniceland.shtml",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "China"
                ],
                "countries_second" => [
                    "Iceland"
                ]
            ],
            [
                "id" => 716,
                "rta_id" => 489,
                "name" => "New Zealand - Malaysia",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2009-10-26",
                "notification_date" => "2012-02-07",
                "entry_force_date" => "2010-08-01",
                "current_signatories" => [
                    "Malaysia",
                    "New Zealand"
                ],
                "original_signatories" => [
                    "Malaysia",
                    "New Zealand"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "East Asia",
                    "Oceania"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.mfat.govt.nz./",
                        "name" => "New Zealand Ministry of Foreign Affairs and Trade"
                    ],
                    [
                        "url" => "http://www.miti.gov.my/",
                        "name" => "Malaysia Trade & Industry Portal"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "https://www.mfat.govt.nz/assets/_securedfiles/FTAs-agreements-in-force/Malaysia/mnzfta-text-of-agreement.pdf",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Malaysia"
                ],
                "countries_second" => [
                    "New Zealand"
                ]
            ],
            [
                "id" => 712,
                "rta_id" => 462,
                "name" => "EFTA - GCC",
                "coverage" => "Goods & Services",
                "status" => "Early announcement-Signed",
                "notification_under" => "",
                "signature_date" => "2009-06-22",
                "notification_date" => null,
                "entry_force_date" => "2014-07-01",
                "current_signatories" => [
                    "Bahrain",
                    "Kuwait",
                    "Oman",
                    "Qatar",
                    "Saudi Arabia",
                    "United Arab Emirates",
                    "Iceland",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland"
                ],
                "original_signatories" => [
                    "Bahrain",
                    "Kuwait",
                    "Oman",
                    "Qatar",
                    "Saudi Arabia",
                    "United Arab Emirates",
                    "Iceland",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland"
                ],
                "rta_composition" => [
                    "Bilateral",
                    "All Parties are RTAs"
                ],
                "region" => [
                    "Middle East",
                    "Europe"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.gcc-sg.org/en-us/Pages/default.aspx",
                        "name" => "Gulf Cooperation Council"
                    ],
                    [
                        "url" => "http://www.efta.int/",
                        "name" => "European Free Trade Association"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.efta.int/free-trade/free-trade-agreements/gcc.aspx",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Oman",
                    "Qatar",
                    "Saudi Arabia",
                    "United Arab Emirates",
                    "Bahrain"
                ],
                "countries_second" => [
                    "Iceland",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland",
                    "Kuwait"
                ]
            ],
            [
                "id" => 709,
                "rta_id" => 438,
                "name" => "ASEAN - India",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "Enabling Clause & GATS Art. V",
                "signature_date" => "2009-08-13",
                "notification_date" => "2010-08-19",
                "entry_force_date" => "2010-01-01",
                "current_signatories" => [
                    "Brunei Darussalam",
                    "Myanmar",
                    "Cambodia",
                    "Indonesia",
                    "Laos",
                    "Malaysia",
                    "Philippines",
                    "Singapore",
                    "Vietnam",
                    "Thailand",
                    "India"
                ],
                "original_signatories" => [
                    "Brunei Darussalam",
                    "Myanmar",
                    "Cambodia",
                    "Indonesia",
                    "Laos",
                    "Malaysia",
                    "Philippines",
                    "Singapore",
                    "Vietnam",
                    "Thailand",
                    "India"
                ],
                "rta_composition" => [
                    "Bilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "East Asia",
                    "West Asia"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.asean.org/",
                        "name" => "Association of Southeast Asian Nations"
                    ],
                    [
                        "url" => "http://www.commerce.nic.in/",
                        "name" => "Ministry of Commerce and Industry, Government of India"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://commerce.gov.in/trade/ASEAN-India%20Trade%20in%20Goods%20Agreement.pdf",
                        "name" => "E"
                    ],
                    [
                        "url" => "https://www.iesingapore.gov.sg/~/media/IE%20Singapore/Files/FTA/Existing%20FTA/ASEAN%20India%20FTA/Legal%20Text/Framework20Agreement20on20Comprehensive20Economic20Cooperation20Between20the20Republic20of20India20and20the20Association20of20Southeast20Asian20Nations.pdf",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "India"
                ],
                "countries_second" => [
                    "Brunei Darussalam",
                    "Myanmar",
                    "Cambodia",
                    "Indonesia",
                    "Laos",
                    "Malaysia",
                    "Philippines",
                    "Singapore",
                    "Vietnam",
                    "Thailand"
                ]
            ],
            [
                "id" => 713,
                "rta_id" => 468,
                "name" => "EU - Eastern African Community (EAC) EPA",
                "coverage" => "",
                "status" => "Early announcement-Under negotiation",
                "notification_under" => "",
                "signature_date" => null,
                "notification_date" => null,
                "entry_force_date" => null,
                "current_signatories" => [
                    ""
                ],
                "original_signatories" => [
                    ""
                ],
                "rta_composition" => [
                    "Plurilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "Europe",
                    "Africa"
                ],
                "wto_members" => true,
                "type" => "",
                "web_addresses" => [
                    [
                        "url" => "http://ec.europa.eu/",
                        "name" => "European Commission"
                    ]
                ],
                "agreement" => [],
                "countries_first" => [
                    "Rwanda",
                    "Uganda",
                    "Tanzania",
                    "Burundi"
                ],
                "countries_second" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom",
                    "Kenya"
                ]
            ],
            [
                "id" => 723,
                "rta_id" => 511,
                "name" => "Chile - Honduras (Chile - Central America)",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "1999-10-19",
                "notification_date" => "2011-11-28",
                "entry_force_date" => "2008-07-19",
                "current_signatories" => [
                    "Chile",
                    "Honduras"
                ],
                "original_signatories" => [
                    "Chile",
                    "Honduras"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "South America",
                    "Central America"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.direcon.gob.cl/",
                        "name" => "Dirección General de Relaciones Económicas Internacionales, Chile"
                    ],
                    [
                        "url" => "https://sde.gob.hn/",
                        "name" => "Secretaría de Desarollo Económico, Honduras"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.direcon.gob.cl/detalle-de-acuerdos/?idacuerdo=6290",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Chile"
                ],
                "countries_second" => [
                    "Honduras"
                ]
            ],
            [
                "id" => 730,
                "rta_id" => 544,
                "name" => "India - Malaysia",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "Enabling Clause & GATS Art. V",
                "signature_date" => "2011-02-18",
                "notification_date" => "2011-09-06",
                "entry_force_date" => "2011-07-01",
                "current_signatories" => [
                    "India",
                    "Malaysia"
                ],
                "original_signatories" => [
                    "India",
                    "Malaysia"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "West Asia",
                    "East Asia"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.commerce.nic.in/",
                        "name" => "Ministry of Commerce and Industry, Government of India"
                    ],
                    [
                        "url" => "http://www.miti.gov.my/",
                        "name" => "Malaysia Trade & Industry Portal"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.commerce.nic.in/trade/IMCECA/title.pdf",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "India"
                ],
                "countries_second" => [
                    "Malaysia"
                ]
            ],
            [
                "id" => 728,
                "rta_id" => 537,
                "name" => "Korea, Republic of - Australia",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2014-04-08",
                "notification_date" => "2014-12-22",
                "entry_force_date" => "2014-12-12",
                "current_signatories" => [
                    "Australia",
                    "North Korea"
                ],
                "original_signatories" => [
                    "Australia",
                    "North Korea"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "Oceania",
                    "East Asia"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.mofa.go.kr/eng/index.do",
                        "name" => "Ministry of Foreign Affairs and Trade, Republic of Korea"
                    ],
                    [
                        "url" => "http://www.dfat.gov.au/",
                        "name" => "Australian Government, Department of Foreign Affairs & Trade"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://dfat.gov.au/trade/agreements/in-force/kafta/official-documents/Pages/full-text-of-kafta.aspx",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Australia"
                ],
                "countries_second" => [
                    "North Korea"
                ]
            ],
            [
                "id" => 724,
                "rta_id" => 514,
                "name" => "Peru - Chile",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2006-08-22",
                "notification_date" => "2011-11-29",
                "entry_force_date" => "2009-03-01",
                "current_signatories" => [
                    "Chile",
                    "Peru"
                ],
                "original_signatories" => [
                    "Chile",
                    "Peru"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "South America"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.direcon.gob.cl/",
                        "name" => "Dirección General de Relaciones Económicas Internacionales, Chile"
                    ],
                    [
                        "url" => "http://www.mincetur.gob.pe/newweb/",
                        "name" => "Ministerio de Comercio Exterior de Perú"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.direcon.gob.cl/detalle-de-acuerdos/?idacuerdo=6255",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Chile"
                ],
                "countries_second" => [
                    "Peru"
                ]
            ],
            [
                "id" => 729,
                "rta_id" => 539,
                "name" => "Turkey - Chile",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "2009-07-14",
                "notification_date" => "2011-02-25",
                "entry_force_date" => "2011-03-01",
                "current_signatories" => [
                    "Chile",
                    "Turkey"
                ],
                "original_signatories" => [
                    "Chile",
                    "Turkey"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "South America",
                    "Europe"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.direcon.gob.cl/",
                        "name" => "Dirección General de Relaciones Económicas Internacionales, Chile"
                    ],
                    [
                        "url" => "http://www.economy.gov.tr/",
                        "name" => "Turkish Government"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.direcon.gob.cl/wp-content/uploads/2011/12/TLC_CHILE_TURQUIA_English.pdf",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://www.direcon.gob.cl/detalle-de-acuerdos/?idacuerdo=6284#tabs-2",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Chile"
                ],
                "countries_second" => [
                    "Turkey"
                ]
            ],
            [
                "id" => 720,
                "rta_id" => 502,
                "name" => "Turkey - Jordan",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "2009-12-01",
                "notification_date" => "2011-03-07",
                "entry_force_date" => "2011-03-01",
                "current_signatories" => [
                    "Jordan",
                    "Turkey"
                ],
                "original_signatories" => [
                    "Jordan",
                    "Turkey"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "Middle East",
                    "Europe"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.economy.gov.tr/",
                        "name" => "Turkish Government"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.ekonomi.gov.tr/portal/content/conn/UCM/path/Contribution%20Folders/web_en/Trade%20Agreements/Free%20Trade%20Agreements/Jordan/ekler/2.%20Turkey-Jordan%20Agreement.pdf?lve",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Jordan"
                ],
                "countries_second" => [
                    "Turkey"
                ]
            ],
            [
                "id" => 727,
                "rta_id" => 530,
                "name" => "United States - Colombia",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2006-11-22",
                "notification_date" => "2012-05-08",
                "entry_force_date" => "2012-05-15",
                "current_signatories" => [
                    "Colombia",
                    "USA"
                ],
                "original_signatories" => [
                    "Colombia",
                    "USA"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "South America",
                    "North America"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.ustr.gov/",
                        "name" => "Office of the United States Trade Representative"
                    ],
                    [
                        "url" => "http://www.tlc.gov.co/index.php",
                        "name" => "Ministerio de Comercio, Industria y Turismo, Colombia"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.ustr.gov/trade-agreements/free-trade-agreements/colombia-fta/final-text",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://www.tlc.gov.co/publicaciones.php?id=727",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Colombia"
                ],
                "countries_second" => [
                    "USA"
                ]
            ],
            [
                "id" => 719,
                "rta_id" => 499,
                "name" => "Ukraine - Singapore",
                "coverage" => "",
                "status" => "Early announcement-Under negotiation",
                "notification_under" => "",
                "signature_date" => null,
                "notification_date" => null,
                "entry_force_date" => null,
                "current_signatories" => [
                    ""
                ],
                "original_signatories" => [
                    ""
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "East Asia",
                    "Commonwealth of Independent States (CIS), including associate and former member States"
                ],
                "wto_members" => true,
                "type" => "",
                "web_addresses" => [
                    [
                        "url" => "http://www.me.gov.ua/?lang=en-GB",
                        "name" => "The Ministry of Economy of Ukraine"
                    ],
                    [
                        "url" => "http://www.iesingapore.gov.sg/",
                        "name" => "International Enterprise Singapore"
                    ]
                ],
                "agreement" => [],
                "countries_first" => [
                    "Ukraine"
                ],
                "countries_second" => [
                    "Singapore"
                ]
            ],
            [
                "id" => 725,
                "rta_id" => 520,
                "name" => "Southern Common Market (MERCOSUR) - India",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "Enabling Clause",
                "signature_date" => "2004-01-25",
                "notification_date" => "2010-02-23",
                "entry_force_date" => "2009-06-01",
                "current_signatories" => [
                    "Argentina",
                    "Brazil",
                    "Paraguay",
                    "Uruguay",
                    "India"
                ],
                "original_signatories" => [
                    "Argentina",
                    "Brazil",
                    "Paraguay",
                    "Uruguay",
                    "India"
                ],
                "rta_composition" => [
                    "Bilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "South America",
                    "West Asia"
                ],
                "wto_members" => true,
                "type" => "Partial Scope Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.mercosur.int/",
                        "name" => "Southern Common Market"
                    ],
                    [
                        "url" => "http://commerce.gov.in",
                        "name" => "Government of India, Ministry of Commerce & Industry"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://commerce.gov.in/PageContent.aspx?Id=46",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://www.sice.oas.org/Trade/MRCSRIndia/Index_s.asp",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "India"
                ],
                "countries_second" => [
                    "Argentina",
                    "Brazil",
                    "Paraguay",
                    "Uruguay"
                ]
            ],
            [
                "id" => 735,
                "rta_id" => 566,
                "name" => "Iceland - Faroe Islands",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2005-08-31",
                "notification_date" => "2008-07-10",
                "entry_force_date" => "2006-11-01",
                "current_signatories" => [
                    "Faeroe Islands",
                    "Iceland"
                ],
                "original_signatories" => [
                    "Faeroe Islands",
                    "Iceland"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "Europe"
                ],
                "wto_members" => false,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.mfa.is",
                        "name" => "Ministry of Foreign Affairs, Iceland"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "https://d3b1dqw2kzexi.cloudfront.net/media/5351/hoyvikssattmalin-en.pdf",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Faeroe Islands"
                ],
                "countries_second" => [
                    "Iceland"
                ]
            ],
            [
                "id" => 733,
                "rta_id" => 562,
                "name" => "India - Bhutan",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "Enabling Clause",
                "signature_date" => "2006-07-28",
                "notification_date" => "2008-06-30",
                "entry_force_date" => "2006-07-29",
                "current_signatories" => [
                    "Bhutan",
                    "India"
                ],
                "original_signatories" => [
                    "Bhutan",
                    "India"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "West Asia"
                ],
                "wto_members" => false,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.commerce.nic.in",
                        "name" => "Ministry of Commerce and Industry, Government of India"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.commerce.nic.in/trade/bhutan.pdf",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Bhutan"
                ],
                "countries_second" => [
                    "India"
                ]
            ],
            [
                "id" => 738,
                "rta_id" => 570,
                "name" => "Ukraine - Azerbaijan",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "1995-07-28",
                "notification_date" => "2008-08-18",
                "entry_force_date" => "1996-09-02",
                "current_signatories" => [
                    "Azerbaijan",
                    "Ukraine"
                ],
                "original_signatories" => [
                    "Azerbaijan",
                    "Ukraine"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "Commonwealth of Independent States (CIS), including associate and former member States"
                ],
                "wto_members" => false,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.me.gov.ua/?lang=en-GB",
                        "name" => "The Ministry of Economy of Ukraine"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "../rtadocs/570/TOA/English/FTA%20Ukraine%20-%20Azerbaijan_ToA.doc",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Azerbaijan"
                ],
                "countries_second" => [
                    "Ukraine"
                ]
            ],
            [
                "id" => 739,
                "rta_id" => 571,
                "name" => "Ukraine - Belarus",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "1992-12-17",
                "notification_date" => "2008-08-18",
                "entry_force_date" => "2006-11-11",
                "current_signatories" => [
                    "Belarus",
                    "Ukraine"
                ],
                "original_signatories" => [
                    "Belarus",
                    "Ukraine"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "Commonwealth of Independent States (CIS), including associate and former member States"
                ],
                "wto_members" => false,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.me.gov.ua/?lang=en-GB",
                        "name" => "The Ministry of Economy of Ukraine"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "../rtadocs/571/TOA/English/FTA%20Ukraine%20-%20Belarus_ToA.doc",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Belarus"
                ],
                "countries_second" => [
                    "Ukraine"
                ]
            ],
            [
                "id" => 740,
                "rta_id" => 572,
                "name" => "Ukraine - Kazakhstan",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "1994-09-17",
                "notification_date" => "2008-08-18",
                "entry_force_date" => "1998-10-19",
                "current_signatories" => [
                    "Kazakhstan",
                    "Ukraine"
                ],
                "original_signatories" => [
                    "Kazakhstan",
                    "Ukraine"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "Commonwealth of Independent States (CIS), including associate and former member States"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.me.gov.ua/?lang=en-GB",
                        "name" => "The Ministry of Economy of Ukraine"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "../rtadocs/572/TOA/English/FTA%20Ukraine%20-%20Kazakhstan_ToA.doc",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Kazakhstan"
                ],
                "countries_second" => [
                    "Ukraine"
                ]
            ],
            [
                "id" => 742,
                "rta_id" => 574,
                "name" => "Ukraine - Moldova, Republic of",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "2003-11-13",
                "notification_date" => "2008-08-18",
                "entry_force_date" => "2005-05-19",
                "current_signatories" => [
                    "Moldova",
                    "Ukraine"
                ],
                "original_signatories" => [
                    "Moldova",
                    "Ukraine"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "Commonwealth of Independent States (CIS), including associate and former member States"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://mec.gov.md/",
                        "name" => "Ministerul Economiei si Comertului al Republicii Moldova"
                    ],
                    [
                        "url" => "http://www.me.gov.ua/?lang=en-GB",
                        "name" => "The Ministry of Economy of Ukraine"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "../rtadocs/574/TOA/English/FTA%20Ukraine%20-%20Moldova_ToA.doc",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Moldova"
                ],
                "countries_second" => [
                    "Ukraine"
                ]
            ],
            [
                "id" => 743,
                "rta_id" => 576,
                "name" => "Ukraine - Tajikistan",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "2001-07-06",
                "notification_date" => "2008-08-18",
                "entry_force_date" => "2002-07-11",
                "current_signatories" => [
                    "Tajikistan",
                    "Ukraine"
                ],
                "original_signatories" => [
                    "Tajikistan",
                    "Ukraine"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "Commonwealth of Independent States (CIS), including associate and former member States"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.me.gov.ua/?lang=en-GB",
                        "name" => "The Ministry of Economy of Ukraine"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "../rtadocs/576/TOA/English/FTA%20Ukraine%20-%20Tajikistan_ToA.doc",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Tajikistan"
                ],
                "countries_second" => [
                    "Ukraine"
                ]
            ],
            [
                "id" => 741,
                "rta_id" => 573,
                "name" => "Ukraine - The former Yugoslav Republic of Macedonia",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "2001-01-18",
                "notification_date" => "2008-08-18",
                "entry_force_date" => "2001-07-05",
                "current_signatories" => [
                    "Macedonia",
                    "Ukraine"
                ],
                "original_signatories" => [
                    "Macedonia",
                    "Ukraine"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "Europe",
                    "Commonwealth of Independent States (CIS), including associate and former member States"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.me.gov.ua/?lang=en-GB",
                        "name" => "The Ministry of Economy of Ukraine"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "../rtadocs/573/TOA/English/Ukraine-FYROM-Agreement.doc",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Ukraine"
                ],
                "countries_second" => [
                    "Macedonia"
                ]
            ],
            [
                "id" => 745,
                "rta_id" => 578,
                "name" => "Ukraine - Uzbekistan",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "1994-12-29",
                "notification_date" => "2008-08-18",
                "entry_force_date" => "1996-01-01",
                "current_signatories" => [
                    "Ukraine",
                    "Uzbekistan"
                ],
                "original_signatories" => [
                    "Ukraine",
                    "Uzbekistan"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "Commonwealth of Independent States (CIS), including associate and former member States"
                ],
                "wto_members" => false,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.me.gov.ua/?lang=en-GB",
                        "name" => "The Ministry of Economy of Ukraine"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "../rtadocs/578/TOA/English/FTA%20Ukraine%20-%20Uzbekistan_ToA.doc",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Ukraine"
                ],
                "countries_second" => [
                    "Uzbekistan"
                ]
            ],
            [
                "id" => 744,
                "rta_id" => 577,
                "name" => "Ukraine -Turkmenistan",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "1994-11-05",
                "notification_date" => "2008-08-18",
                "entry_force_date" => "1995-11-04",
                "current_signatories" => [
                    "Turkmenistan",
                    "Ukraine"
                ],
                "original_signatories" => [
                    "Turkmenistan",
                    "Ukraine"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "Commonwealth of Independent States (CIS), including associate and former member States"
                ],
                "wto_members" => false,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.me.gov.ua/?lang=en-GB",
                        "name" => "The Ministry of Economy of Ukraine"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "../rtadocs/577/TOA/English/FTA%20Ukraine%20-%20Turkmenistan_ToA.doc",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Turkmenistan"
                ],
                "countries_second" => [
                    "Ukraine"
                ]
            ],
            [
                "id" => 746,
                "rta_id" => 579,
                "name" => "Common Economic Zone (CEZ)",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "2003-09-19",
                "notification_date" => "2008-08-18",
                "entry_force_date" => "2004-05-20",
                "current_signatories" => [
                    "Belarus",
                    "Kazakhstan",
                    "Russia",
                    "Ukraine"
                ],
                "original_signatories" => [
                    "Belarus",
                    "Kazakhstan",
                    "Russia",
                    "Ukraine"
                ],
                "rta_composition" => [
                    "Plurilateral"
                ],
                "region" => [
                    "Commonwealth of Independent States (CIS), including associate and former member States"
                ],
                "wto_members" => false,
                "type" => "Free Trade Agreement",
                "web_addresses" => [],
                "agreement" => [
                    [
                        "url" => "../rtadocs/579/TOA/English/FTA%20Ukraine%20-%20Common%20Economic%20Zone_ToA.doc",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Belarus"
                ],
                "countries_second" => [
                    "Kazakhstan",
                    "Russia",
                    "Ukraine"
                ]
            ],
            [
                "id" => 768,
                "rta_id" => 638,
                "name" => "Mauritius - Pakistan",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "Enabling Clause",
                "signature_date" => "2007-07-30",
                "notification_date" => "2015-10-02",
                "entry_force_date" => "2007-11-30",
                "current_signatories" => [
                    "Mauritius",
                    "Pakistan"
                ],
                "original_signatories" => [
                    "Mauritius",
                    "Pakistan"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "Africa",
                    "West Asia"
                ],
                "wto_members" => true,
                "type" => "Partial Scope Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.mcci.org/en/",
                        "name" => "The Mauritius Chamber of Commerce and Industry"
                    ],
                    [
                        "url" => "http://www.commerce.gov.pk/",
                        "name" => "Government of Pakistan, Ministry of Commerce"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.mcci.org/en/global-marketplace/trade-agreements/mauritius-pakistan-pta/",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Mauritius"
                ],
                "countries_second" => [
                    "Pakistan"
                ]
            ],
            [
                "id" => 753,
                "rta_id" => 603,
                "name" => "EFTA - Serbia",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "2009-12-17",
                "notification_date" => "2010-11-24",
                "entry_force_date" => "2010-10-01",
                "current_signatories" => [
                    "Iceland",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland",
                    "Serbia"
                ],
                "original_signatories" => [
                    "Iceland",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland",
                    "Serbia"
                ],
                "rta_composition" => [
                    "Bilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "Europe"
                ],
                "wto_members" => false,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.efta.int/",
                        "name" => "European Free Trade Association"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.efta.int/free-trade/free-trade-agreements/serbia.aspx",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Serbia"
                ],
                "countries_second" => [
                    "Iceland",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland"
                ]
            ],
            [
                "id" => 750,
                "rta_id" => 598,
                "name" => "EFTA - India",
                "coverage" => "",
                "status" => "Early announcement-Under negotiation",
                "notification_under" => "",
                "signature_date" => null,
                "notification_date" => null,
                "entry_force_date" => null,
                "current_signatories" => [
                    ""
                ],
                "original_signatories" => [
                    ""
                ],
                "rta_composition" => [
                    "Plurilateral"
                ],
                "region" => [
                    "Europe",
                    "West Asia"
                ],
                "wto_members" => true,
                "type" => "",
                "web_addresses" => [
                    [
                        "url" => "http://www.efta.int/",
                        "name" => "European Free Trade Association"
                    ],
                    [
                        "url" => "http://www.commerce.nic.in/",
                        "name" => "Government of India, Ministry of Commerce & Industry"
                    ]
                ],
                "agreement" => [],
                "countries_first" => [
                    "India"
                ],
                "countries_second" => [
                    "Iceland",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland"
                ]
            ],
            [
                "id" => 752,
                "rta_id" => 601,
                "name" => "EFTA - Indonesia",
                "coverage" => "",
                "status" => "Early announcement-Under negotiation",
                "notification_under" => "",
                "signature_date" => null,
                "notification_date" => null,
                "entry_force_date" => null,
                "current_signatories" => [
                    ""
                ],
                "original_signatories" => [
                    ""
                ],
                "rta_composition" => [
                    "Plurilateral"
                ],
                "region" => [
                    "Europe",
                    "East Asia"
                ],
                "wto_members" => true,
                "type" => "",
                "web_addresses" => [
                    [
                        "url" => "http://www.efta.int/",
                        "name" => "European Free Trade Association"
                    ]
                ],
                "agreement" => [],
                "countries_first" => [
                    "Indonesia"
                ],
                "countries_second" => [
                    "Iceland",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland"
                ]
            ],
            [
                "id" => 748,
                "rta_id" => 582,
                "name" => "Pacific Island Countries Trade Agreement (PICTA)",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "Enabling Clause",
                "signature_date" => "2001-08-18",
                "notification_date" => "2008-08-28",
                "entry_force_date" => "2003-04-13",
                "current_signatories" => [
                    "Cook Islands",
                    "Fiji",
                    "Kiribati",
                    "Micronesia, Federated States of",
                    "Nauru",
                    "Niue",
                    "Papua New Guinea",
                    "Samoa",
                    "Solomon Islands",
                    "Tonga",
                    "Tuvalu",
                    "Vanuatu"
                ],
                "original_signatories" => [
                    "Cook Islands",
                    "Fiji",
                    "Kiribati",
                    "Micronesia, Federated States of",
                    "Nauru",
                    "Niue",
                    "Papua New Guinea",
                    "Samoa",
                    "Solomon Islands",
                    "Tonga",
                    "Tuvalu",
                    "Vanuatu"
                ],
                "rta_composition" => [
                    "Plurilateral"
                ],
                "region" => [
                    "Oceania"
                ],
                "wto_members" => false,
                "type" => "Free Trade Agreement",
                "web_addresses" => [],
                "agreement" => [
                    [
                        "url" => "../rtadocs/582/TOA/English/PICTA-text.pdf",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Solomon Islands"
                ],
                "countries_second" => [
                    "Cook Islands",
                    "Fiji",
                    "Kiribati",
                    "Nauru",
                    "Vanuatu",
                    "Niue",
                    "Micronesia, Federated States of",
                    "Papua New Guinea",
                    "Tonga",
                    "Tuvalu",
                    "Samoa"
                ]
            ],
            [
                "id" => 749,
                "rta_id" => 583,
                "name" => "Agadir Agreement",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "Enabling Clause",
                "signature_date" => "2004-02-25",
                "notification_date" => "2016-02-22",
                "entry_force_date" => "2007-03-27",
                "current_signatories" => [
                    "Egypt",
                    "Jordan",
                    "Morocco",
                    "Tunisia"
                ],
                "original_signatories" => [
                    "Egypt",
                    "Jordan",
                    "Morocco",
                    "Tunisia"
                ],
                "rta_composition" => [
                    "Plurilateral"
                ],
                "region" => [
                    "Africa",
                    "Middle East"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement",
                "web_addresses" => [],
                "agreement" => [
                    [
                        "url" => "../rtadocs/583/TOA/English/ToA.pdf",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Jordan"
                ],
                "countries_second" => [
                    "Morocco",
                    "Tunisia",
                    "Egypt"
                ]
            ],
            [
                "id" => 751,
                "rta_id" => 600,
                "name" => "EFTA - Albania",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "2009-12-17",
                "notification_date" => "2011-02-07",
                "entry_force_date" => "2010-11-01",
                "current_signatories" => [
                    "Albania",
                    "Iceland",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland"
                ],
                "original_signatories" => [
                    "Albania",
                    "Iceland",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland"
                ],
                "rta_composition" => [
                    "Bilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "Europe"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.efta.int/",
                        "name" => "European Free Trade Association"
                    ],
                    [
                        "url" => "http://www.ekonomia.gov.al/",
                        "name" => "Ministry of Economy, Albania"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.efta.int/free-trade/free-trade-agreements/albania.aspx",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Albania"
                ],
                "countries_second" => [
                    "Iceland",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland"
                ]
            ],
            [
                "id" => 760,
                "rta_id" => 622,
                "name" => "Japan - Philippines",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2006-09-09",
                "notification_date" => "2008-12-11",
                "entry_force_date" => "2008-12-11",
                "current_signatories" => [
                    "Japan",
                    "Philippines"
                ],
                "original_signatories" => [
                    "Japan",
                    "Philippines"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "East Asia"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.mofa.go.jp/",
                        "name" => "Ministry of Foreign Affairs, Japan"
                    ],
                    [
                        "url" => "http://www.dti.gov.ph/",
                        "name" => "Department of Trade and Industry, Philippines"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.mofa.go.jp/region/asia-paci/philippine/epa0609/index.html",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Japan"
                ],
                "countries_second" => [
                    "Philippines"
                ]
            ],
            [
                "id" => 766,
                "rta_id" => 635,
                "name" => "Switzerland - China",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2013-07-06",
                "notification_date" => "2014-06-30",
                "entry_force_date" => "2014-07-01",
                "current_signatories" => [
                    "China",
                    "Switzerland"
                ],
                "original_signatories" => [
                    "China",
                    "Switzerland"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "East Asia",
                    "Europe"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "https://www.seco.admin.ch/seco/fr/home.html",
                        "name" => "Confédération Suisse"
                    ],
                    [
                        "url" => "http://fta.mofcom.gov.cn/enarticle/chinaswisen/chinaswissennews/201102/5083_1.html",
                        "name" => "Ministry of Commerce of the People's Republic of China"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "https://www.seco.admin.ch/seco/en/home/Aussenwirtschaftspolitik_Wirtschaftliche_Zusammenarbeit/Wirtschaftsbeziehungen/Freihandelsabkommen/Partner_weltweit/china/Abkommenstexte.html",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "China"
                ],
                "countries_second" => [
                    "Switzerland"
                ]
            ],
            [
                "id" => 767,
                "rta_id" => 636,
                "name" => "Turkey - Georgia",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "2007-11-21",
                "notification_date" => "2009-02-18",
                "entry_force_date" => "2008-11-01",
                "current_signatories" => [
                    "Georgia",
                    "Turkey"
                ],
                "original_signatories" => [
                    "Georgia",
                    "Turkey"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "Commonwealth of Independent States (CIS), including associate and former member States",
                    "Europe"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.economy.gov.tr/",
                        "name" => "Turkish Government"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "../rtadocs/636/TOA/English/main%20text.doc",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Georgia"
                ],
                "countries_second" => [
                    "Turkey"
                ]
            ],
            [
                "id" => 763,
                "rta_id" => 627,
                "name" => "Turkey - Serbia",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "2009-06-01",
                "notification_date" => "2010-08-10",
                "entry_force_date" => "2010-09-01",
                "current_signatories" => [
                    "Serbia",
                    "Turkey"
                ],
                "original_signatories" => [
                    "Serbia",
                    "Turkey"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "Europe"
                ],
                "wto_members" => false,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.economy.gov.tr/",
                        "name" => "Turkish Government"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "../rtadocs/627/TOA/English/TR-SERBIA%20STA%20main%20text1.doc",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Turkey"
                ],
                "countries_second" => [
                    "Serbia"
                ]
            ],
            [
                "id" => 765,
                "rta_id" => 634,
                "name" => "China - Norway",
                "coverage" => "Goods & Services",
                "status" => "Early announcement-Under negotiation",
                "notification_under" => "",
                "signature_date" => null,
                "notification_date" => null,
                "entry_force_date" => null,
                "current_signatories" => [
                    ""
                ],
                "original_signatories" => [
                    ""
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "East Asia",
                    "Europe"
                ],
                "wto_members" => true,
                "type" => "",
                "web_addresses" => [
                    [
                        "url" => "http://www.mofcom.gov.cn/",
                        "name" => "Ministry of Commerce of the People's Republic of China"
                    ],
                    [
                        "url" => "http://www.norway.info/",
                        "name" => "Royal Norwegian Ministry of Foreign Affairs"
                    ]
                ],
                "agreement" => [],
                "countries_first" => [
                    "China"
                ],
                "countries_second" => [
                    "Norway"
                ]
            ],
            [
                "id" => 764,
                "rta_id" => 631,
                "name" => "Southern Common Market (MERCOSUR) - Mexico",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "Enabling Clause",
                "signature_date" => "2016-07-07",
                "notification_date" => "2017-06-15",
                "entry_force_date" => "2016-12-28",
                "current_signatories" => [
                    "Argentina",
                    "Brazil",
                    "Paraguay",
                    "Uruguay",
                    "Mexico"
                ],
                "original_signatories" => [
                    "Argentina",
                    "Brazil",
                    "Paraguay",
                    "Uruguay",
                    "Mexico"
                ],
                "rta_composition" => [
                    "Bilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "South America",
                    "North America"
                ],
                "wto_members" => true,
                "type" => "Partial Scope Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.mercosur.int/",
                        "name" => "Southern Common Market"
                    ],
                    [
                        "url" => "http://www.gob.mx/se/",
                        "name" => "Secretaría de Economía de México"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.aladi.org/biblioteca/publicaciones/aladi/acuerdos/ace/es/ace55/ACE_055_006_Apendice_002.pdf",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Mexico"
                ],
                "countries_second" => [
                    "Argentina",
                    "Brazil",
                    "Paraguay",
                    "Uruguay"
                ]
            ],
            [
                "id" => 758,
                "rta_id" => 620,
                "name" => "GUAM",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2002-07-20",
                "notification_date" => "2017-04-03",
                "entry_force_date" => "2003-12-10",
                "current_signatories" => [
                    "Azerbaijan",
                    "Georgia",
                    "Moldova",
                    "Ukraine"
                ],
                "original_signatories" => [
                    "Azerbaijan",
                    "Georgia",
                    "Moldova",
                    "Ukraine"
                ],
                "rta_composition" => [
                    "Plurilateral"
                ],
                "region" => [
                    "Commonwealth of Independent States (CIS), including associate and former member States"
                ],
                "wto_members" => false,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://guam-organization.org",
                        "name" => "GUAM Organization"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "../rtadocs/620/TOA/English/GUUAM%20Agreement.pdf",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Azerbaijan"
                ],
                "countries_second" => [
                    "Georgia",
                    "Moldova",
                    "Ukraine"
                ]
            ],
            [
                "id" => 759,
                "rta_id" => 621,
                "name" => "EFTA - SACU",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "2006-06-26",
                "notification_date" => "2008-10-29",
                "entry_force_date" => "2008-05-01",
                "current_signatories" => [
                    "Botswana",
                    "Lesotho",
                    "Namibia",
                    "South Africa",
                    "Swaziland",
                    "Iceland",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland"
                ],
                "original_signatories" => [
                    "Botswana",
                    "Lesotho",
                    "Namibia",
                    "South Africa",
                    "Swaziland",
                    "Iceland",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland"
                ],
                "rta_composition" => [
                    "Bilateral",
                    "All Parties are RTAs"
                ],
                "region" => [
                    "Africa",
                    "Europe"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.efta.int",
                        "name" => "European Free Trade Association"
                    ],
                    [
                        "url" => "http://www.sacu.int",
                        "name" => "Southern African Customs Union"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.efta.int/~/media/Documents/legal-texts/free-trade-relations/southern-african-customs-union-SACU/EFTA-SACU%20Free%20Trade%20Agreement.pdf",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Norway",
                    "Switzerland",
                    "",
                    "Iceland"
                ],
                "countries_second" => [
                    "Botswana",
                    "Lesotho",
                    "Namibia",
                    "South Africa",
                    "Swaziland",
                    "Liechtenstein"
                ]
            ],
            [
                "id" => 771,
                "rta_id" => 646,
                "name" => "Chile - Nicaragua (Chile - Central America)",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "1999-10-19",
                "notification_date" => "2013-06-14",
                "entry_force_date" => "2012-10-19",
                "current_signatories" => [
                    "Chile",
                    "Nicaragua"
                ],
                "original_signatories" => [
                    "Chile",
                    "Nicaragua"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "South America",
                    "Central America"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.direcon.gob.cl/",
                        "name" => "Dirección General de Relaciones Económicas Internacionales, Chile"
                    ],
                    [
                        "url" => "http://www.mific.gob.ni/",
                        "name" => "Ministerio de Fomento, Industria y Comercio, Nicaragua"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.direcon.gob.cl/detalle-de-acuerdos/?idacuerdo=6290",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Chile"
                ],
                "countries_second" => [
                    "Nicaragua"
                ]
            ],
            [
                "id" => 775,
                "rta_id" => 664,
                "name" => "China - New Zealand",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2008-04-07",
                "notification_date" => "2009-04-21",
                "entry_force_date" => "2008-10-01",
                "current_signatories" => [
                    "China",
                    "New Zealand"
                ],
                "original_signatories" => [
                    "China",
                    "New Zealand"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "East Asia",
                    "Oceania"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.mofcom.gov.cn/",
                        "name" => "Ministry of Commerce of the People's Republic of China"
                    ],
                    [
                        "url" => "http://www.mfat.govt.nz./",
                        "name" => "New Zealand Ministry of Foreign Affairs and Trade"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "https://mfat.govt.nz/en/trade/free-trade-agreements/free-trade-agreements-in-force/nz-china-free-trade-agreement/text-of-the-new-zealand-china-fta-agreement/",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "China"
                ],
                "countries_second" => [
                    "New Zealand"
                ]
            ],
            [
                "id" => 772,
                "rta_id" => 649,
                "name" => "Colombia - Mexico",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "1994-06-13",
                "notification_date" => "2010-09-13",
                "entry_force_date" => "1995-01-01",
                "current_signatories" => [
                    "Colombia",
                    "Mexico"
                ],
                "original_signatories" => [
                    "Colombia",
                    "Mexico"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "South America",
                    "North America"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.tlc.gov.co/index.php",
                        "name" => "Ministerio de Comercio, Industria y Turismo, Colombia"
                    ],
                    [
                        "url" => "http://www.economia.gob.mx",
                        "name" => "Secretaría de Economía, México"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.economia.gob.mx/files/comunidad_negocios/tlcs/tlcs_americalatina/TLC_Mexico-Colombia-Venezuela.pdf",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Colombia"
                ],
                "countries_second" => [
                    "Mexico"
                ]
            ],
            [
                "id" => 769,
                "rta_id" => 643,
                "name" => "Korea, Republic of - New Zealand",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2015-03-23",
                "notification_date" => "2015-12-21",
                "entry_force_date" => "2015-12-20",
                "current_signatories" => [
                    "North Korea",
                    "New Zealand"
                ],
                "original_signatories" => [
                    "North Korea",
                    "New Zealand"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "East Asia",
                    "Oceania"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.mofa.go.kr/eng/index.do",
                        "name" => "Ministry of Foreign Affairs and Trade, Republic of Korea"
                    ],
                    [
                        "url" => "https://www.mfat.govt.nz/",
                        "name" => "New Zealand Ministry of Foreign Affairs and Trade"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "https://www.mfat.govt.nz/en/trade/free-trade-agreements/free-trade-agreements-in-force/nz-korea-free-trade-agreement/text-of-the-new-zealand-korea-fta-agreement/",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "North Korea"
                ],
                "countries_second" => [
                    "New Zealand"
                ]
            ],
            [
                "id" => 773,
                "rta_id" => 651,
                "name" => "Mexico - Uruguay",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2003-11-15",
                "notification_date" => "2013-06-28",
                "entry_force_date" => "2004-07-15",
                "current_signatories" => [
                    "Mexico",
                    "Uruguay"
                ],
                "original_signatories" => [
                    "Mexico",
                    "Uruguay"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "North America",
                    "South America"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.economia.gob.mx/",
                        "name" => "Secretaría de Economía, México"
                    ],
                    [
                        "url" => "http://www.mrree.gub.uy/frontend/",
                        "name" => "Ministerio de Relaciones Exteriores de Uruguay"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.economia.gob.mx/files/comunidad_negocios/tlcs/tlcs_americalatina/2004-07-14_TLC_Mexico-Uruguay.pdf",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Mexico"
                ],
                "countries_second" => [
                    "Uruguay"
                ]
            ],
            [
                "id" => 777,
                "rta_id" => 671,
                "name" => "Nicaragua - Chinese Taipei",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2006-06-16",
                "notification_date" => "2009-07-09",
                "entry_force_date" => "2008-01-01",
                "current_signatories" => [
                    "Taiwan",
                    "Nicaragua"
                ],
                "original_signatories" => [
                    "Taiwan",
                    "Nicaragua"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "East Asia",
                    "Central America"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.mific.gob.ni/",
                        "name" => "Ministerio de Fomento, Industria y Comercio, Nicaragua"
                    ],
                    [
                        "url" => "http://eweb.trade.gov.tw/mp.asp?mp=2",
                        "name" => "Bureau of Foreign Trade, The Separate Customs Territory of Taiwan, Penghu, Kinmen and Matsu"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "../rtadocs/671/TOA/English/Nicaragua-TPKM%20Agreement.pdf",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://www.mific.gob.ni/INICIO/COMERCIOEXTERIOR/ACUERDOSCOMERCIALESVIGENTES/NicTaiwan/tabid/426/language/en-US/Default.aspx",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Taiwan"
                ],
                "countries_second" => [
                    "Nicaragua"
                ]
            ],
            [
                "id" => 776,
                "rta_id" => 666,
                "name" => "Peru - China",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2009-04-28",
                "notification_date" => "2010-03-03",
                "entry_force_date" => "2010-03-01",
                "current_signatories" => [
                    "China",
                    "Peru"
                ],
                "original_signatories" => [
                    "China",
                    "Peru"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "East Asia",
                    "South America"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.mincetur.gob.pe/newweb/",
                        "name" => "Ministerio de Comercio Exterior de Perú"
                    ],
                    [
                        "url" => "http://www.mofcom.gov.cn/",
                        "name" => "Ministry of Commerce of the People's Republic of China"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://fta.mofcom.gov.cn/topic/enperu.shtml",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://www.acuerdoscomerciales.gob.pe/index.php?option=com_content&amp;view=category&amp;layout=blog&amp;id=39&amp;Itemid=56",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "China"
                ],
                "countries_second" => [
                    "Peru"
                ]
            ],
            [
                "id" => 770,
                "rta_id" => 645,
                "name" => "Peru - Korea, Republic of",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2011-03-21",
                "notification_date" => "2011-08-09",
                "entry_force_date" => "2011-08-01",
                "current_signatories" => [
                    "North Korea",
                    "Peru"
                ],
                "original_signatories" => [
                    "North Korea",
                    "Peru"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "East Asia",
                    "South America"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.mofa.go.kr/eng/index.do",
                        "name" => "Ministry of Foreign Affairs and Trade, Republic of Korea"
                    ],
                    [
                        "url" => "http://www.mincetur.gob.pe/newweb/",
                        "name" => "Ministerio de Comercio Exterior de Perú"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.acuerdoscomerciales.gob.pe/index.php?option=com_content&amp;view=category&amp;layout=blog&amp;id=82&amp;Itemid=105",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://www.acuerdoscomerciales.gob.pe/index.php?option=com_content&amp;view=category&amp;layout=blog&amp;id=82&amp;Itemid=105",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "North Korea"
                ],
                "countries_second" => [
                    "Peru"
                ]
            ],
            [
                "id" => 774,
                "rta_id" => 662,
                "name" => "Peru - Mexico",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2011-04-06",
                "notification_date" => "2012-02-22",
                "entry_force_date" => "2012-02-01",
                "current_signatories" => [
                    "Mexico",
                    "Peru"
                ],
                "original_signatories" => [
                    "Mexico",
                    "Peru"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "North America",
                    "South America"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.economia.gob.mx/",
                        "name" => "Secretaría de Economía, México"
                    ],
                    [
                        "url" => "http://www.mincetur.gob.pe/newweb/",
                        "name" => "Ministerio de Comercio Exterior de Perú"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.acuerdoscomerciales.gob.pe/index.php?option=com_content&amp;view=category&amp;layout=blog&amp;id=77&amp;Itemid=100",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Mexico"
                ],
                "countries_second" => [
                    "Peru"
                ]
            ],
            [
                "id" => 785,
                "rta_id" => 685,
                "name" => "Chile - Viet Nam",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "2011-11-11",
                "notification_date" => "2015-05-12",
                "entry_force_date" => "2014-01-01",
                "current_signatories" => [
                    "Chile",
                    "Vietnam"
                ],
                "original_signatories" => [
                    "Chile",
                    "Vietnam"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "South America",
                    "East Asia"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.direcon.gob.cl/",
                        "name" => "Dirección General de Relaciones Económicas Internacionales, Chile"
                    ],
                    [
                        "url" => "http://www.moit.gov.vn/en/Pages/default.aspx",
                        "name" => "Ministry of Industry and Trade of the Socialist Republic of Viet Nam"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://wtocenter.vn/sites/wtocenter.vn/files/other-agreements/attachments/fta_chile_vietnam._english_text.pdf",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://www.direcon.gob.cl/detalle-de-acuerdos/?idacuerdo=11857",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Chile"
                ],
                "countries_second" => [
                    "Vietnam"
                ]
            ],
            [
                "id" => 780,
                "rta_id" => 677,
                "name" => "China - Costa Rica",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2010-04-08",
                "notification_date" => "2012-02-27",
                "entry_force_date" => "2011-08-01",
                "current_signatories" => [
                    "China",
                    "Costa Rica"
                ],
                "original_signatories" => [
                    "China",
                    "Costa Rica"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "East Asia",
                    "Central America"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.comex.go.cr",
                        "name" => "Ministerio de Comercio Exterior de Costa Rica"
                    ],
                    [
                        "url" => "http://www.mofcom.gov.cn/",
                        "name" => "Ministry of Commerce of the People's Republic of China"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.sice.oas.org/Trade/CRI_CHN_FTA/Texts_Apr2010_e/CRI_CHN_ToC__PDF_e.asp",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://www.comex.go.cr/tratados/china/texto-del-tratado-1/",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "China"
                ],
                "countries_second" => [
                    "Costa Rica"
                ]
            ],
            [
                "id" => 782,
                "rta_id" => 680,
                "name" => "EU - Cameroon",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "2009-01-15",
                "notification_date" => "2009-09-24",
                "entry_force_date" => "2014-08-04",
                "current_signatories" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom",
                    "Cameroon"
                ],
                "original_signatories" => [
                    "Cameroon"
                ],
                "rta_composition" => [
                    "Bilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "Europe",
                    "Africa"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://ec.europa.eu/",
                        "name" => "European Commission"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1399560086045&amp;uri=CELEX:22009A0228(01)",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1399560086045&amp;uri=CELEX:22009A0228(01)",
                        "name" => "F"
                    ],
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1399560086045&amp;uri=CELEX:22009A0228(01)",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Cameroon"
                ],
                "countries_second" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom"
                ]
            ],
            [
                "id" => 779,
                "rta_id" => 676,
                "name" => "EU - Central America",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2012-06-29",
                "notification_date" => "2013-02-26",
                "entry_force_date" => "2013-08-01",
                "current_signatories" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom",
                    "Costa Rica",
                    "El Salvador",
                    "Guatemala",
                    "Honduras",
                    "Nicaragua",
                    "Panama"
                ],
                "original_signatories" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom",
                    "Costa Rica",
                    "El Salvador",
                    "Guatemala",
                    "Honduras",
                    "Nicaragua",
                    "Panama"
                ],
                "rta_composition" => [
                    "Plurilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "Europe",
                    "Central America"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://ec.europa.eu/",
                        "name" => "European Commission"
                    ],
                    [
                        "url" => "http://www.comex.go.cr/",
                        "name" => "Ministerio de Comercio Exterior de Costa Rica"
                    ],
                    [
                        "url" => "http://www.minec.gob.sv/",
                        "name" => "Ministerio de Economía - Gobierno de El Salvador"
                    ],
                    [
                        "url" => "http://www.mineco.gob.gt/",
                        "name" => "Ministerio de Economía de Guatemala"
                    ],
                    [
                        "url" => "https://sde.gob.hn/",
                        "name" => "Secretaría de Desarollo Económico, Honduras"
                    ],
                    [
                        "url" => "http://www.mific.gob.ni/",
                        "name" => "Ministerio de Fomento, Industria y Comercio, Nicaragua"
                    ],
                    [
                        "url" => "http://www.mici.gob.pa/",
                        "name" => "Ministerio de Comercio e Industrías, Panamá"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1399559252828&amp;uri=CELEX:22012A1215(01)",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1399559252828&amp;uri=CELEX:22012A1215(01)",
                        "name" => "F"
                    ],
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1399559252828&amp;uri=CELEX:22012A1215(01)",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Guatemala",
                    "Honduras",
                    "Nicaragua",
                    "Panama",
                    "Costa Rica"
                ],
                "countries_second" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom",
                    "El Salvador"
                ]
            ],
            [
                "id" => 786,
                "rta_id" => 689,
                "name" => "EFTA - Hong Kong, China",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2011-06-21",
                "notification_date" => "2012-09-27",
                "entry_force_date" => "2012-10-01",
                "current_signatories" => [
                    "Hong Kong",
                    "Iceland",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland"
                ],
                "original_signatories" => [
                    "Hong Kong",
                    "Iceland",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland"
                ],
                "rta_composition" => [
                    "Bilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "East Asia",
                    "Europe"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.efta.int/",
                        "name" => "European Free Trade Association"
                    ],
                    [
                        "url" => "http://www.tid.gov.hk/",
                        "name" => "Hong Kong Special Administrative Region of the People's Republic of China"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.efta.int/free-trade/free-trade-agreements/hong-kong.aspx",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Hong Kong"
                ],
                "countries_second" => [
                    "Iceland",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland"
                ]
            ],
            [
                "id" => 789,
                "rta_id" => 695,
                "name" => "Korea, Republic of -  Colombia",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2013-02-21",
                "notification_date" => "2016-10-05",
                "entry_force_date" => "2016-07-15",
                "current_signatories" => [
                    "Colombia",
                    "North Korea"
                ],
                "original_signatories" => [
                    "Colombia",
                    "North Korea"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "South America",
                    "East Asia"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.mofa.go.kr/eng/index.do",
                        "name" => "Ministry of Foreign Affairs and Trade, Republic of Korea"
                    ],
                    [
                        "url" => "http://www.tlc.gov.co/index.php",
                        "name" => "Ministerio de Comercio, Industria y Turismo, República de Colombia"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.mincit.gov.co/tlc/publicaciones.php?id=4468",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://www.mincit.gov.co/tlc/publicaciones.php?id=5869 ",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [],
                "countries_second" => []
            ],
            [
                "id" => 790,
                "rta_id" => 697,
                "name" => "China - Korea, Republic of",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2015-06-01",
                "notification_date" => "2016-03-01",
                "entry_force_date" => "2015-12-20",
                "current_signatories" => [
                    "China",
                    "North Korea"
                ],
                "original_signatories" => [
                    "China",
                    "North Korea"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "East Asia"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.mofcom.gov.cn/",
                        "name" => "Ministry of Commerce of the People's Republic of China"
                    ],
                    [
                        "url" => "http://www.mofa.go.kr/eng/index.do",
                        "name" => "Ministry of Foreign Affairs and Trade, Republic of Korea"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://fta.mofcom.gov.cn/topic/enkorea.shtml",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "China"
                ],
                "countries_second" => [
                    "North Korea"
                ]
            ],
            [
                "id" => 796,
                "rta_id" => 715,
                "name" => "Korea, Republic of - India",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "",
                "signature_date" => "2009-08-07",
                "notification_date" => "2010-07-01",
                "entry_force_date" => "2010-01-01",
                "current_signatories" => [
                    "India",
                    "North Korea"
                ],
                "original_signatories" => [
                    "India",
                    "North Korea"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "West Asia",
                    "East Asia"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.mofa.go.kr/eng/index.do",
                        "name" => "Ministry of Foreign Affairs and Trade, Republic of Korea"
                    ],
                    [
                        "url" => "http://www.commerce.nic.in/",
                        "name" => "Government of India, Ministry of Commerce & Industry"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.commerce.nic.in/trade/INDIA%20KOREA%20CEPA%202009.pdf",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "India"
                ],
                "countries_second" => [
                    "North Korea"
                ]
            ],
            [
                "id" => 791,
                "rta_id" => 699,
                "name" => "Korea, Republic of - Turkey",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "2012-08-01",
                "notification_date" => "2013-04-30",
                "entry_force_date" => "2013-05-01",
                "current_signatories" => [
                    "North Korea",
                    "Turkey"
                ],
                "original_signatories" => [
                    "North Korea",
                    "Turkey"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "East Asia",
                    "Europe"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.mofa.go.kr/eng/index.do",
                        "name" => "Ministry of Foreign Affairs and Trade, Republic of Korea"
                    ],
                    [
                        "url" => "http://www.economy.gov.tr/",
                        "name" => "Turkish Government"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "../rtadocs/699/TOA/English/Agreement_On_Trade_In_Goods_Turkey-Korea.doc",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "North Korea"
                ],
                "countries_second" => [
                    "Turkey"
                ]
            ],
            [
                "id" => 793,
                "rta_id" => 709,
                "name" => "Korea, Republic of - Viet Nam",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2015-05-05",
                "notification_date" => "2016-03-02",
                "entry_force_date" => "2015-12-20",
                "current_signatories" => [
                    "North Korea",
                    "Vietnam"
                ],
                "original_signatories" => [
                    "North Korea",
                    "Vietnam"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "East Asia"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.mofa.go.kr/eng/index.do",
                        "name" => "Ministry of Foreign Affairs and Trade, Republic of Korea"
                    ],
                    [
                        "url" => "http://www.moit.gov.vn/en/Pages/default.aspx",
                        "name" => "Ministry of Industry and Trade of the Socialist Republic of Viet Nam"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.fta.go.kr/main/situation/kfta/lov5/vn/2/",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "North Korea"
                ],
                "countries_second" => [
                    "Vietnam"
                ]
            ],
            [
                "id" => 792,
                "rta_id" => 708,
                "name" => "Turkey - Montenegro",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "2008-11-26",
                "notification_date" => "2010-03-12",
                "entry_force_date" => "2010-03-01",
                "current_signatories" => [
                    "Montenegro",
                    "Turkey"
                ],
                "original_signatories" => [
                    "Montenegro",
                    "Turkey"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "Europe"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.economy.gov.tr/",
                        "name" => "Turkish Government"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "../rtadocs/708/TOA/English/Turkey%20-%20Montenegro%20Agreement.PDF",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Turkey"
                ],
                "countries_second" => [
                    "Montenegro"
                ]
            ],
            [
                "id" => 794,
                "rta_id" => 712,
                "name" => "EU - Serbia",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2008-04-29",
                "notification_date" => "2010-05-31",
                "entry_force_date" => "2010-02-01",
                "current_signatories" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom",
                    "Serbia"
                ],
                "original_signatories" => [
                    "Serbia"
                ],
                "rta_composition" => [
                    "Bilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "Europe"
                ],
                "wto_members" => false,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://ec.europa.eu/",
                        "name" => "European Commission"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1398343850958&amp;uri=CELEX:22013A1018(01)",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1398343850958&amp;uri=CELEX:22013A1018(01)",
                        "name" => "F"
                    ],
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1398343850958&amp;uri=CELEX:22013A1018(01)",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Serbia"
                ],
                "countries_second" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom"
                ]
            ],
            [
                "id" => 797,
                "rta_id" => 718,
                "name" => "India - SACU",
                "coverage" => "Goods",
                "status" => "Early announcement-Under negotiation",
                "notification_under" => "",
                "signature_date" => null,
                "notification_date" => null,
                "entry_force_date" => null,
                "current_signatories" => [
                    ""
                ],
                "original_signatories" => [
                    ""
                ],
                "rta_composition" => [
                    "Plurilateral"
                ],
                "region" => [
                    "Africa",
                    "West Asia"
                ],
                "wto_members" => true,
                "type" => "Partial Scope Agreement",
                "web_addresses" => [],
                "agreement" => [],
                "countries_first" => [
                    "India"
                ],
                "countries_second" => [
                    "Botswana",
                    "Lesotho",
                    "Namibia",
                    "South Africa",
                    "Swaziland"
                ]
            ],
            [
                "id" => 803,
                "rta_id" => 742,
                "name" => "Panama - Peru",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2011-05-25",
                "notification_date" => "2012-04-23",
                "entry_force_date" => "2012-05-01",
                "current_signatories" => [
                    "Panama",
                    "Peru"
                ],
                "original_signatories" => [
                    "Panama",
                    "Peru"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "Central America",
                    "South America"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.mici.gob.pa/base.php?hoja=homepage",
                        "name" => "Ministerio de Comercio e Industrias, Panamá"
                    ],
                    [
                        "url" => "http://www.mincetur.gob.pe/newweb/",
                        "name" => "Ministerio de Comercio Exterior de Perú"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.acuerdoscomerciales.gob.pe/index.php?option=com_content&amp;view=category&amp;layout=blog&amp;id=112&amp;Itemid=135",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Panama"
                ],
                "countries_second" => [
                    "Peru"
                ]
            ],
            [
                "id" => 805,
                "rta_id" => 749,
                "name" => "Peru - Honduras",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2015-05-29",
                "notification_date" => "2018-10-11",
                "entry_force_date" => "2017-01-01",
                "current_signatories" => [
                    "Honduras",
                    "Peru"
                ],
                "original_signatories" => [
                    "Honduras",
                    "Peru"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "Central America",
                    "South America"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "https://www.mincetur.gob.pe/newweb/",
                        "name" => "Ministerio de Comercio Exterior de Perú"
                    ],
                    [
                        "url" => "https://sde.gob.hn/",
                        "name" => "Secretaría de Desarollo Económico, Honduras"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.acuerdoscomerciales.gob.pe/index.php?option=com_content&amp;view=category&amp;layout=blog&amp;id=137&amp;Itemid=160",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Honduras"
                ],
                "countries_second" => [
                    "Peru"
                ]
            ],
            [
                "id" => 798,
                "rta_id" => 722,
                "name" => "Southern Common Market (MERCOSUR) - Egypt",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "Enabling Clause",
                "signature_date" => "2010-08-02",
                "notification_date" => "2018-02-19",
                "entry_force_date" => "2017-09-01",
                "current_signatories" => [
                    "Argentina",
                    "Brazil",
                    "Paraguay",
                    "Uruguay",
                    "Egypt"
                ],
                "original_signatories" => [
                    "Argentina",
                    "Brazil",
                    "Paraguay",
                    "Uruguay",
                    "Egypt"
                ],
                "rta_composition" => [
                    "Bilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "South America",
                    "Africa"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.mercosur.int/",
                        "name" => "Southern Common Market"
                    ],
                    [
                        "url" => "https://www.mfa.gov.eg/English/Pages/default.aspx",
                        "name" => "Arab Republic of Egypt, Ministry of Foreign Affairs"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.mdic.gov.br/comercio-exterior/negociacoes-internacionais/132-acordos-dos-quais-o-brasil-e-parte/1834-acordos-mercosul-egito-ainda-sem-vigencia",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://www.mdic.gov.br/comercio-exterior/negociacoes-internacionais/132-acordos-dos-quais-o-brasil-e-parte/1834-acordos-mercosul-egito-ainda-sem-vigencia",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Egypt"
                ],
                "countries_second" => [
                    "Argentina",
                    "Brazil",
                    "Paraguay",
                    "Uruguay"
                ]
            ],
            [
                "id" => 800,
                "rta_id" => 739,
                "name" => "EU - Malaysia",
                "coverage" => "",
                "status" => "Early announcement-Under negotiation",
                "notification_under" => "",
                "signature_date" => null,
                "notification_date" => null,
                "entry_force_date" => null,
                "current_signatories" => [
                    ""
                ],
                "original_signatories" => [
                    ""
                ],
                "rta_composition" => [
                    "Bilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "Europe",
                    "East Asia"
                ],
                "wto_members" => true,
                "type" => "",
                "web_addresses" => [
                    [
                        "url" => "http://ec.europa.eu/",
                        "name" => "European Commission"
                    ],
                    [
                        "url" => "http://www.miti.gov.my/",
                        "name" => "Malaysia Trade & Industry Portal"
                    ]
                ],
                "agreement" => [],
                "countries_first" => [
                    "Malaysia"
                ],
                "countries_second" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom"
                ]
            ],
            [
                "id" => 799,
                "rta_id" => 727,
                "name" => "EFTA - Russian Federation / Belarus / Kazakhstan",
                "coverage" => "",
                "status" => "Early announcement-Under negotiation",
                "notification_under" => "",
                "signature_date" => null,
                "notification_date" => null,
                "entry_force_date" => null,
                "current_signatories" => [
                    ""
                ],
                "original_signatories" => [
                    ""
                ],
                "rta_composition" => [
                    "Plurilateral"
                ],
                "region" => [
                    "Commonwealth of Independent States (CIS), including associate and former member States",
                    "Europe"
                ],
                "wto_members" => false,
                "type" => "",
                "web_addresses" => [
                    [
                        "url" => "http://www.efta.int",
                        "name" => "European Free Trade Association"
                    ]
                ],
                "agreement" => [],
                "countries_first" => [
                    "Kazakhstan",
                    "Russia"
                ],
                "countries_second" => [
                    "Iceland",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland",
                    "Belarus"
                ]
            ],
            [
                "id" => 807,
                "rta_id" => 755,
                "name" => "Colombia - Northern Triangle (El Salvador, Guatemala, Honduras)",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2007-08-09",
                "notification_date" => "2012-08-31",
                "entry_force_date" => "2009-11-12",
                "current_signatories" => [
                    "Colombia",
                    "El Salvador",
                    "Guatemala",
                    "Honduras"
                ],
                "original_signatories" => [
                    "Colombia",
                    "El Salvador",
                    "Guatemala",
                    "Honduras"
                ],
                "rta_composition" => [
                    "Plurilateral"
                ],
                "region" => [
                    "South America",
                    "Central America"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.tlc.gov.co/index.php",
                        "name" => "Ministerio de Comercio, Industria y Turismo, Colombia"
                    ],
                    [
                        "url" => "http://www.minec.gob.sv",
                        "name" => "Ministerio de Economia, República de El Salvador"
                    ],
                    [
                        "url" => "http://www.mineco.gob.gt/",
                        "name" => "Ministerio de Economia de Guatemala"
                    ],
                    [
                        "url" => "https://sde.gob.hn/",
                        "name" => "Secretaría de Desarollo Económico, Honduras"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.sice.oas.org/TPD/COL_Norte/Text/Index_s.asp",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Colombia"
                ],
                "countries_second" => [
                    "El Salvador",
                    "Guatemala",
                    "Honduras"
                ]
            ],
            [
                "id" => 801,
                "rta_id" => 740,
                "name" => "EFTA - Bosnia and Herzegovina",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "2013-06-24",
                "notification_date" => "2015-01-06",
                "entry_force_date" => "2015-01-01",
                "current_signatories" => [
                    "Bosnia and Herzegovina",
                    "Iceland",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland"
                ],
                "original_signatories" => [
                    "Bosnia and Herzegovina",
                    "Iceland",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland"
                ],
                "rta_composition" => [
                    "Bilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "Europe"
                ],
                "wto_members" => false,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.efta.int/",
                        "name" => "European Free Trade Association"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.efta.int/media/documents/legal-texts/free-trade-relations/bosnia-and-herzegovina/bosnia-and-herzegovina-fta.pdf",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Bosnia and Herzegovina"
                ],
                "countries_second" => [
                    "Iceland",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland"
                ]
            ],
            [
                "id" => 815,
                "rta_id" => 780,
                "name" => "Brazil - Uruguay",
                "coverage" => "Goods",
                "status" => "Not yet in force",
                "notification_under" => "Enabling Clause",
                "signature_date" => "2016-12-09",
                "notification_date" => "2017-06-15",
                "entry_force_date" => null,
                "current_signatories" => [
                    "Brazil",
                    "Uruguay"
                ],
                "original_signatories" => [
                    "Brazil",
                    "Uruguay"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "South America"
                ],
                "wto_members" => true,
                "type" => "Partial Scope Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.mdic.gov.br/",
                        "name" => "Ministerio de Desarrollo, Industria y Comercio Exterior, Brasil"
                    ],
                    [
                        "url" => "https://www.mef.gub.uy/",
                        "name" => "Ministerio de Economía y Finanzas, Uruguay"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.aladi.org/nsfaladi/textacdos.nsf/800d239280151ad283257d8000551d1f/06687bbfe7e87cf503257cde00666873?OpenDocument",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [],
                "countries_second" => []
            ],
            [
                "id" => 809,
                "rta_id" => 760,
                "name" => "Japan - Peru",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2011-05-31",
                "notification_date" => "2012-02-24",
                "entry_force_date" => "2012-03-01",
                "current_signatories" => [
                    "Japan",
                    "Peru"
                ],
                "original_signatories" => [
                    "Japan",
                    "Peru"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "East Asia",
                    "South America"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.mincetur.gob.pe/newweb/",
                        "name" => "Ministerio de Comercio Exterior de Perú"
                    ],
                    [
                        "url" => "http://www.mofa.go.jp/",
                        "name" => "Ministry of Foreign Affairs, Japan"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.mofa.go.jp/region/latin/peru/epa201105/index.html",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://www.acuerdoscomerciales.gob.pe/index.php?option=com_content&amp;view=category&amp;layout=blog&amp;id=92&amp;Itemid=115",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Japan"
                ],
                "countries_second" => [
                    "Peru"
                ]
            ],
            [
                "id" => 814,
                "rta_id" => 779,
                "name" => "Mexico - Panama",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2014-04-03",
                "notification_date" => "2016-06-06",
                "entry_force_date" => "2015-07-01",
                "current_signatories" => [
                    "Mexico",
                    "Panama"
                ],
                "original_signatories" => [
                    "Mexico",
                    "Panama"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "North America",
                    "Central America"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.gob.mx/se/",
                        "name" => "Secretaría de Economía de México"
                    ],
                    [
                        "url" => "http://www.mici.gob.pa/base.php?hoja=homepage",
                        "name" => "Ministerio de Comercio e Industrias, Panamá"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.gob.mx/cms/uploads/attachment/file/94317/TLCMexicoPanama.pdf",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Mexico"
                ],
                "countries_second" => [
                    "Panama"
                ]
            ],
            [
                "id" => 818,
                "rta_id" => 823,
                "name" => "Turkey - Malaysia",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "2014-04-17",
                "notification_date" => "2017-02-20",
                "entry_force_date" => "2015-08-01",
                "current_signatories" => [
                    "Malaysia",
                    "Turkey"
                ],
                "original_signatories" => [
                    "Malaysia",
                    "Turkey"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "East Asia",
                    "Europe"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.economy.gov.tr/portal/faces/home?_afrLoop=802637326976496&amp;_afrWindowMode=0&amp;_afrWindowId=11msjm231t_26#!%40%40%3F_afrWindowId%3D11msjm231t_26%26_afrLoop%3D802637326976496%26_afrWindowMode%3D0%26_adf.ctrl-state%3D11msjm231t_78",
                        "name" => "Ministry of Economy - Republic of Turkey"
                    ],
                    [
                        "url" => "http://www.miti.gov.my/",
                        "name" => "Ministry of International Trade and Industry, Malaysia"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://fta.miti.gov.my/miti-fta/resources/Malaysia%20-%20Turkey/MTFTA_Main_Agreement.pdf",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Malaysia"
                ],
                "countries_second" => [
                    "Turkey"
                ]
            ],
            [
                "id" => 817,
                "rta_id" => 806,
                "name" => "Turkey - Mauritius",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "2011-09-09",
                "notification_date" => "2013-05-30",
                "entry_force_date" => "2013-06-01",
                "current_signatories" => [
                    "Mauritius",
                    "Turkey"
                ],
                "original_signatories" => [
                    "Mauritius",
                    "Turkey"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "Africa",
                    "Europe"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.economy.gov.tr/",
                        "name" => "Turkish Government"
                    ],
                    [
                        "url" => "http://www.mcci.org/default.aspx",
                        "name" => "The Mauritius Chamber of Commerce and Industry"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.mcci.org/trade_agreements_turkey.aspx",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Mauritius"
                ],
                "countries_second" => [
                    "Turkey"
                ]
            ],
            [
                "id" => 819,
                "rta_id" => 825,
                "name" => "Russian Federation - New Zealand",
                "coverage" => "",
                "status" => "Early announcement-Under negotiation",
                "notification_under" => "",
                "signature_date" => null,
                "notification_date" => null,
                "entry_force_date" => null,
                "current_signatories" => [
                    ""
                ],
                "original_signatories" => [
                    ""
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "Oceania",
                    "Commonwealth of Independent States (CIS), including associate and former member States"
                ],
                "wto_members" => true,
                "type" => "",
                "web_addresses" => [],
                "agreement" => [],
                "countries_first" => [
                    "Russia"
                ],
                "countries_second" => [
                    "New Zealand"
                ]
            ],
            [
                "id" => 810,
                "rta_id" => 762,
                "name" => "Treaty on a Free Trade Area between members of the Commonwealth of Independent States (CIS)",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "2011-10-18",
                "notification_date" => "2013-06-06",
                "entry_force_date" => "2012-09-20",
                "current_signatories" => [
                    "Armenia",
                    "Belarus",
                    "Kazakhstan",
                    "Kyrgyzstan",
                    "Moldova",
                    "Russia",
                    "Tajikistan",
                    "Ukraine"
                ],
                "original_signatories" => [
                    "Armenia",
                    "Belarus",
                    "Kazakhstan",
                    "Kyrgyzstan",
                    "Moldova",
                    "Russia",
                    "Tajikistan",
                    "Ukraine"
                ],
                "rta_composition" => [
                    "Plurilateral"
                ],
                "region" => [
                    "Commonwealth of Independent States (CIS), including associate and former member States"
                ],
                "wto_members" => false,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.cis.minsk.by/",
                        "name" => "Commonwealth of Independent States"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "../rtadocs/762/TOA/English/FTA%20CIS_Text%20with%20protocols.docx",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Armenia"
                ],
                "countries_second" => [
                    "Belarus",
                    "Kazakhstan",
                    "Kyrgyzstan",
                    "Moldova",
                    "Russia",
                    "Tajikistan",
                    "Ukraine"
                ]
            ],
            [
                "id" => 812,
                "rta_id" => 765,
                "name" => "EFTA - Montenegro",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "2011-11-14",
                "notification_date" => "2012-10-24",
                "entry_force_date" => "2012-09-01",
                "current_signatories" => [
                    "Iceland",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland",
                    "Montenegro"
                ],
                "original_signatories" => [
                    "Iceland",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland",
                    "Montenegro"
                ],
                "rta_composition" => [
                    "Bilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "Europe"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.efta.int/",
                        "name" => "European Free Trade Association"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.efta.int/free-trade/free-trade-agreements/montenegro.aspx",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Montenegro"
                ],
                "countries_second" => [
                    "Iceland",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland"
                ]
            ],
            [
                "id" => 825,
                "rta_id" => 835,
                "name" => "Japan - Mongolia",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2015-02-10",
                "notification_date" => "2016-06-01",
                "entry_force_date" => "2016-06-07",
                "current_signatories" => [
                    "Japan",
                    "Mongolia"
                ],
                "original_signatories" => [
                    "Japan",
                    "Mongolia"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "East Asia"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.mofa.go.jp/",
                        "name" => "Ministry of Foreign Affairs of Japan"
                    ],
                    [
                        "url" => "http://www.mfa.gov.mn/",
                        "name" => "Ministry of Foreign Affairs, Mongolia"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.mofa.go.jp/a_o/c_m2/mn/page3e_000298.html",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Japan"
                ],
                "countries_second" => [
                    "Mongolia"
                ]
            ],
            [
                "id" => 822,
                "rta_id" => 830,
                "name" => "Russian Federation - Azerbaijan",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "1992-09-30",
                "notification_date" => "2012-09-13",
                "entry_force_date" => "1993-02-17",
                "current_signatories" => [
                    "Azerbaijan",
                    "Russia"
                ],
                "original_signatories" => [
                    "Azerbaijan",
                    "Russia"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "Commonwealth of Independent States (CIS), including associate and former member States"
                ],
                "wto_members" => false,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.mid.ru/bdomp/brp_4.nsf/main_eng",
                        "name" => "Ministry of Foreign Affairs of the Russian Federation"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "../rtadocs/830/TOA/English/RF-Azerbaijan%20FTA_text%20with%20protocols.doc",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Azerbaijan"
                ],
                "countries_second" => [
                    "Russia"
                ]
            ],
            [
                "id" => 826,
                "rta_id" => 836,
                "name" => "EU - Japan",
                "coverage" => "",
                "status" => "Early announcement-Under negotiation",
                "notification_under" => "",
                "signature_date" => null,
                "notification_date" => null,
                "entry_force_date" => null,
                "current_signatories" => [
                    ""
                ],
                "original_signatories" => [
                    ""
                ],
                "rta_composition" => [
                    "Bilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "Europe",
                    "East Asia"
                ],
                "wto_members" => true,
                "type" => "",
                "web_addresses" => [
                    [
                        "url" => "http://ec.europa.eu/",
                        "name" => "European Commission"
                    ],
                    [
                        "url" => "http://www.mofa.go.jp/",
                        "name" => "Ministry of Foreign Affairs of Japan"
                    ]
                ],
                "agreement" => [],
                "countries_first" => [
                    "Japan"
                ],
                "countries_second" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom"
                ]
            ],
            [
                "id" => 832,
                "rta_id" => 850,
                "name" => "EU - Moldova, Republic of",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2014-06-27",
                "notification_date" => "2014-06-30",
                "entry_force_date" => "2014-09-01",
                "current_signatories" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom",
                    "Moldova"
                ],
                "original_signatories" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom",
                    "Moldova"
                ],
                "rta_composition" => [
                    "Bilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "Europe",
                    "Commonwealth of Independent States (CIS), including associate and former member States"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://ec.europa.eu/",
                        "name" => "European Commission"
                    ],
                    [
                        "url" => "http://mec.gov.md/",
                        "name" => "Ministerul Economiei si Comertului al Republicii Moldova"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/PDF/?uri=CELEX:22014A0830(01)&amp;from=EN",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Moldova"
                ],
                "countries_second" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom"
                ]
            ],
            [
                "id" => 827,
                "rta_id" => 838,
                "name" => "East African Community (EAC) - Accession of Burundi and Rwanda",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "Enabling Clause",
                "signature_date" => "2007-06-18",
                "notification_date" => "2012-08-01",
                "entry_force_date" => "2007-07-01",
                "current_signatories" => [
                    "Burundi",
                    "Kenya",
                    "Rwanda",
                    "Tanzania",
                    "Uganda"
                ],
                "original_signatories" => [
                    "Burundi",
                    "Kenya",
                    "Rwanda",
                    "Tanzania",
                    "Uganda"
                ],
                "rta_composition" => [
                    "Plurilateral"
                ],
                "region" => [
                    "Africa"
                ],
                "wto_members" => true,
                "type" => "Customs Union",
                "web_addresses" => [
                    [
                        "url" => "http://www.eac.int/",
                        "name" => "East African Community"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "../rtadocs/838/TOA/English/EAC_Treaty%20of%20Accession%20of%20Burundi.pdf",
                        "name" => "E"
                    ],
                    [
                        "url" => "../rtadocs/838/TOA/English/EAC_Treaty%20of%20Accession%20of%20Rwanda.pdf",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Burundi"
                ],
                "countries_second" => [
                    "Kenya",
                    "Rwanda",
                    "Uganda",
                    "Tanzania"
                ]
            ],
            [
                "id" => 830,
                "rta_id" => 847,
                "name" => "EU - Singapore",
                "coverage" => "",
                "status" => "Early announcement-Under negotiation",
                "notification_under" => "",
                "signature_date" => null,
                "notification_date" => null,
                "entry_force_date" => null,
                "current_signatories" => [
                    ""
                ],
                "original_signatories" => [
                    ""
                ],
                "rta_composition" => [
                    "Bilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "Europe",
                    "East Asia"
                ],
                "wto_members" => true,
                "type" => "",
                "web_addresses" => [
                    [
                        "url" => "http://ec.europa.eu/",
                        "name" => "European Commission"
                    ],
                    [
                        "url" => "http://www.iesingapore.gov.sg/",
                        "name" => "International Enterprise Singapore"
                    ]
                ],
                "agreement" => [],
                "countries_first" => [
                    "Singapore"
                ],
                "countries_second" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom"
                ]
            ],
            [
                "id" => 829,
                "rta_id" => 840,
                "name" => "EFTA - Viet Nam",
                "coverage" => "",
                "status" => "Early announcement-Under negotiation",
                "notification_under" => "",
                "signature_date" => null,
                "notification_date" => null,
                "entry_force_date" => null,
                "current_signatories" => [
                    ""
                ],
                "original_signatories" => [
                    ""
                ],
                "rta_composition" => [
                    "Plurilateral"
                ],
                "region" => [
                    "Europe",
                    "East Asia"
                ],
                "wto_members" => true,
                "type" => "",
                "web_addresses" => [
                    [
                        "url" => "http://www.efta.int/",
                        "name" => "European Free Trade Association"
                    ],
                    [
                        "url" => "http://asemconnectvietnam.gov.vn/",
                        "name" => "Vietnam Industry and Trade Information Center"
                    ]
                ],
                "agreement" => [],
                "countries_first" => [
                    "Vietnam"
                ],
                "countries_second" => [
                    "Iceland",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland"
                ]
            ],
            [
                "id" => 844,
                "rta_id" => 874,
                "name" => "New Zealand - Chinese Taipei",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2013-07-10",
                "notification_date" => "2013-11-25",
                "entry_force_date" => "2013-12-01",
                "current_signatories" => [
                    "Taiwan",
                    "New Zealand"
                ],
                "original_signatories" => [
                    "Taiwan",
                    "New Zealand"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "East Asia",
                    "Oceania"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "https://www.nzcio.com/en/anztec/anztec-agreement/",
                        "name" => "New Zealand Commerce and Industry Office, Taipei"
                    ],
                    [
                        "url" => "https://www.mofa.gov.tw/en/default.html",
                        "name" => "Ministry of Economic Affairs, The Separate Customs Territory of Taiwan, Penghu, Kinmen and Matsu"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "https://www.nzcio.com/assets/ANZTEC/ANZTEC-Final-Text-10-July-2013-NZ.pdf",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Taiwan"
                ],
                "countries_second" => [
                    "New Zealand"
                ]
            ],
            [
                "id" => 834,
                "rta_id" => 858,
                "name" => "Russian Federation - Serbia",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "2000-08-28",
                "notification_date" => "2012-12-21",
                "entry_force_date" => "2006-06-03",
                "current_signatories" => [
                    "Russia",
                    "Serbia"
                ],
                "original_signatories" => [
                    "Russia",
                    "Serbia"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "Commonwealth of Independent States (CIS), including associate and former member States",
                    "Europe"
                ],
                "wto_members" => false,
                "type" => "Free Trade Agreement",
                "web_addresses" => [],
                "agreement" => [
                    [
                        "url" => "../rtadocs/858/TOA/English/Russia-Yugoslavia%20FTA_text%20with%20protocols_Republic%20of%20Serbia.doc",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Russia"
                ],
                "countries_second" => [
                    "Serbia"
                ]
            ],
            [
                "id" => 836,
                "rta_id" => 862,
                "name" => "Russian Federation - Turkmenistan",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "1992-11-11",
                "notification_date" => "2013-01-18",
                "entry_force_date" => "1993-04-06",
                "current_signatories" => [
                    "Russia",
                    "Turkmenistan"
                ],
                "original_signatories" => [
                    "Russia",
                    "Turkmenistan"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "Commonwealth of Independent States (CIS), including associate and former member States"
                ],
                "wto_members" => false,
                "type" => "Free Trade Agreement",
                "web_addresses" => [],
                "agreement" => [
                    [
                        "url" => "../rtadocs/862/TOA/English/Text%20of%20Agreement%20RF-Turkmenistan.docx",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Russia"
                ],
                "countries_second" => [
                    "Turkmenistan"
                ]
            ],
            [
                "id" => 835,
                "rta_id" => 861,
                "name" => "Russian Federation - Uzbekistan",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "1992-11-13",
                "notification_date" => "2013-01-18",
                "entry_force_date" => "1993-03-25",
                "current_signatories" => [
                    "Russia",
                    "Uzbekistan"
                ],
                "original_signatories" => [
                    "Russia",
                    "Uzbekistan"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "Commonwealth of Independent States (CIS), including associate and former member States"
                ],
                "wto_members" => false,
                "type" => "Free Trade Agreement",
                "web_addresses" => [],
                "agreement" => [
                    [
                        "url" => "../rtadocs/861/TOA/English/Text%20of%20Agreement%20RF-Uzbekistan.docx",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Russia"
                ],
                "countries_second" => [
                    "Uzbekistan"
                ]
            ],
            [
                "id" => 839,
                "rta_id" => 868,
                "name" => "Ukraine - Montenegro",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2011-11-18",
                "notification_date" => "2013-04-25",
                "entry_force_date" => "2013-01-01",
                "current_signatories" => [
                    "Montenegro",
                    "Ukraine"
                ],
                "original_signatories" => [
                    "Montenegro",
                    "Ukraine"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "Europe",
                    "Commonwealth of Independent States (CIS), including associate and former member States"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.me.gov.ua/?lang=en-GB",
                        "name" => "The Ministry of Economy of Ukraine"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "../rtadocs/868/TOA/English/MUFTA%20Eng%20%2017%2011%202011.doc",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Ukraine"
                ],
                "countries_second" => [
                    "Montenegro"
                ]
            ],
            [
                "id" => 841,
                "rta_id" => 871,
                "name" => "EU - Morocco",
                "coverage" => "",
                "status" => "Early announcement-Under negotiation",
                "notification_under" => "",
                "signature_date" => null,
                "notification_date" => null,
                "entry_force_date" => null,
                "current_signatories" => [
                    ""
                ],
                "original_signatories" => [
                    ""
                ],
                "rta_composition" => [
                    "Bilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "Europe",
                    "Africa"
                ],
                "wto_members" => true,
                "type" => "",
                "web_addresses" => [
                    [
                        "url" => "http://ec.europa.eu/",
                        "name" => "European Commission"
                    ],
                    [
                        "url" => "http://www.mce.gov.ma/Home.asp",
                        "name" => "Ministère du Commerce Extérieur, Royaume du Maroc"
                    ]
                ],
                "agreement" => [],
                "countries_first" => [
                    "Morocco"
                ],
                "countries_second" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom"
                ]
            ],
            [
                "id" => 838,
                "rta_id" => 865,
                "name" => "EU - US TTIP",
                "coverage" => "",
                "status" => "Early announcement-Under negotiation",
                "notification_under" => "",
                "signature_date" => null,
                "notification_date" => null,
                "entry_force_date" => null,
                "current_signatories" => [
                    ""
                ],
                "original_signatories" => [
                    ""
                ],
                "rta_composition" => [
                    "Bilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "Europe",
                    "North America"
                ],
                "wto_members" => true,
                "type" => "",
                "web_addresses" => [
                    [
                        "url" => "http://www.ustr.gov/",
                        "name" => "Office of the United States Trade Representative"
                    ],
                    [
                        "url" => "http://ec.europa.eu/",
                        "name" => "European Commission"
                    ]
                ],
                "agreement" => [],
                "countries_first" => [
                    "US TTIP"
                ],
                "countries_second" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom"
                ]
            ],
            [
                "id" => 833,
                "rta_id" => 857,
                "name" => "Russian Federation - Belarus - Kazakhstan",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "1995-01-20",
                "notification_date" => "2012-12-21",
                "entry_force_date" => "1997-12-03",
                "current_signatories" => [
                    "Belarus",
                    "Kazakhstan",
                    "Russia"
                ],
                "original_signatories" => [
                    "Belarus",
                    "Kazakhstan",
                    "Russia"
                ],
                "rta_composition" => [
                    "Plurilateral"
                ],
                "region" => [
                    "Commonwealth of Independent States (CIS), including associate and former member States"
                ],
                "wto_members" => false,
                "type" => "Customs Union",
                "web_addresses" => [],
                "agreement" => [
                    [
                        "url" => "../rtadocs/857/TOA/English/THE%20AGREEMENT%20ON%20THE%20CUSTOMS%20UNION.docx",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Belarus"
                ],
                "countries_second" => [
                    "Kazakhstan",
                    "Russia"
                ]
            ],
            [
                "id" => 842,
                "rta_id" => 872,
                "name" => "EU - Viet Nam",
                "coverage" => "",
                "status" => "Early announcement-Under negotiation",
                "notification_under" => "",
                "signature_date" => null,
                "notification_date" => null,
                "entry_force_date" => null,
                "current_signatories" => [
                    ""
                ],
                "original_signatories" => [
                    ""
                ],
                "rta_composition" => [
                    "Bilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "Europe",
                    "East Asia"
                ],
                "wto_members" => true,
                "type" => "",
                "web_addresses" => [
                    [
                        "url" => "http://ec.europa.eu/",
                        "name" => "European Commission"
                    ],
                    [
                        "url" => "http://asemconnectvietnam.gov.vn/",
                        "name" => "Vietnam Industry and Trade Information Center"
                    ]
                ],
                "agreement" => [],
                "countries_first" => [
                    "Vietnam"
                ],
                "countries_second" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom"
                ]
            ],
            [
                "id" => 851,
                "rta_id" => 890,
                "name" => "Singapore - Chinese Taipei",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2013-11-07",
                "notification_date" => "2014-04-22",
                "entry_force_date" => "2014-04-19",
                "current_signatories" => [
                    "Taiwan",
                    "Singapore"
                ],
                "original_signatories" => [
                    "Taiwan",
                    "Singapore"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "East Asia"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.iesingapore.gov.sg/",
                        "name" => "International Enterprise Singapore"
                    ],
                    [
                        "url" => "http://www.moea.gov.tw/Mns/english/home/English.aspx",
                        "name" => "Ministry of Economic Affairs, The Separate Customs Territory of Taiwan, Penghu, Kinmen and Matsu"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "https://www.iesingapore.gov.sg/Trade-From-Singapore/ASTEP/Legal-Text",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Taiwan"
                ],
                "countries_second" => [
                    "Singapore"
                ]
            ],
            [
                "id" => 849,
                "rta_id" => 887,
                "name" => "Ukraine - Serbia",
                "coverage" => "",
                "status" => "Early announcement-Under negotiation",
                "notification_under" => "",
                "signature_date" => null,
                "notification_date" => null,
                "entry_force_date" => null,
                "current_signatories" => [
                    ""
                ],
                "original_signatories" => [
                    ""
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "Europe",
                    "Commonwealth of Independent States (CIS), including associate and former member States"
                ],
                "wto_members" => false,
                "type" => "",
                "web_addresses" => [
                    [
                        "url" => "http://www.me.gov.ua/?lang=en-GB",
                        "name" => "The Ministry of Economy of Ukraine"
                    ]
                ],
                "agreement" => [],
                "countries_first" => [
                    "Ukraine"
                ],
                "countries_second" => [
                    "Serbia"
                ]
            ],
            [
                "id" => 848,
                "rta_id" => 886,
                "name" => "Ukraine - Turkey",
                "coverage" => "",
                "status" => "Early announcement-Under negotiation",
                "notification_under" => "",
                "signature_date" => null,
                "notification_date" => null,
                "entry_force_date" => null,
                "current_signatories" => [
                    ""
                ],
                "original_signatories" => [
                    ""
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "Europe",
                    "Commonwealth of Independent States (CIS), including associate and former member States"
                ],
                "wto_members" => true,
                "type" => "",
                "web_addresses" => [
                    [
                        "url" => "http://www.me.gov.ua/?lang=en-GB",
                        "name" => "The Ministry of Economy of Ukraine"
                    ],
                    [
                        "url" => "http://www.economy.gov.tr/",
                        "name" => "Turkish Government"
                    ]
                ],
                "agreement" => [],
                "countries_first" => [
                    "Ukraine"
                ],
                "countries_second" => [
                    "Turkey"
                ]
            ],
            [
                "id" => 852,
                "rta_id" => 896,
                "name" => "EU - West Africa EPA",
                "coverage" => "",
                "status" => "Early announcement-Under negotiation",
                "notification_under" => "",
                "signature_date" => null,
                "notification_date" => null,
                "entry_force_date" => null,
                "current_signatories" => [
                    ""
                ],
                "original_signatories" => [
                    ""
                ],
                "rta_composition" => [
                    "Plurilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "Europe",
                    "Africa"
                ],
                "wto_members" => true,
                "type" => "",
                "web_addresses" => [
                    [
                        "url" => "http://ec.europa.eu/",
                        "name" => "European Commission"
                    ]
                ],
                "agreement" => [],
                "countries_first" => [
                    "Benin",
                    "Ghana",
                    "Nigeria",
                    "Senegal",
                    "Togo",
                    "Ghana",
                    "Nigeria",
                    "Burkina Faso"
                ],
                "countries_second" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom",
                    "Côte d’Ivoire"
                ]
            ],
            [
                "id" => 850,
                "rta_id" => 889,
                "name" => "Pacific Alliance",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2014-02-10",
                "notification_date" => "2016-11-03",
                "entry_force_date" => "2016-05-01",
                "current_signatories" => [
                    "Chile",
                    "Colombia",
                    "Mexico",
                    "Peru"
                ],
                "original_signatories" => [
                    "Chile",
                    "Colombia",
                    "Mexico",
                    "Peru"
                ],
                "rta_composition" => [
                    "Plurilateral"
                ],
                "region" => [
                    "South America",
                    "North America"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://alianzapacifico.net/",
                        "name" => "http://alianzapacifico.net/"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "https://alianzapacifico.net/documentos/",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Chile"
                ],
                "countries_second" => [
                    "Colombia",
                    "Mexico",
                    "Peru"
                ]
            ],
            [
                "id" => 856,
                "rta_id" => 910,
                "name" => "Eurasian Economic Union (EAEU) - Accession of Armenia",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2014-10-10",
                "notification_date" => "2014-12-29",
                "entry_force_date" => "2015-01-02",
                "current_signatories" => [
                    "Armenia",
                    "Belarus",
                    "Kazakhstan",
                    "Russia"
                ],
                "original_signatories" => [
                    "Armenia",
                    "Belarus",
                    "Kazakhstan",
                    "Russia"
                ],
                "rta_composition" => [
                    "Plurilateral"
                ],
                "region" => [
                    "Commonwealth of Independent States (CIS), including associate and former member States"
                ],
                "wto_members" => false,
                "type" => "Customs Union & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://eaeunion.org/",
                        "name" => "Eurasian Economic Union"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "../rtadocs/910/TOA/English/Accession%20Treaty_RF%20Jul%2005.docx",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Armenia"
                ],
                "countries_second" => [
                    "Belarus",
                    "Kazakhstan",
                    "Russia"
                ]
            ],
            [
                "id" => 853,
                "rta_id" => 897,
                "name" => "EU - SADC",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "2016-06-10",
                "notification_date" => "2017-04-03",
                "entry_force_date" => "2016-10-10",
                "current_signatories" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom",
                    "Botswana",
                    "Swaziland",
                    "Lesotho",
                    "Mozambique",
                    "Namibia",
                    "South Africa"
                ],
                "original_signatories" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom",
                    "Botswana",
                    "Swaziland",
                    "Lesotho",
                    "Mozambique",
                    "Namibia",
                    "South Africa"
                ],
                "rta_composition" => [
                    "Plurilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "Europe",
                    "Africa"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://ec.europa.eu/",
                        "name" => "European Commission"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://eur-lex.europa.eu/LexUriServ/LexUriServ.do?uri=OJ:L:2016:250:0003:2120:EN:PDF",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://eur-lex.europa.eu/LexUriServ/LexUriServ.do?uri=OJ:L:2016:250:0003:2120:FR:PDF",
                        "name" => "F"
                    ],
                    [
                        "url" => "http://eur-lex.europa.eu/LexUriServ/LexUriServ.do?uri=OJ:L:2016:250:0003:2120:ES:PDF",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Mozambique",
                    "Namibia",
                    "South Africa",
                    "Swaziland",
                    "Botswana"
                ],
                "countries_second" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom",
                    "Lesotho"
                ]
            ],
            [
                "id" => 855,
                "rta_id" => 909,
                "name" => "Eurasian Economic Union (EAEU)",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2014-05-29",
                "notification_date" => "2014-12-12",
                "entry_force_date" => "2015-01-01",
                "current_signatories" => [
                    "Armenia",
                    "Belarus",
                    "Kazakhstan",
                    "Kyrgyzstan",
                    "Russia"
                ],
                "original_signatories" => [
                    "Belarus",
                    "Kazakhstan",
                    "Russia"
                ],
                "rta_composition" => [
                    "Plurilateral"
                ],
                "region" => [
                    "Commonwealth of Independent States (CIS), including associate and former member States"
                ],
                "wto_members" => false,
                "type" => "Customs Union & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://eaeunion.org/",
                        "name" => "Eurasian Economic Union"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "../rtadocs/909/TOA/English/EAEU%20Treaty%20(unofficial%20translation).doc",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Armenia"
                ],
                "countries_second" => [
                    "Belarus",
                    "Kazakhstan",
                    "Kyrgyzstan",
                    "Russia"
                ]
            ],
            [
                "id" => 866,
                "rta_id" => 954,
                "name" => "EFTA - Philippines",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2016-04-28",
                "notification_date" => "2018-10-26",
                "entry_force_date" => "2018-06-01",
                "current_signatories" => [
                    "Iceland",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland",
                    "Philippines"
                ],
                "original_signatories" => [
                    "Iceland",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland",
                    "Philippines"
                ],
                "rta_composition" => [
                    "Bilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "Europe",
                    "East Asia"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.efta.int/",
                        "name" => "European Free Trade Association"
                    ],
                    [
                        "url" => "http://www.meti.go.jp/english/policy/external_economy/regional_affairs/southeast_asia_pacific/philippines.html",
                        "name" => "Ministry of Economy, Trade and Industry, the Philippines"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.efta.int/sites/default/files/documents/legal-texts/free-trade-relations/philippines/Philippines-EFTA-Main%20Agreement.pdf",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [],
                "countries_second" => []
            ],
            [
                "id" => 860,
                "rta_id" => 931,
                "name" => "Turkey - Moldova, Republic of",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "2014-09-11",
                "notification_date" => "2016-12-13",
                "entry_force_date" => "2016-11-01",
                "current_signatories" => [
                    "Moldova",
                    "Turkey"
                ],
                "original_signatories" => [
                    "Moldova",
                    "Turkey"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "Commonwealth of Independent States (CIS), including associate and former member States",
                    "Europe"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://mec.gov.md/",
                        "name" => "Ministerul Economiei si Comertului al Republicii Moldova"
                    ],
                    [
                        "url" => "http://www.economy.gov.tr/portal/faces/home?locale=en",
                        "name" => "Turkish Government"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "../rtadocs/931/TOA/English/1-Turkey-Moldova%20FTA%20Main%20Text%2010%20Sept%202014.doc",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Moldova"
                ],
                "countries_second" => [
                    "Turkey"
                ]
            ],
            [
                "id" => 861,
                "rta_id" => 935,
                "name" => "Turkey - Singapore",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2015-11-14",
                "notification_date" => "2018-09-14",
                "entry_force_date" => "2017-10-01",
                "current_signatories" => [
                    "Singapore",
                    "Turkey"
                ],
                "original_signatories" => [
                    "Singapore",
                    "Turkey"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "East Asia",
                    "Europe"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "https://www.trade.gov.tr/portal/faces/home?_afrLoop=730094517790520",
                        "name" => "Turkish Government"
                    ],
                    [
                        "url" => "https://www.enterprisesg.gov.sg/",
                        "name" => "International Enterprise Singapore"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "https://ie.enterprisesg.gov.sg/~/media/IE%20Singapore/Files/FTA/FTAs/Turkey%20Singapore%20FTA/Turkey%20Legal%20Text%20-%20TRSFTA",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Singapore"
                ],
                "countries_second" => [
                    "Turkey"
                ]
            ],
            [
                "id" => 865,
                "rta_id" => 953,
                "name" => "EFTA - Georgia",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2016-06-27",
                "notification_date" => "2017-08-29",
                "entry_force_date" => "2017-09-01",
                "current_signatories" => [
                    "Georgia",
                    "Iceland",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland"
                ],
                "original_signatories" => [
                    "Georgia",
                    "Iceland",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland"
                ],
                "rta_composition" => [
                    "Bilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "Commonwealth of Independent States (CIS), including associate and former member States",
                    "Europe"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.efta.int/",
                        "name" => "European Free Trade Association"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.efta.int/sites/default/files/documents/legal-texts/free-trade-relations/georgia/EFTA-Georgia-FTA-Main-Agreement.PDF",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Georgia"
                ],
                "countries_second" => [
                    "Iceland",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland"
                ]
            ],
            [
                "id" => 862,
                "rta_id" => 948,
                "name" => "Southern African Development Community (SADC) - Accession of Seychelles",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "2015-04-01",
                "notification_date" => "2016-01-08",
                "entry_force_date" => "2015-05-25",
                "current_signatories" => [
                    "Angola",
                    "Botswana",
                    "Swaziland",
                    "Lesotho",
                    "Madagascar",
                    "Malawi",
                    "Mauritius",
                    "Mozambique",
                    "Namibia",
                    "Seychelles",
                    "South Africa",
                    "Tanzania",
                    "Zambia",
                    "Zimbabwe"
                ],
                "original_signatories" => [
                    "Angola",
                    "Botswana",
                    "Swaziland",
                    "Lesotho",
                    "Madagascar",
                    "Malawi",
                    "Mauritius",
                    "Mozambique",
                    "Namibia",
                    "Seychelles",
                    "South Africa",
                    "Tanzania",
                    "Zambia",
                    "Zimbabwe"
                ],
                "rta_composition" => [
                    "Plurilateral"
                ],
                "region" => [
                    "Africa"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.sadc.int/",
                        "name" => "Southern African Development Community"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "../rtadocs/948/TOA/English/SADC%20Protocol_on_Trade1996.pdf",
                        "name" => "E"
                    ],
                    [
                        "url" => "../rtadocs/948/TOA/English/INSTRUMENT%20%20OF%20ACCESSION%20FOR%20SADC.pdf",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Angola"
                ],
                "countries_second" => [
                    "Botswana",
                    "Lesotho",
                    "Madagascar",
                    "Malawi",
                    "Mauritius",
                    "Mozambique",
                    "Namibia",
                    "Seychelles",
                    "South Africa",
                    "Zimbabwe",
                    "Swaziland",
                    "Tanzania",
                    "Zambia"
                ]
            ],
            [
                "id" => 863,
                "rta_id" => 949,
                "name" => "EU - Philippines",
                "coverage" => "",
                "status" => "Early announcement-Under negotiation",
                "notification_under" => "",
                "signature_date" => null,
                "notification_date" => null,
                "entry_force_date" => null,
                "current_signatories" => [
                    ""
                ],
                "original_signatories" => [
                    ""
                ],
                "rta_composition" => [
                    "Bilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "Europe",
                    "East Asia"
                ],
                "wto_members" => true,
                "type" => "",
                "web_addresses" => [
                    [
                        "url" => "http://ec.europa.eu/",
                        "name" => "European Commission"
                    ],
                    [
                        "url" => "http://www.dti.gov.ph/",
                        "name" => "Department of Trade and Industry, Philippines"
                    ]
                ],
                "agreement" => [],
                "countries_first" => [
                    "Philippines"
                ],
                "countries_second" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom"
                ]
            ],
            [
                "id" => 867,
                "rta_id" => 957,
                "name" => "EU - Tunisia",
                "coverage" => "",
                "status" => "Early announcement-Under negotiation",
                "notification_under" => "",
                "signature_date" => null,
                "notification_date" => null,
                "entry_force_date" => null,
                "current_signatories" => [
                    ""
                ],
                "original_signatories" => [
                    ""
                ],
                "rta_composition" => [
                    "Bilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "Europe",
                    "Africa"
                ],
                "wto_members" => true,
                "type" => "",
                "web_addresses" => [],
                "agreement" => [],
                "countries_first" => [
                    "Tunisia"
                ],
                "countries_second" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom"
                ]
            ],
            [
                "id" => 859,
                "rta_id" => 922,
                "name" => "Eurasian Economic Union (EAEU) - Accession of the Kyrgyz Republic",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2014-12-23",
                "notification_date" => "2015-09-01",
                "entry_force_date" => "2015-08-12",
                "current_signatories" => [
                    "Armenia",
                    "Belarus",
                    "Kazakhstan",
                    "Kyrgyzstan",
                    "Russia"
                ],
                "original_signatories" => [
                    "Armenia",
                    "Belarus",
                    "Kazakhstan",
                    "Kyrgyzstan",
                    "Russia"
                ],
                "rta_composition" => [
                    "Plurilateral"
                ],
                "region" => [
                    "Commonwealth of Independent States (CIS), including associate and former member States"
                ],
                "wto_members" => false,
                "type" => "Customs Union & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://eaeunion.org/",
                        "name" => "Eurasian Economic Union"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "../rtadocs/922/TOA/English/Treaty%20on%20accession_KR_EAEU_UnoffTransENG.doc",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Armenia"
                ],
                "countries_second" => [
                    "Belarus",
                    "Kazakhstan",
                    "Kyrgyzstan",
                    "Russia"
                ]
            ],
            [
                "id" => 864,
                "rta_id" => 950,
                "name" => "EFTA - Central America - Accession of Guatemala",
                "coverage" => "Goods & Services",
                "status" => "Early announcement-Signed",
                "notification_under" => "",
                "signature_date" => "2015-06-22",
                "notification_date" => null,
                "entry_force_date" => null,
                "current_signatories" => [
                    "Costa Rica",
                    "Guatemala",
                    "Iceland",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland",
                    "Panama"
                ],
                "original_signatories" => [
                    "Costa Rica",
                    "Guatemala",
                    "Iceland",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland",
                    "Panama"
                ],
                "rta_composition" => [
                    "Plurilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "Central America",
                    "Europe"
                ],
                "wto_members" => true,
                "type" => "",
                "web_addresses" => [],
                "agreement" => [
                    [
                        "url" => "http://www.efta.int/free-trade/Free-Trade-Agreements/central-american-states%20",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Norway",
                    "Switzerland",
                    "Iceland"
                ],
                "countries_second" => [
                    "Costa Rica",
                    "Guatemala",
                    "Panama",
                    "Liechtenstein"
                ]
            ],
            [
                "id" => 868,
                "rta_id" => 966,
                "name" => "South Asian Free Trade Agreement (SAFTA) - Accession of  Afghanistan",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "Enabling Clause",
                "signature_date" => "2008-08-03",
                "notification_date" => "2016-07-29",
                "entry_force_date" => "2011-08-07",
                "current_signatories" => [
                    "Afghanistan",
                    "Bangladesh",
                    "Bhutan",
                    "India",
                    "Maldives",
                    "Nepal",
                    "Pakistan",
                    "Sri Lanka"
                ],
                "original_signatories" => [
                    "Afghanistan",
                    "Bangladesh",
                    "Bhutan",
                    "India",
                    "Maldives",
                    "Nepal",
                    "Pakistan",
                    "Sri Lanka"
                ],
                "rta_composition" => [
                    "Plurilateral"
                ],
                "region" => [
                    "West Asia"
                ],
                "wto_members" => false,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.saarc-sec.org/",
                        "name" => "South Asian Association for Regional Cooperation"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://saarc-sec.org/areaofcooperation/detail.php?activity_id=5",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [],
                "countries_second" => []
            ],
            [
                "id" => 879,
                "rta_id" => 1048,
                "name" => "El Salvador - Ecuador",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "Enabling Clause",
                "signature_date" => "2017-02-13",
                "notification_date" => "2018-03-22",
                "entry_force_date" => "2017-11-16",
                "current_signatories" => [
                    "Ecuador",
                    "El Salvador"
                ],
                "original_signatories" => [
                    "Ecuador",
                    "El Salvador"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "South America",
                    "Central America"
                ],
                "wto_members" => true,
                "type" => "Partial Scope Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://infotrade.minec.gob.sv/ecuador/",
                        "name" => "Ministerio de Economía - Gobierno de El Salvador"
                    ],
                    [
                        "url" => "http://www.comercioexterior.gob.ec/",
                        "name" => "Ministerio de Comercio Exterior, Ecuador"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://infotrade.minec.gob.sv/ecuador/wp-content/uploads/sites/15/2017/11/26.01.2017-ACUERDO-DE-ALCANCE-PARCIAL-SALVADOR-ECUADOR-y-ANEXOS.pdf",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Ecuador"
                ],
                "countries_second" => [
                    "El Salvador"
                ]
            ],
            [
                "id" => 877,
                "rta_id" => 1029,
                "name" => "Hong Kong, China - Macao, China",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2017-10-27",
                "notification_date" => "2017-12-18",
                "entry_force_date" => "2017-10-27",
                "current_signatories" => [
                    "Hong Kong",
                    "Macau"
                ],
                "original_signatories" => [
                    "Hong Kong",
                    "Macau"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "East Asia"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "https://www.tid.gov.hk/",
                        "name" => "Hong Kong Special Administrative Region of the People's Republic of China"
                    ],
                    [
                        "url" => "https://www.economia.gov.mo/zh_TW/pg_home",
                        "name" => "Macao Special Administrative Region Economic Services"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "https://www.tid.gov.hk/english/ita/fta/hkmacao/files/full_text_of_the_agreement.pdf",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Hong Kong"
                ],
                "countries_second" => [
                    "Macau"
                ]
            ],
            [
                "id" => 878,
                "rta_id" => 1035,
                "name" => "Republic of Moldova - China",
                "coverage" => "",
                "status" => "Early announcement-Under negotiation",
                "notification_under" => "",
                "signature_date" => null,
                "notification_date" => null,
                "entry_force_date" => null,
                "current_signatories" => [
                    ""
                ],
                "original_signatories" => [
                    ""
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "East Asia",
                    "Commonwealth of Independent States (CIS), including associate and former member States"
                ],
                "wto_members" => true,
                "type" => "",
                "web_addresses" => [],
                "agreement" => [],
                "countries_first" => [
                    "Moldova"
                ],
                "countries_second" => [
                    "China"
                ]
            ],
            [
                "id" => 870,
                "rta_id" => 973,
                "name" => "Eurasian Economic Union (EAEU) - Viet Nam",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2015-05-29",
                "notification_date" => "2017-05-04",
                "entry_force_date" => "2016-10-05",
                "current_signatories" => [
                    "Vietnam"
                ],
                "original_signatories" => [
                    "Vietnam"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "East Asia"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.eaeunion.org/",
                        "name" => "Eurasian Economic Union"
                    ],
                    [
                        "url" => "http://www.moit.gov.vn/en/Pages/default.aspx",
                        "name" => "Ministry of Industry and Trade of the Socialist Republic of Viet Nam"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "../rtadocs/973/TOA/English/FTA%20text.docx",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Vietnam"
                ],
                "countries_second" => []
            ],
            [
                "id" => 869,
                "rta_id" => 971,
                "name" => "EFTA - Ecuador",
                "coverage" => "",
                "status" => "Early announcement-Under negotiation",
                "notification_under" => "",
                "signature_date" => null,
                "notification_date" => null,
                "entry_force_date" => null,
                "current_signatories" => [
                    ""
                ],
                "original_signatories" => [
                    ""
                ],
                "rta_composition" => [
                    "Bilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "South America",
                    "Europe"
                ],
                "wto_members" => true,
                "type" => "",
                "web_addresses" => [
                    [
                        "url" => "http://www.efta.int/",
                        "name" => "European Free Trade Association"
                    ],
                    [
                        "url" => "http://www.comercioexterior.gob.ec/",
                        "name" => "Ministerio de Comercio Exterior, Ecuador"
                    ]
                ],
                "agreement" => [],
                "countries_first" => [
                    "Ecuador"
                ],
                "countries_second" => [
                    "Iceland",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland"
                ]
            ],
            [
                "id" => 875,
                "rta_id" => 1022,
                "name" => "EFTA - MERCOSUR",
                "coverage" => "",
                "status" => "Early announcement-Under negotiation",
                "notification_under" => "",
                "signature_date" => null,
                "notification_date" => null,
                "entry_force_date" => null,
                "current_signatories" => [
                    ""
                ],
                "original_signatories" => [
                    ""
                ],
                "rta_composition" => [
                    "Bilateral",
                    "All Parties are RTAs"
                ],
                "region" => [
                    "South America",
                    "Europe"
                ],
                "wto_members" => true,
                "type" => "",
                "web_addresses" => [
                    [
                        "url" => "http://www.efta.int/",
                        "name" => "European Free Trade Association"
                    ],
                    [
                        "url" => "http://www.mercosur.int/",
                        "name" => "Southern Common Market"
                    ]
                ],
                "agreement" => [],
                "countries_first" => [
                    "MERCOSUR"
                ],
                "countries_second" => [
                    "Iceland",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland"
                ]
            ],
            [
                "id" => 874,
                "rta_id" => 1002,
                "name" => "EU - Ghana",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "2016-07-28",
                "notification_date" => "2017-04-03",
                "entry_force_date" => "2016-12-15",
                "current_signatories" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom",
                    "Ghana"
                ],
                "original_signatories" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom",
                    "Ghana"
                ],
                "rta_composition" => [
                    "Bilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "Europe",
                    "Africa"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://ec.europa.eu/",
                        "name" => "European Commission"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?uri=uriserv:OJ.L_.2016.287.01.0003.01.ENG&amp;toc=OJ:L:2016:287:TOC",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Ghana"
                ],
                "countries_second" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom"
                ]
            ],
            [
                "id" => 871,
                "rta_id" => 982,
                "name" => "EU - Indonesia",
                "coverage" => "",
                "status" => "Early announcement-Under negotiation",
                "notification_under" => "",
                "signature_date" => null,
                "notification_date" => null,
                "entry_force_date" => null,
                "current_signatories" => [
                    ""
                ],
                "original_signatories" => [
                    ""
                ],
                "rta_composition" => [
                    "Bilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "Europe",
                    "East Asia"
                ],
                "wto_members" => true,
                "type" => "",
                "web_addresses" => [
                    [
                        "url" => "http://europa.eu/",
                        "name" => "European Commission"
                    ],
                    [
                        "url" => "http://www.kemlu.go.id/en/Default.aspx",
                        "name" => "Ministry of Foreign Affairs of Indonesia"
                    ]
                ],
                "agreement" => [],
                "countries_first" => [
                    "Indonesia"
                ],
                "countries_second" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom"
                ]
            ],
            [
                "id" => 816,
                "rta_id" => 783,
                "name" => "Argentina - Brazil",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "Enabling Clause",
                "signature_date" => "2016-06-29",
                "notification_date" => "2017-06-15",
                "entry_force_date" => "2016-07-01",
                "current_signatories" => [
                    "Argentina",
                    "Brazil"
                ],
                "original_signatories" => [
                    "Argentina",
                    "Brazil"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "South America"
                ],
                "wto_members" => true,
                "type" => "Partial Scope Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.economia.gob.ar/",
                        "name" => "Ministerio de Economía y Finanzas Públicas, Argentina"
                    ],
                    [
                        "url" => "http://www.mdic.gov.br/",
                        "name" => "Ministerio de Desarrollo, Industria y Comercio Exterior, Brasil"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.aladi.org/biblioteca/publicaciones/aladi/acuerdos/ace/es/ace14/ACE_014_042.pdf",
                        "name" => "S"
                    ],
                    [
                        "url" => "http://www.aladi.org/nsfaladi/textacdos.nsf/800d239280151ad283257d8000551d1f/92266e44c2dbb1dc032578d2005038cb?OpenDocument",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Argentina"
                ],
                "countries_second" => [
                    "Brazil"
                ]
            ],
            [
                "id" => 380,
                "rta_id" => 50,
                "name" => "Armenia - Kazakhstan",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "1999-09-02",
                "notification_date" => "2004-06-17",
                "entry_force_date" => "2001-12-25",
                "current_signatories" => [
                    "Armenia",
                    "Kazakhstan"
                ],
                "original_signatories" => [
                    "Armenia",
                    "Kazakhstan"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "Commonwealth of Independent States (CIS), including associate and former member States"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement",
                "web_addresses" => [],
                "agreement" => [
                    [
                        "url" => "http://docsonline.wto.org/imrd/gen_redirectsearchdirect.asp?RN=0&amp;searchtype=browse&amp;query=@meta_Symbol&quot;WT/REG172/1&quot;&amp;language=1&amp;ct=DDFEnglish",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://docsonline.wto.org/imrd/gen_redirectsearchdirect.asp?RN=0&amp;searchtype=browse&amp;query=@meta_Symbol&quot;WT/REG172/1&quot;&amp;language=2&amp;ct=DDFFrench",
                        "name" => "F"
                    ],
                    [
                        "url" => "http://docsonline.wto.org/imrd/gen_redirectsearchdirect.asp?RN=0&amp;searchtype=browse&amp;query=@meta_Symbol&quot;WT/REG172/1&quot;&amp;language=3&amp;ct=DDFSpanish",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Armenia"
                ],
                "countries_second" => [
                    "Kazakhstan"
                ]
            ],
            [
                "id" => 379,
                "rta_id" => 49,
                "name" => "Armenia - Moldova, Republic of",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "1993-12-24",
                "notification_date" => "2004-06-17",
                "entry_force_date" => "1995-12-21",
                "current_signatories" => [
                    "Armenia",
                    "Moldova"
                ],
                "original_signatories" => [
                    "Armenia",
                    "Moldova"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "Commonwealth of Independent States (CIS), including associate and former member States"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://mec.gov.md/",
                        "name" => "Ministerul Economiei si Comertului al Republicii Moldova"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://docsonline.wto.org/imrd/gen_redirectsearchdirect.asp?RN=0&amp;searchtype=browse&amp;query=@meta_Symbol&quot;WT/REG173/1&quot;&amp;language=1&amp;ct=DDFEnglish",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://docsonline.wto.org/imrd/gen_redirectsearchdirect.asp?RN=0&amp;searchtype=browse&amp;query=@meta_Symbol&quot;WT/REG173/1&quot;&amp;language=2&amp;ct=DDFFrench",
                        "name" => "F"
                    ],
                    [
                        "url" => "http://docsonline.wto.org/imrd/gen_redirectsearchdirect.asp?RN=0&amp;searchtype=browse&amp;query=@meta_Symbol&quot;WT/REG173/1&quot;&amp;language=3&amp;ct=DDFSpanish",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Armenia"
                ],
                "countries_second" => [
                    "Moldova"
                ]
            ],
            [
                "id" => 377,
                "rta_id" => 47,
                "name" => "Armenia - Turkmenistan",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "1995-10-03",
                "notification_date" => "2004-06-17",
                "entry_force_date" => "1996-07-07",
                "current_signatories" => [
                    "Armenia",
                    "Turkmenistan"
                ],
                "original_signatories" => [
                    "Armenia",
                    "Turkmenistan"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "Commonwealth of Independent States (CIS), including associate and former member States"
                ],
                "wto_members" => false,
                "type" => "Free Trade Agreement",
                "web_addresses" => [],
                "agreement" => [
                    [
                        "url" => "http://docsonline.wto.org/imrd/gen_redirectsearchdirect.asp?RN=0&amp;searchtype=browse&amp;query=@meta_Symbol&quot;WT/REG175/1&quot;&amp;language=1&amp;ct=DDFEnglish",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://docsonline.wto.org/imrd/gen_redirectsearchdirect.asp?RN=0&amp;searchtype=browse&amp;query=@meta_Symbol&quot;WT/REG175/1&quot;&amp;language=2&amp;ct=DDFFrench",
                        "name" => "F"
                    ],
                    [
                        "url" => "http://docsonline.wto.org/imrd/gen_redirectsearchdirect.asp?RN=0&amp;searchtype=browse&amp;query=@meta_Symbol&quot;WT/REG175/1&quot;&amp;language=3&amp;ct=DDFSpanish",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Armenia"
                ],
                "countries_second" => [
                    "Turkmenistan"
                ]
            ],
            [
                "id" => 484,
                "rta_id" => 157,
                "name" => "Australia - Chile",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2008-07-30",
                "notification_date" => "2009-03-03",
                "entry_force_date" => "2009-03-06",
                "current_signatories" => [
                    "Australia",
                    "Chile"
                ],
                "original_signatories" => [
                    "Australia",
                    "Chile"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "Oceania",
                    "South America"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.dfat.gov.au",
                        "name" => "Australian Government, Department of Foreign Affairs & Trade"
                    ],
                    [
                        "url" => "http://www.direcon.gob.cl/",
                        "name" => "Dirección General de Relaciones Económicas Internacionales, Chile"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.dfat.gov.au/trade/agreements/aclfta/Pages/australia-chile-fta.aspx#documents",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://www.direcon.gob.cl/detalle-de-acuerdos/?idacuerdo=6242",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Australia"
                ],
                "countries_second" => [
                    "Chile"
                ]
            ],
            [
                "id" => 461,
                "rta_id" => 134,
                "name" => "Australia - New Zealand Closer Economic Relations Trade Agreement (ANZCERTA)",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "1982-12-14",
                "notification_date" => "1983-04-14",
                "entry_force_date" => "1983-01-01",
                "current_signatories" => [
                    "Australia",
                    "New Zealand"
                ],
                "original_signatories" => [
                    "Australia",
                    "New Zealand"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "Oceania"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.dfat.gov.au/",
                        "name" => "Australian Government, Department of Foreign Affairs & Trade"
                    ],
                    [
                        "url" => "http://www.mfat.govt.nz/",
                        "name" => "New Zealand Ministry of Foreign Affairs and Trade"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://dfat.gov.au/trade/agreements/in-force/anzcerta/Pages/australia-new-zealand-closer-economic-relations-trade-agreement.aspx",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Australia"
                ],
                "countries_second" => [
                    "New Zealand"
                ]
            ],
            [
                "id" => 466,
                "rta_id" => 139,
                "name" => "Australia - Papua New Guinea (PATCRA)",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "1976-11-06",
                "notification_date" => "1976-12-20",
                "entry_force_date" => "1977-02-01",
                "current_signatories" => [
                    "Australia",
                    "Papua New Guinea"
                ],
                "original_signatories" => [
                    "Australia",
                    "Papua New Guinea"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "Oceania"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.dfat.gov.au/",
                        "name" => "Australian Government, Department of Foreign Affairs"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "../rtadocs/139/TOA/English/PATCRAAgreement.pdf",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Australia"
                ],
                "countries_second" => [
                    "Papua New Guinea"
                ]
            ],
            [
                "id" => 736,
                "rta_id" => 567,
                "name" => "Brunei Darussalam - Japan",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2007-06-18",
                "notification_date" => "2008-07-31",
                "entry_force_date" => "2008-07-31",
                "current_signatories" => [
                    "Brunei Darussalam",
                    "Japan"
                ],
                "original_signatories" => [
                    "Brunei Darussalam",
                    "Japan"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "East Asia"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.mofa.go.jp",
                        "name" => "Ministry of Foreign Affairs, Japan"
                    ],
                    [
                        "url" => "http://www.mofat.gov.bn/",
                        "name" => "Ministry of Foreign Affairs and Trade, Brunei Darussalam"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.mofa.go.jp/region/asia-paci/brunei/epa0706/agreement.pdf",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Brunei Darussalam"
                ],
                "countries_second" => [
                    "Japan"
                ]
            ],
            [
                "id" => 438,
                "rta_id" => 110,
                "name" => "Canada - Chile",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "1996-12-05",
                "notification_date" => "1997-07-30",
                "entry_force_date" => "1997-07-05",
                "current_signatories" => [
                    "Canada",
                    "Chile"
                ],
                "original_signatories" => [
                    "Canada",
                    "Chile"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "North America",
                    "South America"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.international.gc.ca/",
                        "name" => "Global Affairs Canada"
                    ],
                    [
                        "url" => "http://www.direcon.gob.cl/",
                        "name" => "Dirección General de Relaciones Económicas Internacionales, Chile"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://international.gc.ca/trade-commerce/trade-agreements-accords-commerciaux/agr-acc/chile-chili/fta-ale/index.aspx?lang=eng",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://international.gc.ca/trade-commerce/trade-agreements-accords-commerciaux/agr-acc/chile-chili/fta-ale/index.aspx?lang=fra",
                        "name" => "F"
                    ],
                    [
                        "url" => "http://www.direcon.gob.cl/detalle-de-acuerdos/?idacuerdo=6205",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Canada"
                ],
                "countries_second" => [
                    "Chile"
                ]
            ],
            [
                "id" => 489,
                "rta_id" => 162,
                "name" => "Canada - Colombia",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2008-11-21",
                "notification_date" => "2011-10-07",
                "entry_force_date" => "2011-08-15",
                "current_signatories" => [
                    "Canada",
                    "Colombia"
                ],
                "original_signatories" => [
                    "Canada",
                    "Colombia"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "North America",
                    "South America"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.international.gc.ca/",
                        "name" => "Global Affairs Canada"
                    ],
                    [
                        "url" => "http://www.tlc.gov.co/index.php",
                        "name" => "Ministerio de Comercio, Industria y Turismo, República de Colombia"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://international.gc.ca/trade-commerce/trade-agreements-accords-commerciaux/agr-acc/colombia-colombie/fta-ale/index.aspx?lang=eng",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://international.gc.ca/trade-commerce/trade-agreements-accords-commerciaux/agr-acc/colombia-colombie/fta-ale/index.aspx?lang=fra",
                        "name" => "F"
                    ],
                    [
                        "url" => "http://www.tlc.gov.co/publicaciones.php?id=16157",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Canada"
                ],
                "countries_second" => [
                    "Colombia"
                ]
            ],
            [
                "id" => 395,
                "rta_id" => 66,
                "name" => "Canada - Costa Rica",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "2001-04-23",
                "notification_date" => "2003-01-13",
                "entry_force_date" => "2002-11-01",
                "current_signatories" => [
                    "Canada",
                    "Costa Rica"
                ],
                "original_signatories" => [
                    "Canada",
                    "Costa Rica"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "North America",
                    "Central America"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.international.gc.ca/",
                        "name" => "Global Affairs Canada"
                    ],
                    [
                        "url" => "http://www.comex.go.cr/",
                        "name" => "Ministerio de Comercio Exterior de Costa Rica"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://international.gc.ca/trade-commerce/trade-agreements-accords-commerciaux/agr-acc/costa_rica/fta-ale/index.aspx?lang=eng",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://international.gc.ca/trade-commerce/trade-agreements-accords-commerciaux/agr-acc/costa_rica/fta-ale/index.aspx?lang=fra",
                        "name" => "F"
                    ],
                    [
                        "url" => "http://www.comex.go.cr/media/5154/canada-ley-7870.pdf",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Canada"
                ],
                "countries_second" => [
                    "Costa Rica"
                ]
            ],
            [
                "id" => 857,
                "rta_id" => 911,
                "name" => "Canada - Honduras",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2013-11-05",
                "notification_date" => "2015-02-05",
                "entry_force_date" => "2014-10-01",
                "current_signatories" => [
                    "Canada",
                    "Honduras"
                ],
                "original_signatories" => [
                    "Canada",
                    "Honduras"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "North America",
                    "Central America"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.international.gc.ca/",
                        "name" => "Global Affairs Canada"
                    ],
                    [
                        "url" => "https://sde.gob.hn/",
                        "name" => "Secretaría de Desarollo Económico, Honduras"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://international.gc.ca/trade-commerce/trade-agreements-accords-commerciaux/agr-acc/honduras/fta-ale/index.aspx?lang=eng",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://international.gc.ca/trade-commerce/trade-agreements-accords-commerciaux/agr-acc/honduras/fta-ale/index.aspx?lang=fra",
                        "name" => "F"
                    ],
                    [
                        "url" => "https://sde.gob.hn/2017/08/17/canada/",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Canada"
                ],
                "countries_second" => [
                    "Honduras"
                ]
            ],
            [
                "id" => 441,
                "rta_id" => 113,
                "name" => "Canada - Israel",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "1996-07-31",
                "notification_date" => "1997-01-15",
                "entry_force_date" => "1997-01-01",
                "current_signatories" => [
                    "Canada",
                    "Israel"
                ],
                "original_signatories" => [
                    "Canada",
                    "Israel"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "North America",
                    "Middle East"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.international.gc.ca/",
                        "name" => "Global Affairs Canada"
                    ],
                    [
                        "url" => "http://economy.gov.il/English/Pages/default.aspx",
                        "name" => "Ministry of Industry, Trade & Labor, Israel"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://international.gc.ca/trade-commerce/trade-agreements-accords-commerciaux/agr-acc/israel/fta-ale/index.aspx?lang=eng",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://international.gc.ca/trade-commerce/trade-agreements-accords-commerciaux/agr-acc/israel/fta-ale/index.aspx?lang=fra",
                        "name" => "F"
                    ]
                ],
                "countries_first" => [
                    "Canada"
                ],
                "countries_second" => [
                    "Israel"
                ]
            ],
            [
                "id" => 737,
                "rta_id" => 568,
                "name" => "Canada - Jordan",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "2009-06-28",
                "notification_date" => "2013-04-10",
                "entry_force_date" => "2012-10-01",
                "current_signatories" => [
                    "Canada",
                    "Jordan"
                ],
                "original_signatories" => [
                    "Canada",
                    "Jordan"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "North America",
                    "Middle East"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.international.gc.ca/",
                        "name" => "Global Affairs Canada"
                    ],
                    [
                        "url" => "http://www.mit.gov.jo/",
                        "name" => "Jordan's Foreign Trade Policy Department, Ministry of Industry and Trade"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://international.gc.ca/trade-commerce/trade-agreements-accords-commerciaux/agr-acc/jordan-jordanie/fta-ale/index.aspx?lang=eng",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://international.gc.ca/trade-commerce/trade-agreements-accords-commerciaux/agr-acc/jordan-jordanie/fta-ale/index.aspx?lang=fra",
                        "name" => "F"
                    ]
                ],
                "countries_first" => [
                    "Canada"
                ],
                "countries_second" => [
                    "Jordan"
                ]
            ],
            [
                "id" => 494,
                "rta_id" => 168,
                "name" => "Canada - Korea, Republic of",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2014-09-22",
                "notification_date" => "2015-01-20",
                "entry_force_date" => "2015-01-01",
                "current_signatories" => [
                    "Canada",
                    "North Korea"
                ],
                "original_signatories" => [
                    "Canada",
                    "North Korea"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "North America",
                    "East Asia"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.mofa.go.kr/eng/index.do",
                        "name" => "Ministry of Foreign Affairs and Trade, Republic of Korea"
                    ],
                    [
                        "url" => "http://www.international.gc.ca/",
                        "name" => "Global Affairs Canada"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://international.gc.ca/trade-commerce/trade-agreements-accords-commerciaux/agr-acc/korea-coree/fta-ale/index.aspx?lang=eng",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://international.gc.ca/trade-commerce/trade-agreements-accords-commerciaux/agr-acc/korea-coree/fta-ale/index.aspx?lang=fra",
                        "name" => "F"
                    ]
                ],
                "countries_first" => [
                    "Canada"
                ],
                "countries_second" => [
                    "North Korea"
                ]
            ],
            [
                "id" => 756,
                "rta_id" => 608,
                "name" => "Canada - Panama",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2010-05-14",
                "notification_date" => "2013-04-10",
                "entry_force_date" => "2013-04-01",
                "current_signatories" => [
                    "Canada",
                    "Panama"
                ],
                "original_signatories" => [
                    "Canada",
                    "Panama"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "North America",
                    "Central America"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.international.gc.ca/",
                        "name" => "Global Affairs Canada"
                    ],
                    [
                        "url" => "http://www.mici.gob.pa/base.php?hoja=homepage",
                        "name" => "Ministerio de Comercio e Industrias, Panamá"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://international.gc.ca/trade-commerce/trade-agreements-accords-commerciaux/agr-acc/panama/fta-ale/index.aspx?lang=eng",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://international.gc.ca/trade-commerce/trade-agreements-accords-commerciaux/agr-acc/panama/fta-ale/index.aspx?lang=fra",
                        "name" => "F"
                    ],
                    [
                        "url" => "http://www.mici.gob.pa/detalle.php?cid=15&amp;sid=57&amp;clid=186&amp;id=4035",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Canada"
                ],
                "countries_second" => [
                    "Panama"
                ]
            ],
            [
                "id" => 778,
                "rta_id" => 672,
                "name" => "Canada - Peru",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2008-05-29",
                "notification_date" => "2009-07-31",
                "entry_force_date" => "2009-08-01",
                "current_signatories" => [
                    "Canada",
                    "Peru"
                ],
                "original_signatories" => [
                    "Canada",
                    "Peru"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "North America",
                    "South America"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.mincetur.gob.pe/newweb/",
                        "name" => "Ministerio de Comercio Exterior de Perú"
                    ],
                    [
                        "url" => "http://www.international.gc.ca/",
                        "name" => "Global Affairs Canada"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://international.gc.ca/trade-commerce/trade-agreements-accords-commerciaux/agr-acc/peru-perou/fta-ale/index.aspx?lang=eng",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://international.gc.ca/trade-commerce/trade-agreements-accords-commerciaux/agr-acc/peru-perou/fta-ale/index.aspx?lang=fra",
                        "name" => "F"
                    ],
                    [
                        "url" => "http://www.acuerdoscomerciales.gob.pe/index.php?option=com_content&amp;view=category&amp;layout=blog&amp;id=62&amp;Itemid=85",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Canada"
                ],
                "countries_second" => [
                    "Peru"
                ]
            ],
            [
                "id" => 788,
                "rta_id" => 694,
                "name" => "Canada - Ukraine",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "2016-07-11",
                "notification_date" => "2017-09-13",
                "entry_force_date" => "2017-08-01",
                "current_signatories" => [
                    "Canada",
                    "Ukraine"
                ],
                "original_signatories" => [
                    "Canada",
                    "Ukraine"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "North America",
                    "Commonwealth of Independent States (CIS), including associate and former member States"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.international.gc.ca/",
                        "name" => "Global Affairs Canada"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://international.gc.ca/trade-commerce/trade-agreements-accords-commerciaux/agr-acc/ukraine/text-texte/toc-tdm.aspx?lang=eng",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://international.gc.ca/trade-commerce/trade-agreements-accords-commerciaux/agr-acc/ukraine/text-texte/toc-tdm.aspx?lang=fra",
                        "name" => "F"
                    ]
                ],
                "countries_first" => [
                    "Canada"
                ],
                "countries_second" => [
                    "Ukraine"
                ]
            ],
            [
                "id" => 340,
                "rta_id" => 8,
                "name" => "Chile - China",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2005-11-18",
                "notification_date" => "2007-06-20",
                "entry_force_date" => "2006-10-01",
                "current_signatories" => [
                    "Chile",
                    "China"
                ],
                "original_signatories" => [
                    "Chile",
                    "China"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "South America",
                    "East Asia"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.direcon.gob.cl/",
                        "name" => "Dirección General de Relaciones Económicas Internacionales, Chile"
                    ],
                    [
                        "url" => "http://www.mofcom.gov.cn/",
                        "name" => "Ministry of Commerce of the People's Republic of China"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://fta.mofcom.gov.cn/topic/enchile.shtml",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://www.direcon.gob.cl/detalle-de-acuerdos/?idacuerdo=6246#tabs-2",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Chile"
                ],
                "countries_second" => [
                    "China"
                ]
            ],
            [
                "id" => 721,
                "rta_id" => 509,
                "name" => "Chile - Colombia",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2006-11-27",
                "notification_date" => "2009-08-14",
                "entry_force_date" => "2009-05-08",
                "current_signatories" => [
                    "Chile",
                    "Colombia"
                ],
                "original_signatories" => [
                    "Chile",
                    "Colombia"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "South America"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.direcon.gob.cl/",
                        "name" => "Dirección General de Relaciones Económicas Internacionales, Chile"
                    ],
                    [
                        "url" => "http://www.tlc.gov.co/index.php",
                        "name" => "Ministerio de Comercio, Industria y Turismo, Colombia"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.direcon.gob.cl/detalle-de-acuerdos/?idacuerdo=6271",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Chile"
                ],
                "countries_second" => [
                    "Colombia"
                ]
            ],
            [
                "id" => 402,
                "rta_id" => 74,
                "name" => "Chile - Costa Rica (Chile - Central America)",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "1999-10-18",
                "notification_date" => "2002-04-16",
                "entry_force_date" => "2002-02-15",
                "current_signatories" => [
                    "Chile",
                    "Costa Rica"
                ],
                "original_signatories" => [
                    "Chile",
                    "Costa Rica"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "South America",
                    "Central America"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.direcon.gob.cl/",
                        "name" => "Dirección General de Relaciones Económicas Internacionales, Chile"
                    ],
                    [
                        "url" => "http://www.comex.go.cr",
                        "name" => "Ministerio de Comercio Exterior de Costa Rica"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.direcon.gob.cl/detalle-de-acuerdos/?idacuerdo=6290",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Chile"
                ],
                "countries_second" => [
                    "Costa Rica"
                ]
            ],
            [
                "id" => 722,
                "rta_id" => 510,
                "name" => "Chile - Guatemala (Chile - Central America)",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "1999-10-18",
                "notification_date" => "2012-03-30",
                "entry_force_date" => "2010-03-23",
                "current_signatories" => [
                    "Chile",
                    "Guatemala"
                ],
                "original_signatories" => [
                    "Chile",
                    "Guatemala"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "South America",
                    "Central America"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.direcon.gob.cl/",
                        "name" => "Dirección General de Relaciones Económicas Internacionales, Chile"
                    ],
                    [
                        "url" => "http://www.mineco.gob.gt/",
                        "name" => "Ministerio de Economía de Guatemala"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.direcon.gob.cl/detalle-de-acuerdos/?idacuerdo=6290",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Chile"
                ],
                "countries_second" => [
                    "Guatemala"
                ]
            ],
            [
                "id" => 762,
                "rta_id" => 625,
                "name" => "Chile - India",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "Enabling Clause",
                "signature_date" => "2006-03-08",
                "notification_date" => "2009-01-13",
                "entry_force_date" => "2007-08-17",
                "current_signatories" => [
                    "Chile",
                    "India"
                ],
                "original_signatories" => [
                    "Chile",
                    "India"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "South America",
                    "West Asia"
                ],
                "wto_members" => true,
                "type" => "Partial Scope Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.direcon.gob.cl/",
                        "name" => "Dirección General de Relaciones Económicas Internacionales, Chile"
                    ],
                    [
                        "url" => "http://www.commerce.nic.in/",
                        "name" => "Ministry of Commerce and Industry, Government of India"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://commerce.gov.in/PageContent.aspx?Id=43",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://www.direcon.gob.cl/detalle-de-acuerdos/?idacuerdo=6235",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Chile"
                ],
                "countries_second" => [
                    "India"
                ]
            ],
            [
                "id" => 710,
                "rta_id" => 453,
                "name" => "Chile - Malaysia",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "2010-11-13",
                "notification_date" => "2013-02-12",
                "entry_force_date" => "2012-02-25",
                "current_signatories" => [
                    "Chile",
                    "Malaysia"
                ],
                "original_signatories" => [
                    "Chile",
                    "Malaysia"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "South America",
                    "East Asia"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.direcon.gob.cl/",
                        "name" => "Dirección General de Relaciones Económicas Internacionales, Chile"
                    ],
                    [
                        "url" => "http://www.miti.gov.my/",
                        "name" => "Malaysia Trade & Industry Portal"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://fta.miti.gov.my/index.php/pages/view/Malaysia-Chile?mid=43",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://www.direcon.gob.cl/detalle-de-acuerdos/?idacuerdo=6230",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Chile"
                ],
                "countries_second" => [
                    "Malaysia"
                ]
            ],
            [
                "id" => 408,
                "rta_id" => 80,
                "name" => "Chile - Mexico",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "1998-04-17",
                "notification_date" => "2001-02-27",
                "entry_force_date" => "1999-08-01",
                "current_signatories" => [
                    "Chile",
                    "Mexico"
                ],
                "original_signatories" => [
                    "Chile",
                    "Mexico"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "South America",
                    "North America"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.direcon.gob.cl/",
                        "name" => "Dirección General de Relaciones Económicas Internacionales, Chile"
                    ],
                    [
                        "url" => "http://www.economia.gob.mx",
                        "name" => "Secretaría de Economía, México"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.direcon.gob.cl/detalle-de-acuerdos/?idacuerdo=6208",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Chile"
                ],
                "countries_second" => [
                    "Mexico"
                ]
            ],
            [
                "id" => 784,
                "rta_id" => 684,
                "name" => "Chile - Thailand",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2013-10-04",
                "notification_date" => "2017-09-12",
                "entry_force_date" => "2015-11-05",
                "current_signatories" => [
                    "Chile",
                    "Thailand"
                ],
                "original_signatories" => [
                    "Chile",
                    "Thailand"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "South America",
                    "East Asia"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "https://www.direcon.gob.cl/",
                        "name" => "Dirección General de Relaciones Económicas Internacionales, Chile"
                    ],
                    [
                        "url" => "http://www.mfa.go.th/main/",
                        "name" => "Ministry of Foreign Affairs, Kingdom of Thailand"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "https://www.direcon.gob.cl/wp-content/uploads/2015/11/FTA-Chile-Thailand-english.pdf",
                        "name" => "E"
                    ],
                    [
                        "url" => "https://www.direcon.gob.cl/wp-content/uploads/2015/11/TLC-Chile-Tailandia-espa--ol.pdf",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Chile"
                ],
                "countries_second" => [
                    "Thailand"
                ]
            ],
            [
                "id" => 873,
                "rta_id" => 991,
                "name" => "China - Georgia",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2017-05-13",
                "notification_date" => "2018-04-05",
                "entry_force_date" => "2018-01-01",
                "current_signatories" => [
                    "China",
                    "Georgia"
                ],
                "original_signatories" => [
                    "China",
                    "Georgia"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "East Asia",
                    "Commonwealth of Independent States (CIS), including associate and former member States"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.mofcom.gov.cn/",
                        "name" => "Ministry of Commerce of the People's Republic of China"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://fta.mofcom.gov.cn/topic/engeorgia.shtml",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "China"
                ],
                "countries_second" => [
                    "Georgia"
                ]
            ],
            [
                "id" => 828,
                "rta_id" => 839,
                "name" => "Costa Rica - Colombia",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2013-05-22",
                "notification_date" => "2016-10-31",
                "entry_force_date" => "2016-08-01",
                "current_signatories" => [
                    "Colombia",
                    "Costa Rica"
                ],
                "original_signatories" => [
                    "Colombia",
                    "Costa Rica"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "South America",
                    "Central America"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.comex.go.cr",
                        "name" => "Ministerio de Comercio Exterior de Costa Rica"
                    ],
                    [
                        "url" => "http://www.tlc.gov.co/index.php",
                        "name" => "Ministerio de Comercio, Industria y Turismo de la República de Colombia"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.tlc.gov.co/publicaciones/6669/texto_del_acuerdo_-_espanol",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Colombia"
                ],
                "countries_second" => [
                    "Costa Rica"
                ]
            ],
            [
                "id" => 802,
                "rta_id" => 741,
                "name" => "Costa Rica - Peru",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2011-05-26",
                "notification_date" => "2013-06-05",
                "entry_force_date" => "2013-06-01",
                "current_signatories" => [
                    "Costa Rica",
                    "Peru"
                ],
                "original_signatories" => [
                    "Costa Rica",
                    "Peru"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "Central America",
                    "South America"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.comex.go.cr/",
                        "name" => "Ministerio de Comercio Exterior de Costa Rica"
                    ],
                    [
                        "url" => "http://www.mincetur.gob.pe/newweb/",
                        "name" => "Ministerio de Comercio Exterior de Perú"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.acuerdoscomerciales.gob.pe/index.php?option=com_content&amp;view=category&amp;layout=blog&amp;id=117&amp;Itemid=140",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Costa Rica"
                ],
                "countries_second" => [
                    "Peru"
                ]
            ],
            [
                "id" => 781,
                "rta_id" => 678,
                "name" => "Costa Rica - Singapore",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2010-04-06",
                "notification_date" => "2013-09-16",
                "entry_force_date" => "2013-07-01",
                "current_signatories" => [
                    "Costa Rica",
                    "Singapore"
                ],
                "original_signatories" => [
                    "Costa Rica",
                    "Singapore"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "Central America",
                    "East Asia"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.comex.go.cr",
                        "name" => "Ministerio de Comercio Exterior de Costa Rica"
                    ],
                    [
                        "url" => "http://www.iesingapore.gov.sg/",
                        "name" => "International Enterprise Singapore"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.iesingapore.gov.sg/~/media/IE%20Singapore/Files/FTA/Non%20Priority%20FTAs/Singapore%20Coasta%20Rica%20FTA/Legal%20Text/Costa20Rica20SCRFTA20Legal20Text.pdf",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://www.comex.go.cr/tratados/singapur/texto-del-tratado-1/",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Costa Rica"
                ],
                "countries_second" => [
                    "Singapore"
                ]
            ],
            [
                "id" => 846,
                "rta_id" => 881,
                "name" => "El Salvador - Cuba",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "Enabling Clause",
                "signature_date" => "2011-09-19",
                "notification_date" => "2013-11-27",
                "entry_force_date" => "2012-08-01",
                "current_signatories" => [
                    "Cuba",
                    "El Salvador"
                ],
                "original_signatories" => [
                    "Cuba",
                    "El Salvador"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "Caribbean",
                    "Central America"
                ],
                "wto_members" => true,
                "type" => "Partial Scope Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.minec.gob.sv/",
                        "name" => "Ministerio de Economía - Gobierno de El Salvador"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "../rtadocs/881/TOA/Spanish/TOA%20El%20Salvador-Cuba.pdf",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Cuba"
                ],
                "countries_second" => [
                    "El Salvador"
                ]
            ],
            [
                "id" => 443,
                "rta_id" => 115,
                "name" => "Faroe Islands - Norway",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "1992-08-28",
                "notification_date" => "1996-02-12",
                "entry_force_date" => "1993-07-01",
                "current_signatories" => [
                    "Faeroe Islands",
                    "Norway"
                ],
                "original_signatories" => [
                    "Faeroe Islands",
                    "Norway"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "Europe"
                ],
                "wto_members" => false,
                "type" => "Free Trade Agreement",
                "web_addresses" => [],
                "agreement" => [
                    [
                        "url" => "http://docsonline.wto.org/imrd/gen_redirectsearchdirect.asp?RN=0&amp;searchtype=browse&amp;query=@meta_Symbol&quot;WT/REG25/1&quot;&amp;language=1&amp;ct=DDFEnglish",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://docsonline.wto.org/imrd/gen_redirectsearchdirect.asp?RN=0&amp;searchtype=browse&amp;query=@meta_Symbol&quot;WT/REG25/1&quot;&amp;language=2&amp;ct=DDFFrench",
                        "name" => "F"
                    ],
                    [
                        "url" => "http://docsonline.wto.org/imrd/gen_redirectsearchdirect.asp?RN=0&amp;searchtype=browse&amp;query=@meta_Symbol&quot;WT/REG25/1&quot;&amp;language=3&amp;ct=DDFSpanish",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Faeroe Islands"
                ],
                "countries_second" => [
                    "Norway"
                ]
            ],
            [
                "id" => 410,
                "rta_id" => 82,
                "name" => "Georgia - Armenia",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "1995-08-14",
                "notification_date" => "2001-02-08",
                "entry_force_date" => "1998-11-11",
                "current_signatories" => [
                    "Armenia",
                    "Georgia"
                ],
                "original_signatories" => [
                    "Armenia",
                    "Georgia"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "Commonwealth of Independent States (CIS), including associate and former member States"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement",
                "web_addresses" => [],
                "agreement" => [
                    [
                        "url" => "http://docsonline.wto.org/imrd/gen_redirectsearchdirect.asp?RN=0&amp;searchtype=browse&amp;query=@meta_Symbol&quot;WT/REG119/1&quot;&amp;language=1&amp;ct=DDFEnglish",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://docsonline.wto.org/imrd/gen_redirectsearchdirect.asp?RN=0&amp;searchtype=browse&amp;query=@meta_Symbol&quot;WT/REG119/1&quot;&amp;language=2&amp;ct=DDFFrench",
                        "name" => "F"
                    ],
                    [
                        "url" => "http://docsonline.wto.org/imrd/gen_redirectsearchdirect.asp?RN=0&amp;searchtype=browse&amp;query=@meta_Symbol&quot;WT/REG119/1&quot;&amp;language=3&amp;ct=DDFSpanish",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Armenia"
                ],
                "countries_second" => [
                    "Georgia"
                ]
            ],
            [
                "id" => 706,
                "rta_id" => 424,
                "name" => "Guatemala - Chinese Taipei",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2005-09-22",
                "notification_date" => "2011-07-11",
                "entry_force_date" => "2006-07-01",
                "current_signatories" => [
                    "Taiwan",
                    "Guatemala"
                ],
                "original_signatories" => [
                    "Taiwan",
                    "Guatemala"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "East Asia",
                    "Central America"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.mineco.gob.gt/",
                        "name" => "Ministerio de Economia de Guatemala"
                    ],
                    [
                        "url" => "https://www.mofa.gov.tw/en/default.html",
                        "name" => "Bureau of Foreign Trade, The Separate Customs Territory of Taiwan, Penghu, Kinmen and Matsu"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.trade.gov.tw/english/Pages/List.aspx?nodeID=676",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://www.mineco.gob.gt/node/540",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Taiwan"
                ],
                "countries_second" => [
                    "Guatemala"
                ]
            ],
            [
                "id" => 804,
                "rta_id" => 746,
                "name" => "Hong Kong, China - Chile",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2012-12-07",
                "notification_date" => "2014-10-15",
                "entry_force_date" => "2014-10-09",
                "current_signatories" => [
                    "Chile",
                    "Hong Kong"
                ],
                "original_signatories" => [
                    "Chile",
                    "Hong Kong"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "South America",
                    "East Asia"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.tid.gov.hk/",
                        "name" => "Hong Kong Special Administrative Region of the People's Republic of China"
                    ],
                    [
                        "url" => "http://www.direcon.gob.cl/",
                        "name" => "Dirección General de Relaciones Económicas Internacionales, Chile"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.tid.gov.hk/english/trade_relations/hkclfta/text_agreement.html",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://www.direcon.gob.cl/detalle-de-acuerdos/?idacuerdo=18142",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Chile"
                ],
                "countries_second" => [
                    "Hong Kong"
                ]
            ],
            [
                "id" => 843,
                "rta_id" => 873,
                "name" => "India - Thailand",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "Enabling Clause",
                "signature_date" => "2003-10-09",
                "notification_date" => "2017-06-18",
                "entry_force_date" => "2004-09-01",
                "current_signatories" => [
                    "India",
                    "Thailand"
                ],
                "original_signatories" => [
                    "India",
                    "Thailand"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "West Asia",
                    "East Asia"
                ],
                "wto_members" => true,
                "type" => "Partial Scope Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://commerce.gov.in/",
                        "name" => "Ministry of Commerce and Industry, Government of India"
                    ],
                    [
                        "url" => "http://www.mfa.go.th/main/",
                        "name" => "Ministry of Foreign Affairs, Kingdom of Thailand"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.thaifta.com/english/eng_in.html",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "India"
                ],
                "countries_second" => [
                    "Thailand"
                ]
            ],
            [
                "id" => 352,
                "rta_id" => 22,
                "name" => "Japan - Malaysia",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2005-12-13",
                "notification_date" => "2006-07-12",
                "entry_force_date" => "2006-07-13",
                "current_signatories" => [
                    "Japan",
                    "Malaysia"
                ],
                "original_signatories" => [
                    "Japan",
                    "Malaysia"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "East Asia"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.mofa.go.jp/",
                        "name" => "Ministry of Foreign Affairs of Japan"
                    ],
                    [
                        "url" => "http://www.miti.gov.my",
                        "name" => "Malaysia Trade & Industry Portal"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.mofa.go.jp/region/asia-paci/malaysia/epa/content.pdf",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Japan"
                ],
                "countries_second" => [
                    "Malaysia"
                ]
            ],
            [
                "id" => 505,
                "rta_id" => 179,
                "name" => "Japan - Switzerland",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2009-02-19",
                "notification_date" => "2009-09-01",
                "entry_force_date" => "2009-09-01",
                "current_signatories" => [
                    "Japan",
                    "Switzerland"
                ],
                "original_signatories" => [
                    "Japan",
                    "Switzerland"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "East Asia",
                    "Europe"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.mofa.go.jp/",
                        "name" => "Ministry of Foreign Affairs of Japan"
                    ],
                    [
                        "url" => "http://www.seco.admin.ch",
                        "name" => "Confédération suisse"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.mofa.go.jp/region/europe/switzerland/epa0902/agreement.pdf",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Japan"
                ],
                "countries_second" => [
                    "Switzerland"
                ]
            ],
            [
                "id" => 353,
                "rta_id" => 23,
                "name" => "Jordan - Singapore",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2004-05-16",
                "notification_date" => "2006-07-07",
                "entry_force_date" => "2005-08-22",
                "current_signatories" => [
                    "Jordan",
                    "Singapore"
                ],
                "original_signatories" => [
                    "Jordan",
                    "Singapore"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "Middle East",
                    "East Asia"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.mit.gov.jo",
                        "name" => "Jordan's Foreign Trade Policy Department, Ministry of Industry and Trade"
                    ],
                    [
                        "url" => "http://www.iesingapore.gov.sg",
                        "name" => "International Enterprise Singapore"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.iesingapore.gov.sg/~/media/IE%20Singapore/Files/FTA/Non%20Priority%20FTAs/Hashemite%20Kingdom%20of%20Jordan/Legal%20Text/Hashemite20Kingdom20of20Jordan20SJFTA20Legal20Text.pdf",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Jordan"
                ],
                "countries_second" => [
                    "Singapore"
                ]
            ],
            [
                "id" => 358,
                "rta_id" => 28,
                "name" => "Korea, Republic of - Singapore",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2005-08-04",
                "notification_date" => "2006-02-21",
                "entry_force_date" => "2006-03-02",
                "current_signatories" => [
                    "North Korea",
                    "Singapore"
                ],
                "original_signatories" => [
                    "North Korea",
                    "Singapore"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "East Asia"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.mofa.go.kr/eng/index.do",
                        "name" => "Ministry of Foreign Affairs and Trade, Republic of Korea"
                    ],
                    [
                        "url" => "http://www.iesingapore.gov.sg",
                        "name" => "International Enterprise Singapore"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "https://www.iesingapore.gov.sg/~/media/IE%20Singapore/Files/FTA/Existing%20FTA/Korea%20Singapore%20FTA/Legal%20Text/KSFTA20Legal20Text1.pdf",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "North Korea"
                ],
                "countries_second" => [
                    "Singapore"
                ]
            ],
            [
                "id" => 509,
                "rta_id" => 183,
                "name" => "Korea, Republic of - United States",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2007-06-30",
                "notification_date" => "2012-03-15",
                "entry_force_date" => "2012-03-15",
                "current_signatories" => [
                    "North Korea",
                    "USA"
                ],
                "original_signatories" => [
                    "North Korea",
                    "USA"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "East Asia",
                    "North America"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.mofa.go.kr/eng/index.do",
                        "name" => "Ministry of Foreign Affairs and Trade, Republic of Korea"
                    ],
                    [
                        "url" => "http://www.ustr.gov/",
                        "name" => "United States Trade Representative"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "https://ustr.gov/trade-agreements/free-trade-agreements/korus-fta/final-text",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "North Korea"
                ],
                "countries_second" => [
                    "USA"
                ]
            ],
            [
                "id" => 418,
                "rta_id" => 90,
                "name" => "Kyrgyz Republic - Armenia",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "1994-07-04",
                "notification_date" => "2000-12-12",
                "entry_force_date" => "1995-10-27",
                "current_signatories" => [
                    "Armenia",
                    "Kyrgyzstan"
                ],
                "original_signatories" => [
                    "Armenia",
                    "Kyrgyzstan"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "Commonwealth of Independent States (CIS), including associate and former member States"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement",
                "web_addresses" => [],
                "agreement" => [
                    [
                        "url" => "http://docsonline.wto.org/imrd/gen_redirectsearchdirect.asp?RN=0&amp;searchtype=browse&amp;query=@meta_Symbol&quot;WT/REG114/1&quot;&amp;language=1&amp;ct=DDFEnglish",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://docsonline.wto.org/imrd/gen_redirectsearchdirect.asp?RN=0&amp;searchtype=browse&amp;query=@meta_Symbol&quot;WT/REG114/1&quot;&amp;language=2&amp;ct=DDFFrench",
                        "name" => "F"
                    ],
                    [
                        "url" => "http://docsonline.wto.org/imrd/gen_redirectsearchdirect.asp?RN=0&amp;searchtype=browse&amp;query=@meta_Symbol&quot;WT/REG114/1&quot;&amp;language=3&amp;ct=DDFSpanish",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Armenia"
                ],
                "countries_second" => [
                    "Kyrgyzstan"
                ]
            ],
            [
                "id" => 428,
                "rta_id" => 100,
                "name" => "Kyrgyz Republic - Kazakhstan",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "1995-06-22",
                "notification_date" => "1999-06-29",
                "entry_force_date" => "1995-11-11",
                "current_signatories" => [
                    "Kazakhstan",
                    "Kyrgyzstan"
                ],
                "original_signatories" => [
                    "Kazakhstan",
                    "Kyrgyzstan"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "Commonwealth of Independent States (CIS), including associate and former member States"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement",
                "web_addresses" => [],
                "agreement" => [
                    [
                        "url" => "http://docsonline.wto.org/imrd/gen_redirectsearchdirect.asp?RN=0&amp;searchtype=browse&amp;query=@meta_Symbol&quot;WT/REG81/1&quot;&amp;language=1&amp;ct=DDFEnglish",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://docsonline.wto.org/imrd/gen_redirectsearchdirect.asp?RN=0&amp;searchtype=browse&amp;query=@meta_Symbol&quot;WT/REG81/1&quot;&amp;language=2&amp;ct=DDFFrench",
                        "name" => "F"
                    ],
                    [
                        "url" => "http://docsonline.wto.org/imrd/gen_redirectsearchdirect.asp?RN=0&amp;searchtype=browse&amp;query=@meta_Symbol&quot;WT/REG81/1&quot;&amp;language=3&amp;ct=DDFSpanish",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Kazakhstan"
                ],
                "countries_second" => [
                    "Kyrgyzstan"
                ]
            ],
            [
                "id" => 732,
                "rta_id" => 561,
                "name" => "Pakistan - Sri Lanka",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "Enabling Clause",
                "signature_date" => "2002-08-01",
                "notification_date" => "2008-06-11",
                "entry_force_date" => "2005-06-12",
                "current_signatories" => [
                    "Pakistan",
                    "Sri Lanka"
                ],
                "original_signatories" => [
                    "Pakistan",
                    "Sri Lanka"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "West Asia"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.commerce.gov.pk/",
                        "name" => "Ministry of Commerce, Government of Pakistan"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.commerce.gov.pk/wp-content/uploads/pdf/PSFTA_Text.pdf",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Sri Lanka"
                ],
                "countries_second" => [
                    "Pakistan"
                ]
            ],
            [
                "id" => 707,
                "rta_id" => 425,
                "name" => "Panama - Chinese Taipei",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2003-08-21",
                "notification_date" => "2009-07-28",
                "entry_force_date" => "2004-01-01",
                "current_signatories" => [
                    "Taiwan",
                    "Panama"
                ],
                "original_signatories" => [
                    "Taiwan",
                    "Panama"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "East Asia",
                    "Central America"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://eweb.trade.gov.tw/mp.asp?mp=2",
                        "name" => "Bureau of Foreign Trade, The Separate Customs Territory of Taiwan, Penghu, Kinmen and Matsu"
                    ],
                    [
                        "url" => "http://www.mici.gob.pa/",
                        "name" => "Ministerio de Comercio e Industrias, Panamá"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "../rtadocs/425/TOA/English/Panama-Chinese%20Taipei%20Agreement.pdf",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://www.mici.gob.pa/detalle.php?cid=15&amp;sid=57&amp;clid=30&amp;id=2466",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Taiwan"
                ],
                "countries_second" => [
                    "Panama"
                ]
            ],
            [
                "id" => 717,
                "rta_id" => 495,
                "name" => "Peru - Singapore",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2008-05-29",
                "notification_date" => "2009-07-30",
                "entry_force_date" => "2009-08-01",
                "current_signatories" => [
                    "Peru",
                    "Singapore"
                ],
                "original_signatories" => [
                    "Peru",
                    "Singapore"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "South America",
                    "East Asia"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.mincetur.gob.pe/newweb/",
                        "name" => "Ministerio de Comercio Exterior de Perú"
                    ],
                    [
                        "url" => "http://www.iesingapore.gov.sg/",
                        "name" => "International Enterprise Singapore"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.iesingapore.gov.sg/~/media/IE%20Singapore/Files/FTA/Non%20Priority%20FTAs/Peru%20Singapore%20FTA/Legal%20text/Peru20PESFTA20Legal20Text.pdf",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://www.acuerdoscomerciales.gob.pe/index.php?option=com_content&amp;view=category&amp;layout=blog&amp;id=65&amp;Itemid=88",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Peru"
                ],
                "countries_second" => [
                    "Singapore"
                ]
            ],
            [
                "id" => 359,
                "rta_id" => 29,
                "name" => "Turkey - Morocco",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "2004-04-07",
                "notification_date" => "2006-02-10",
                "entry_force_date" => "2006-01-01",
                "current_signatories" => [
                    "Morocco",
                    "Turkey"
                ],
                "original_signatories" => [
                    "Morocco",
                    "Turkey"
                ],
                "rta_composition" => [
                    "Bilateral"
                ],
                "region" => [
                    "Africa",
                    "Europe"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.economy.gov.tr/",
                        "name" => "Turkish Government"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://docsonline.wto.org/imrd/gen_redirectsearchdirect.asp?RN=0&amp;searchtype=browse&amp;query=@meta_Symbol&quot;WT/REG209/1&quot;&amp;language=1&amp;ct=DDFEnglish",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://docsonline.wto.org/imrd/gen_redirectsearchdirect.asp?RN=0&amp;searchtype=browse&amp;query=@meta_Symbol&quot;WT/REG209/1&quot;&amp;language=2&amp;ct=DDFFrench",
                        "name" => "F"
                    ],
                    [
                        "url" => "http://docsonline.wto.org/imrd/gen_redirectsearchdirect.asp?RN=0&amp;searchtype=browse&amp;query=@meta_Symbol&quot;WT/REG209/1&quot;&amp;language=3&amp;ct=DDFSpanish",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Morocco"
                ],
                "countries_second" => [
                    "Turkey"
                ]
            ],
            [
                "id" => 795,
                "rta_id" => 713,
                "name" => "The Cross-Straits Economic Cooperation Framework Agreement (ECFA)",
                "coverage" => "Goods & Services, etc.",
                "status" => "Early announcement-Signed",
                "notification_under" => "",
                "signature_date" => "2010-06-29",
                "notification_date" => null,
                "entry_force_date" => "2010-09-12",
                "current_signatories" => [
                    "The Association for Relations Across the Taiwan Straits (China)",
                    "The Straits Exchange Foundation (the Separate Customs Territory of Taiwan, Penghu, Kinmen and Matsu)"
                ],
                "original_signatories" => [
                    "The Association for Relations Across the Taiwan Straits (China)",
                    "The Straits Exchange Foundation (the Separate Customs Territory of Taiwan, Penghu, Kinmen and Matsu)"
                ],
                "rta_composition" => [
                    ""
                ],
                "region" => [
                    "East Asia"
                ],
                "wto_members" => true,
                "type" => "",
                "web_addresses" => [],
                "agreement" => [
                    [
                        "url" => "../rtadocs/713/TOA/English/Combined%20ECFA%20Text.pdf",
                        "name" => "E"
                    ],
                    [
                        "url" => "../rtadocs/713/TOA/English/990921-ECFA(text_and_annexes_combined).pdf",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Taiwan"
                ],
                "countries_second" => [
                    "China"
                ]
            ],
            [
                "id" => 845,
                "rta_id" => 878,
                "name" => "Central American Common Market (CACM) - Accession of Panama",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "2012-06-29",
                "notification_date" => "2017-04-24",
                "entry_force_date" => "2013-05-06",
                "current_signatories" => [
                    "Costa Rica",
                    "El Salvador",
                    "Guatemala",
                    "Honduras",
                    "Nicaragua",
                    "Panama"
                ],
                "original_signatories" => [
                    "Costa Rica",
                    "El Salvador",
                    "Guatemala",
                    "Honduras",
                    "Nicaragua",
                    "Panama"
                ],
                "rta_composition" => [
                    "Plurilateral"
                ],
                "region" => [
                    "Central America"
                ],
                "wto_members" => true,
                "type" => "Customs Union",
                "web_addresses" => [
                    [
                        "url" => "https://www.sieca.int/",
                        "name" => "Secretaría de Integración Económica Centroamericana"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.mici.gob.pa/detalle.php?cid=15&amp;sid=57&amp;clid=193&amp;id=4133",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Panama"
                ],
                "countries_second" => [
                    "Costa Rica",
                    "El Salvador",
                    "Guatemala",
                    "Honduras",
                    "Nicaragua"
                ]
            ],
            [
                "id" => 811,
                "rta_id" => 764,
                "name" => "Dominican Republic - Central America",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "1998-04-16",
                "notification_date" => "2012-01-06",
                "entry_force_date" => "2001-10-04",
                "current_signatories" => [
                    "Costa Rica",
                    "Dominican Republic",
                    "El Salvador",
                    "Guatemala",
                    "Honduras",
                    "Nicaragua"
                ],
                "original_signatories" => [
                    "Costa Rica",
                    "Dominican Republic",
                    "El Salvador",
                    "Guatemala",
                    "Honduras",
                    "Nicaragua"
                ],
                "rta_composition" => [
                    "Plurilateral"
                ],
                "region" => [
                    "Central America",
                    "Caribbean"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "https://mic.gob.do/",
                        "name" => "Secretaría de Estado de Industría y Comercio de República Dominicana"
                    ],
                    [
                        "url" => "http://www.comex.go.cr",
                        "name" => "Ministerio de Comercio Exterior de Costa Rica"
                    ],
                    [
                        "url" => "http://www.minec.gob.sv/",
                        "name" => "Ministerio de Economia de El Salvador"
                    ],
                    [
                        "url" => "http://www.mineco.gob.gt/",
                        "name" => "Ministerio de Economía de Guatemala"
                    ],
                    [
                        "url" => "https://sde.gob.hn/",
                        "name" => "Secretaría de Desarollo Económico, Honduras"
                    ],
                    [
                        "url" => "http://www.mific.gob.ni/",
                        "name" => "Ministerio de Fomento, Industría y Comercio de Nicaragua"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.sice.oas.org/Trade/camdrep/indice.asp",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Costa Rica"
                ],
                "countries_second" => [
                    "Dominican Republic",
                    "El Salvador",
                    "Guatemala",
                    "Honduras",
                    "Nicaragua"
                ]
            ],
            [
                "id" => 476,
                "rta_id" => 149,
                "name" => "EFTA - Accession of Iceland",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "1969-12-04",
                "notification_date" => "1970-01-30",
                "entry_force_date" => "1970-03-01",
                "current_signatories" => [
                    "Iceland",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland"
                ],
                "original_signatories" => [
                    "Liechtenstein",
                    "Norway",
                    "Switzerland"
                ],
                "rta_composition" => [
                    "Plurilateral"
                ],
                "region" => [
                    "Europe"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.efta.int",
                        "name" => "European Free Trade Association"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.efta.int/legal-texts/efta-convention",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Iceland"
                ],
                "countries_second" => [
                    "Liechtenstein",
                    "Norway",
                    "Switzerland"
                ]
            ],
            [
                "id" => 394,
                "rta_id" => 65,
                "name" => "EFTA - Singapore",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2002-06-26",
                "notification_date" => "2003-01-14",
                "entry_force_date" => "2003-01-01",
                "current_signatories" => [
                    "Iceland",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland",
                    "Singapore"
                ],
                "original_signatories" => [
                    "Iceland",
                    "Liechtenstein",
                    "Norway",
                    "Singapore",
                    "Switzerland"
                ],
                "rta_composition" => [
                    "Bilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "Europe",
                    "East Asia"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.efta.int",
                        "name" => "European Free Trade Association"
                    ],
                    [
                        "url" => "http://www.iesingapore.gov.sg/",
                        "name" => "International Enterprise Singapore"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.efta.int/~/media/Documents/legal-texts/free-trade-relations/singapore/EFTA-Singapore%20Free%20Trade%20Agreement.pdf",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Singapore"
                ],
                "countries_second" => [
                    "Iceland",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland"
                ]
            ],
            [
                "id" => 754,
                "rta_id" => 604,
                "name" => "EFTA - Ukraine",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2010-06-24",
                "notification_date" => "2012-06-18",
                "entry_force_date" => "2012-06-01",
                "current_signatories" => [
                    "Iceland",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland",
                    "Ukraine"
                ],
                "original_signatories" => [
                    "Iceland",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland",
                    "Ukraine"
                ],
                "rta_composition" => [
                    "Bilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "Europe",
                    "Commonwealth of Independent States (CIS), including associate and former member States"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.efta.int/",
                        "name" => "European Free Trade Association"
                    ],
                    [
                        "url" => "http://www.me.gov.ua/?lang=en-GB",
                        "name" => "The Ministry of Economy of Ukraine"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://efta.int/~/media/Documents/legal-texts/free-trade-relations/ukraine/EFTA-Ukraine%20Free%20Trade%20Agreement.pdf",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Ukraine"
                ],
                "countries_second" => [
                    "Iceland",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland"
                ]
            ],
            [
                "id" => 343,
                "rta_id" => 12,
                "name" => "EU - Albania",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2006-06-12",
                "notification_date" => "2007-03-07",
                "entry_force_date" => "2006-12-01",
                "current_signatories" => [
                    "Albania",
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom"
                ],
                "original_signatories" => [
                    "Albania"
                ],
                "rta_composition" => [
                    "Bilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "Europe"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://ec.europa.eu/",
                        "name" => "European Commission"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://ec.europa.eu/world/agreements/downloadFile.do?fullText=yes&amp;treatyTransId=13127",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Albania"
                ],
                "countries_second" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom"
                ]
            ],
            [
                "id" => 437,
                "rta_id" => 109,
                "name" => "EU - Andorra",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "1991-06-28",
                "notification_date" => "1998-02-23",
                "entry_force_date" => "1991-07-01",
                "current_signatories" => [
                    "Andorra",
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom"
                ],
                "original_signatories" => [
                    "Andorra"
                ],
                "rta_composition" => [
                    "Bilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "Europe"
                ],
                "wto_members" => false,
                "type" => "Customs Union",
                "web_addresses" => [
                    [
                        "url" => "http://ec.europa.eu/",
                        "name" => "European Commission"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1398350679054&amp;uri=CELEX:21990A1231(02)",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1398350679054&amp;uri=CELEX:21990A1231(02)",
                        "name" => "F"
                    ],
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1398350679054&amp;uri=CELEX:21990A1231(02)",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Andorra"
                ],
                "countries_second" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom"
                ]
            ],
            [
                "id" => 757,
                "rta_id" => 619,
                "name" => "EU - Canada",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2016-10-30",
                "notification_date" => "2017-09-19",
                "entry_force_date" => "2017-09-21",
                "current_signatories" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom",
                    "Canada"
                ],
                "original_signatories" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom",
                    "Canada"
                ],
                "rta_composition" => [
                    "Bilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "Europe",
                    "North America"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.international.gc.ca/",
                        "name" => "Global Affairs Canada"
                    ],
                    [
                        "url" => "http://ec.europa.eu/",
                        "name" => "European Commission"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/HTML/?uri=OJ:L:2017:011:FULL&amp;from=EN",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://international.gc.ca/trade-commerce/trade-agreements-accords-commerciaux/agr-acc/ceta-aecg/text-texte/toc-tdm.aspx?lang=fra",
                        "name" => "F"
                    ]
                ],
                "countries_first" => [
                    "Canada"
                ],
                "countries_second" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom"
                ]
            ],
            [
                "id" => 831,
                "rta_id" => 848,
                "name" => "EU - Georgia",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2014-06-27",
                "notification_date" => "2014-07-02",
                "entry_force_date" => "2014-09-01",
                "current_signatories" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom",
                    "Georgia"
                ],
                "original_signatories" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom",
                    "Georgia"
                ],
                "rta_composition" => [
                    "Bilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "Europe",
                    "Commonwealth of Independent States (CIS), including associate and former member States"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://ec.europa.eu/",
                        "name" => "European Commission"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/PDF/?uri=OJ:L:2014:261:FULL&amp;from=EN",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Georgia"
                ],
                "countries_second" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom"
                ]
            ],
            [
                "id" => 421,
                "rta_id" => 93,
                "name" => "EU - Israel",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "1995-11-20",
                "notification_date" => "2000-09-20",
                "entry_force_date" => "2000-06-01",
                "current_signatories" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom",
                    "Israel"
                ],
                "original_signatories" => [
                    "Israel"
                ],
                "rta_composition" => [
                    "Bilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "Europe",
                    "Middle East"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://ec.europa.eu/",
                        "name" => "European Commission"
                    ],
                    [
                        "url" => "http://economy.gov.il/English/Pages/default.aspx",
                        "name" => "Ministry of Industry, Trade & Labor, Israel"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://docsonline.wto.org/imrd/gen_redirectsearchdirect.asp?RN=0&amp;searchtype=browse&amp;query=@meta_Symbol&quot;WT/REG110/1&quot;&amp;language=1&amp;ct=DDFEnglish",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://docsonline.wto.org/imrd/gen_redirectsearchdirect.asp?RN=0&amp;searchtype=browse&amp;query=@meta_Symbol&quot;WT/REG110/1&quot;&amp;language=2&amp;ct=DDFFrench",
                        "name" => "F"
                    ],
                    [
                        "url" => "http://docsonline.wto.org/imrd/gen_redirectsearchdirect.asp?RN=0&amp;searchtype=browse&amp;query=@meta_Symbol&quot;WT/REG110/1&quot;&amp;language=3&amp;ct=DDFSpanish",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Israel"
                ],
                "countries_second" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom"
                ]
            ],
            [
                "id" => 397,
                "rta_id" => 68,
                "name" => "EU - Jordan",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "1997-11-24",
                "notification_date" => "2002-12-17",
                "entry_force_date" => "2002-05-01",
                "current_signatories" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom",
                    "Jordan"
                ],
                "original_signatories" => [
                    "Jordan"
                ],
                "rta_composition" => [
                    "Bilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "Europe",
                    "Middle East"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://ec.europa.eu/",
                        "name" => "European Commission"
                    ],
                    [
                        "url" => "http://www.mit.gov.jo",
                        "name" => "Jordan's Foreign Trade Policy Department, Ministry of Industry and Trade"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?uri=OJ:L:2002:129:TOC",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Jordan"
                ],
                "countries_second" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom"
                ]
            ],
            [
                "id" => 481,
                "rta_id" => 154,
                "name" => "EU - Montenegro",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2007-10-15",
                "notification_date" => "2008-01-16",
                "entry_force_date" => "2008-01-01",
                "current_signatories" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom",
                    "Montenegro"
                ],
                "original_signatories" => [
                    "Montenegro"
                ],
                "rta_composition" => [
                    "Bilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "Europe"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://ec.europa.eu/",
                        "name" => "European Commission"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1474016437229&amp;uri=CELEX:02010A0429(01)-20150201",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1474016437229&amp;uri=CELEX:02010A0429(01)-20150201",
                        "name" => "F"
                    ],
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1474016437229&amp;uri=CELEX:02010A0429(01)-20150201",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Montenegro"
                ],
                "countries_second" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom"
                ]
            ],
            [
                "id" => 699,
                "rta_id" => 386,
                "name" => "EU - San Marino",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "1991-12-16",
                "notification_date" => "2010-02-24",
                "entry_force_date" => "2002-04-01",
                "current_signatories" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom",
                    "San Marino"
                ],
                "original_signatories" => [
                    "San Marino"
                ],
                "rta_composition" => [
                    "Bilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "Europe"
                ],
                "wto_members" => false,
                "type" => "Customs Union",
                "web_addresses" => [
                    [
                        "url" => "http://ec.europa.eu/",
                        "name" => "European Commission"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1399392072653&amp;uri=CELEX:22002A0328(01)",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1399392072653&amp;uri=CELEX:22002A0328(01)",
                        "name" => "F"
                    ],
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1399392072653&amp;uri=CELEX:22002A0328(01)",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "San Marino"
                ],
                "countries_second" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom"
                ]
            ],
            [
                "id" => 465,
                "rta_id" => 138,
                "name" => "EU - Syria",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "1977-01-18",
                "notification_date" => "1977-07-15",
                "entry_force_date" => "1977-07-01",
                "current_signatories" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom",
                    "Syria"
                ],
                "original_signatories" => [
                    "Syria"
                ],
                "rta_composition" => [
                    "Bilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "Europe",
                    "Middle East"
                ],
                "wto_members" => false,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://ec.europa.eu/",
                        "name" => "European Commission"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1399543172698&amp;uri=CELEX:21977A0118(05)",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1399543172698&amp;uri=CELEX:21977A0118(05)",
                        "name" => "F"
                    ],
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1399543172698&amp;uri=CELEX:21977A0118(05)",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Syria"
                ],
                "countries_second" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom"
                ]
            ],
            [
                "id" => 783,
                "rta_id" => 681,
                "name" => "EU - Ukraine",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2014-06-27",
                "notification_date" => "2014-07-01",
                "entry_force_date" => "2014-04-23",
                "current_signatories" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom",
                    "Ukraine"
                ],
                "original_signatories" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom",
                    "Ukraine"
                ],
                "rta_composition" => [
                    "Bilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "Europe",
                    "Commonwealth of Independent States (CIS), including associate and former member States"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://ec.europa.eu/",
                        "name" => "European Commission"
                    ],
                    [
                        "url" => "http://www.kmu.gov.ua/",
                        "name" => "The Ministry of Economy of Ukraine"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/PDF/?uri=OJ:L:2014:161:FULL&amp;from=EN",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Ukraine"
                ],
                "countries_second" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom"
                ]
            ],
            [
                "id" => 858,
                "rta_id" => 921,
                "name" => "Gulf Cooperation Council (GCC) - Singapore",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "Enabling Clause & GATS Art. V",
                "signature_date" => "2008-12-15",
                "notification_date" => "2015-06-30",
                "entry_force_date" => "2013-09-01",
                "current_signatories" => [
                    "Bahrain",
                    "Kuwait",
                    "Oman",
                    "Qatar",
                    "Saudi Arabia",
                    "United Arab Emirates",
                    "Singapore"
                ],
                "original_signatories" => [
                    "Bahrain",
                    "Kuwait",
                    "Oman",
                    "Qatar",
                    "Saudi Arabia",
                    "United Arab Emirates",
                    "Singapore"
                ],
                "rta_composition" => [
                    "Bilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "Middle East",
                    "East Asia"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.gcc-sg.org/en-us/Pages/default.aspx",
                        "name" => "Gulf Cooperation Council"
                    ],
                    [
                        "url" => "http://www.iesingapore.gov.sg/",
                        "name" => "International Enterprise Singapore"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.iesingapore.gov.sg/~/media/IE%20Singapore/Files/FTA/Non%20Priority%20FTAs/The%20Gulf%20Cooperation%20Council/Legal%20Text/The20Gulf20Cooperation20Council2020GSFTA20Legal20Text.pdf",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Singapore"
                ],
                "countries_second" => [
                    "Bahrain",
                    "Kuwait",
                    "Oman",
                    "Qatar",
                    "Saudi Arabia",
                    "United Arab Emirates"
                ]
            ],
            [
                "id" => 876,
                "rta_id" => 1026,
                "name" => "Latin American Integration Association (LAIA) - Accession of Cuba",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "Enabling Clause",
                "signature_date" => "1999-08-26",
                "notification_date" => "1999-09-23",
                "entry_force_date" => "1999-08-26",
                "current_signatories" => [
                    "Argentina",
                    "Bolivia",
                    "Brazil",
                    "Chile",
                    "Colombia",
                    "Cuba",
                    "Ecuador",
                    "Mexico",
                    "Paraguay",
                    "Peru",
                    "Uruguay",
                    "Venezuela"
                ],
                "original_signatories" => [
                    "Argentina",
                    "Bolivia",
                    "Brazil",
                    "Chile",
                    "Colombia",
                    "Cuba",
                    "Ecuador",
                    "Mexico",
                    "Paraguay",
                    "Peru",
                    "Uruguay",
                    "Venezuela"
                ],
                "rta_composition" => [
                    "Plurilateral"
                ],
                "region" => [
                    "South America",
                    "Caribbean",
                    "North America"
                ],
                "wto_members" => true,
                "type" => "Partial Scope Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.aladi.org/sitioAladi/index.html",
                        "name" => "Asociación Latino Americana de Integración"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.aladi.org/nsfaladi/histtextacdos.nsf/ad93db5f5d5d2a6f83257e29004efdf3/7ae89e361db13f2e03257a910050584a?OpenDocument",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Argentina"
                ],
                "countries_second" => [
                    "Bolivia",
                    "Brazil",
                    "Chile",
                    "Colombia",
                    "Cuba",
                    "Ecuador",
                    "Mexico",
                    "Paraguay",
                    "Peru",
                    "Uruguay",
                    "Venezuela"
                ]
            ],
            [
                "id" => 837,
                "rta_id" => 863,
                "name" => "Mexico - Central America",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2011-11-22",
                "notification_date" => "2014-01-20",
                "entry_force_date" => "2012-09-01",
                "current_signatories" => [
                    "Costa Rica",
                    "El Salvador",
                    "Guatemala",
                    "Honduras",
                    "Mexico",
                    "Nicaragua"
                ],
                "original_signatories" => [
                    "Costa Rica",
                    "El Salvador",
                    "Guatemala",
                    "Honduras",
                    "Mexico",
                    "Nicaragua"
                ],
                "rta_composition" => [
                    "Plurilateral"
                ],
                "region" => [
                    "Central America",
                    "North America"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.economia.gob.mx/",
                        "name" => "Secretaría de Economía de México"
                    ],
                    [
                        "url" => "http://www.comex.go.cr/",
                        "name" => "Ministerio de Comercio Exterior de Costa Rica"
                    ],
                    [
                        "url" => "http://www.minec.gob.sv/",
                        "name" => "Ministerio de Economia, República de El Salvador"
                    ],
                    [
                        "url" => "http://www.mineco.gob.gt/",
                        "name" => "Ministerio de Economia de Guatemala"
                    ],
                    [
                        "url" => "https://sde.gob.hn/",
                        "name" => "Secretaría de Desarollo Económico, Honduras"
                    ],
                    [
                        "url" => "http://www.mific.gob.ni/",
                        "name" => "Ministerio de Fomento, Industria y Comercio, Nicaragua"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.sice.oas.org/Trade/CACM_MEX_FTA/Index_s.asp",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Costa Rica"
                ],
                "countries_second" => [
                    "El Salvador",
                    "Guatemala",
                    "Honduras",
                    "Mexico",
                    "Nicaragua"
                ]
            ],
            [
                "id" => 697,
                "rta_id" => 380,
                "name" => "Southern Common Market (MERCOSUR) - Chile",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "Enabling Clause",
                "signature_date" => "2016-11-07",
                "notification_date" => "2017-06-15",
                "entry_force_date" => "2017-03-10",
                "current_signatories" => [
                    "Argentina",
                    "Brazil",
                    "Paraguay",
                    "Uruguay",
                    "Chile"
                ],
                "original_signatories" => [
                    "Argentina",
                    "Brazil",
                    "Paraguay",
                    "Uruguay",
                    "Chile"
                ],
                "rta_composition" => [
                    "Bilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "South America"
                ],
                "wto_members" => true,
                "type" => "Partial Scope Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.mercosur.int/",
                        "name" => "Southern Common Market"
                    ],
                    [
                        "url" => "https://www.direcon.gob.cl/",
                        "name" => "Dirección General de Relaciones Económicas Internacionales, Chile"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.aladi.org/biblioteca/publicaciones/aladi/acuerdos/ace/es/ace35/ACE_035_059.pdf",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Chile"
                ],
                "countries_second" => [
                    "Argentina",
                    "Brazil",
                    "Paraguay",
                    "Uruguay"
                ]
            ],
            [
                "id" => 847,
                "rta_id" => 885,
                "name" => "EU - Thailand",
                "coverage" => "",
                "status" => "Early announcement-Under negotiation",
                "notification_under" => "",
                "signature_date" => null,
                "notification_date" => null,
                "entry_force_date" => null,
                "current_signatories" => [
                    ""
                ],
                "original_signatories" => [
                    ""
                ],
                "rta_composition" => [
                    "Bilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "Europe",
                    "East Asia"
                ],
                "wto_members" => true,
                "type" => "",
                "web_addresses" => [
                    [
                        "url" => "http://ec.europa.eu/",
                        "name" => "European Commission"
                    ],
                    [
                        "url" => "http://www.mfa.go.th/main/",
                        "name" => "Ministry of Foreign Affairs, Kingdom of Thailand"
                    ]
                ],
                "agreement" => [],
                "countries_first" => [
                    "Thailand"
                ],
                "countries_second" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom"
                ]
            ],
            [
                "id" => 435,
                "rta_id" => 107,
                "name" => "EU - Tunisia",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "1995-07-07",
                "notification_date" => "1999-01-15",
                "entry_force_date" => "1998-03-01",
                "current_signatories" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom",
                    "Tunisia"
                ],
                "original_signatories" => [
                    "Tunisia"
                ],
                "rta_composition" => [
                    "Bilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "Europe",
                    "Africa"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://ec.europa.eu/",
                        "name" => "European Commission"
                    ],
                    [
                        "url" => "http://www.cepex.nat.tn/",
                        "name" => "Ministère du Commerce et de l'Artisanat, République Tunisienne"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1399544134573&amp;uri=CELEX:21998A0330(01)",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1399544134573&amp;uri=CELEX:21998A0330(01)",
                        "name" => "F"
                    ],
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1399544134573&amp;uri=CELEX:21998A0330(01)",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Tunisia"
                ],
                "countries_second" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom"
                ]
            ],
            [
                "id" => 854,
                "rta_id" => 907,
                "name" => "EU - Colombia and Peru - Accession of Ecuador",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2016-11-11",
                "notification_date" => "2017-03-02",
                "entry_force_date" => "2017-01-01",
                "current_signatories" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom",
                    "Colombia",
                    "Ecuador",
                    "Peru"
                ],
                "original_signatories" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom",
                    "Colombia",
                    "Ecuador",
                    "Peru"
                ],
                "rta_composition" => [
                    "Plurilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "Europe",
                    "South America"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://ec.europa.eu/",
                        "name" => "European Commission"
                    ],
                    [
                        "url" => "http://www.tlc.gov.co/index.php",
                        "name" => "Ministerio de Comercio, Industria y Turismo, Colombia"
                    ],
                    [
                        "url" => "http://www.mincetur.gob.pe/",
                        "name" => "Ministerio de Comercio Exterior de Perú"
                    ],
                    [
                        "url" => "http://www.comercioexterior.gob.ec/",
                        "name" => "Ministerio de Comercio Exterior, Ecuador"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?uri=OJ:L:2016:356:TOC",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/FR/TXT/?uri=OJ%3AL%3A2016%3A356%3ATOC",
                        "name" => "F"
                    ],
                    [
                        "url" => "http://www.comercioexterior.gob.ec/wp-content/uploads/downloads/2016/11/Protocolo-suscrito-11-11-2016-EU-EC-Prot-to-CO-PE.pdf",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Peru",
                    "Colombia"
                ],
                "countries_second" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom",
                    "Ecuador"
                ]
            ],
            [
                "id" => 475,
                "rta_id" => 148,
                "name" => "EU – Overseas Countries and Territories (OCT)",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "1970-09-29",
                "notification_date" => "1970-12-14",
                "entry_force_date" => "1971-01-01",
                "current_signatories" => [
                    "Anguilla",
                    "Aruba, the Netherlands with respect to",
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom",
                    "Bermuda",
                    "British Indian Ocean Territory",
                    "Cayman Islands",
                    "Falkland Islands (Islas Malvinas)",
                    "French Polynesia",
                    "French Southern Territories",
                    "Greenland",
                    "Montserrat",
                    "Netherlands Antilles",
                    "New Caledonia",
                    "Pitcairn",
                    "Saint Helena",
                    "Saint Pierre and Miquelon",
                    "South Georgia and the South Sandwich Islands",
                    "Turks and Caicos Islands",
                    "Virgin Islands, British",
                    "Wallis and Futuna Islands"
                ],
                "original_signatories" => [
                    "French Polynesia",
                    "French Southern Territories",
                    "Netherlands Antilles",
                    "New Caledonia",
                    "Saint Pierre and Miquelon",
                    "Wallis and Futuna Islands"
                ],
                "rta_composition" => [
                    "Plurilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "Caribbean",
                    "Europe",
                    "North America",
                    "Africa",
                    "South America",
                    "Oceania"
                ],
                "wto_members" => false,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://ec.europa.eu/",
                        "name" => "European Commission"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/ES/TXT/?qid=1504794329050&amp;uri=CELEX%3A32013D0755",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/ES/TXT/?qid=1504794329050&amp;uri=CELEX%3A32013D0755",
                        "name" => "F"
                    ],
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/ES/TXT/?qid=1504794329050&amp;uri=CELEX%3A32013D0755",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Virgin Islands, British",
                    "Cayman Islands",
                    "Falkland Islands (Islas Malvinas)",
                    "South Georgia and the South Sandwich Islands",
                    "French Polynesia",
                    "French Southern Territories",
                    "Greenland",
                    "Montserrat",
                    "Netherlands Antilles",
                    "Aruba, the Netherlands with respect to",
                    "New Caledonia",
                    "Pitcairn",
                    "Saint Helena",
                    "Anguilla",
                    "Saint Pierre and Miquelon",
                    "Turks and Caicos Islands",
                    "Wallis and Futuna Islands",
                    "Bermuda"
                ],
                "countries_second" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom",
                    "British Indian Ocean Territory"
                ]
            ],
            [
                "id" => 806,
                "rta_id" => 752,
                "name" => "Southern Common Market (MERCOSUR) - Southern African Customs Union (SACU)",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "Enabling Clause",
                "signature_date" => "2008-12-15",
                "notification_date" => "2017-07-19",
                "entry_force_date" => "2016-04-01",
                "current_signatories" => [
                    "Argentina",
                    "Brazil",
                    "Paraguay",
                    "Uruguay",
                    "Botswana",
                    "Lesotho",
                    "Namibia",
                    "South Africa",
                    "Swaziland"
                ],
                "original_signatories" => [
                    "Argentina",
                    "Brazil",
                    "Paraguay",
                    "Uruguay",
                    "Botswana",
                    "Lesotho",
                    "Namibia",
                    "South Africa",
                    "Swaziland"
                ],
                "rta_composition" => [
                    "Bilateral",
                    "All Parties are RTAs"
                ],
                "region" => [
                    "South America",
                    "Africa"
                ],
                "wto_members" => true,
                "type" => "Partial Scope Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.sacu.int/",
                        "name" => "Southern African Customs Union"
                    ],
                    [
                        "url" => "http://www.mercosur.int/",
                        "name" => "Southern Common Market"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.sacu.int/docs/agreements/2016/mercosur-and-sacu-trade-agreement.pdf",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://www.sice.oas.org/TPD/MER_SACU/MER_SACU_s.ASP#Texts",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Paraguay",
                    "Uruguay",
                    "",
                    "Argentina"
                ],
                "countries_second" => [
                    "Botswana",
                    "Lesotho",
                    "Namibia",
                    "South Africa",
                    "Swaziland",
                    "Brazil"
                ]
            ],
            [
                "id" => 454,
                "rta_id" => 127,
                "name" => "Andean Community (CAN)",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "Enabling Clause",
                "signature_date" => "1987-05-12",
                "notification_date" => "1990-10-01",
                "entry_force_date" => "1988-05-25",
                "current_signatories" => [
                    "Bolivia",
                    "Colombia",
                    "Ecuador",
                    "Peru",
                    "Venezuela"
                ],
                "original_signatories" => [
                    "Bolivia",
                    "Colombia",
                    "Ecuador",
                    "Peru",
                    "Venezuela"
                ],
                "rta_composition" => [
                    "Plurilateral"
                ],
                "region" => [
                    "South America"
                ],
                "wto_members" => true,
                "type" => "Customs Union",
                "web_addresses" => [
                    [
                        "url" => "http://www.comunidadandina.org/",
                        "name" => "Secretaria General de la Comunidad Andina"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.sice.oas.org/Trade/Junac/Carta_Ag/index.asp",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://intranet.comunidadandina.org/IDocumentos/c_Newdocs.asp?GruDoc=14",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Bolivia"
                ],
                "countries_second" => [
                    "Colombia",
                    "Ecuador",
                    "Peru",
                    "Venezuela"
                ]
            ],
            [
                "id" => 427,
                "rta_id" => 99,
                "name" => "Commonwealth of Independent States (CIS)",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "1994-04-15",
                "notification_date" => "1999-06-29",
                "entry_force_date" => "1994-12-30",
                "current_signatories" => [
                    "Azerbaijan",
                    "Georgia",
                    "Turkmenistan",
                    "Uzbekistan"
                ],
                "original_signatories" => [
                    "Armenia",
                    "Azerbaijan",
                    "Belarus",
                    "Georgia",
                    "Kazakhstan",
                    "Kyrgyzstan",
                    "Moldova",
                    "Russia",
                    "Tajikistan",
                    "Ukraine",
                    "Uzbekistan"
                ],
                "rta_composition" => [
                    "Plurilateral"
                ],
                "region" => [
                    "Commonwealth of Independent States (CIS), including associate and former member States"
                ],
                "wto_members" => false,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.cis.minsk.by",
                        "name" => "Commonwealth of Independent States"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://docsonline.wto.org/imrd/gen_redirectsearchdirect.asp?RN=0&amp;searchtype=browse&amp;query=@meta_Symbol&quot;WT/REG82/1&quot;&amp;language=1&amp;ct=DDFEnglish",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://docsonline.wto.org/imrd/gen_redirectsearchdirect.asp?RN=0&amp;searchtype=browse&amp;query=@meta_Symbol&quot;WT/REG82/1&quot;&amp;language=2&amp;ct=DDFFrench",
                        "name" => "F"
                    ],
                    [
                        "url" => "http://docsonline.wto.org/imrd/gen_redirectsearchdirect.asp?RN=0&amp;searchtype=browse&amp;query=@meta_Symbol&quot;WT/REG82/1&quot;&amp;language=3&amp;ct=DDFSpanish",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Azerbaijan"
                ],
                "countries_second" => [
                    "Georgia",
                    "Turkmenistan",
                    "Uzbekistan"
                ]
            ],
            [
                "id" => 450,
                "rta_id" => 123,
                "name" => "EC (15) Enlargement",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "1994-06-24",
                "notification_date" => "1994-12-15",
                "entry_force_date" => "1995-01-01",
                "current_signatories" => [
                    "Austria",
                    "Belgium",
                    "Denmark",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Ireland",
                    "Italy",
                    "Luxembourg",
                    "Netherlands",
                    "Portugal",
                    "Spain",
                    "Sweden",
                    "United Kingdom"
                ],
                "original_signatories" => [
                    "Austria",
                    "Belgium",
                    "Denmark",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Ireland",
                    "Italy",
                    "Luxembourg",
                    "Netherlands",
                    "Portugal",
                    "Spain",
                    "Sweden",
                    "United Kingdom"
                ],
                "rta_composition" => [
                    "Plurilateral"
                ],
                "region" => [
                    "Europe"
                ],
                "wto_members" => true,
                "type" => "Customs Union & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://ec.europa.eu/",
                        "name" => "European Commission"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?uri=OJ:C:1994:241:TOC",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Austria"
                ],
                "countries_second" => [
                    "Belgium",
                    "Denmark",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Ireland",
                    "Italy",
                    "Luxembourg",
                    "Netherlands",
                    "Portugal",
                    "Spain",
                    "Sweden",
                    "United Kingdom"
                ]
            ],
            [
                "id" => 338,
                "rta_id" => 6,
                "name" => "EC (27) Enlargement",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2005-04-25",
                "notification_date" => "2006-09-27",
                "entry_force_date" => "2007-01-01",
                "current_signatories" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom"
                ],
                "original_signatories" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom"
                ],
                "rta_composition" => [
                    "Plurilateral"
                ],
                "region" => [
                    "Europe"
                ],
                "wto_members" => true,
                "type" => "Customs Union & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://ec.europa.eu/",
                        "name" => "European Commission"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1398341783245&amp;uri=CELEX:12005S/TTE",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1398341783245&amp;uri=CELEX:12005S/TTE",
                        "name" => "F"
                    ],
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1398341783245&amp;uri=CELEX:12005S/TTE",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Austria"
                ],
                "countries_second" => [
                    "Belgium",
                    "Bulgaria",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom"
                ]
            ],
            [
                "id" => 447,
                "rta_id" => 120,
                "name" => "EC Treaty",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "1957-03-25",
                "notification_date" => "1957-04-24",
                "entry_force_date" => "1958-01-01",
                "current_signatories" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom"
                ],
                "original_signatories" => [
                    "Belgium",
                    "France",
                    "Germany",
                    "Italy",
                    "Luxembourg",
                    "Netherlands"
                ],
                "rta_composition" => [
                    "Plurilateral"
                ],
                "region" => [
                    "Europe"
                ],
                "wto_members" => true,
                "type" => "Customs Union & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://ec.europa.eu/",
                        "name" => "European Commission"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:12012M/TXT",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:12012M/TXT",
                        "name" => "F"
                    ],
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:12012M/TXT",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Austria"
                ],
                "countries_second" => [
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom"
                ]
            ],
            [
                "id" => 423,
                "rta_id" => 95,
                "name" => "Economic and Monetary Community of Central Africa (CEMAC)",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "Enabling Clause",
                "signature_date" => "1994-03-16",
                "notification_date" => "1999-07-21",
                "entry_force_date" => "1999-06-24",
                "current_signatories" => [
                    "Cameroon",
                    "Central African Republic",
                    "Chad",
                    "Congo",
                    "Equatorial Guinea",
                    "Gabon"
                ],
                "original_signatories" => [
                    "Cameroon",
                    "Central African Republic",
                    "Chad",
                    "Congo",
                    "Equatorial Guinea",
                    "Gabon"
                ],
                "rta_composition" => [
                    "Plurilateral"
                ],
                "region" => [
                    "Africa"
                ],
                "wto_members" => false,
                "type" => "Customs Union",
                "web_addresses" => [
                    [
                        "url" => "http://www.cemac.int",
                        "name" => "Commission de la Communauté Economique"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://docsonline.wto.org/imrd/gen_redirectsearchdirect.asp?RN=0&amp;searchtype=browse&amp;query=@meta_Symbol&quot;WT/COMTD/24&quot;&amp;language=1&amp;ct=DDFEnglish",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://docsonline.wto.org/imrd/gen_redirectsearchdirect.asp?RN=0&amp;searchtype=browse&amp;query=@meta_Symbol&quot;WT/COMTD/24&quot;&amp;language=2&amp;ct=DDFFrench",
                        "name" => "F"
                    ],
                    [
                        "url" => "http://docsonline.wto.org/imrd/gen_redirectsearchdirect.asp?RN=0&amp;searchtype=browse&amp;query=@meta_Symbol&quot;WT/COMTD/24&quot;&amp;language=3&amp;ct=DDFSpanish",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Cameroon"
                ],
                "countries_second" => [
                    "Central African Republic",
                    "Chad",
                    "Congo",
                    "Equatorial Guinea",
                    "Gabon"
                ]
            ],
            [
                "id" => 726,
                "rta_id" => 527,
                "name" => "El Salvador- Honduras - Chinese Taipei",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2007-05-07",
                "notification_date" => "2010-04-06",
                "entry_force_date" => "2008-03-01",
                "current_signatories" => [
                    "Taiwan",
                    "El Salvador",
                    "Honduras"
                ],
                "original_signatories" => [
                    "Taiwan",
                    "El Salvador",
                    "Honduras"
                ],
                "rta_composition" => [
                    "Plurilateral"
                ],
                "region" => [
                    "East Asia",
                    "Central America"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "https://www.mofa.gov.tw/en/default.html",
                        "name" => "Bureau of Foreign Trade, The Separate Customs Territory of Taiwan, Penghu, Kinmen and Matsu"
                    ],
                    [
                        "url" => "https://sde.gob.hn/",
                        "name" => "Secretaría de Desarollo Económico, Honduras"
                    ],
                    [
                        "url" => "http://www.minec.gob.sv/",
                        "name" => "Ministerio de Economía - Gobierno de El Salvador"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.trade.gov.tw/english/Pages/List.aspx?nodeID=678",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://www.minec.gob.sv/index.php?option=com_phocadownload&amp;view=category&amp;id=19:tlc-taiwan-honduras-y-el-salvador.&amp;Itemid=142",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Taiwan"
                ],
                "countries_second" => [
                    "El Salvador",
                    "Honduras"
                ]
            ],
            [
                "id" => 755,
                "rta_id" => 605,
                "name" => "EU - CARIFORUM States EPA",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2008-10-15",
                "notification_date" => "2008-10-16",
                "entry_force_date" => "2008-12-29",
                "current_signatories" => [
                    "Antigua and Barbuda",
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom",
                    "Bahamas",
                    "Barbados",
                    "Belize",
                    "Dominica",
                    "Dominican Republic",
                    "Grenada",
                    "Guyana",
                    "Jamaica",
                    "Saint Kitts and Nevis",
                    "Saint Lucia",
                    "Saint Vincent and the Grenadines",
                    "Suriname",
                    "Trinidad and Tobago"
                ],
                "original_signatories" => [
                    "Antigua and Barbuda",
                    "Bahamas",
                    "Barbados",
                    "Belize",
                    "Dominica",
                    "Dominican Republic",
                    "Grenada",
                    "Guyana",
                    "Jamaica",
                    "Saint Kitts and Nevis",
                    "Saint Lucia",
                    "Saint Vincent and the Grenadines",
                    "Suriname",
                    "Trinidad and Tobago"
                ],
                "rta_composition" => [
                    "Plurilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "Caribbean",
                    "Europe",
                    "Central America",
                    "South America"
                ],
                "wto_members" => false,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://ec.europa.eu/",
                        "name" => "European Commission"
                    ],
                    [
                        "url" => "http://www.crnm.org",
                        "name" => "Caribbean Regional Negotiating Machinery"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1398342443880&amp;uri=CELEX:22008A1030(01)",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1398342443880&amp;uri=CELEX:22008A1030(01)",
                        "name" => "F"
                    ],
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1398342443880&amp;uri=CELEX:22008A1030(01)",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Barbados",
                    "Belize",
                    "Dominica",
                    "Dominican Republic",
                    "Grenada",
                    "Guyana",
                    "Jamaica",
                    "Saint Kitts and Nevis",
                    "Saint Lucia",
                    "Saint Vincent and the Grenadines",
                    "Suriname",
                    "Trinidad and Tobago",
                    "Antigua and Barbuda"
                ],
                "countries_second" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom",
                    "Bahamas"
                ]
            ],
            [
                "id" => 787,
                "rta_id" => 692,
                "name" => "EU - Colombia and Peru",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2012-06-26",
                "notification_date" => "2013-02-26",
                "entry_force_date" => "2013-03-01",
                "current_signatories" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom",
                    "Colombia",
                    "Ecuador",
                    "Peru"
                ],
                "original_signatories" => [
                    "Colombia",
                    "Peru"
                ],
                "rta_composition" => [
                    "Plurilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "Europe",
                    "South America"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://ec.europa.eu/",
                        "name" => "European Commission"
                    ],
                    [
                        "url" => "http://www.tlc.gov.co/index.php",
                        "name" => "Ministerio de Comercio, Industria y Turismo, Colombia"
                    ],
                    [
                        "url" => "http://www.mincetur.gob.pe/newweb/",
                        "name" => "Ministerio de Comercio Exterior de Perú"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1399559825164&amp;uri=CELEX:22012A1221(01)",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1399559825164&amp;uri=CELEX:22012A1221(01)",
                        "name" => "F"
                    ],
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1399559825164&amp;uri=CELEX:22012A1221(01)",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Peru",
                    "Colombia"
                ],
                "countries_second" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom",
                    "Ecuador"
                ]
            ],
            [
                "id" => 473,
                "rta_id" => 146,
                "name" => "Global System of Trade Preferences among Developing Countries (GSTP)",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "Enabling Clause",
                "signature_date" => "1988-04-13",
                "notification_date" => "1989-09-25",
                "entry_force_date" => "1989-04-19",
                "current_signatories" => [
                    "Algeria",
                    "Argentina",
                    "Bangladesh",
                    "Benin",
                    "Bolivia",
                    "Brazil",
                    "Cameroon",
                    "Chile",
                    "Colombia",
                    "Cuba",
                    "Ecuador",
                    "Egypt",
                    "Ghana",
                    "Guinea",
                    "Guyana",
                    "India",
                    "Indonesia",
                    "Iran",
                    "Iraq",
                    "North Korea",
                    "North Korea",
                    "Libya",
                    "Malaysia",
                    "Mexico",
                    "Morocco",
                    "Mozambique",
                    "Myanmar",
                    "Nicaragua",
                    "Nigeria",
                    "Pakistan",
                    "Peru",
                    "Philippines",
                    "Singapore",
                    "Sri Lanka",
                    "Sudan",
                    "Tanzania",
                    "Thailand",
                    "Trinidad and Tobago",
                    "Tunisia",
                    "Venezuela",
                    "Vietnam",
                    "Zimbabwe"
                ],
                "original_signatories" => [
                    "Algeria",
                    "Argentina",
                    "Bangladesh",
                    "Benin",
                    "Bolivia",
                    "Brazil",
                    "Cameroon",
                    "Chile",
                    "Colombia",
                    "Cuba",
                    "Ecuador",
                    "Egypt",
                    "Ghana",
                    "Guinea",
                    "Guyana",
                    "India",
                    "Indonesia",
                    "Iran",
                    "Iraq",
                    "North Korea",
                    "North Korea",
                    "Libya",
                    "Malaysia",
                    "Mexico",
                    "Morocco",
                    "Mozambique",
                    "Myanmar",
                    "Nicaragua",
                    "Nigeria",
                    "Pakistan",
                    "Peru",
                    "Philippines",
                    "Romania",
                    "Singapore",
                    "Sri Lanka",
                    "Sudan",
                    "Tanzania",
                    "Thailand",
                    "Trinidad and Tobago",
                    "Tunisia",
                    "Venezuela",
                    "Vietnam",
                    "Zimbabwe"
                ],
                "rta_composition" => [
                    "Plurilateral"
                ],
                "region" => [
                    "Africa",
                    "South America",
                    "West Asia",
                    "Caribbean",
                    "East Asia",
                    "Middle East",
                    "North America",
                    "Central America"
                ],
                "wto_members" => false,
                "type" => "Partial Scope Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.unctad.org",
                        "name" => "GSTP / SGPC"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "../rtadocs/146/TOA/English/ToA_GSTP.pdf",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Algeria"
                ],
                "countries_second" => [
                    "Argentina",
                    "Bangladesh",
                    "Bolivia",
                    "Brazil",
                    "Myanmar",
                    "Cameroon",
                    "Sri Lanka",
                    "Chile",
                    "Colombia",
                    "Cuba",
                    "Benin",
                    "Ecuador",
                    "Ghana",
                    "Guinea",
                    "Guyana",
                    "India",
                    "Indonesia",
                    "Iran",
                    "Iraq",
                    "North Korea",
                    "North Korea",
                    "Libya",
                    "Malaysia",
                    "Mexico",
                    "Morocco",
                    "Mozambique",
                    "Nicaragua",
                    "Nigeria",
                    "Pakistan",
                    "Peru",
                    "Philippines",
                    "Singapore",
                    "Vietnam",
                    "Zimbabwe",
                    "Sudan",
                    "Thailand",
                    "Trinidad and Tobago",
                    "Tunisia",
                    "Egypt",
                    "Tanzania",
                    "Venezuela"
                ]
            ],
            [
                "id" => 696,
                "rta_id" => 373,
                "name" => "Bay of Bengal Initiative on Multi-Sectoral Technical and Economic Cooperation (BIMSTEC)",
                "coverage" => "Goods",
                "status" => "Early announcement-Under negotiation",
                "notification_under" => "",
                "signature_date" => null,
                "notification_date" => null,
                "entry_force_date" => null,
                "current_signatories" => [
                    ""
                ],
                "original_signatories" => [
                    ""
                ],
                "rta_composition" => [
                    "Plurilateral"
                ],
                "region" => [
                    "West Asia",
                    "East Asia"
                ],
                "wto_members" => false,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.commerce.nic.in/",
                        "name" => "Government of India, Ministry of Commerce & Industry"
                    ]
                ],
                "agreement" => [],
                "countries_first" => [
                    "Bangladesh"
                ],
                "countries_second" => [
                    "Bhutan",
                    "India",
                    "Myanmar",
                    "Nepal",
                    "Sri Lanka",
                    "Thailand"
                ]
            ],
            [
                "id" => 734,
                "rta_id" => 564,
                "name" => "EU - Bosnia and Herzegovina",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2008-06-16",
                "notification_date" => "2008-07-11",
                "entry_force_date" => "2008-07-01",
                "current_signatories" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom",
                    "Bosnia and Herzegovina"
                ],
                "original_signatories" => [
                    "Bosnia and Herzegovina"
                ],
                "rta_composition" => [
                    "Bilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "Europe"
                ],
                "wto_members" => false,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.europa.eu",
                        "name" => "European Commission"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://europa.ba/wp-content/uploads/2015/05/delegacijaEU_2011121405063686eng.pdf",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Bosnia and Herzegovina"
                ],
                "countries_second" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom"
                ]
            ],
            [
                "id" => 363,
                "rta_id" => 33,
                "name" => "EU - Chile",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2002-11-18",
                "notification_date" => "2004-02-03",
                "entry_force_date" => "2003-02-01",
                "current_signatories" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom",
                    "Chile"
                ],
                "original_signatories" => [
                    "Chile"
                ],
                "rta_composition" => [
                    "Bilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "Europe",
                    "South America"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://ec.europa.eu/",
                        "name" => "European Commission"
                    ],
                    [
                        "url" => "http://www.direcon.gob.cl/",
                        "name" => "Dirección General de Relaciones Económicas Internacionales, Chile"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://trade.ec.europa.eu/doclib/docs/2004/november/tradoc_111620.pdf",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://www.direcon.gob.cl/detalle-de-acuerdos/?idacuerdo=6286",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Chile"
                ],
                "countries_second" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom"
                ]
            ],
            [
                "id" => 714,
                "rta_id" => 469,
                "name" => "EU - Eastern and Southern Africa States Interim EPA",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "2009-08-29",
                "notification_date" => "2012-02-09",
                "entry_force_date" => "2012-05-14",
                "current_signatories" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom",
                    "Madagascar",
                    "Mauritius",
                    "Seychelles",
                    "Zimbabwe"
                ],
                "original_signatories" => [
                    "Madagascar",
                    "Mauritius",
                    "Seychelles",
                    "Zimbabwe"
                ],
                "rta_composition" => [
                    "Plurilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "Europe",
                    "Africa"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://ec.europa.eu/",
                        "name" => "European Commission"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1398352746247&amp;uri=CELEX:22012A0424(01)",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/FR/TXT/?uri=uriserv:OJ.L_.2012.111.01.0001.01.FRA",
                        "name" => "F"
                    ],
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1398352746247&amp;uri=CELEX:22012A0424(01)",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Seychelles",
                    "Zimbabwe",
                    "Madagascar"
                ],
                "countries_second" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom",
                    "Mauritius"
                ]
            ],
            [
                "id" => 708,
                "rta_id" => 437,
                "name" => "ASEAN - Australia - New Zealand",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2009-02-27",
                "notification_date" => "2010-04-08",
                "entry_force_date" => "2010-01-01",
                "current_signatories" => [
                    "Australia",
                    "Brunei Darussalam",
                    "Myanmar",
                    "Cambodia",
                    "Indonesia",
                    "Laos",
                    "Malaysia",
                    "Philippines",
                    "Singapore",
                    "Vietnam",
                    "Thailand",
                    "New Zealand"
                ],
                "original_signatories" => [
                    "Australia",
                    "Brunei Darussalam",
                    "Myanmar",
                    "Cambodia",
                    "Indonesia",
                    "Laos",
                    "Malaysia",
                    "Philippines",
                    "Singapore",
                    "Vietnam",
                    "Thailand",
                    "New Zealand"
                ],
                "rta_composition" => [
                    "Plurilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "Oceania",
                    "East Asia"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.asean.org/",
                        "name" => "Association of Southeast Asian Nations"
                    ],
                    [
                        "url" => "http://www.dfat.gov.au/",
                        "name" => "Australian Government, Department of Foreign Affairs & Trade"
                    ],
                    [
                        "url" => "http://www.mfat.govt.nz./",
                        "name" => "New Zealand Ministry of Foreign Affairs and Trade"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.asean.org/wp-content/uploads/images/archive/22260.pdf",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "",
                    "Australia"
                ],
                "countries_second" => [
                    "Brunei Darussalam",
                    "Myanmar",
                    "Cambodia",
                    "Indonesia",
                    "Laos",
                    "Malaysia",
                    "Philippines",
                    "Singapore",
                    "Vietnam",
                    "Thailand",
                    "New Zealand"
                ]
            ],
            [
                "id" => 813,
                "rta_id" => 766,
                "name" => "EFTA - Central America (Costa Rica and Panama)",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2013-06-24",
                "notification_date" => "2014-11-19",
                "entry_force_date" => "2014-08-19",
                "current_signatories" => [
                    "Costa Rica",
                    "Iceland",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland",
                    "Panama"
                ],
                "original_signatories" => [
                    "Costa Rica",
                    "Iceland",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland",
                    "Panama"
                ],
                "rta_composition" => [
                    "Plurilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "Central America",
                    "Europe"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.efta.int/",
                        "name" => "European Free Trade Association"
                    ],
                    [
                        "url" => "http://www.comex.go.cr",
                        "name" => "Ministerio de Comercio Exterior de Costa Rica"
                    ],
                    [
                        "url" => "http://www.mici.gob.pa",
                        "name" => "Ministerio de Comercio e Industrias, Panamá"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.efta.int/free-trade/Free-Trade-Agreements/central-american-states",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://www.comex.go.cr/tratados/aelc/texto-del-tratado-1/",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "",
                    "Costa Rica"
                ],
                "countries_second" => [
                    "Iceland",
                    "Liechtenstein",
                    "Norway",
                    "Switzerland",
                    "Panama"
                ]
            ],
            [
                "id" => 808,
                "rta_id" => 759,
                "name" => "EU - Papua New Guinea / Fiji",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "2009-07-30",
                "notification_date" => "2011-10-18",
                "entry_force_date" => "2009-12-20",
                "current_signatories" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom",
                    "Fiji",
                    "Papua New Guinea"
                ],
                "original_signatories" => [
                    "Fiji",
                    "Papua New Guinea"
                ],
                "rta_composition" => [
                    "Plurilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "Europe",
                    "Oceania"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://ec.europa.eu/",
                        "name" => "European Commission"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1399391908038&amp;uri=CELEX:22009A1016(01)",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1399391908038&amp;uri=CELEX:22009A1016(01)",
                        "name" => "F"
                    ],
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1399391908038&amp;uri=CELEX:22009A1016(01)",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Fiji"
                ],
                "countries_second" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom",
                    "Papua New Guinea"
                ]
            ],
            [
                "id" => 453,
                "rta_id" => 126,
                "name" => "ASEAN Free Trade Area (AFTA)",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "Enabling Clause",
                "signature_date" => "1992-01-28",
                "notification_date" => "1992-10-30",
                "entry_force_date" => "1993-01-01",
                "current_signatories" => [
                    "Brunei Darussalam",
                    "Cambodia",
                    "Indonesia",
                    "Laos",
                    "Malaysia",
                    "Myanmar",
                    "Philippines",
                    "Singapore",
                    "Thailand",
                    "Vietnam"
                ],
                "original_signatories" => [
                    "Brunei Darussalam",
                    "Cambodia",
                    "Indonesia",
                    "Laos",
                    "Malaysia",
                    "Myanmar",
                    "Philippines",
                    "Singapore",
                    "Thailand",
                    "Vietnam"
                ],
                "rta_composition" => [
                    "Plurilateral"
                ],
                "region" => [
                    "East Asia"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.asean.org/",
                        "name" => "Association of Southeast Asian Nations"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.iesingapore.gov.sg/~/media/IE%20Singapore/Files/FTA/Existing%20FTA/ASEAN%20FTA/Legal%20Text/ASEAN20Free20Trade20Area20Legal20Text.pdf",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Brunei Darussalam"
                ],
                "countries_second" => [
                    "Myanmar",
                    "Cambodia",
                    "Indonesia",
                    "Laos",
                    "Malaysia",
                    "Philippines",
                    "Singapore",
                    "Vietnam",
                    "Thailand"
                ]
            ],
            [
                "id" => 448,
                "rta_id" => 121,
                "name" => "Common Market for Eastern and Southern Africa (COMESA)",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "Enabling Clause",
                "signature_date" => "1993-11-05",
                "notification_date" => "1995-05-04",
                "entry_force_date" => "1994-12-08",
                "current_signatories" => [
                    "Angola",
                    "Burundi",
                    "Comoros",
                    "Egypt",
                    "Eritrea",
                    "Swaziland",
                    "Ethiopia",
                    "Kenya",
                    "Lesotho",
                    "Malawi",
                    "Mauritius",
                    "Rwanda",
                    "Sudan",
                    "Tanzania",
                    "Uganda",
                    "Zambia",
                    "Zimbabwe"
                ],
                "original_signatories" => [
                    "Angola",
                    "Burundi",
                    "Comoros",
                    "Eritrea",
                    "Swaziland",
                    "Ethiopia",
                    "Kenya",
                    "Lesotho",
                    "Malawi",
                    "Mauritius",
                    "Rwanda",
                    "Sudan",
                    "Tanzania",
                    "Uganda",
                    "Zambia",
                    "Zimbabwe"
                ],
                "rta_composition" => [
                    "Plurilateral"
                ],
                "region" => [
                    "Africa"
                ],
                "wto_members" => false,
                "type" => "Customs Union",
                "web_addresses" => [
                    [
                        "url" => "http://www.comesa.int/",
                        "name" => "Common Market for Eastern and Southern Africa"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.comesacompetition.org/wp-content/uploads/2016/03/COMESA_Treaty.pdf",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Angola"
                ],
                "countries_second" => [
                    "Burundi",
                    "Comoros",
                    "Ethiopia",
                    "Eritrea",
                    "Kenya",
                    "Lesotho",
                    "Malawi",
                    "Mauritius",
                    "Rwanda",
                    "Zimbabwe",
                    "Sudan",
                    "Swaziland",
                    "Uganda",
                    "Egypt",
                    "Tanzania",
                    "Zambia"
                ]
            ],
            [
                "id" => 382,
                "rta_id" => 52,
                "name" => "EC (25) Enlargement",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2003-04-16",
                "notification_date" => "2004-04-26",
                "entry_force_date" => "2004-05-01",
                "current_signatories" => [
                    "Austria",
                    "Belgium",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom"
                ],
                "original_signatories" => [
                    "Austria",
                    "Belgium",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom"
                ],
                "rta_composition" => [
                    "Plurilateral"
                ],
                "region" => [
                    "Europe"
                ],
                "wto_members" => true,
                "type" => "Customs Union & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://ec.europa.eu/",
                        "name" => "European Commission"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1398333422810&amp;uri=CELEX:12003T/TTE",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1398333422810&amp;uri=CELEX:12003T/TTE",
                        "name" => "F"
                    ],
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1398333422810&amp;uri=CELEX:12003T/TTE",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Austria"
                ],
                "countries_second" => [
                    "Belgium",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom"
                ]
            ],
            [
                "id" => 840,
                "rta_id" => 869,
                "name" => "EU (28) Enlargement",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2011-12-09",
                "notification_date" => "2013-04-25",
                "entry_force_date" => "2013-07-01",
                "current_signatories" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom"
                ],
                "original_signatories" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom"
                ],
                "rta_composition" => [
                    "Plurilateral"
                ],
                "region" => [
                    "Europe"
                ],
                "wto_members" => true,
                "type" => "Customs Union & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://ec.europa.eu/",
                        "name" => "European Commission"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?uri=uriserv:OJ.L_.2012.112.01.0006.01.ENG&amp;toc=OJ:L:2012:112:TOC#L_2012112EN.01001001",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Austria"
                ],
                "countries_second" => [
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom"
                ]
            ],
            [
                "id" => 372,
                "rta_id" => 42,
                "name" => "ASEAN - China",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "Enabling Clause & GATS Art. V",
                "signature_date" => "2004-11-29",
                "notification_date" => "2005-09-21",
                "entry_force_date" => "2005-01-01",
                "current_signatories" => [
                    "Brunei Darussalam",
                    "Myanmar",
                    "Cambodia",
                    "Indonesia",
                    "Laos",
                    "Malaysia",
                    "Philippines",
                    "Singapore",
                    "Vietnam",
                    "Thailand",
                    "China"
                ],
                "original_signatories" => [
                    "Brunei Darussalam",
                    "Myanmar",
                    "Cambodia",
                    "Indonesia",
                    "Laos",
                    "Malaysia",
                    "Philippines",
                    "Singapore",
                    "Vietnam",
                    "Thailand",
                    "China"
                ],
                "rta_composition" => [
                    "Bilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "East Asia"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.asean.org/",
                        "name" => "Association of Southeast Asian Nations"
                    ],
                    [
                        "url" => "http://www.mofcom.gov.cn/",
                        "name" => "Ministry of Commerce of the People's Republic of China"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://fta.mofcom.gov.cn/topic/chinaasean.shtml",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "China"
                ],
                "countries_second" => [
                    "Brunei Darussalam",
                    "Myanmar",
                    "Cambodia",
                    "Indonesia",
                    "Laos",
                    "Malaysia",
                    "Philippines",
                    "Singapore",
                    "Vietnam",
                    "Thailand"
                ]
            ],
            [
                "id" => 502,
                "rta_id" => 176,
                "name" => "ASEAN - Japan",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "2008-03-26",
                "notification_date" => "2009-11-23",
                "entry_force_date" => "2008-12-01",
                "current_signatories" => [
                    "Brunei Darussalam",
                    "Myanmar",
                    "Cambodia",
                    "Indonesia",
                    "Laos",
                    "Malaysia",
                    "Philippines",
                    "Singapore",
                    "Vietnam",
                    "Thailand",
                    "Japan"
                ],
                "original_signatories" => [
                    "Brunei Darussalam",
                    "Myanmar",
                    "Cambodia",
                    "Indonesia",
                    "Laos",
                    "Malaysia",
                    "Philippines",
                    "Singapore",
                    "Vietnam",
                    "Thailand",
                    "Japan"
                ],
                "rta_composition" => [
                    "Bilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "East Asia"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.mofa.go.jp/",
                        "name" => "Ministry of Foreign Affairs of Japan"
                    ],
                    [
                        "url" => "http://www.asean.org/",
                        "name" => "Association of Southeast Asian Nations"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.mofa.go.jp/policy/economy/fta/asean/agreement.pdf",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Japan"
                ],
                "countries_second" => [
                    "Brunei Darussalam",
                    "Myanmar",
                    "Cambodia",
                    "Indonesia",
                    "Laos",
                    "Malaysia",
                    "Philippines",
                    "Singapore",
                    "Vietnam",
                    "Thailand"
                ]
            ],
            [
                "id" => 872,
                "rta_id" => 983,
                "name" => "Common Market for Eastern and Southern Africa (COMESA) - Accession of Egypt",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "Enabling Clause",
                "signature_date" => "1998-06-29",
                "notification_date" => "2017-01-03",
                "entry_force_date" => "1999-02-17",
                "current_signatories" => [
                    "Burundi",
                    "Comoros",
                    "Congo, Democratic Republic",
                    "Djibouti",
                    "Egypt",
                    "Eritrea",
                    "Swaziland",
                    "Ethiopia",
                    "Kenya",
                    "Libya",
                    "Madagascar",
                    "Malawi",
                    "Mauritius",
                    "Rwanda",
                    "Seychelles",
                    "Sudan",
                    "Uganda",
                    "Zambia",
                    "Zimbabwe"
                ],
                "original_signatories" => [
                    "Burundi",
                    "Comoros",
                    "Congo, Democratic Republic",
                    "Djibouti",
                    "Egypt",
                    "Eritrea",
                    "Swaziland",
                    "Ethiopia",
                    "Kenya",
                    "Libya",
                    "Madagascar",
                    "Malawi",
                    "Mauritius",
                    "Rwanda",
                    "Seychelles",
                    "Sudan",
                    "Uganda",
                    "Zambia",
                    "Zimbabwe"
                ],
                "rta_composition" => [
                    "Plurilateral"
                ],
                "region" => [
                    "Africa"
                ],
                "wto_members" => false,
                "type" => "Customs Union",
                "web_addresses" => [
                    [
                        "url" => "http://www.comesa.int/",
                        "name" => "Common Market for Eastern and Southern Africa"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://www.tas.gov.eg/NR/rdonlyres/4AF5E263-75D7-407E-85B8-3C2A1E9FA427/1200/comesa1.pdf",
                        "name" => "E"
                    ]
                ],
                "countries_first" => [
                    "Burundi"
                ],
                "countries_second" => [
                    "Comoros",
                    "Congo, Democratic Republic",
                    "Ethiopia",
                    "Eritrea",
                    "Djibouti",
                    "Kenya",
                    "Libya",
                    "Madagascar",
                    "Malawi",
                    "Mauritius",
                    "Rwanda",
                    "Seychelles",
                    "Zimbabwe",
                    "Sudan",
                    "Swaziland",
                    "Uganda",
                    "Egypt",
                    "Zambia"
                ]
            ],
            [
                "id" => 348,
                "rta_id" => 18,
                "name" => "EU - Algeria",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "2002-04-22",
                "notification_date" => "2006-07-24",
                "entry_force_date" => "2005-09-01",
                "current_signatories" => [
                    "Algeria",
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom"
                ],
                "original_signatories" => [
                    "Algeria"
                ],
                "rta_composition" => [
                    "Bilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "Africa",
                    "Europe"
                ],
                "wto_members" => false,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://ec.europa.eu/",
                        "name" => "European Commission"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1398350436383&amp;uri=CELEX:22005A1010(01)",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1398350436383&amp;uri=CELEX:22005A1010(01)",
                        "name" => "F"
                    ],
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1398350436383&amp;uri=CELEX:22005A1010(01)",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Algeria"
                ],
                "countries_second" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom"
                ]
            ],
            [
                "id" => 761,
                "rta_id" => 623,
                "name" => "EU - Côte d'Ivoire",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "2008-11-26",
                "notification_date" => "2008-12-11",
                "entry_force_date" => "2016-09-03",
                "current_signatories" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom",
                    "Côte d'Ivoire"
                ],
                "original_signatories" => [
                    "Côte d'Ivoire"
                ],
                "rta_composition" => [
                    "Bilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "Europe",
                    "Africa"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://ec.europa.eu/",
                        "name" => "European Commission"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1398352060784&amp;uri=CELEX:22009A0303(01)",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1398352060784&amp;uri=CELEX:22009A0303(01)",
                        "name" => "F"
                    ],
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1398352060784&amp;uri=CELEX:22009A0303(01)",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Côte d'Ivoire"
                ],
                "countries_second" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom"
                ]
            ],
            [
                "id" => 374,
                "rta_id" => 44,
                "name" => "EU - Egypt",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "2001-06-25",
                "notification_date" => "2004-09-03",
                "entry_force_date" => "2004-06-01",
                "current_signatories" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom",
                    "Egypt"
                ],
                "original_signatories" => [
                    "Egypt"
                ],
                "rta_composition" => [
                    "Bilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "Europe",
                    "Africa"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://ec.europa.eu/",
                        "name" => "European Commission"
                    ],
                    [
                        "url" => "http://www.mfa.gov.eg/English/Pages/default.aspx",
                        "name" => "Arab Republic of Egypt, Ministry of Foreign Affairs"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1398411944881&amp;uri=CELEX:22004A0930(03)",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1398411944881&amp;uri=CELEX:22004A0930(03)",
                        "name" => "F"
                    ],
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1398411944881&amp;uri=CELEX:22004A0930(03)",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Egypt"
                ],
                "countries_second" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom"
                ]
            ],
            [
                "id" => 470,
                "rta_id" => 143,
                "name" => "EU - Iceland",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "1972-12-19",
                "notification_date" => "1972-11-24",
                "entry_force_date" => "1973-04-01",
                "current_signatories" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom",
                    "Iceland"
                ],
                "original_signatories" => [
                    "Iceland"
                ],
                "rta_composition" => [
                    "Bilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "Europe"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://ec.europa.eu/",
                        "name" => "European Commission"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1399389051991&amp;uri=CELEX:21972A0722(05)",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1399389051991&amp;uri=CELEX:21972A0722(05)",
                        "name" => "F"
                    ],
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1399389051991&amp;uri=CELEX:21972A0722(05)",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Iceland"
                ],
                "countries_second" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom"
                ]
            ],
            [
                "id" => 493,
                "rta_id" => 167,
                "name" => "EU - Korea, Republic of",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2010-10-06",
                "notification_date" => "2011-07-07",
                "entry_force_date" => "2011-07-01",
                "current_signatories" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom",
                    "North Korea"
                ],
                "original_signatories" => [
                    "North Korea"
                ],
                "rta_composition" => [
                    "Bilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "Europe",
                    "East Asia"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://www.mofa.go.kr/eng/index.do",
                        "name" => "Ministry of Foreign Affairs and Trade, Republic of Korea"
                    ],
                    [
                        "url" => "http://ec.europa.eu/",
                        "name" => "European Commission"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1399390040762&amp;uri=CELEX:22011A0514(01)",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1399390040762&amp;uri=CELEX:22011A0514(01)",
                        "name" => "F"
                    ],
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1399390040762&amp;uri=CELEX:22011A0514(01)",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "North Korea"
                ],
                "countries_second" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom"
                ]
            ],
            [
                "id" => 439,
                "rta_id" => 111,
                "name" => "EU - Palestinian Authority",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "1997-02-24",
                "notification_date" => "1997-05-29",
                "entry_force_date" => "1997-07-01",
                "current_signatories" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom",
                    "Palestine"
                ],
                "original_signatories" => [
                    "Palestine"
                ],
                "rta_composition" => [
                    "Bilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "Europe",
                    "Middle East"
                ],
                "wto_members" => false,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://ec.europa.eu/",
                        "name" => "European Commission"
                    ],
                    [
                        "url" => "http://www.paltrade.org",
                        "name" => "Palestine Trade Center"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1399391758208&amp;uri=CELEX:21997A0716(01)",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1399391758208&amp;uri=CELEX:21997A0716(01)",
                        "name" => "F"
                    ],
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1399391758208&amp;uri=CELEX:21997A0716(01)",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Palestine"
                ],
                "countries_second" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom"
                ]
            ],
            [
                "id" => 405,
                "rta_id" => 77,
                "name" => "EU - The former Yugoslav Republic of Macedonia",
                "coverage" => "Goods & Services",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV & GATS Art. V",
                "signature_date" => "2001-04-09",
                "notification_date" => "2001-10-23",
                "entry_force_date" => "2001-06-01",
                "current_signatories" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom",
                    "Macedonia"
                ],
                "original_signatories" => [
                    "Macedonia"
                ],
                "rta_composition" => [
                    "Bilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "Europe"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement & Economic Integration Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://ec.europa.eu/",
                        "name" => "European Commission"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1399388804412&amp;uri=CELEX:22001A0504(01)",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1399388804412&amp;uri=CELEX:22001A0504(01)",
                        "name" => "F"
                    ],
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1399388804412&amp;uri=CELEX:22001A0504(01)",
                        "name" => "S"
                    ],
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?uri=uriserv:OJ.L_.2004.084.01.0013.01.ENG",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?uri=uriserv:OJ.L_.2004.084.01.0013.01.ENG",
                        "name" => "F"
                    ],
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?uri=uriserv:OJ.L_.2004.084.01.0013.01.ENG",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Macedonia"
                ],
                "countries_second" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom"
                ]
            ],
            [
                "id" => 446,
                "rta_id" => 118,
                "name" => "EU - Turkey",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "1995-03-06",
                "notification_date" => "1995-12-22",
                "entry_force_date" => "1996-01-01",
                "current_signatories" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom",
                    "Turkey"
                ],
                "original_signatories" => [
                    "Turkey"
                ],
                "rta_composition" => [
                    "Bilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "Europe"
                ],
                "wto_members" => true,
                "type" => "Customs Union",
                "web_addresses" => [
                    [
                        "url" => "http://ec.europa.eu/",
                        "name" => "European Commission"
                    ],
                    [
                        "url" => "http://www.economy.gov.tr/",
                        "name" => "Turkish Government"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/PDF/?uri=OJ:L:1996:035:FULL&amp;from=EN",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/FR/TXT/PDF/?uri=OJ:L:1996:035:FULL&amp;from=EN",
                        "name" => "F"
                    ],
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/ES/TXT/PDF/?uri=OJ:L:1996:035:FULL&amp;from=EN",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Turkey"
                ],
                "countries_second" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom"
                ]
            ],
            [
                "id" => 420,
                "rta_id" => 92,
                "name" => "EU - Morocco",
                "coverage" => "Goods",
                "status" => "In Force",
                "notification_under" => "GATT Art. XXIV",
                "signature_date" => "1996-02-26",
                "notification_date" => "2000-10-13",
                "entry_force_date" => "2000-03-01",
                "current_signatories" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom",
                    "Morocco"
                ],
                "original_signatories" => [
                    "Morocco"
                ],
                "rta_composition" => [
                    "Bilateral",
                    "One Party is an RTA"
                ],
                "region" => [
                    "Europe",
                    "Africa"
                ],
                "wto_members" => true,
                "type" => "Free Trade Agreement",
                "web_addresses" => [
                    [
                        "url" => "http://ec.europa.eu/",
                        "name" => "European Commission"
                    ],
                    [
                        "url" => "http://www.mce.gov.ma/Home.asp",
                        "name" => "Ministère du Commerce Extérieur, Royaume du Maroc"
                    ]
                ],
                "agreement" => [
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1399391205944&amp;uri=CELEX:22000A0318(01)",
                        "name" => "E"
                    ],
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1399391205944&amp;uri=CELEX:22000A0318(01)",
                        "name" => "F"
                    ],
                    [
                        "url" => "http://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1399391205944&amp;uri=CELEX:22000A0318(01)",
                        "name" => "S"
                    ]
                ],
                "countries_first" => [
                    "Morocco"
                ],
                "countries_second" => [
                    "Austria",
                    "Belgium",
                    "Bulgaria",
                    "Croatia",
                    "Cyprus",
                    "Czech Republic",
                    "Denmark",
                    "Estonia",
                    "Finland",
                    "France",
                    "Germany",
                    "Greece",
                    "Hungary",
                    "Ireland",
                    "Italy",
                    "Latvia",
                    "Lithuania",
                    "Luxembourg",
                    "Malta",
                    "Netherlands",
                    "Poland",
                    "Portugal",
                    "Romania",
                    "Slovakia",
                    "Slovenia",
                    "Spain",
                    "Sweden",
                    "United Kingdom"
                ]
            ]
        ];

        foreach ($rta as $item) {
            unset($item['id']);
            $rta_item = Rta::with('countries', 'countries_first_side', 'countries_second_side')
                ->where('rta_id', $item['rta_id'])->first();
            if ($rta_item) {
                $rta_item->update($item);
                $rta_item->save();
            } else {
                $rta_item = Rta::create($item);
            }

            foreach ($item['current_signatories'] as $country) {
                $q = strtolower($country);
                $c = Country::where(function ($query) use ($q) {
                    $query->whereTranslationInsensitiveLike('name', str_replace("'", "`", $q));
                })->first();

                if ($c) {
                    $rta_item->countries()->sync($c->id, false);
                }
            }

            foreach ($item['countries_first'] as $country) {
                $q = strtolower($country);
                $c = Country::where(function ($query) use ($q) {
                    $query->whereTranslationInsensitiveLike('name', str_replace("'", "`", $q));
                })->first();

                if ($c) {
                    $rta_item->countries_first_side()->sync($c->id, false);
                    $rta_item->countries()->sync($c->id, false);
                }
            }

            foreach ($item['countries_second'] as $country) {
                $q = strtolower($country);
                $c = Country::where(function ($query) use ($q) {
                    $query->whereTranslationInsensitiveLike('name', str_replace("'", "`", $q));
                })->first();

                if ($c) {
                    $rta_item->countries_second_side()->sync($c->id, false);
                    $rta_item->countries()->sync($c->id, false);
                }
            }
        }
    }
}
