<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		DB::table('users')->updateOrInsert(['email' => 'tester@gmail.com'], [
//		    'name' => str_random(10),
			'email' => 'tester@gmail.com',
			'phone' => '+380' . random_int(100000000, 999999999),
			'password' => bcrypt('123456'),
			'country_id' => 2,
			'language_id' => 1,
            'is_verified' => true
		]);
	}
}
