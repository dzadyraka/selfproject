<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LanguagesTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {

		$languages = [
			['language_id' => 1, 'name' => 'Ukrainian', 'code' => 'uk', 'locale' => 'uk-ua', 'image' => 'uk', 'is_active' => true],
			['language_id' => 2, 'name' => 'Russian', 'code' => 'ru', 'locale' => 'ru-ru', 'image' => 'ru', 'is_active' => true],
			['language_id' => 3, 'name' => 'English', 'code' => 'en', 'locale' => 'en-us', 'image' => 'en', 'is_active' => true]
		];

		foreach ($languages as $language) {
			DB::table('languages')->updateOrInsert(['language_id' => $language['language_id']], $language);
		}

		$translations = [
			['language_id' => 1, 'locale' => 'uk', 'name' => 'Українська'],
			['language_id' => 1, 'locale' => 'ru', 'name' => 'Украинский'],
			['language_id' => 1, 'locale' => 'en', 'name' => 'Ukrainian'],
			['language_id' => 2, 'locale' => 'uk', 'name' => 'Російська'],
			['language_id' => 2, 'locale' => 'ru', 'name' => 'Русский'],
			['language_id' => 2, 'locale' => 'en', 'name' => 'Russian'],
			['language_id' => 3, 'locale' => 'uk', 'name' => 'Англіська'],
			['language_id' => 3, 'locale' => 'ru', 'name' => 'Английский'],
			['language_id' => 3, 'locale' => 'en', 'name' => 'English']
		];

		foreach ($translations as $translation) {
			DB::table('language_translations')->updateOrInsert([
				'language_id' => $translation['language_id'],
				'locale' => $translation['locale']
			], $translation);
		}
	}
}
