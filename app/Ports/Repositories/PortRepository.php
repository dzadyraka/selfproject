<?php

namespace App\Ports\Repositories;


use App\Ports\Models\ContainerTerminal;
use App\Repositories\Repository;
use Illuminate\Support\Collection;

class PortRepository extends Repository {
    public function __construct(ContainerTerminal $model) {
        parent::__construct($model);
    }

    public function getCountryContainerTerminals(int $countryId): Collection {
        return $this->getModel()
            ->whereCountryId($countryId)
            ->get();
    }
}