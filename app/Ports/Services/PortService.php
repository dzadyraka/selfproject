<?php

namespace App\Ports\Services;


use App\Locations\Models\City;
use App\Locations\Services\CityService;
use App\Ports\Repositories\PortRepository;
use App\Services\Service;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class PortService extends Service {
    /**
     * @var CityService
     */
    private $cityService;

    public function __construct(PortRepository $repository, CityService $cityService) {
        parent::__construct($repository);

        $this->cityService = $cityService;
    }
    
    public function getClosestCityPorts(City $city): Collection {
        $coordinates = $this->cityService->getCityCoordinates($city);

        return $this->repository
            ->getModel()
            ->select('*', DB::raw("(point(coordinate_lng, coordinate_lat) <@> point($coordinates[lng], $coordinates[lat])) * 1.60934 as distance"))
            ->orderBy('distance')
            ->limit(10)
            ->with('country')
            ->get();
    }
}
