<?php

namespace App\Ports\Commands;


use App\Ports\Jobs\SynchronizeSeaRatesPorts;
use Illuminate\Console\Command;

class GetSeaRatesPorts extends Command {
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'searates:parse';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        (new SynchronizeSeaRatesPorts())->handle();
    }
}