<?php

namespace App\Ports\Models;


use App\Locations\Models\Country;
use Illuminate\Database\Eloquent\Model;

class ContainerTerminal extends Model {

    public $timestamps = false;

    protected $fillable = [
        'key', 'name', 'coordinate_lat', 'coordinate_lng', 'country_id'
    ];

    public function country() {
        return $this->belongsTo(Country::class);
    }
}