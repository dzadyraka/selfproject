<?php

namespace App\Ports\Jobs;


use App\Locations\Models\Country;
use App\Ports\Models\ContainerTerminal;
use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SynchronizeSeaRatesPorts implements ShouldQueue {

    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    const SEA_RATES_PORTS = 'https://www.searates.com/maritime/ports-map/';

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle() {
        $client = new Client(['base_uri' => self::SEA_RATES_PORTS]);

        Country::all()->each(function (Country $country) use ($client) {
            $response = json_decode(
                $client->post('', ['form_params' => ['c' => strtoupper($country->code)]])
                    ->getBody()->getContents(), true
            );

            foreach ($response['cports'] as $cport) {
                $port = ContainerTerminal::where('key', $cport['ckey'])
                    ->whereCountryId($country->id)
                    ->first();

                if ($cport['t']) {
                    continue;
                }

                if (!$port) {
                    ContainerTerminal::create([
                        'key' => $cport['ckey'],
                        'name' => $cport['name'],
                        'coordinate_lat' => $cport['lat'],
                        'coordinate_lng' => $cport['lng'],
                        'country_id' => $country->id
                    ]);
                } else {
                    $port->update([
                        'key' => $cport['ckey'],
                        'name' => $cport['name'],
                        'coordinate_lat' => $cport['lat'],
                        'coordinate_lng' => $cport['lng']
                    ]);
                }
            }
        });
    }

}