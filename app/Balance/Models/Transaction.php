<?php

namespace App\Balance\Models;


use App\Currency\Currency;
use App\Http\Traits\HasUserId;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model {
	use HasUserId;

	const CREATED_AT = 'timestamp';

	protected $fillable = [
		'user_id', 'value', 'comment', 'currency_id', 'currency_code',
		'payment_method', 'payment_system', 'payment_details'
	];

	public static function boot() {
		parent::boot();

		self::saving(function (Transaction $transaction) {
			$user = $transaction->user;
			$user->balance += $transaction->value;
			$user->save();
		});
	}

	public function currency() {
		return $this->belongsTo(Currency::class);
	}

	public function setUpdatedAtAttribute($value) {
		// to Disable updated_at
	}
}