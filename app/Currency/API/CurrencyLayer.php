<?php

namespace App\Currency\API;


use GuzzleHttp\Client;

class CurrencyLayer {

	const API_URI = 'http://apilayer.net/api/';

	private static $instance;

	private $api_key;

	private function __construct() {
		$this->api_key = env('CURRENCY_LAYER_API_KEY', '5c567c301cfee5c2c0f9f7633301c13d');
	}

	public static function getInstance(): self {
		if (!static::$instance) {
			static::$instance = new static();
		}

		return static::$instance;
	}

	/**
	 * Return's list of all currencies
	 *
	 * @return mixed
	 */
	public function get(): array {
		$currencies = $this->request();
		$rate = [];

		foreach ($currencies as $codes => $currency) {
			$rate[] = [
				'source' => substr($codes, 0, 3),
				'destination' => substr($codes, 3, 6),
				'rate' => $currency
			];
		}

		return $rate;
	}

	private function request(): array {
		$request = (new Client())
			->request('GET', static::API_URI . 'live?access_key=' . $this->api_key)
			->getBody()
			->getContents();

		return json_decode($request, true)['quotes'];
	}
}