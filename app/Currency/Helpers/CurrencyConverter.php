<?php

namespace App\Currency\Helpers;


use App\Currency\CurrencyRate;

class CurrencyConverter {
    static public function convert(float $value, int $from, int $to): float {
        $currencies = CurrencyRate::currencies();

        return round($value / $currencies[$from]['rate'] * $currencies[$to]['rate'], 2);
    }
}