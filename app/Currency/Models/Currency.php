<?php

namespace App\Currency;


use App\Http\Traits\Translatable;
use Illuminate\Database\Eloquent\Model;

class Currency extends Model {
	use Translatable;

	/**
	 * @var array List of currencies.
	 */
	static protected $_currencies_list;

	public $translatedAttributes = ['name'];
	public $timestamps = false;

	protected $fillable = [
		'code', 'symbol', 'thousand_separator', 'decimal_separator', 'active'
	];

	/**
	 * Function validates currency id.
	 *
	 * @param int $id example: 1, 2, 3, ...
	 *
	 * @return bool true - if id is valid, false - if not.
	 */
	static public function validateCurrencyId($id) {
		return in_array($id, array_column(self::getCurrencies(), 'id'));
	}

	/**
	 * Function returns list of available currencies as array.
	 *
	 * @return array example: [ { id: 1, code: 'USD', name: 'Dollar', thousand_separator: ',', decimal_separator: '.', symbol: '$' }, ... ]
	 */
	static public function getCurrencies() {
		if (!empty(self::$_currencies_list)) {
			return self::$_currencies_list;
		}

		$currencies = [];

		Currency::get()->each(function (Currency $currency, $_) use (&$currencies) {
			$currencies[$currency['code']] = $currency->toArray();
		});

		self::$_currencies_list = $currencies;

		return $currencies;
	}

	/**
	 * Function returns currency code, by currency ID in database.
	 *
	 * @param int $id example: 1, 2, 3, ...
	 *
	 * @return string example: 'USD', 'UAH', ...
	 */
	static public function getCodeByCurrencyId($id) {
		if (!in_array($id, array_column(self::getCurrencies(), 'id'))) {
			return '';
		}

		$currencies = self::getCurrencies();

		foreach ($currencies as $currency) {
			if ($currency['id'] === $id) {
				return $currency['code'];
			}
		}
	}

	/**
	 * Function returns currency code, by currency ID in database.
	 *
	 * @param $code
	 * @return string example: 'USD', 'UAH', ...
	 * @internal param int $id example: 1, 2, 3, ...
	 *
	 */
	static public function getIdByCurrencyCode($code) {
		if (!in_array($code, static::getCodes())) {
			return null;
		}

		return static::getCurrencies()[$code]['id'];
	}

	/**
	 * Function return's list of available currency codes.
	 *
	 * @return array example: ['USD', 'UAH', ... ]
	 */
	static public function getCodes() {
		return array_keys(self::getCurrencies());
	}

	/**
	 * Function returns currency symbol, by currency code.
	 *
	 * @param string $code example: 'USD'
	 *
	 * @return string '$'
	 */
	static public function getSymbol($code) {
		if (!self::codeExist($code)) {
			return '';
		}

		return self::getCurrencies()[$code]['symbol'];
	}

	/**
	 * Function validates currency code.
	 *
	 * @param string $code example: 'UAH', 'USD', ...
	 *
	 * @return bool true - if code exist, false - if not
	 */
	static public function codeExist($code) {
		return in_array($code, array_keys(self::getCurrencies()));
	}

	/**
	 * Function returns currency name, by currency code.
	 *
	 * @param string $code example: 'USD'
	 *
	 * @return string example: 'Dollar'
	 */
	static public function getName($code) {
		if (!self::codeExist($code)) {
			return '';
		}

		return self::getCurrencies()[$code]['name'];
	}

	/**
	 * Function returns currency thousand separator, by currency code.
	 *
	 * @param string $code example: 'USD'
	 *
	 * @return string example: ','
	 */
	static public function getThousandSeparator($code) {
		if (!self::codeExist($code)) {
			return ',';
		}

		return self::getCurrencies()[$code]['thousand_separator'];
	}

	/**
	 * Function returns currency decimal separator, by currency code.
	 *
	 * @param string $code example: 'USD'
	 *
	 * @return string example: '.'
	 */
	static public function getDecimalSeparator($code) {
		if (!self::codeExist($code)) {
			return '.';
		}

		return self::getCurrencies()[$code]['decimal_separator'];
	}
}