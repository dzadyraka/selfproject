<?php

namespace App\Currency;


use Illuminate\Database\Eloquent\Model;

class CurrencyTranslation extends Model {
	public $timestamps = false;
	protected $fillable = ['name'];
}