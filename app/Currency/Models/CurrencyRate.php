<?php

namespace App\Currency;


use Illuminate\Database\Eloquent\Model;

class CurrencyRate extends Model {
	public static $_currencies = [];

	public $timestamps = false;

	protected $fillable = [
		'source', 'source_id', 'destination', 'destination_id', 'rate', 'date_from', 'date_to'
	];

	static public function currencies($time = null) {
		if (!empty(static::$_currencies)) {
			return static::$_currencies;
		}

		$time = $time ?? date("Y-m-d H:i:s");

		CurrencyRate::where('date_from', '<=', $time)
			->where(function ($query) use ($time) {
				return $query->where('date_to', '<=', $time)
					->orWhereNull('date_to');
			})->get()->each(function (CurrencyRate $rate, $_) {
				static::$_currencies[$rate['destination_id']] = $rate->toArray();
			});

		return static::$_currencies;
	}

	public function source() {
		return $this->belongsTo(Currency::class);
	}

	public function destination() {
		return $this->belongsTo(Currency::class);
	}
}