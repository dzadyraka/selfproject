<?php

namespace App\Http\Controllers\Products\Feedback;

use App\Products\Models\Product;
use App\Products\Models\ProductFeedback;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FeedbackController extends Controller {
    /**
     * @SWG\Put(
     *     path="/api/v1/products/{product_id}/feedback",
     *     tags={"product-feedback"},
     *     operationId="create",
     *     description="Product create feedback",
     *     @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          @SWG\Schema(
     *              ref="#/definitions/newProductFeedback"
     *          )
     *     ),
     *     @SWG\Response(
     *          response="201",
     *          description="Product create feedback",
     *          @SWG\Schema(ref="#/definitions/ProductFeedback")
     *     ),
     *     security={{
     *       "Bearer":{}
     *     }}
     * )
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create($id, Request $request) {
        $request->merge(['product_id' => $id]);

        $feedback = ProductFeedback::create($request->input())
            ->fresh([
                'creator',
                'company'
            ]);

        (new Product())->updateRating($id);
        return response()->json(
            $feedback
        );
    }

    /**
     * @SWG\Get(
     *     path="/api/v1/products/{product_id}/feedback",
     *     tags={"product-feedback"},
     *     operationId="getList",
     *     description="Get products feedback list",
     *     @SWG\Response(
     *          response="200",
     *          description="Get products feedback list",
     *          @SWG\Schema(
     *              type="array",
     *              @SWG\Items(ref="#/definitions/ProductFeedback"),
     *          )
     *     ),
     *     security={{
     *       "Bearer":{}
     *     }}
     * )
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getList($id) {
        return response()->json(
            ProductFeedback::where('product_id', $id)->with(['creator', 'company'])->get()
        );
    }

    /**
     * @SWG\Get(
     *     path="/api/v1/products/{product_id}/feedback/{feedback_id}",
     *     tags={"product-feedback"},
     *     operationId="view",
     *     description="View product feedback",
     *     @SWG\Response(
     *          response="200",
     *          description="View product feedback",
     *          @SWG\Schema(
     *              ref="#/definitions/ProductFeedback"
     *          )
     *     ),
     *     security={{
     *       "Bearer":{}
     *     }}
     * )
     * @param $id
     * @param $feedback_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function view($id, $feedback_id) {
        return response()->json(
            ProductFeedback::with(['creator', 'company'])->findOrFail($feedback_id)
        );
    }

    /**
     * @SWG\Post(
     *     path="/api/v1/products/{product_id}/feedback/{feedback_id}",
     *     tags={"product-feedback"},
     *     operationId="update",
     *     description="Update product feedback",
     *     @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          @SWG\Schema(
     *              ref="#/definitions/newProductFeedback"
     *          )
     *     ),
     *     @SWG\Response(
     *          response="200",
     *          description="Update product feedback",
     *          @SWG\Schema(ref="#/definitions/ProductFeedback")
     *     ),
     *     security={{
     *       "Bearer":{}
     *     }}
     * )
     * @param $id
     * @param $feedback_id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id, $feedback_id, Request $request) {
        $feedback = ProductFeedback::findOrfail($feedback_id);
        $request->merge(['product_id' => $id]);
        $feedback->update($request->input());

        (new Product())->updateRating($id);

        return response()->json(
            $feedback->fresh(['creator', 'product', 'company'])
        );
    }

    /**
     * @SWG\Delete(
     *     path="/api/v1/products//{product_id}/feedback/{feedback_id}",
     *     tags={"product-feedback"},
     *     operationId="delete",
     *     description="Delete product feedback",
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=204,
     *         description="Delete product feedback"
     *     ),
     *     security={{"Bearer":{}}}
     * )
     * @param $id
     * @param $feedback_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id, $feedback_id) {
        ProductFeedback::findOrFail($feedback_id)->delete();
        (new Product())->updateRating($id);
        return response()->json(
            [],
            204
        );
    }
}
