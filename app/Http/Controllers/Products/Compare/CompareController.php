<?php

namespace App\Http\Controllers\Products\Compare;

use App\Products\Models\ProductCompare;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CompareController extends Controller
{
    /**
     * @SWG\Get(
     *     path="/api/v1/products/compare",
     *     tags={"product-compare"},
     *     operationId="listCompare",
     *     description="Get list product compare",
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Get list product compare",
     *         @SWG\Schema(
     *              type="array",
     *              @SWG\Items(ref="#/definitions/ProductCompare")
     *         ),
     *     ),
     *     security={{"Bearer":{}}}
     * )
     * @return \Illuminate\Http\JsonResponse
     */
    public function listCompare() {
        return response()->json(
            (new ProductCompare())->with([
                'product' => function($product) {
                    return $product->with('company', 'currency');
                }
            ])->get()
        );
    }

    /**
     * @SWG\Put(
     *     path="/api/v1/products/compare",
     *     tags={"product-compare"},
     *     operationId="create",
     *     description="Add product to compare",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="product-compare",
     *         in="body",
     *         description="Add product to compare",
     *         required=true,
     *         @SWG\Schema(
     *              ref="#/definitions/newProductCompare"
     *         ),
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Add product to compare",
     *         @SWG\Schema(
     *              ref="#/definitions/ProductCompare"
     *         ),
     *     ),
     *     security={{"Bearer":{}}}
     * )
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request) {
        return response()->json(
            ProductCompare::create($request->input())->fresh([
                'product' => function($product) {
                    return $product->with('company', 'currency');
                }
            ])
        );
    }

    /**
     * @SWG\Delete(
     *     path="/api/v1/products/compare/{compare_id}",
     *     tags={"product-compare"},
     *     operationId="delete",
     *     description="Delete product from compare",
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=204,
     *         description="Delete product from compare"
     *     ),
     *     security={{"Bearer":{}}}
     * )
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id) {
        return response()->json(
            ProductCompare::findOrFail($id)->delete(),
            204
        );
    }

    /**
     * @SWG\Delete(
     *     path="/api/v1/products/compare",
     *     tags={"product-compare"},
     *     operationId="deleteAll",
     *     description="Delete all products from compare",
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=204,
     *         description="Delete all products from compare"
     *     ),
     *     security={{"Bearer":{}}}
     * )
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteAll(Request $request) {
        return response()->json(
            ProductCompare::where('user_id', $request->input('user_id'))->delete(),
            204
        );
    }
}
