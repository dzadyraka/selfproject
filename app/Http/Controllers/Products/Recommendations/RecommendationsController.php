<?php

namespace App\Http\Controllers\Products\Recommendations;

use Illuminate\Http\Request;
use App\Products\Models\Product;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Products\Models\ProductView;
use Illuminate\Support\Facades\Cache;

class RecommendationsController extends Controller {

    /**
     * Get recomendation list products with 1 hour cache
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function listRecommendations(int $id) {
        $views = Cache::get("recomendation:product:{$id}:views");

        if (empty($views)) {
            $views = (new ProductView())->recommendations($id);
            Cache::put("recomendation:product:{$id}:views", $views, 1800);
            $views = Cache::get("recomendation:product:{$id}:views");
        }

        return response()->json(
            [
                'type' => 'also-viewed',
                'products' => $views
            ]
        );
    }

    /**
     * Get products by category
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function recommendationByCategory($id) {
        return response()->json(
            [
                'type' => 'similar-products',
                'products' => $this->_recommendationByCategory($id)->get()
            ]
        );
    }

    private function _recommendationByCategory(int $product_id) {
        $categories = Cache::get('product_model_categories');
        if (empty($categories)) {
            $categories = DB::select(DB::raw("SELECT id, parent_id FROM categories WHERE id IN (SELECT category_id FROM product_categories WHERE product_id = {$product_id})"));
            $categories = array_map(function ($value) {
                return ((array)$value);
            }, $categories);
            Cache::put('product_model_categories', $categories, 3600);
            $categories = Cache::get('product_model_categories');
        }

        $parent_ids = array_map(function ($value) {
            return ((array)$value)['parent_id'];
        }, $categories);

        foreach ($categories as $key => $category) {
            if (!in_array($category['id'], $parent_ids)) {
                $ids[] = $category['id'];
            }
        }

        $product = Product::findOrFail($product_id);
        $q = $product->name;
        $attributes = $product['attributes_list'];

        $clear_name = preg_replace('/\s+/u', ' ', preg_replace('/[^\p{L}\p{N}]/u', ' ', str_replace("'", "", $q)));
        $name_to_array = explode(' ', $clear_name);
        $products = Product::query();


        foreach ($name_to_array as $name) {
            $search_name = strtolower($name);
            $products->whereTranslationInsensitiveLike('name', "%$search_name%");
        }

//        foreach ($attributes as $name) {
//            $search_name = $name['name'];// strtolower($name->name);
//            $search_value = $name['value'];// strtolower($name->value);
//            $product_model->orWhere(function ($query) use ($search_name, $search_value) {
//                $query->whereTranslationInsensitiveLike('attributes_list.name', "%$search_name%")
//                    ->orwhereTranslationInsensitiveLike('attributes_list.value', "%$search_value%");;
//            });
//        }

        return $products->where('id', '<>', $product_id);
    }
}
