<?php

namespace App\Http\Controllers\Products\AttributeOptions;

use function foo\func;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\AttributeOptions\Models\AttributeOption;

class AttributeOptionsController extends Controller {

    /**
     * @SWG\Get(
     *     path="/api/v1/products/attributes/options",
     *     tags={"product-attributes-options"},
     *     operationId="listAttributeOptions",
     *     description="Get list attributes options",
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Get list attributes options",
     *         @SWG\Schema(
     *             @SWG\Items(ref="#/definitions/AttributeOptions")
     *         ),
     *     ),
     *     security={{"Bearer":{}}}
     * )
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function listAttributeOptions() {
        return response()->mjson(
            (new AttributeOption())->get()
        );
    }

    /**
     * @SWG\Post(
     *     path="/api/v1/products/attributes/options/search",
     *     tags={"product-attributes-options"},
     *     operationId="searchAttributeOptions",
     *     description="Search attributes options",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="q",
     *         in="query",
     *         description="Query for search",
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Search attributes options",
     *         @SWG\Schema(ref="#/definitions/AttributeOptions"),
     *     ),
     *     security={{"Bearer":{}}}
     * )
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function searchAttributeOptions(Request $request) {
        $q = strtolower($request->input('q'));
        return response()->mjson(
            AttributeOption::where(function ($query) use ($q) {
                $query->whereTranslationInsensitiveLike('name', "%$q%");
            })->get()
        );
    }

    public function getAttributeOptions(int $attribute, Request $request) {
        return response()->mjson(
            AttributeOption::whereAttributeId($attribute)
                ->when(!empty($request->input('q', '')), function ($query) use ($request) {
                    $search_query = strtolower($request->input('q'));
                    $query->whereTranslationInsensitiveLike('name', "%$search_query%");
                })->get()
        );
    }

    public function createAttributeOption(int $attribute, Request $request) {
        $option = AttributeOption::whereAttributeId($attribute)->whereTranslation('name', $request->input('name'))->first();

        if ($option) {
            return response()->mjson($option);
        }

        return response()->mjson(
            AttributeOption::create(array_merge($request->input(), [
                'attribute_id' => $attribute
            ]))
        );
    }

    /**
     * @SWG\Get(
     *     path="/api/v1/products/attributes/options/{attribute_option_id}",
     *     tags={"product-attributes-options"},
     *     operationId="view",
     *     description="View product attribute option",
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="View product attribute option",
     *         @SWG\Schema(
     *              ref="#/definitions/AttributeOptions"
     *         ),
     *     ),
     *     security={{"Bearer":{}}}
     * )
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function view($id) {
        return response()->mjson(
            (new AttributeOption())::findOrFail($id)
        );
    }

    /**
     * @SWG\Put(
     *     path="/api/v1/products/attributes/options",
     *     tags={"product-attributes-options"},
     *     operationId="create",
     *     description="Add product attributes",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Add product attribute option",
     *         required=true,
     *         @SWG\Schema(
     *              ref="#/definitions/newAttributeOptions"
     *         ),
     *     ),
     *     @SWG\Response(
     *         response=201,
     *         description="Add product attribute option",
     *         @SWG\Schema(
     *              ref="#/definitions/AttributeOptions"
     *         ),
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Can not add attribute"
     *     ),
     *     security={{"Bearer":{}}}
     * )
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request) {
        return response()->json(
            AttributeOption::create($request->input()),
            201
        );
    }

    /**
     * @SWG\Post(
     *     path="/api/v1/products/attributes/options/{attribute_option_id}",
     *     tags={"product-attributes-options"},
     *     operationId="edit",
     *     description="Edit product attribute option",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Edit product attribute option",
     *         required=true,
     *         @SWG\Schema(
     *              ref="#/definitions/newAttributeOptions"
     *         ),
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Edit product attribute option",
     *         @SWG\Schema(
     *              ref="#/definitions/AttributeOptions"
     *         ),
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Can not update attribute option"
     *     ),
     *     security={{"Bearer":{}}}
     * )
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit($id, Request $request) {
        $attribute_option = (new AttributeOption())::findOrFail($id);
        $attribute_option->update($request->input());
        return response()->json(
            $attribute_option->fresh()
        );
    }

    /**
     * @SWG\Delete(
     *     path="/api/v1/products/attributes/options/{attribute_option_id}",
     *     tags={"product-attributes-options"},
     *     operationId="delete",
     *     description="Delete product attribute option",
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=204,
     *         description="Delete product attribute option"
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Can not delete attribute option"
     *     ),
     *     security={{"Bearer":{}}}
     * )
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id) {
        return response()->json(
            (new AttributeOption())::findOrFail($id)->delete(),
            204
        );
    }
}
