<?php

namespace App\Http\Controllers\Products\Attributes;

use Illuminate\Http\Request;
use App\Attributes\Models\Attribute;
use App\Http\Controllers\Controller;

class AttributesController extends Controller {

    /**
     * @SWG\Get(
     *     path="/api/v1/products/attributes",
     *     tags={"product-attributes"},
     *     operationId="listAttributes",
     *     description="Get list attributes",
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Get list attributes",
     *         @SWG\Schema(
     *             @SWG\Items(ref="#/definitions/Attributes")
     *         ),
     *     ),
     *     security={{"Bearer":{}}}
     * )
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function listAttributes(Request $request) {
        $empty_options = !empty($request->input('empty_options')) ? $request->input('empty_options') : false;

        return response()->mjson(
            Attribute::with(['attribute_options'])->get()
        );
    }

    /**
     * @SWG\Post(
     *     path="/api/v1/products/attributes/search",
     *     tags={"product-attributes"},
     *     operationId="searchAttributes",
     *     description="Search attributes",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="q",
     *         in="query",
     *         description="Query for search",
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Search attributes",
     *         @SWG\Schema(ref="#/definitions/Attributes"),
     *     ),
     *     security={{"Bearer":{}}}
     * )
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function searchAttributes(Request $request) {
        return response()->mjson(
            Attribute::where(function ($query) use ($request) {
                $search = strtolower($request->input('q'));
                $query->whereTranslationInsensitiveLike('name', "%$search%");
            })->get()
        );
    }

    /**
     * @SWG\Get(
     *     path="/api/v1/products/attributes/{attribute_id}",
     *     tags={"product-attributes"},
     *     operationId="view",
     *     description="View product attributes",
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="View product attributes",
     *         @SWG\Schema(
     *              ref="#/definitions/Attributes"
     *         ),
     *     ),
     *     security={{"Bearer":{}}}
     * )
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function view($id) {
        return response()->mjson(
            (new Attribute())::findOrFail($id)
        );
    }

    /**
     * @SWG\Put(
     *     path="/api/v1/products/attributes",
     *     tags={"product-attributes"},
     *     operationId="create",
     *     description="Add product attributes",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Add product attributes",
     *         required=true,
     *         @SWG\Schema(
     *              ref="#/definitions/newAttribute"
     *         ),
     *     ),
     *     @SWG\Response(
     *         response=201,
     *         description="Add product attributes",
     *         @SWG\Schema(
     *              ref="#/definitions/Attributes"
     *         ),
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Can not add attribute"
     *     ),
     *     security={{"Bearer":{}}}
     * )
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request) {
        return response()->mjson(Attribute::create($request->input()), 201);
    }

    /**
     * @SWG\Post(
     *     path="/api/v1/products/attributes/{attribute_id}",
     *     tags={"product-attributes"},
     *     operationId="edit",
     *     description="Edit product attribute",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Edit product attributes",
     *         required=true,
     *         @SWG\Schema(
     *              ref="#/definitions/newAttribute"
     *         ),
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Edit product attributes",
     *         @SWG\Schema(
     *              ref="#/definitions/Attributes"
     *         ),
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Can not update attribute"
     *     ),
     *     security={{"Bearer":{}}}
     * )
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit($id, Request $request) {
        $attribute = (new Attribute())::findOrFail($id);
        $attribute->update($request->input());

        return response()->json(
            $attribute->fresh(),
            204
        );
    }

    /**
     * @SWG\Delete(
     *     path="/api/v1/products/attributes/{attribute_id}",
     *     tags={"product-attributes"},
     *     operationId="delete",
     *     description="Delete product attribute",
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=204,
     *         description="Delete product attribute"
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Can not delete attribute"
     *     ),
     *     security={{"Bearer":{}}}
     * )
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id) {
        return response()->json(
            (new Attribute())::fidnOrFail($id)->delete(),
            204
        );
    }
}
