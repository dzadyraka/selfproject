<?php

namespace App\Http\Controllers\Products\Views;

use JWTAuth;
use Illuminate\Http\Request;
use App\Products\Models\ProductView;
use App\Http\Controllers\Controller;

class ViewsController extends Controller {
    /**
     * Save count views
     *
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createViews(int $id, Request $request) {

        if (!empty($request->bearerToken())) {
            $user_id = JWTAuth::parseToken()->toUser()->id;
        } else {
            $user_id = 0;
        }

        $view = ProductView::where('user_id', $user_id)->where('product_id', $id);

        if (!$view->exists()) {
            $view->create([
                'user_id' => $user_id,
                'product_id' => $id
            ]);
        } else {
            $view->update(['count_views' => $view->first()->count_views + 1]);
        }
        return response()->json();
    }

    /**
     * Get list view
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function listViews() {
        return response()->json(
            ProductView::with(['product', 'creator'])->get()
        );
    }
}
