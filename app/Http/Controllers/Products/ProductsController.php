<?php

namespace App\Http\Controllers\Products;

use App\Products\Exporters\ProductExporter;
use App\Products\Services\ProductParserService;
use App\Products\Services\ProductService;
use App\Products\Validator\ImportProductsValidator;
use Validator;
use Illuminate\Http\Request;
use App\Products\Models\Product;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\DB;
use App\Categoties\Models\Category;
use App\Attributes\Models\Attribute;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use App\Products\Models\ProductVariation;

class ProductsController extends Controller {

    private $_categories_ids = [];
    private $_categories = [];

    protected $parserService;
    protected $service;

    public function __construct(ProductService $service, ProductParserService $parserService) {
        $this->service = $service;
        $this->parserService = $parserService;
    }

    /**
     * @SWG\Post(
     *     path="/api/v1/products",
     *     tags={"product"},
     *          @SWG\Parameter(
     *          name="filter",
     *          type="object",
     *          in="body",
     *          @SWG\Schema(
     *              @SWG\Property(
     *                  property="q",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="sort",
     *                  type="object"
     *              ),
     *              @SWG\Property(
     *                  property="attributes",
     *                  type="object"
     *              ),
     *              @SWG\Property(
     *                  property="personal",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="price",
     *                  type="object"
     *              ),
     *              @SWG\Property(
     *                  property="minimum_ordery",
     *                  type="integer"
     *              ),
     *              @SWG\Property(
     *                  property="request_sample",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="quantity_available",
     *                  type="integer"
     *              ),
     *              @SWG\Property(
     *                  property="categories",
     *                  type="ARRAY"
     *              ),
     *              @SWG\Property(
     *                  property="is_draft",
     *                  type="boolean"
     *              ),
     *          )
     *     ),
     *     @SWG\Response(
     *          response="200",
     *          description="Products list",
     *          @SWG\Schema(
     *              @SWG\Property(
     *                  property="products",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Product"),
     *              ),
     *              @SWG\Property(
     *                  property="attributes",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Attributes"),
     *              ),
     *              @SWG\Property(
     *                  property="categories",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Category"),
     *              ),
     *          )
     *     )
     * )
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function productsList(Request $request) {
        return response()->json(
            $this->service->getFilteredProductsList($request)
        );
    }

    public function getPaginatedProductsList(Request $request) {
        return response()->json(
            $this->service
                ->filterProducts($request->input('filter', []))
                ->paginate($request->input('per_page', 10))
        );
    }

    /**
     * @SWG\Put(
     *     path="/api/v1/products",
     *     tags={"product"},
     *          @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          @SWG\Schema(
     *              ref="#/definitions/newProduct"
     *          )
     *     ),
     *     @SWG\Response(
     *          response="201",
     *          description="Product create",
     *          @SWG\Schema(ref="#/definitions/Product")
     *     ),
     *     security={{
     *       "Bearer":{}
     *     }}
     * )
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request) {
        return response()->json(
            $this->service->createProduct($request->input()), 201
        );
    }

    /**
     * @SWG\Get(
     *     path="/api/v1/products/{product_id}",
     *     tags={"product"},
     *     operationId="view",
     *     description="View product",
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="View product",
     *          @SWG\Schema(ref="#/definitions/Product")
     *     ),
     *     security={{"Bearer":{}}}
     * )
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function view(int $id) {
        return response()->json(
            $this->service->getRepository()->show($id)
        );
    }

    /**
     * Bulk setting
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function productsBulkSet(Request $request) {
        return response()->json(
            $this->service->bulkSetValueForProducts($request->input('products', []), $request)
        );
    }

    public function import(Request $request) {
        $request->validate([
            'file' => 'required',
            'skip_first_row' => 'required',
            'company_id' => 'required|numeric',
            // export_info
            'fields' => 'required|array',
//            'export_info_id' => 'numeric',
//            'place_of_origin' => 'required',
//            'location_country' => 'required',
//            'location_city' => 'required|numeric',
            'negotiate' => 'required'
        ]);

        ImportProductsValidator::validateFileUpload($request->file('file'));

        return response()->json(
            $this->parserService->parseFile($request->file('file'), $request->input())
        );
    }

    public function importFirstRow(Request $request) {
        ImportProductsValidator::validateFileUpload($request->file('file'));

        return response()->json(
            $this->parserService->getFirstRow($request->file('file'))
        );
    }

    public function export(Request $request, string $type) {
        return (new ProductExporter(
            $this->service->filterProducts(array_merge($request->input('filter', []), [
                'personal' => true
            ])),
            $request->input('sample', false) === 'true')
        )->download("products-" . date("YmdHis") . ".{$type}");
    }

    /**
     * @SWG\Post(
     *     path="/api/v1/products/{product_id}",
     *     tags={"product"},
     *     operationId="update",
     *     description="Update product",
     *          @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          @SWG\Schema(
     *              ref="#/definitions/newProduct"
     *          )
     *     ),
     *     @SWG\Response(
     *          response="200",
     *          description="Product create",
     *          @SWG\Schema(ref="#/definitions/Product")
     *     ),
     *     security={{
     *       "Bearer":{}
     *     }}
     * )
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(int $id, Request $request) {
        return response()->json(
            $this->service->updateProduct($id, $request->input())
        );
    }

    public function deleteProductsList(Request $request) {
        $this->service->deleteProducts($request->input('products'));

        return response()->json([], 204);
    }

    /**
     * @SWG\Delete(
     *     path="/api/v1/products/{product_id}",
     *     tags={"product"},
     *     operationId="delete",
     *     summary="Delete product",
     *     description="",
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=204,
     *         description="Delete product"
     *     ),
     *     security={{"Bearer":{}}}
     * )
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id) {
        return response()->json(
            $this->service->getRepository()->delete($id), 204
        );
    }

    /**
     * @SWG\Get(
     *     path="/api/v1/companies/{company_id}/products",
     *     tags={"company"},
     *     @SWG\Response(
     *          response="200",
     *          description="Get products by company id",
     *          @SWG\Schema(
     *              @SWG\Property(
     *                  property="products",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Product"),
     *              ),
     *          )
     *     )
     * )
     *
     * @param int $companyId
     * @return \Illuminate\Http\JsonResponse
     */
    public function companyProducts(int $companyId) {
        return response()->json(
            $this->service->getRepository()->getCompanyProducts($companyId)
        );
    }

    /**
     * @SWG\Put(
     *     path="/api/v1/products/{product_id}/bookmarks",
     *     tags={"product"},
     *     operationId="delete",
     *     description="Add product to bookmarks",
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=201,
     *         description="Add product to bookmarks"
     *     ),
     *     security={{"Bearer":{}}}
     * )
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function addToBookmarks(int $id) {
        $this->service->addProductToUserBookmarks(
            JWTAuth::parseToken()->toUser(),
            $this->service->getRepository()->show($id)
        );

        return response()->json([], 201);
    }

    /**
     * @SWG\Delete(
     *     path="/api/v1/products/{product_id}/bookmarks",
     *     tags={"product"},
     *     operationId="delete",
     *     description="Delete product from bookmarks",
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=201,
     *         description="Delete product from bookmarks"
     *     ),
     *     security={{"Bearer":{}}}
     * )
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteFromBookmarks(int $id) {
        $this->service->removeProductFromUserBookmarks(
            JWTAuth::parseToken()->toUser(),
            $this->service->getRepository()->show($id)
        );

        return response()->json([], 204);
    }

    public function bulkEditProductsList(Request $request) {
        return response()->json(
            $this->service->bulkEditProducts($request->input('products', []))
        );
    }

    // Some shit at the bottom. Maybe it will be removed in future. By somebody else..

//    public function elasticSet() {
//        App::$registry->elastic
//            ->setIndex('products')
//            ->deleteIndex()
//            ->createIndex(require_once __DIR__ . '/Config/elastic-mapping.conf.php');
//
//        (new ProductsCollection(App::$registry))->reindex();
//    }

    /**
     * Get products by category id
     *
     * @param int $product_id
     * @return ProductsController
     */
    public function recommendationByCategory(int $product_id) {
        $categories = Cache::get('product_model_categories');
        if (empty($categories)) {
            $categories = DB::select(DB::raw("SELECT id, parent_id FROM categories WHERE id IN (SELECT category_id FROM product_categories WHERE product_id = {$product_id})"));
            $categories = array_map(function ($value) {
                return ((array)$value);
            }, $categories);
            Cache::put('product_model_categories', $categories, 3600);
            $categories = Cache::get('product_model_categories');
        }

        $parent_ids = array_map(function ($value) {
            return ((array)$value)['parent_id'];
        }, $categories);


        foreach ($categories as $key => $category) {
            if (!in_array($category['id'], $parent_ids)) {
                $ids[] = $category['id'];
            }
        }


        $product = Product::find($product_id);
        $q = $product->name;
        $attributes = json_decode($product['attributes']['attributes']);


        $clear_name = preg_replace('/\s+/u', ' ', preg_replace('/[^\p{L}\p{N}]/u', ' ', str_replace("'", "", $q)));
        $name_to_array = explode(' ', $clear_name);


        foreach ($name_to_array as $name) {
            $search_name = strtolower($name);
            $this->where(function ($query) use ($search_name) {
                $query->whereTranslationInsensitiveLike('name', "%test%");
            });
        }

        return $this;
    }


    /**
     * Generate sort products array with new and top rating (3 new then 3 top rating, etc.)
     *
     * @param $offset
     * @return array
     */
    private function _getSortList($offset) {
        $products = [];
        $products_rating = $this->topRatingProducts($offset);
        $new_products = $this->newProducts($offset);
        $paid_order_products = $this->paidOrderProducts($offset);

        for ($k = 0; $k <= 15; $k += 1) {
            $products = array_merge(
                $products,
                array_slice($paid_order_products, $k, 1),
                array_slice($products_rating, $k, 1),
                array_slice($new_products, $k, 1)
            );
        }

        return [
            'products' => $this->my_array_unique($products),
            'attributes' => $this->_productAttributes($this->my_array_unique($products)),
            'categories' => $this->_categories
        ];
    }

    /**
     * Get 30 top rating products
     *
     * @param $offset
     * @return mixed
     */
    private function topRatingProducts($offset) {
        return Product::
        orderByDesc('rating')
            ->where('parent_id', null)
            ->offset($offset)
            ->limit(30)
            ->with(['company', 'creator', 'currency', 'categories'])
            ->get()->toArray();
    }

    /**
     * Get 30 new products
     *
     * @param $offset
     * @return mixed
     */
    private function newProducts($offset) {
        return Product::
        orderByDesc('created_date')
            ->where('parent_id', null)
            ->offset($offset)
            ->limit(30)
            ->with(['company', 'creator', 'currency', 'categories'])
            ->get()->toArray();
    }

    /**
     * Get 30 new products
     *
     * @param $offset
     * @return mixed
     */
    private function paidOrderProducts($offset) {
        return Product::
        orderByDesc('paid_order')
            ->where('parent_id', null)
            ->offset($offset)
            ->limit(30)
            ->with(['company', 'creator', 'currency', 'categories'])
            ->get()->toArray();
    }

    function my_array_unique($array) {
        $keys = [];

        $length = count($array);

        for ($i = 0; $i < $length; $i++) {

            if (!in_array($array[$i]['id'], $keys)) {
                $keys[] = $array[$i]['id'];
            } else {
                unset($array[$i]);
            }
        }

        return array_slice($array, 0, 30);
    }

    /**
     * Get all unique products attributes
     *
     * @param null $filter_data
     * @return array
     */
    private function _productAttributes($filter_data = null) {
        $products_attributes = [];
        $products = [];
        $new_products = [];
        $ids = [];
        $products_ids = [];
        $query_categories = [];

        if (!empty($filter_data['q'])) {
            $q = mb_strtolower($filter_data['q']);
            $products = DB::select(DB::raw('SELECT product_id, attributes_list FROM product_translations WHERE LOWER(name) LIKE \'%' . $q . '%\''));
            foreach ($products as $product) {
                $new_products[] = (array)$product;
            }
            $products = $new_products;

            if (!empty($products)) {
                $products_ids = array_column($products, 'product_id');
                $attributes = array_reduce(array_map(function ($product) {
                    return array_column(json_decode($product['attributes_list'], true), 'attribute_id');
                }, $products), 'array_merge', []);
                $ids = array_unique(array_merge($ids, $attributes));
            }
            $products_attributes = Attribute::with(['attribute_options'])->whereIn('id', $ids)->get()->toArray();
//            $query_categories = Cache::get('query-category');

            if (!empty($products_ids)) {
                $query_categories = DB::select(DB::raw('SELECT category_id FROM product_categories WHERE product_id IN (' . implode(', ', $products_ids) . ')'));
                $query_categories = array_map(function ($value) {
                    return (array)$value;
                }, $query_categories);

//                Cache::put('query-category', $query_categories, 3600);
                $this->_categories_ids = array_column($query_categories, 'category_id');
            }
        }

        if (!empty($filter_data['categories'])) {
            $products_attributes = Attribute::with(['attribute_options'])->whereIn('id', function ($query) use ($filter_data) {
                $query->select('attribute_id')->from('category_attributes')->whereIn('category_id', $filter_data['categories']);
            })->get()->toArray();
            $this->_categories_ids = array_merge($filter_data['categories'], $this->_categories_ids);
        }

        if (empty($filter_data['categories']) && empty($filter_data['q'])) {
            $products_attributes = Attribute::with(['attribute_options'])->get();
        }

        if (!empty($this->_categories_ids)) {
            $this->_categories = Category::getList($this->_categories_ids);
        }

        return $products_attributes;
    }

    public function getProductTableFields() {
        $products = DB::connection()->getSchemaBuilder()->getColumnListing("products");
        $products_trans = DB::connection()->getSchemaBuilder()->getColumnListing("product_translations");

        return response()->json(array_unique(array_merge($products, $products_trans)));
    }

    public function deleteProductVariation(int $variation_id) {
        return response()->json(ProductVariation::findOrFail($variation_id)->delete(), 204);
    }
}
