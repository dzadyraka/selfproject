<?php

namespace App\Http\Controllers\Faq;


use App\Faq\Models\Faq;
use App\Faq\Models\FaqCategory;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FaqController extends Controller {

	public function create(Request $request) {
	    $faq = Faq::create($request->post());
	    $faq->sections()->sync($request->input()['en']['sections']);
		return response()->mjson($faq->fresh(['sections']), 201);
	}

	public function delete(int $id, Request $request) {
		Faq::findOrFail($id)->delete();

		return response()->json(null, 204);
	}

	public function edit(int $id, Request $request) {
		$faq = Faq::findOrFail($id);

		$faq->update($request->post());

        $faq->sections()->sync($request->input()['en']['sections']);

		return response()->mjson($faq->fresh('sections'), 201);
	}

	public function get(int $id, Request $request) {
		return response()->mjson(Faq::with('sections')->findOrFail($id));
	}

	public function getList(Request $request) {
		$faqs = Faq::with('sections');

		if ($request->post('is_published')) {
			$faqs->where('is_published', '=', $request->post('is_published') == 'true' ? 1 : 0);
		} else {
			$faqs->where('is_published', '=', true);
		}

		if ($request->post('is_featured')) {
			$faqs->where('is_featured', '=', true);
		}

		if ($request->post('q')) {
			$q = strtolower($request->post('q'));
			$faqs->where(function ($query) use ($q) {
				$query->whereTranslationInsensitiveLike('name', "%$q%")
					->orWhereTranslationInsensitiveLike('description', "%$q%");
			});
		}

		$faqs->orderByDesc('created_date');

		if ($request->post('section_id')) {
			$faqs->whereHas('sections', function ($q) use ($request) {
				$q->where('id', (int)$request->post('section_id'));
			});
		}

		return response()->mjson($faqs->get());
	}

	public function getRecommendations(int $id, Request $request) {
		$faqs = Faq::with('sections')
			->where('id', '<>', $id)
			->where('is_published', '=', true)
			->orderByDesc('created_date');

		$faqs->limit($request->get('limit', 3));

		if ($request->get('section_id')) {
			$faqs->whereHas('sections', function ($q) use ($request) {
				$q->where('id', (int)$request->post('section_id'));
			});
		}

		return response()->mjson($faqs->get());
	}

	public function getNext(int $id, Request $request) {
		$prev = Faq::with('sections')
			->where('id', '<', $id)
			->where('is_published', '=', true)
			->orderByDesc('created_date');

		if ($request->get('section_id')) {
			$prev->whereHas('sections', function ($q) use ($request) {
				$q->where('id', (int)$request->post('section_id'));
			});
		}

		$next = Faq::with('sections')
			->where('id', '>', $id)
			->where('is_published', '=', true)
			->orderByDesc('created_date');

		if ($request->get('section_id')) {
			$next->whereHas('sections', function ($q) use ($request) {
				$q->where('id', (int)$request->post('section_id'));
			});
		}

		return response()->mjson([
			'previous' => $prev->first(),
			'next' => $next->first()
		]);
	}

	public function createSection(Request $request) {
		return response()->mjson(FaqCategory::create($request->post()), 201);
	}

	public function deleteSection(int $id, Request $request) {
		FaqCategory::findOrFail($id)->delete();

		return response()->json(null, 204);
	}

	public function updateSection(int $id, Request $request) {
		$category = FaqCategory::findOrFail($id);
		$category->update($request->post());

		return response()->mjson($category, 201);
	}

	public function getSection(int $id, Request $request) {
		return response()->mjson(FaqCategory::findOrFail($id));
	}

	public function getSectionsList(Request $request) {
		$categories = FaqCategory::query();

		if ($request->post('is_active')) {
			$categories->where('is_active', '=', $request->post('is_active'));
		} else if ($request->post('all')) {
			$categories->where('is_active', '=', true);
		}

		if ($request->post('q')) {
			$q = strtolower($request->post('q'));
			$categories->where(function ($query) use ($q) {
				$query->whereTranslationInsensitiveLike('name', "%$q%");
			});
		}

		$categories->orderBy('position');

		return response()->mjson($categories->get());
	}
}