<?php

namespace App\Http\Controllers\Users;

use JWTAuth;
use Validator;
use App\Users\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Social\Events\SocialValidateEvent;
use App\Social\Events\SocialRegisterEvent;
use Rossjcooper\LaravelHubSpot\Facades\HubSpot;

class UsersController {
    public function register(Request $request) {
        if ($request->get('_embed') == 'social') {
            event(new SocialValidateEvent($request));
        }

        $user = User::create($request->post());

        if ($request->get('_embed') == 'social') {
            event(new SocialRegisterEvent($request, $user));
        }

        return response()->mjson($user->get(), 201);
    }

    /**
     * @SWG\Get(
     *     path="/api/v1/users",
     *     tags={"user"},
     *     @SWG\Parameter(
     *         name="limit",
     *         type="integer",
     *         in="path",
     *         description="Limit of users on response."
     *     ),
     *     @SWG\Parameter(
     *         name="offset",
     *         type="integer",
     *         in="path",
     *         description="Offset (used for limit)."
     *     ),
     *     @SWG\Parameter(
     *         name="is_blocked",
     *         type="boolean",
     *         in="path",
     *         description="Boolean (true or false) to show blocked users or not."
     *     ),
     *     @SWG\Parameter(
     *         name="is_approved",
     *         type="boolean",
     *         in="path",
     *         description="Boolean (true or false) to show approved users or not."
     *     ),
     *     @SWG\Parameter(
     *         name="q",
     *         type="string",
     *         in="path",
     *         description="String search keyword."
     *     ),
     *     @SWG\Response(
     *          response="200",
     *          description="Get users list",
     *          @SWG\Schema(
     *              @SWG\Items(ref="#/definitions/User")
     *          )
     *     )
     * )
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getList(Request $request) {
        $users = User::with([
            'companies' => function ($companies) {
                return $companies->with(['country', 'city', 'region']);
            }
        ]);

        if (!empty($request->input('is_blocked'))) {
            $users->where('is_blocked', '=', $request->input('is_blocked'));
        }

        if (!empty($request->input('is_approved'))) {
            $users->where('is_approved', '=', $request->input('is_approved'));
        }

        if (!empty($request->input('q'))) {
            $q = strtolower($request->post('q'));
            $users->where(function ($query) use ($q) {
                $query->whereTranslationInsensitiveLike('first_name', "%$q%")
                    ->orWhereTranslationInsensitiveLike('last_name', "%$q%");
            })->orWhere('email', 'like', "%$q%")
                ->orWhereHas('companies', function ($companies) use ($q) {
                    return $companies->where(function ($query) use ($q) {
                        $query->whereTranslationInsensitiveLike('name', "%$q%");
                    });
                });
        }

        if (!empty($request->input('company_id'))) {
            $id = $request->input('company_id');
            $users->whereHas('companies', function ($companies) use ($id) {
                return $companies->where('id', $id);
            });
        }

        return response()->mjson(
            $users->get()
        );
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function get($id) {
        return response()->mjson(
            User::findOrfail($id)
        );
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id) {
        return response()->mjson(
            User::findOrFail($id)->delete(),
            204
        );
    }

    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit($id, Request $request) {
        $user = User::findOrFail($id);
        $user->update($request->input());
        return response()->mjson(
            $user->fresh()
        );
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getFavoriteTenders($id) {
        return response()->json(
            User::findOrFail($id)->userFavoriteTenders()->get()
        );
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getBookmarksProducts($id) {
        return response()->json(
            User::findOrFail($id)->userFavoriteProducts()->get()
        );
    }

    /**
     * @SWG\Post(
     *     path="/api/v1/users/{user_id}/orders",
     *     tags={"user"},
     *     operationId="orderList",
     *     summary="Get user orders list",
     *     description="",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *          name="filter",
     *          type="object",
     *          in="body",
     *          @SWG\Schema(
     *              @SWG\Property(
     *                  property="offset",
     *                  type="integer"
     *              ),
     *              @SWG\Property(
     *                  property="limit",
     *                  type="integer"
     *              ),
     *          )
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Get user orders list",
     *          @SWG\Schema(
     *              type="array",
     *              @SWG\Items(),
     *          )
     *     ),
     *     security={{"Bearer":{}}}
     * )
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */

    //TODO: add this to items ref="#/definitions/Order"
    public function ordersList($id) {
        $orders = [];
        return response()->json(
            $orders
        );
    }

    public function changePassword($id, Request $request) {
        $user = User::findOrFail($id);
        $password_fields = $request->only('password', 'confirm');

        $validator = Validator::make($password_fields, ['password' => 'required', 'confirm' => 'required|same:password']);

        if ($validator->fails()) {
            return response()->json(['success' => false, 'errors' => $validator->messages()], 400);
        }

        if($user->id != JWTAuth::parseToken()->toUser()->id) {
            return response()->json(['success' => false, 'errors' => 'You can not update a third-party password.'], 400);
        }

        if ($user->update(['password' => Hash::make($request->input('password'))])) {
            return response()->json(['success' => true, 'message' => "You have password change success."]);
        }
    }
}