<?php

namespace App\Http\Controllers\KnowledgeBase;


use App\Http\Controllers\Controller;
use App\KnowledgeBase\Models\KnowledgeBaseArticle;
use App\KnowledgeBase\Models\KnowledgeBaseCategory;
use App\KnowledgeBase\Models\KnowledgeBaseForWhom;
use App\KnowledgeBase\Models\KnowledgeBaseSubCategory;
use Illuminate\Http\Request;

class KnowledgeBaseController extends Controller {
	public function createArticle(Request $request) {
	    $base = KnowledgeBaseArticle::create($request->input());

        $base->sub_sections()->sync($request->input()['en']['sub_sections']);

		return response()->mjson($base->fresh(['sections', 'sub_sections', 'for_whom']), 201);
	}

	public function deleteArticle(int $id, Request $request) {
		KnowledgeBaseArticle::findOrFail($id)->delete();

		return response()->json(null, 204);
	}

	public function updateArticle(int $id, Request $request) {
		$post = KnowledgeBaseArticle::findOrFail($id);
		$post->update($request->post());

        $post->sub_sections()->sync($request->input()['en']['sub_sections']);

		return response()->mjson($post->fresh(['sections', 'sub_sections', 'for_whom']), 201);
	}

	public function getArticle(int $id, Request $request) {
		return response()->mjson(KnowledgeBaseArticle::with('sections', 'sub_sections', 'for_whom')->findOrFail($id));
	}

	public function getArticlesList(Request $request) {
		$posts = KnowledgeBaseArticle::with('sections', 'sub_sections', 'for_whom');

		if ($request->input('is_published')) {
			$posts->where('is_published', '=', $request->input('is_published') == 'true' ? 1 : 0);
		} else {
			$posts->where('is_published', '=', true);
		}

		if ($request->input('is_featured')) {
			$posts->where('is_featured', '=', true);
		}

		if ($request->input('q')) {
			$q = strtolower($request->input('q'));
			$posts->where(function ($query) use ($q) {
				$query->whereTranslationInsensitiveLike('name', "%$q%")
					->orWhereTranslationInsensitiveLike('description', "%$q%")
					->orWhereTranslationInsensitiveLike('content', "%$q%");
			});
		}

		$posts->orderByDesc('created_date');

		if ($request->input('section_id')) {
			$posts->whereHas('sections', function ($q) use ($request) {
				$q->where('id', (int)$request->input('section_id'));
			});
		}

		if ($request->input('sub_section_id')) {
			$posts->whereHas('sub_sections', function ($q) use ($request) {
				$q->where('id', (int)$request->input('sub_section_id'));
			});
		}

		if ($request->input('for_whom_id')) {
			$posts->whereHas('for_whom', function ($q) use ($request) {
				$q->where('id', (int)$request->input('for_whom_id'));
			});
		}

		if (!empty($request->input('post_type')) && $request->input('post_type')) {
			$posts->wherePostType((int)$request->input('post_type'));
		}

		return response()->mjson($posts->get());
	}

	public function getArticleRecommendations(int $id, Request $request) {
		$posts = KnowledgeBaseArticle::with('sections', 'sub_sections', 'for_whom')
			->where('id', '<>', $id)
			->where('is_published', '=', true)
			->orderByDesc('created_date');

		$posts->limit($request->get('limit', 3));

		if ($request->get('section_id')) {
			$posts->whereHas('sections', function ($q) use ($request) {
				$q->where('id', (int)$request->post('section_id'));
			});
		}

		if ($request->post('sub_section_id')) {
			$posts->whereHas('sub_sections', function ($q) use ($request) {
				$q->where('id', (int)$request->post('sub_section_id'));
			});
		}

		if ($request->post('for_whom_id')) {
			$posts->whereHas('for_whom', function ($q) use ($request) {
				$q->where('id', (int)$request->post('for_whom_id'));
			});
		}

		return response()->mjson($posts->get());
	}

	public function getNextArticle(int $id, Request $request) {
		$prev = KnowledgeBaseArticle::with('sections', 'sub_sections', 'for_whom')
			->where('id', '<', $id)
			->where('is_published', '=', true)
			->orderByDesc('created_date');

		if ($request->get('section_id')) {
			$prev->whereHas('sections', function ($q) use ($request) {
				$q->where('id', (int)$request->get('section_id'));
			});
		}

		if ($request->get('sub_section_id')) {
			$prev->whereHas('sub_sections', function ($q) use ($request) {
				$q->where('id', (int)$request->get('sub_section_id'));
			});
		}

		$next = KnowledgeBaseArticle::with('sections', 'sub_sections', 'for_whom')
			->where('id', '>', $id)
			->where('is_published', '=', true)
			->orderByDesc('created_date');

		if ($request->get('section_id')) {
			$next->whereHas('sections', function ($q) use ($request) {
				$q->where('id', (int)$request->get('section_id'));
			});
		}

		if ($request->get('sub_section_id')) {
			$next->whereHas('sub_sections', function ($q) use ($request) {
				$q->where('id', (int)$request->get('sub_section_id'));
			});
		}

		return response()->mjson([
			'previous' => $prev->first(),
			'next' => $next->first()
		]);
	}

	public function createSection(Request $request) {
		return response()->mjson(KnowledgeBaseCategory::create($request->post()), 201);
	}

	public function deleteSection(Request $request, int $section) {
		KnowledgeBaseCategory::findOrFail($section)->delete();

		return response()->json(null, 204);
	}

	public function updateSection(Request $request, int $section) {
		$category = KnowledgeBaseCategory::findOrFail($section);
		$category->update($request->post());

		return response()->mjson($category, 201);
	}

	public function getSection(Request $request, int $section) {
		return response()->mjson(KnowledgeBaseCategory::findOrFail($section));
	}

	public function getSectionsList(Request $request) {
		$categories = KnowledgeBaseCategory::query();

		if ($request->post('is_active')) {
			$categories->where('is_active', '=', $request->post('is_active'));
		} else if ($request->post('all')) {
			$categories->where('is_active', '=', true);
		}

		if ($request->post('q')) {
			$q = strtolower($request->post('q'));
			$categories->where(function ($query) use ($q) {
				$query->whereTranslationLike('LOWER(name)', "%$q%");
			});
		}

		$categories->orderBy('position');

		return response()->mjson($categories->get());
	}

	public function createSubSection(Request $request, int $section) {
		return response()->mjson(KnowledgeBaseSubCategory::create($request->post()), 201);
	}

	public function deleteSubSection(Request $request, int $section, int $id) {
		KnowledgeBaseSubCategory::findOrFail($id)->delete();

		return response()->json(null, 204);
	}

	public function updateSubSection(Request $request, int $section, int $id) {
		$category = KnowledgeBaseSubCategory::findOrFail($id);
		$category->update($request->post());

		return response()->mjson($category, 201);
	}

	public function getSubSection(Request $request, int $section, int $id) {
		return response()->mjson(KnowledgeBaseSubCategory::findOrFail($id));
	}

	public function getSubSectionsList(Request $request, int $section) {
		$categories = KnowledgeBaseSubCategory::whereHas('section', function ($q) use ($section) {
			$q->where('id', $section);
		});

		if ($request->post('is_active')) {
			$categories->where('is_active', '=', $request->post('is_active'));
		} else if ($request->post('all')) {
			$categories->where('is_active', '=', true);
		}

		if ($request->post('q')) {
			$q = strtolower($request->post('q'));
			$categories->where(function ($query) use ($q) {
				$query->whereTranslationLike('LOWER(name)', "%$q%");
			});
		}

		$categories->orderBy('position');

		return response()->mjson($categories->get());
	}

	public function createForWhom(Request $request, int $section) {
		return response()->mjson(KnowledgeBaseForWhom::create($request->post()), 201);
	}

	public function deleteForWhom(Request $request, int $section, int $id) {
		KnowledgeBaseForWhom::findOrFail($id)->delete();

		return response()->json(null, 204);
	}

	public function updateForWhom(Request $request, int $section, int $id) {
		$category = KnowledgeBaseForWhom::findOrFail($id);
		$category->update($request->post());

		return response()->mjson($category, 201);
	}

	public function getForWhom(Request $request, int $section, int $id) {
		return response()->mjson(KnowledgeBaseForWhom::findOrFail($id));
	}

	public function getForWhomsList(Request $request, int $section) {
		$categories = KnowledgeBaseForWhom::whereHas('section', function ($q) use ($section) {
			$q->where('id', $section);
		});

		if ($request->post('is_active')) {
			$categories->where('is_active', '=', $request->post('is_active'));
		} else if ($request->post('all')) {
			$categories->where('is_active', '=', true);
		}

		if ($request->post('q')) {
			$q = strtolower($request->post('q'));
			$categories->where(function ($query) use ($q) {
				$query->whereTranslationLike('LOWER(name)', "%$q%");
			});
		}

		$categories->orderBy('position');

		return response()->mjson($categories->get());
	}
}