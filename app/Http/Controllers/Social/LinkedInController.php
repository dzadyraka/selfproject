<?php

namespace App\Http\Controllers\Social;


use App\Http\Controllers\Controller;
use App\Social\Services\LinkedInService;
use Illuminate\Http\Request;

class LinkedInController extends Controller {

    private $service;

    public function __construct(LinkedInService $service) {
        $this->service = $service;
    }

    public function getOAuthUrl(Request $request) {
        return response()->json([
            'url' => $this->service->getOAuthUrl()
        ]);
    }

    public function oAuthCallback(Request $request) {
        if ($request->has('error')) {
            return view('linkedin', [
                'error' => $request->input('error')
            ]);
        }

        $token = $this->service->getAccessToken($request->input('code'));

        if (!empty($token['error'])) {
            return view('linkedin', [
                'error' => $token['error']
            ]);
        }

        return view('linkedin', [
            'success' => true
        ]);
//        dd($this->service->getProfileInfo($token['access_token']));
    }
}