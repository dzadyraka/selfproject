<?php

namespace App\Http\Controllers\Social;

use Validator;
use GuzzleHttp\Client;
use App\Users\Models\User;
use Illuminate\Http\Request;
use PulkitJalan\GeoIP\GeoIP;
use App\Locations\Models\Country;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Controllers\Controller;
use App\Social\Models\SocialAccount;
use App\Social\Validators\UserValidator;

class SocialController extends Controller {
    /**
     * Add new social auth
     * @param string $integration
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addIntegration(string $integration, Request $request) {
        ini_set('memory_limit', '4096M');
        $request->merge(['integration' => $integration]);
        $data = [];

        UserValidator::validate($integration, $request->post());

        $request->merge(['integration_user_id' => $request->input('user_id')]);
        $field_user_id = $integration . '_id';

        $geo_ip = new GeoIP();
        $geo_ip->setIp($request->ip());
        $geo_country = $geo_ip->getCountry();

        $q = strtolower($geo_country);
        $country = Country::where(function ($query) use ($q) {
            $query->whereTranslationInsensitiveLike('name', "%$q%");
        })->first();

        $language_id = !empty($request->input('language_id')) ? $request->input('language_id') : 3;

        $request->merge([$field_user_id => $request->input('user_id'), 'country_id' => $country->id, 'language_id' => $language_id]);

        DB::transaction(function () use ($request, &$data) {
            $user = User::where('email', $request->input('email'))->first();
            if (!empty($user)) {
                $user->update($request->input());
                $request->merge(['is_new' => false]);
            } else {
                $user = User::create($request->input());
                $request->merge(['is_new' => true]);
            }

            $request->merge(['user_id' => $user->id]);

            $data = SocialAccount::updateOrCreate(
                $request->only('integration_user_id', 'user_id', 'integration'),
                ['access_token' => $request->input('access_token')]
            );

            $data['is_new'] = $request->input('is_new');
        });
        return response()->json($data, 201);
    }

    /**
     * Remove integration credentials from db
     * @param string $integration
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function removeIntegration(string $integration, Request $request) {
        SocialAccount::whereIntegration($integration)
            ->delete();

        return response()->json(null, 204);
    }

    public function login(Request $request, string $integration) {
        $validation = Validator::make($request->only('user_id', 'access_token'), [
            'user_id' => 'required',
            'access_token' => 'required'
        ]);

        if ($validation->fails()) {
            abort(401, $validation->errors());
        }

        UserValidator::validate($integration, $request->all());

        $user = SocialAccount::where('integration', $integration)
            ->where('integration_user_id', $request->post('user_id'))
            ->firstOrFail();

        $auth = JWTAuth::fromUser($user->user);

        // all good so return the token
        return response()->json([
            'access_token' => $auth,
            'user_id' => $user->user_id,
            'expires_in' => 60 * 60 * 24 * 3,
            'token_type' => 'bearer',
            'user' => $user->user
        ], 201);
    }

    public function googleUserInfo(Request $request) {
        $client = new Client(['base_uri' => 'https://www.googleapis.com']);
        $response = $client->get('/oauth2/v1/userinfo?alt=json&access_token=' . $request->input('access_token'));
        return response()->json(json_decode($response->getBody()->getContents(), true));
    }
}