<?php

namespace App\Http\Controllers\Orders\Negotiates;

use JWTAuth;
use App\Users\Models\User;
use App\Orders\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Orders\Validator\OrdersValidators;
use App\Orders\Models\OrderNegotiationOffer;
use App\Orders\Models\OrderNegotiationOfferProduct;

class NegotiatesController extends Controller {

    /**
     * @SWG\Get(
     *     path="/api/v1/orders/{order_id}/negotiates",
     *     tags={"order-negotiates"},
     *     operationId="listNegotiates",
     *     description="Get negotiates list to order",
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Get negotiates list to order",
     *          @SWG\Schema(
     *              type="array",
     *              @SWG\Items(ref="#/definitions/Negotiates"),
     *          )
     *     ),
     *     security={{"Bearer":{}}}
     * )
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function listNegotiates(int $id) {
        return response()->json(OrderNegotiationOffer::with(
            [
                'products' => function ($products) {
                    $products->with('product');
                },
                'company',
                'proposer'
            ]
        )->where('order_id', $id)->orderBy('created_date')->get());
    }

    /**
     * @SWG\Put(
     *     path="/api/v1/orders/{order_id}/negotiates",
     *     tags={"order-negotiates"},
     *     operationId="create",
     *     description="Create negotiates",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *          in="body",
     *          name="body",
     *          required=true,
     *          @SWG\Schema(
     *              ref="#/definitions/newNegotiates"
     *          )
     *     ),
     *     @SWG\Response(
     *         response=201,
     *         description="Create negotiates",
     *          @SWG\Schema(ref="#/definitions/Negotiates")
     *     ),
     *     security={{"Bearer":{}}}
     * )
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(int $id, Request $request) {
        $negotiate = DB::transaction(function () use ($id, $request) {
            $order = Order::findOrFail($id);
            $check_companies = JWTAuth::parseToken()->toUser()->companies()->whereIn('id', [$order->company_id, $order->customer_company_id])->count();

            if (!$check_companies) {
                return response()->json(['success' => false, 'error' => 'Access denied. You not have permission to create this negotiate.'], 400);
            }

//            if ($order->order_status != 'negotiation') {
//                return response()->json(['success' => false, 'error' => 'Your order not negotiation stage.']);
//            }

            $request->merge(['order_id' => $order->id, 'company_id' => $order->company_id]);

            $negotiate = OrderNegotiationOffer::create($request->input());

            $products = !empty($request->input('products')) ? $request->input('products') : [];
            foreach ($products as $product) {
                OrdersValidators::checkProduct($product['product_id'], $product['quantity'], true);

                OrderNegotiationOfferProduct::create([
                    'order_negotiation_offer_id' => $negotiate->id,
                    'product_id' => $product['product_id'],
                    'price' => $product['price'],
                    'quantity' => $product['quantity'],
                    'subtotal' => $product['price'] * $product['quantity'],
                ]);
            }

            return $negotiate->fresh('products', 'company', 'proposer');
        });

        return response()->json($negotiate, 201);
    }

    public function approveNegotiate($id, $negotiate_id) {
        $order = Order::findOrFail($id);
        $check_companies = JWTAuth::parseToken()->toUser()->companies()->whereIn('id', [$order['company_id'], $order['customer_company_id']])->count();

        if (!$check_companies) {
            return response()->json(['success' => false, 'error' => 'Access denied. You not have permission to approve this negotiate.'], 400);
        }

        $negotiate = OrderNegotiationOffer::with('products')->findOrFail($negotiate_id);

        if (JWTAuth::parseToken()->toUser()->companies()->where('id', $negotiate['company_id'])->count() || $negotiate['user_id'] == JWTAuth::parseToken()->toUser()->id) {
            return response()->json(['success' => false, 'error' => 'Your negotiation type not have permission to approve this negotiate.'], 400);
        }

        if ($order->order_status != 'negotiation') {
            return response()->json(['success' => false, 'error' => 'Your negotiation is approved.'], 400);
        }

        DB::transaction(function () use ($order, $negotiate) {
            $negotiate->update(['is_accepted' => true]);
            $price = [
                'subtotal' => 0,
                'total' => 0
            ];

            foreach ($negotiate->products->toArray() as $product_negotiate) {
                $order->products()->where('product_id', $product_negotiate['product_id'])->updateExistingPivot($product_negotiate['product_id'], [
                    'quantity' => $product_negotiate['quantity'],
                    'price' => $product_negotiate['price'],
                    'subtotal' => $product_negotiate['price'] * $product_negotiate['quantity']
                ]);

                $price = [
                    'subtotal' => $price['subtotal'] + $product_negotiate['price'] * $product_negotiate['quantity'],
                    'total' => $price['total'] + $product_negotiate['price'] * $product_negotiate['quantity']
                ];
            }

            $order->update(array_merge($price, ['order_status' => 'processing']));
        });

        return response()->json(['success' => true, 'error' => 'Success negotiate approve.']);
    }
}
