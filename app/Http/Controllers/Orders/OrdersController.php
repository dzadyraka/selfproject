<?php

namespace App\Http\Controllers\Orders;

use JWTAuth;
use Validator;
use App\Users\Models\User;
use App\Orders\Models\Order;
use Illuminate\Http\Request;
use App\Products\Models\Product;
use App\Companies\Models\Company;
use Illuminate\Support\Facades\DB;
use App\Orders\Models\OrderProduct;
use App\Orders\Models\OrderRequest;
use App\Http\Controllers\Controller;
use App\Orders\Validator\OrdersValidators;

class OrdersController extends Controller {

    public function create(Request $request) {
        $data = [];

        if (empty($request->input('product_id'))) {
            return response()->json(['success' => false, 'errors' => ['Empty product id.']], 404);
        }

        $data['negotiate'] = $this->_checkProduct($request->input('product_id'));

        $order = DB::transaction(function () use ($request) {
            $order = Order::create($request->input());

            $request->merge(['order_id' => $order->id]);

            if ($this->_checkCustomer($request->input('product_id'))) {
                $order->customers()->sync([JWTAuth::parseToken()->toUser()->id]);
            }
            if (!empty($request->input('customers'))) {
                $order->customers()->sync($request->input('customers'));
            }

            $product_model = OrdersValidators::checkProduct($request->input('product_id'), $request->input('quantity'));

            $quantity = $request->input('quantity');
            $price = $product_model['price'];

            if (!empty($product_model['bulk_prices'])) {
                foreach ($product_model['bulk_prices'] as $bulk_price) {
                    if ($bulk_price['quantity'] <= $quantity) {
                        $price = $bulk_price['price'];
                    }
                }
            }

            if (!empty($data['subtotal'])) {
                $data['subtotal'] = $data['subtotal'] + $price * $request->input('quantity');
            } else {
                $data['subtotal'] = $price * $request->input('quantity');
            }

            OrderProduct::create([
                'order_id' => $order->id,
                'product_id' => $request->input('product_id'),
                'quantity' => $request->input('quantity'),
                'subtotal' => $price * $request->input('quantity'),
                'price' => $price,
            ]);

            return $order->fresh(['products']);
        });
        return response()->json(['order' => $order], 201);
    }

    public function orderList(Request $request, string $slug = null) {
        $orders = Order::whereHas('company', function ($company) {
            return $company->whereHas('users', function ($users) {
                return $users->where('user_id', JWTAuth::parseToken()->toUser()->id);
            });
        })->orWhereHas('companyCreator', function ($company) {
            return $company->whereHas('users', function ($users) {
                return $users->where('user_id', JWTAuth::parseToken()->toUser()->id);
            });
        })->with([
            'company' => function ($company) {
                return $company->with([
                    'country', 'city', 'region', //'companies_addresses'
                ]);
            },
            'creator'
        ]);

        if (!empty($request->input('id'))) {
            $orders->where('id', $request->input('id'));
        }

        if (!empty($request->input('order_status'))) {
            $orders->where('order_status', $request->input('order_status'));
        }

        if (!empty($request->input('q'))) {
            $query = strtolower($request->input('q'));
            $orders->whereHas('products', function ($products) use ($query) {
                return $products->where(function ($q) use ($query) {
                    $q->whereTranslationInsensitiveLike('name', "%$query%");
                });
            });
        }
        if (!empty($request->input('stage'))) {
            switch ($request->input('stage')) {
                case 'drafts':
                    $orders->where('is_draft', '=', true);
                    break;
                case 'completed':
                    $orders->where('is_completed', '=', true);
                    break;
                case 'cancelled':
                    $orders->where('is_cancelled', '=', true);
                    break;
            }
        }
        if (!empty($slug) && in_array($slug, ['seller', 'buyer'])) {
            if ($slug == 'seller') {
                $orders->whereHas('companyCreator', function ($companyCreator) {
                    return $companyCreator->with(['users' => function ($users) {
                        return $users->where('id', JWTAuth::parseToken()->toUser()->id);
                    }]);
                });
            } else {
                $orders->where('creator_id', JWTAuth::parseToken()->toUser()->id);
            }
        }
        if (!empty($request->input())) {
        }

        return response()->json($orders->paginate($request->input('per_page')));
    }

    public function stats() {
        $orders = new Order();
        return response()->json(
            [
                'drafts' => $orders->where('is_draft', true)->count(),
                'completed' => $orders->where('is_completed', true)->count(),
                'cancelled' => $orders->where('is_cancelled', true)->count(),
            ]
        );
    }

    public function customers() {
        return response()->json(
            User::whereHas('createdOrders', function ($orders) {
                return $orders->whereHas('companyCreator', function ($creator) {
                    return $creator->whereHas('users', function ($companies) {
                        return $companies->where('user_id', JWTAuth::parseToken()->toUser()->id);
                    });
                });
            })->get()
        );
    }

    public function update(int $id, Request $request) {

        $order = DB::transaction(function () use ($request, $id) {
            $order = Order::findOrFail($id);
            $order->products()->detach();

            if (!empty($request->input('products'))) {
                foreach ($request->input('products') as $product) {
                    OrdersValidators::validateProducts($product);

                    $product_model = OrdersValidators::checkProduct($product['product_id'], $product['quantity']);
                    $quantity = $product['quantity'];
                    $price = $product_model['price'];

                    if (!empty($product_model['bulk_prices'])) {
                        foreach ($product_model['bulk_prices'] as $bulk_price) {
                            if ($bulk_price['quantity'] <= $quantity) {
                                $price = $bulk_price['price'];
                            }
                        }
                    }

                    if (!empty($data['subtotal'])) {
                        $data['subtotal'] = $data['subtotal'] + $price * $product['quantity'];
                    } else {
                        $data['subtotal'] = $price * $product['quantity'];
                    }

                    OrderProduct::create([
                        'order_id' => $order->id,
                        'product_id' => $product['product_id'],
                        'quantity' => $product['quantity'],
                        'subtotal' => $price * $product['quantity'],
                        'price' => $price,
                    ]);
                }
            }
            $request->merge(['subtotal' => $data['subtotal'], 'total' => $data['subtotal']]);
            $order->update($request->input());

            if (!empty($request->input('customers'))) {
                $order->customers()->sync($request->input('customers'));
            }
            if ($request->input('request_user_id')) {
                OrderRequest::create([
                    'order_id' => $id,
                    'user_id' => $request->input('request_user_id')
                ]);
            }

            return $order->fresh(['products', 'creator', 'company', 'customers']);
        });
        return response()->json(
            $order
        );
    }

    public function view(int $id) {
        return response()->json(
            Order::with(
                [
                    'products' => function ($products) {
                        return $products->with('currency');
                    },
                    'creator',
                    'company' => function ($company) {
                        return $company->with([
                            'country', 'city', 'region', //'companies_addresses'
                        ]);
                    },
                    'customers' => function ($customer) {
                        return $customer->with(['companies']);
                    }
                ]
            )->findOrFail($id)
        );
    }

    public function addNewProduct(int $id, Request $request) {
        $data = [];

        $data['negotiate'] = $this->_checkProduct($request->input('product_id'));

        OrderProduct::create($request->input());
        $order = Order::findOrFail($id);
        $order->update($request->input());

        $data['order'] = $order->fresh(
            [
                'products' => function ($products) {
                    return $products->with(['product' => function ($product) {
                        return $product->with('company', 'creator', 'currency');
                    }]);
                },
            ]
        );

        return response()->json($data, 201);
    }

    public function editOrderProduct(int $id, int $order_product_id, Request $request) {
        $data = [];
        $data['negotiate'] = $this->_checkProduct($request->input('product_id'));

        $order_product = OrderProduct::findOrFail($order_product_id);

        $order_product->update([
            'quantity' => $request->input('quantity'),
            'price' => $request->input('price'),
            'subtotal' => $request->input('price') * $request->input('quantity'),
        ])->save();

        $data['order'] = Order::with(
            [
                'products' => function ($products) {
                    return $products->with(['product' => function ($product) {
                        return $product->with('company', 'creator', 'currency');
                    }]);
                },
            ]
        )->findOrFail($id);

        return response()->json($data);
    }

    public function deleteOrderProduct(int $id, int $order_product_id) {
        return response()->json(OrderProduct::findOrFail($order_product_id)->delete(), 204);
    }

    /**
     * Check if product has negotiate action
     *
     * @param $product_id
     * @return array
     */
    private function _checkProduct($product_id) {
        return Product::findOrFail($product_id, ['negotiate']);
    }

    /**
     * @param $product_id
     * @return bool
     */
    private function _checkCustomer(int $product_id) {
        $product_company = Product::findOrFail($product_id, ['company_id']);
        $companiesCollection = Company::whereHas(
            'users',
            function ($users) {
                return $users->where('id', JWTAuth::parseToken()->toUser()->id);
            }
        )->get();

        foreach ($companiesCollection as $ids) {
            if ($ids['id'] == $product_company['company_id']) {
                return false;
            }
        }

        return true;
    }

    /**
     *
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function createOrderByCompany(int $id, Request $request) {
        $data = [];
        Company::findOrFail($id);

        $order = null;

        $products = OrdersValidators::validateProductsArray($request->input('products'));

        OrdersValidators::validateAddress($request->input('billing_address'));
        OrdersValidators::validateAddress($request->input('shipping_address'));
        OrdersValidators::validateShippingDetails($request->input('shipping_details'));
        OrdersValidators::validateOrderInputs($request->input());

        DB::transaction(function () use ($products, $request, &$order) {

            $order = Order::create($request->input());

            foreach ($products as $product) {
                OrdersValidators::validateProducts($product);

                $product_model = OrdersValidators::checkProduct($product['product_id'], $product['quantity']);
                $quantity = $product['quantity'];
                $price = $product_model['price'];

                if (!empty($product_model['bulk_prices'])) {
                    foreach ($product_model['bulk_prices'] as $bulk_price) {
                        if ($bulk_price['quantity'] <= $quantity) {
                            $price = $bulk_price['price'];
                        }
                    }
                }

                if (!empty($data['subtotal'])) {
                    $data['subtotal'] = $data['subtotal'] + $price * $product['quantity'];
                } else {
                    $data['subtotal'] = $price * $product['quantity'];
                }

                OrderProduct::create([
                    'order_id' => $order->id,
                    'product_id' => $product['product_id'],
                    'quantity' => $product['quantity'],
                    'subtotal' => $price * $product['quantity'],
                    'price' => $price,
                ]);
            }
            $order->update(['subtotal' => $data['subtotal'], 'total' => $data['subtotal']]);

            if (!empty($request->input('customers'))) {
                $order->customers()->sync($request->input('customers'));
            }

            return $order->fresh(['products', 'creator', 'company', 'customers']);
        });

        return response()->json($order, 201);
    }


}
