<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

/**
 * @SWG\Info(title="Tenders documentation", version="2.0")
 */

class Controller extends BaseController {
	use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
