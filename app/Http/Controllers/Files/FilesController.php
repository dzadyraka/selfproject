<?php

namespace App\Http\Controllers\Files;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;


/**
 * @SWG\Definition(
 *   definition="File",
 *   type="object",
 *   allOf={
 *       @SWG\Schema(
 *           @SWG\Property(property="name", type="string"),
 *           @SWG\Property(property="url", type="string"),
 *           @SWG\Property(property="type", type="string"),
 *       )
 *   }
 * )
 */
class FilesController extends Controller {

    /**
     * @SWG\Post(
     *     path="/api/v1/files/upload",
     *     tags={"files"},
     *     operationId="upload",
     *     description="Upload file or files",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="Use for single file to upload",
     *         in="formData",
     *         name="file",
     *         type="file"
     *     ),
     *     @SWG\Parameter(
     *         description="Or if can use multi upload files",
     *         in="formData",
     *         name="files",
     *         type="file"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Get when single file upload and if multi upload we get array objects",
     *          @SWG\Schema(
     *              description="Get when single file upload",
     *              ref="#/definitions/File"
     *          ),
     *     ),
     *     security={{"Bearer":{}}}
     * )
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function upload(Request $request) {
        $file = $request->file('file');
        $files = $request->file('files');
        if ($file && !is_array($file)) {
            return response()->json([$this->_singleUpload($file)], 201);
        } elseif ($files || is_array($file)) {
            if(!empty($file)) $files = $file;

            return response()->json($this->_multiUpload($files), 201);
        }
        return response()->json(['errors' => ['Empty file']], 422);
    }

    public function uploadFiles(Request $request) {
        $file = $request->file('file');
        $files = $request->file('files');
        if ($file && !is_array($file)) {
            return response()->json([$this->_singleUpload($file)], 201);
        } elseif ($files || is_array($file)) {
            if(!empty($file)) $files = $file;

            return response()->json($this->_multiUpload($files), 201);
        }
        return response()->json(['errors' => ['Empty file']], 422);
    }

    private function _singleUpload($file): array {

        $upload_file = Storage::putFile('uploads', $file);

        return [
            'name' => basename($upload_file),
            'url' => '/' . $upload_file,
            'type' => $file->getClientOriginalExtension(),
        ];
    }

    private function _multiUpload($files): array {
        $data = [];
        foreach ($files as $file) {
            $upload_file = Storage::putFile('uploads', $file);
            $data[] = [
                'name' => basename($upload_file),
                'url' => '/' . $upload_file,
                'type' => $file->getClientOriginalExtension(),
            ];
        }

        return $data;
    }
}
