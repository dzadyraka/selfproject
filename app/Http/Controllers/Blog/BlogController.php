<?php

namespace App\Http\Controllers\Blog;


use App\Blog\Models\BlogCategory;
use App\Blog\Models\BlogPost;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BlogController extends Controller {

	public function createPost(Request $request) {
	    $post = BlogPost::create($request->input());

	    $post->sections()->sync($request->input()['en']['sections']);
		return response()->mjson($post->fresh(['sections']), 201);
	}

	public function deletePost(int $id, Request $request) {
		BlogPost::findOrFail($id)->delete();

		return response()->json(null, 204);
	}

	public function updatePost(int $id, Request $request) {
		$post = BlogPost::with('sections')
			->findOrFail($id);
		$post->update($request->post());

        $post->sections()->sync($request->input()['en']['sections']);

		return response()->mjson($post->fresh(['sections']), 201);
	}

	public function getPost(int $id, Request $request) {
		return response()->mjson(BlogPost::with('sections')->findOrFail($id));
	}

	public function getPostsList(Request $request) {
		$posts = BlogPost::with('sections');

		if ($request->post('is_published')) {
			$posts->where('is_published', '=', $request->post('is_published') == 'true' ? 1 : 0);
		} else {
			$posts->where('is_published', '=', true);
		}

		if ($request->post('is_featured')) {
			$posts->where('is_featured', '=', true);
		}

		if ($request->post('q')) {
			$q = strtolower($request->post('q'));
			$posts->where(function ($query) use ($q) {
				$query->whereTranslationInsensitiveLike('name', "%$q%")
					->orWhereTranslationInsensitiveLike('description', "%$q%");
			});
		}

		$posts->orderByDesc('created_date');

		if ($request->post('section_id')) {
			$posts->whereHas('sections', function ($q) use ($request) {
				$q->where('id', (int)$request->post('section_id'));
			});
		}

		return response()->mjson($posts->get());
	}

	public function getPostRecommendations(int $id, Request $request) {
		$posts = BlogPost::with('sections')
			->where('id', '<>', $id)
			->where('is_published', '=', true)
			->orderByDesc('created_date');

		$posts->limit($request->get('limit', 3));

		if ($request->get('section_id')) {
			$posts->whereHas('sections', function ($q) use ($request) {
				$q->where('id', (int)$request->post('section_id'));
			});
		}

		return response()->mjson($posts->get());
	}

	public function createSection(Request $request) {
		return response()->mjson(BlogCategory::create($request->post()), 201);
	}

	public function deleteSection(int $id, Request $request) {
		BlogCategory::findOrFail($id)->delete();

		return response()->json(null, 204);
	}

	public function updateSection(int $id, Request $request) {
		$category = BlogCategory::findOrFail($id);
		$category->update($request->post());

		return response()->mjson($category, 201);
	}

	public function getSection(int $id, Request $request) {
		return response()->mjson(BlogCategory::findOrFail($id));
	}

	public function getSectionsList(Request $request) {
		$categories = BlogCategory::query();

		if ($request->post('is_active')) {
			$categories->where('is_active', '=', $request->post('is_active'));
		} else if ($request->post('all')) {
			$categories->where('is_active', '=', true);
		}

		if ($request->post('q')) {
			$q = strtolower($request->post('q'));
			$categories->where(function ($query) use ($q) {
				$query->whereTranslationInsensitiveLike('name', "%$q%");
			});
		}

		$categories->orderBy('position');

		return response()->mjson($categories->get());
	}
}