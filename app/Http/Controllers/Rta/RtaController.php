<?php

namespace App\Http\Controllers\Rta;

use App\Locations\Models\Country;
use App\Rta\Models\Rta;

use App\Http\Controllers\Controller;
use App\Rta\Models\RtaComtrade;
use Illuminate\Http\Request;

class RtaController extends Controller {
    /**
     * @SWG\Get(
     *     path="/api/v1/rta",
     *     tags={"rta"},
     *     @SWG\Response(
     *          response="200",
     *          description="Get list",
     *          @SWG\Schema(
     *              @SWG\Items(ref="#/definitions/Rta")
     *          )
     *     )
     * )
     * @param Request $request
     * @return object
     */
    public function getList(Request $request) {
        $rta = Rta::with(['countries', 'countries_first_side', 'countries_second_side']);

        $status = $request->input('status');
        $rta->when($status, function ($query, $status) {
            return $query->where('status', $status);
        });

        $countries = $request->input('countries');
        $rta->when($countries, function ($query, $countries) {
            return $query->whereHas('countries', function ($q) use ($countries) {
                return $q->whereIn('country_id', $countries);
            });
        });

        return response()->json($rta->get());
    }

    /**
     * @SWG\Get(
     *     path="/api/v1/rta/{id}",
     *     tags={"rta"},
     *     @SWG\Response(
     *          response="200",
     *          description="Get RTA",
     *          @SWG\Schema(
     *              @SWG\Items(ref="#/definitions/Rta")
     *          )
     *     )
     * )
     * @param int $id
     * @return object
     */
    public function getRta(int $id) {
        return response()->json(Rta::findOrFail($id));
    }

    public function getCountryRta(int $country, int $partner) {
        return response()->json(
            Rta::with('countries', 'countries_first_side', 'countries_second_side')->where(
                function ($query) use ($country, $partner) {
                    $query->whereHas('countries_first_side', function ($query) use ($country) {
                        return $query->where('country_id', $country);
                    })->whereHas('countries_second_side', function ($query) use ($partner) {
                        return $query->where('country_id', $partner);
                    });
                }
            )->orWhere(
                function ($query) use ($country, $partner) {
                    $query->whereHas('countries_second_side', function ($query) use ($country) {
                        return $query->where('country_id', $country);
                    })->whereHas('countries_first_side', function ($query) use ($partner) {
                        return $query->where('country_id', $partner);
                    });
                }
            )->first()
        );
    }
}
