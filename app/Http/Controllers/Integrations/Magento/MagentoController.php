<?php

namespace App\Http\Controllers\Integrations\Magento;

use App\Products\Services\ProductService;
use JWTAuth;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Integrations\Models\Integration;
use App\Http\Controllers\Integrations\Settings\MagentoSettingsController;

class MagentoController extends Controller {

    protected $service;

    public function __construct(ProductService $service) {
        $this->service = $service;
    }

    /**
     * Check if module install
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkModule(Request $request) {

        $url = $request->input('site_url');

        if (strpos($url, 'http://') && strpos($url, 'https://')) {
            $url = 'http://' . $url;
        }

        $client = new Client(['base_uri' => $url]);
        $response = $client->get('/products/index/status');
        $status = $response->getStatusCode();

        return response()->json([],isset($status) == 200 ? $status : 400);
    }

    /**
     * Get list of products
     */
    public function updateProducts() {
        $products_settings = Integration::where('type', 'magento')->get()->toArray();

        foreach ($products_settings as $setting) {
            $url = $setting['site'];

            if (strpos($url, 'http://') && strpos($url, 'https://')) {
                $url = 'http://' . $url;
            }

            $client = new Client(['base_uri' => $url]);
            $response = $client->get('/products/index');
            $xml = simplexml_load_string($response->getBody()->getContents());

            $products_setting = MagentoSettingsController::getInstance($setting['company_id'], $this->service);

            $products_setting->toProduct($xml);
        }

        return response()->json();
    }

    /**
     * Update products
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function parseProducts(Request $request) {
        $products_setting = MagentoSettingsController::getInstance($request->input('company_id'), $this->service);
        $setting = $products_setting->_data;
        $saved_products = [];

        $url = $setting['site'];

        if (strpos($url, 'http://') && strpos($url, 'https://')) {
            $url = 'http://' . $url;
        }

        $client = new Client(['base_uri' => $url]);
        $response = $client->get('/products/index');
        $xml = simplexml_load_string($response->getBody()->getContents());
        $saved_products = $products_setting->toProduct($xml);

        return response()->json($saved_products);
    }


    /**
     * Get fields
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getFields(Request $request) {
        $url = $request->input('site_url');

        if (strpos($url, 'http://') && strpos($url, 'https://')) {
            $url = 'http://' . $url;
        }

        $client = new Client(['base_uri' => $url]);
        $response = $client->get('/products/index/attributes');

        $xml = simplexml_load_string($response->getBody()->getContents());
        $json = json_encode($xml); // convert the XML string to JSON
        $array = json_decode($json,TRUE);

        return response()->json($array);
    }

    public function getFieldsWithIntegration(Request $request) {
        $settings = (new MagentoSettingsController($request->input('integration_name'),  $request->input('company_id')))->getInstance($request->input('company_id'));
        $settings = $settings->_data;
        $url = $settings['site'];

        if (strpos($url, 'http://') && strpos($url, 'https://')) {
            $url = 'http://' . $url;
        }

        $client = new Client(['base_uri' => $url]);
        $response = $client->get('/products/index/attributes');

        $xml = simplexml_load_string($response->getBody()->getContents());
        $json = json_encode($xml); // convert the XML string to JSON
        $array = json_decode($json,TRUE);

        return response()->json($array);
    }
}
