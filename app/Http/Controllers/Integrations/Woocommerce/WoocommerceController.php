<?php

namespace App\Http\Controllers\Integrations\Woocommerce;

use Illuminate\Http\Request;
use Automattic\WooCommerce\Client;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use App\Settings\Models\ProductsSetting;
use App\Products\Services\ProductService;
use Pixelpeter\Woocommerce\Facades\Woocommerce;
use Automattic\WooCommerce\HttpClient\HttpClientException;
use App\Http\Controllers\Integrations\Settings\WoocommerceSettingsController;

class WoocommerceController extends Controller {

    protected $service;

    public function __construct(ProductService $service) {
        $this->service = $service;
    }

    public function getFields(Request $request) {

        $url = $request->input('site_url');
        $consumer_key = $request->input('WOOCOMMERCE_CONSUMER_KEY');
        $consumer_secret = $request->input('WOOCOMMERCE_CONSUMER_SECRET');


        $params = [
            'version' => 'wc/'.config('woocommerce.api_version'),
            'verify_ssl' => config('woocommerce.verify_ssl'),
            'wp_api' => config('woocommerce.wp_api'),
            'query_string_auth' => config('woocommerce.query_string_auth'),
            'timeout' => config('woocommerce.timeout'),
        ];

        $wc =  new Client(
            $url,
            $consumer_key,
            $consumer_secret,
            $params
        );

        $fields = $wc->get('products', [
            'per_page' => 1,
            'page' => 1
        ]);

        $product_fields = [];

        if (!empty($fields)) {
            $product_fields = array_keys($fields[0]);
        }

        return response()->json($product_fields);
    }

    /**
     * Parse products
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function parseProducts(Request $request) {
        $settings = WoocommerceSettingsController::getInstance($request->input('company_id'), $this->service);
        $saved_products = [];
        if (empty($settings->_data['keys'])) {
            return response()->json(['success' => false, 'errors' => 'Api keys empty'], 400);
        }

        $params = [
            'version' => 'wc/'.config('woocommerce.api_version'),
            'verify_ssl' => config('woocommerce.verify_ssl'),
            'wp_api' => config('woocommerce.wp_api'),
            'query_string_auth' => config('woocommerce.query_string_auth'),
            'timeout' => config('woocommerce.timeout'),
        ];

        $keys = $settings->_data['keys'];

        $url = $keys['WOOCOMMERCE_STORE_URL'];
        $consumer_key = $keys['WOOCOMMERCE_CONSUMER_KEY'];
        $consumer_secret = $keys['WOOCOMMERCE_CONSUMER_SECRET'];

        $wc =  new Client(
            $url,
            $consumer_key,
            $consumer_secret,
            $params
        );
        
        $products = $wc->get('products');

        $saved_products = $settings->toProduct($products);

        return response()->json($saved_products, 200);
    }

    /**
     * Get list of products
     */
    public function updateProducts() {
        $saved_products = [];
        $products_settings = ProductsSetting::where('type', 'woocommerce')->get()->toArray();

        $params = [
            'version' => 'wc/'.config('woocommerce.api_version'),
            'verify_ssl' => config('woocommerce.verify_ssl'),
            'wp_api' => config('woocommerce.wp_api'),
            'query_string_auth' => config('woocommerce.query_string_auth'),
            'timeout' => config('woocommerce.timeout'),
        ];

        foreach ($products_settings as $setting) {
            $settings = WoocommerceSettingsController::getInstance($setting['company_id'], $this->service);

            if (empty($settings->_data['keys'])) {
                continue;
            }

            $keys = $settings->_data['keys'];

            $url = $keys['WOOCOMMERCE_STORE_URL'];
            $consumer_key = $keys['WOOCOMMERCE_CONSUMER_KEY'];
            $consumer_secret = $keys['WOOCOMMERCE_CONSUMER_SECRET'];

            $wc =  new Client(
                $url,
                $consumer_key,
                $consumer_secret,
                $params
            );
            $products = $wc->get('products');
            $saved_products = $settings->toProduct($products);
        }

        return response()->json($saved_products);
    }

    public function checkModule(Request $request) {
        $url = $request->input('WOOCOMMERCE_STORE_URL');
        $consumer_key = $request->input('WOOCOMMERCE_CONSUMER_KEY');
        $consumer_secret = $request->input('WOOCOMMERCE_CONSUMER_SECRET');

        $params = [
            'version' => 'wc/'.config('woocommerce.api_version'),
            'verify_ssl' => config('woocommerce.verify_ssl'),
            'wp_api' => config('woocommerce.wp_api'),
            'query_string_auth' => config('woocommerce.query_string_auth'),
            'timeout' => config('woocommerce.timeout'),
        ];

        $wc =  new Client(
            $url,
            $consumer_key,
            $consumer_secret,
                $params
            );


        try {
            $results = $wc->get("products");
        } catch (HttpClientException $e) {
            return response()->json(['success' => false, 'errors' => $e->getMessage()], 400);
        }

        return response()->json(['success' => true]);
    }

}
