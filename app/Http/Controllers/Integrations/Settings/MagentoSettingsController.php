<?php

namespace App\Http\Controllers\Integrations\Settings;

use App\Products\Repositories\ProductRepository;
use App\Products\Services\ProductParserService;
use App\Products\Services\ProductService;
use Illuminate\Http\Request;
use App\Products\Models\Product;
use App\Companies\Models\Company;
use App\Http\Controllers\Products\ProductsController;


class MagentoSettingsController extends BaseSettingsController {

    protected static $instance = null;
    protected $service;

    function __construct($name, $company_id, $service) {
        parent::__construct($name, $company_id);

        $this->service = $service;
    }

    /**
     * Create product object
     *
     * @param $xml
     * @return array
     */
    public function toProduct($xml) {
        $product_object = [];
        $products = [];
        $setting = $this->_data;
        $company_currency = Company::find($setting['company_id'], ['currency_id']);

        foreach ($xml as $product) {
            $product_object['attributes_list'] = [];
            foreach ($setting['fields'] as $key => $field) {
                $array_product = (array)$product;

                if (($pos = strpos($field, ":")) !== false) {
                    $attribute_id = substr($field, $pos + 1);
                    $product_object['attributes_list'][] = [
                        'attribute_id' => $attribute_id,
                        'value' => $array_product[$key]
                    ];
                } else {
                    $product_object[$field] = !empty($array_product[$key]) ? $array_product[$key] : 0;
                }
                $product_object['company_id'] = $setting['company_id'];
            }
            $product_object['integration_name'] = 'magento';
            $product_object['is_draft'] = true;
            $product_object['negotiate'] = false;
            $product_object['currency_id'] = $company_currency['currency_id'];

            if (!empty($product_object['images'])) {
                foreach ($product_object['images'] as $image) {
                    $url = $image['src'];
                    $base_name = explode('.', basename($url));
                    $extension = '.'.end($base_name);

                    $contents = file_get_contents($url);
                    $storage_path = config('filesystems.disks.self_storage.root') . '/uploads/';
                    $name = md5(basename($url) . time()) . $extension;
                    file_put_contents($storage_path . $name, $contents);
                    $mime_type = mime_content_type($storage_path . $name);

                    $images[] = [
                        'name' => $name,
                        'url' => '/uploads/' . $name,
                        'type' => $mime_type,
                    ];

                }
                $product_object['images'] = [];
                $product_object['images'] = $images;
            }

            $product = Product::where('external_id', $product_object['external_id'])->where('company_id', $setting['company_id'])->where('integration_name', 'magento')->first();
            $request = new Request();
            $request->merge($product_object);
            if(empty($product)){
                $product_object = json_decode($this->service->createProduct($request->input()), true);
            } else {
                $product_object = json_decode($this->service->updateProduct($product['id'], $request->input()), true);
            }
            $products[] = $product_object;
        }
        return $products;
    }

    /**
     * Get instance
     *
     * @param $company_id
     * @param $service
     * @return MagentoSettings|null
     */
    public static function getInstance($company_id, $service) {

        if(empty(self::$instance)){
            return self::$instance = new self('magento', $company_id, $service);
        }
        return self::$instance;
    }
}
