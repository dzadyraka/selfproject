<?php

namespace App\Http\Controllers\Integrations\Settings;

use App\Http\Controllers\Controller;
use App\Settings\Models\ProductsSetting;

class BaseSettingsController extends Controller {

    protected $_integration_name = '';
    protected $_company_id;
    public $_data;

    /**
     * Set integration name
     *
     * ProductSettings constructor.
     * @param $name
     * @param $company_id
     */
    protected function __construct($name, $company_id) {
        $this->_integration_name = $name;
        $this->_company_id = $company_id;
        $this->_data = ProductsSetting::where('type', $this->_integration_name)->where('company_id', $this->_company_id)->first();
    }

    /**
     * Get setting for company
     *
     * @return mixed
     */
    public function settings() {
        return $this->_data;
    }
}
