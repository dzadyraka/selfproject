<?php

namespace App\Http\Controllers\Integrations\Settings;

use App\AttributeOptions\Models\AttributeOption;
use App\Attributes\Models\Attribute;
use App\Companies\Models\Company;
use App\Http\Controllers\Products\ProductsController;
use App\Products\Models\ProductVariation;
use App\Products\Models\ProductVariationAttribute;
use Faker\Provider\Image;
use Illuminate\Http\Request;
use App\Products\Models\Product;
use Illuminate\Support\Facades\Storage;


class WoocommerceSettingsController extends BaseSettingsController {

    protected static $instance = null;
    protected $service;

    function __construct($name, $company_id, $service) {
        parent::__construct($name, $company_id);

        $this->service = $service;
    }

    /**
     * Create product object
     *
     * @param $xml
     * @return array
     */
    public function toProduct($xml) {
        $product_object = [];
        $products = [];
        $images = [];
        $setting = $this->_data;
        $company_currency = Company::find($setting['company_id'], ['currency_id']);

        foreach ($xml as $product) {
            $product_object['attributes_list'] = [];
            foreach ($setting['fields'] as $key => $field) {
                $array_product = (array)$product;
                $product_object[$field] = !empty($array_product[$key]) ? $array_product[$key] : 0;

                $product_object['company_id'] = $setting['company_id'];
            }
            if (!empty($product_object['attributes_list'])) {
                foreach ($product_object['attributes_list'] as $attribute) {
                    $attribute_name = mb_strtolower($attribute['name']);
                    $attribute_id = Attribute::where(function ($query) use ($attribute_name) {
                        return $query->whereTranslationInsensitiveLike('name', "%$attribute_name%");
                    })->get(['id'])->toArray();

                    if (empty($attribute_id)) {
                        $attribute_id = Attribute::create([
                            'name' => $attribute['name']
                        ])->id;
                    } else {
                        $attribute_id = $attribute_id[0]['id'];
                    }

                    $new_attributes[$attribute_id]['name'] = $attribute_name;
                    $new_attributes[$attribute_id]['attribute_id'] = $attribute_id;

                    if (!empty($attribute['options'])) {
                        foreach ($attribute['options'] as $option) {
                            $att_val_name = mb_strtolower($option);
                            $attribute_value_id = AttributeOption::where(function ($query) use ($att_val_name) {
                                $query->whereTranslationInsensitiveLike('name', "%$att_val_name%");
                            })->get(['id'])->toArray();

                            if (empty($attribute_value_id)) {
                                $attribute_value_id = AttributeOption::create([
                                    'name' => $att_val_name,
                                    'attribute_id' => $attribute_id
                                ])->id;
                            } else {
                                $attribute_value_id = $attribute_value_id[0]['id'];
                            }

                            $new_attributes[$attribute_id]['options_list'][$attribute_value_id]['value'] = $att_val_name;
                            $new_attributes[$attribute_id]['options_list'][$attribute_value_id]['id'] = $attribute_value_id;
                        }
                    }
                    $attribute_options = $new_attributes;
                }
                $product_object['attributes_list'] = $attribute_options;
            }

            $product_object['integration_name'] = 'woocommerce';
            $product_object['is_draft'] = true;
            $product_object['negotiate'] = false;
            $product_object['currency_id'] = $company_currency['currency_id'];
            $product_object['quantity_available'] = !empty($product_object['quantity_available']) ? $product_object['quantity_available'] : 0;
            $product_object['minimum_ordery'] = !empty($product_object['minimum_ordery']) ? $product_object['minimum_ordery'] : 0;

            if (!empty($product_object['images'])) {
                foreach ($product_object['images'] as $image) {
                    $url = $image['src'];

                    $base_name = explode('.', basename($url));
                    $extension = '.' . end($base_name);

                    $contents = file_get_contents($url);
                    $storage_path = config('filesystems.disks.self_storage.root') . '/uploads/';

                    $name = md5(basename($url) . time()) . $extension;
                    file_put_contents($storage_path . $name, $contents);
                    $mime_type = mime_content_type($storage_path . $name);

                    $images[] = [
                        'name' => $name,
                        'url' => '/uploads/' . $name,
                        'type' => $mime_type,
                    ];

                }
                $product_object['images'] = [];
                $product_object['images'] = $images;
            }

            $product_db = Product::where('external_id', $product_object['external_id'])->where('company_id', $setting['company_id'])->where('integration_name', 'woocommerce')->first();

            if (!empty($product['variations'])) {
                $product_id = !empty($product_db['id']) ? $product_db['id'] : null;
                $product_object['variations'] = $this->variationProduct($product['variations'], $setting, $product_id);
            } else {
                unset($product_object['variations']);
            }
            $request = new Request();
            $request->merge($product_object);
            if (empty($product_db)) {
                $product_object = json_decode($this->service->createProduct($product_object), true);
            } else {
                $product_object = json_decode($this->service->updateProduct($product_db['id'], $product_object), true);
            }
            $products[] = $product_object;
        }

        return $products;
    }

    private function variationProduct($variation_list, $setting, $product_id = null) {
        $products = [];
        $product_object = [];
        $attribute_options = [];

        foreach ($variation_list as $product) {
            foreach ($setting['fields'] as $key => $field) {
                $array_product = $product;
                if (!in_array($field, ['name', 'description'])) {
                    if ($key == 'images') {
                        $key = 'image';
                    }
                    $product_object[$field] = $array_product[$key];
                }
                $product_object['company_id'] = $setting['company_id'];

            }
            if (!empty($product_object['attributes_list'])) {
                foreach ($product_object['attributes_list'] as $attribute) {
                    $new_attributes = [];
                    $attribute_name = mb_strtolower($attribute['name']);
                    $attribute_id = Attribute::where(function ($query) use ($attribute_name) {
                        return $query->whereTranslationInsensitiveLike('name', "%$attribute_name%");
                    })->get(['id'])->toArray();

                    if (empty($attribute_id)) {
                        $attribute_id = Attribute::create([
                            'name' => $attribute['name']
                        ])->id;
                    } else {
                        $attribute_id = $attribute_id[0]['id'];
                    }

                    $new_attributes[$attribute_id]['name'] = $attribute_name;
                    $new_attributes[$attribute_id]['attribute_id'] = $attribute_id;

                    if (!empty($attribute['option'])) {

                        $att_val_name = mb_strtolower($attribute['option']);
                        $attribute_value_id = AttributeOption::where(function ($query) use ($att_val_name) {
                            $query->whereTranslationInsensitiveLike('name', "%$att_val_name%");
                        })->get(['id'])->toArray();

                        if (empty($attribute_value_id)) {
                            $attribute_value_id = AttributeOption::create([
                                'name' => $att_val_name,
                                'attribute_id' => $attribute_id
                            ])->id;
                        } else {
                            $attribute_value_id = $attribute_value_id[0]['id'];
                        }

                        $new_attributes[$attribute_id]['options_list'][$attribute_value_id]['value'] = $att_val_name;
                        $new_attributes[$attribute_id]['options_list'][$attribute_value_id]['id'] = $attribute_value_id;
                        $new_attributes[$attribute_id]['id'] = $attribute_value_id;
                    }
                    $product_object['attribute_id'] = $attribute_id;
                    $product_object['attribute_option_id'] = $attribute_value_id;
                    $attribute_options = $new_attributes;
                    $product_object['quantity_available'] = !empty($product_object['quantity_available']) ? $product_object['quantity_available'] : 0;
                    $product_object['minimum_ordery'] = !empty($product_object['minimum_ordery']) ? $product_object['minimum_ordery'] : 0;
                    if (!empty($product_id)) {
                        $product_object['id'] = $this->_checkProductVariation($product_id, $attribute_id, $attribute_value_id);
                    }
                }
                $product_object['attributes_list'] = $attribute_options;
            }

            if (!empty($product_object['images'])) {
                foreach ($product_object['images'] as $image) {
                    $url = $image['src'];
                    $base_name = explode('.', basename($url));
                    $extension = '.' . end($base_name);

                    $contents = file_get_contents($url);
                    $storage_path = config('filesystems.disks.self_storage.root') . '/uploads/';
                    $name = md5(basename($url) . time()) . $extension;
                    file_put_contents($storage_path . $name, $contents);
                    $mime_type = mime_content_type($storage_path . $name);

                    $images[] = [
                        'name' => $name,
                        'url' => '/uploads/' . $name,
                        'type' => $mime_type,
                    ];

                }
                $product_object['images'] = [];
                $product_object['images'] = $images;
            }

            $products[] = $product_object;
        }

        return $products;
    }

    /**
     * Get instance
     *
     * @param $company_id
     * @return WoocommerceSettingsController|null
     */
    public static function getInstance($company_id, $service) {
        if (empty(self::$instance)) {
            return self::$instance = new self('woocommerce', $company_id, $service);
        }
        return self::$instance;
    }

    private function _checkProductVariation($product_id, $attribute_id, $option_id) {
        $variation = ProductVariation::whereHas('attributesList', function($attributes_list) use ($product_id, $attribute_id, $option_id) {
            return $attributes_list->where('product_id', $product_id)->where('attribute_id', $attribute_id)->where('attribute_option_id', $option_id)->limit(1);
        })->first();

        if (!empty($variation)) {
            return $variation->id;
        }
        return null;
    }
}
