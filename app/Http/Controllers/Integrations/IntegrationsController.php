<?php

namespace App\Http\Controllers\Integrations;

use App\Settings\Models\ProductsSetting;
use JWTAuth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Integrations\Models\Integration;
use App\Integrations\Validator\IntegrationValidator;

class IntegrationsController extends Controller {

    /**
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getList() {
        return response()->json(
            ProductsSetting::with(['company'])
                ->whereHas('company', function ($company) {
                    $company->whereHas('users', function ($users) {
                            return $users->where('id', JWTAuth::parseToken()->toUser()->id);
                        });
                })
                ->get()
        );
    }

    /**
     *
     * @param Request $request
     * @return mixed
     * @throws \Exception
     */
    public function create(Request $request) {
        IntegrationValidator::checkIntegration($request->input('type'), $request->input('company_id'), $request->input('site'));

        if($request->input('type') == 'magento') {
            $settings = $request->input('fields');
            $settings['entity_id'] = 'external_id';
            $request->merge(['fields' => $settings]);
        } elseif ($request->input('type') == 'woocommerce') {
            $settings = $request->input('fields');
            $settings['id'] = 'external_id';
            $request->merge(['fields' => $settings]);
        }
        
        $integration = ProductsSetting::create($request->input());

        return response()->json($integration->fresh(), 201);
    }

    public function delete($id) {
        $check_integration = ProductsSetting::with(['company'])
            ->whereHas('company', function ($company) {
                $company->whereHas('users', function ($users) {
                    return $users->where('id', JWTAuth::parseToken()->toUser()->id);
                });
            })
            ->findOrFail($id);
        return response()->json($check_integration->delete(), 204);
    }

    public function view($id) {
        $check_integration = ProductsSetting::with(['company'])
            ->findOrFail($id);
        return response()->json($check_integration);
    }

    public function edit($id, Request $request) {
        $check_integration = ProductsSetting::findOrFail($id);
        $check_integration->update($request->input());
        return response()->json($check_integration->fresh('company'));
    }
}
