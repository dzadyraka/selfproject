<?php

namespace App\Http\Controllers\Certificates;


use App\Certificates\Services\CertificateService;
use App\Certificates\Validators\CertificateValidator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CertificateController extends Controller {
    private $service;
    private $validator;

    public function __construct(CertificateService $certificateService, CertificateValidator $certificateValidator) {
        $this->service = $certificateService;
        $this->validator = $certificateValidator;
    }

    public function getCertificates(Request $request) {
        return response()->json(
            $this->service->getRepository()
                ->getFilteredCertificates($request->all())
        );
    }

    public function retrieveCertificate(int $certificateId, Request $request) {
        return response()->json(
            $this->service->getRepository()->show($certificateId)
        );
    }

    public function getCompanyCertificates(int $companyId, Request $request) {
        return response()->json(
            $this->service->getCompanyCertificates($companyId)
        );
    }

    public function createCertificate(Request $request) {
        return response()->json(
            $this->service->getRepository()->create(
                $this->validate($request, $this->validator->createCertificateRules())
            )
            , Response::HTTP_CREATED
        );
    }

    public function addCompanyCertificate(int $companyId, Request $request) {
        return response()->json(
            $this->service->addCompanyCertificate(
                $companyId, $this->validate($request, $this->validator->addCompanyCertificateRules())
            ), Response::HTTP_CREATED
        );
    }

    public function verifyCompanyCertificate(int $companyId, int $certificateId) {
        return response()->json([
            'success' => $this->service->verifyCompanyCertificate($companyId, $certificateId)
        ], Response::HTTP_OK);
    }

    public function deleteCompanyCertificate(int $companyId, int $certificateId) {
        $this->service->deleteCompanyCertificate($companyId, $certificateId);

        return response()->json([], Response::HTTP_NO_CONTENT);
    }
}