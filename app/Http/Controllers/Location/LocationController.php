<?php

namespace App\Http\Controllers\Location;


use App\Comtrade\Models\Comtrade;
use App\Locations\Services\CityService;
use App\Rta\Models\Rta;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use PulkitJalan\GeoIP\GeoIP;
use Illuminate\Http\Request;
use App\Locations\Models\City;
use App\Locations\Models\Region;
use App\Locations\Models\Country;
use App\Http\Controllers\Controller;

class LocationController extends Controller {

    private $cityService;

    public function __construct(CityService $cityService) {
        $this->cityService = $cityService;
    }

    public function getCountriesList() {
        return response()->mjson(Country::all());
    }

    public function detectUser(Request $request) {
        $geo_ip = new GeoIP();
        $geo_ip->setIp($request->ip());
        $geo_country = $geo_ip->getCountry();

        $q = strtolower($geo_country);
        $country = Country::where(function ($query) use ($q) {
            $query->whereTranslationInsensitiveLike('name', "%$q%");
        })->first();
        return response()->json($country);
    }

    public function getPartnerCountriesList(int $id, Request $request) {
        $countries = [];
        $data = [];
        $country_data = [];

        $rta = Rta::with('countries_first_side', 'countries_second_side');

        $from = $request->input('year_from');
        $to = $request->input('year_to');

        $status = $request->input('fta', false) === 'true' ? true : false;
        $rta->when($status == true, function ($query) {
            return $query->where('status', 'In Force');
        });

        $rta_id = $request->input('rta');
        $rta->when($rta_id, function ($query, $rta_id) {
            return $query->where('id', $rta_id);
        });

        $rta->whereHas('countries', function ($query) use ($id) {
            return $query->where('country_id', $id);
        })->each(function (Rta $rta, $_) use ($id, &$countries) {
            $countries_first = $rta->countries_first_side->pluck('id')->toArray();
            $countries_second = $rta->countries_second_side->pluck('id')->toArray();

            if (in_array($id, $countries_first)) {
                $countries = array_merge($countries, $countries_second);
            } else {
                $countries = array_merge($countries, $countries_first);
            }
        });

        $from_where = !empty($from) ? "AND year >= {$from}" : '';
        $to_where = !empty($to) ? "AND year <= {$to}" : '';

        $has_fta = $countries;

        $totals = DB::table('comtrades')
            ->groupBy('rg_desc')
            ->whereCountryId($id)
            ->when($from, function ($q, $from) {
                return $q->where('year', '>=', $from);
            })
            ->when($to, function ($q, $to) {
                return $q->where('year', '<=', $to);
            })->select(DB::raw('sum(trade_value)'), 'rg_desc')
            ->get()
            ->groupBy('rg_desc')
            ->toArray();

        $value = [
            'import' => isset($totals['Imports']) ? (int)$totals['Imports'][0]->sum : 1,
            'export' => isset($totals['Exports']) ? (int)$totals['Exports'][0]->sum : 1
        ];

        Comtrade::select(
            'rg_desc',
            'partner_id',
            DB::raw('sum(trade_value) as turnover'),
            DB::raw("round(100.0 * SUM(trade_value) / CASE WHEN rg_desc='Imports' THEN {$value['import']}
            ELSE {$value['export']} END, 2) as percent")
        )
            ->where('country_id', '=', $id)
            ->when($from, function ($q, $from) {
                return $q->where('year', '>=', $from);
            })
            ->when($to, function ($q, $to) {
                return $q->where('year', '<=', $to);
            })
            ->groupBy('partner_id')
            ->groupBy('rg_desc')
            ->get()->each(function ($partners) use (&$data) {
                $data[] = $partners->toArray();
            });

        foreach ($data as $datum) {
            if (!$status) {
                $countries[] = $datum['partner_id'];
            }
            $name = strtolower($datum['rg_desc']) . '_percent';
            $country_data[$datum['partner_id']][$name] = $datum['percent'];
        }

        return response()->mjson(
            Country::whereIn('id', $countries)
                ->where('id', '<>', $id)
                ->get()
                ->map(function ($data_countries) use ($countries, $country_data, $has_fta) {
                    $imports_percent = !empty($country_data[$data_countries->id]) ? $country_data[$data_countries->id] : ['imports_percent' => 0];
                    $exports_percent = !empty($country_data[$data_countries->id]) ? $country_data[$data_countries->id] : ['exports_percent' => 0];

                    return array_merge(
                        $data_countries->toArray(),
                        ['has_fta' => in_array($data_countries->id, $has_fta)],
                        $imports_percent,
                        $exports_percent
                    );
                })
        );
    }

    public function getCountryRegionsList(int $id) {
        return response()->json(
            Region::where('country_id', $id)->get()
        );
    }

    public function getCountryCitiesList(int $id, Request $request) {
        return response()->json(
            $this->cityService->getCountryCities($id, $request->input())
        );
    }
//
//    public function getCountryRegionCitiesList(int $id, int $region_id) {
//        return response()->json(
//            City::where('country_id', $id)->where('region_id', $region_id)->get()
//        );
//        App::$response->assign(
//            new CitiesCollection(App::$registry, $args['country_id'], $args['region_id'])
//        );
//    }

    public function getCity(int $id, int $city_id) {
        return response()->json(
            $this->cityService->getRepository()->show($city_id)
        );
    }

    public function getTopTurnover(int $id, Request $request) {
        $from = $request->input('year_from');
        $to = $request->input('year_to');

        $from_where = !empty($from) ? "AND year >= {$from}" : '';

        $to_where = !empty($to) ? "AND year <= {$to}" : '';

        $name_import = "comtrades_import_{$id}_{$from}_{$to}";
//        $comtrades_import = Cache::get($name_import);

//        if (empty($comtrades_import)) {
        Cache::put($name_import, Comtrade::select(
            'rg_desc',
            'partner_id',
            DB::raw('sum(trade_value) as turnover'),
            DB::raw("round(100.0 * SUM(trade_value) / (SELECT SUM(trade_value) FROM comtrades WHERE country_id = {$id} {$from_where} {$to_where} AND rg_desc = 'Imports'), 2) as percent")
        )
            ->where('country_id', $id)
            ->where('rg_desc', 'Imports')
            ->when($from, function ($q, $from) {
                return $q->where('year', '>=', $from);
            })
            ->when($to, function ($q, $to) {
                return $q->where('year', '<=', $to);
            })
            ->groupBy('rg_desc')
            ->groupBy('partner_id')
            ->orderByDesc('turnover')
            ->limit(5)
            ->with(['partner'])
            ->get(), 10080);
        $comtrades_import = Cache::get($name_import);
//        }

        $name_export = "comtrades_export_{$id}_{$from}_{$to}";
//        $comtrades_export = Cache::get($name_export);

//        if (empty($comtrades_export)) {
        Cache::put($name_export, Comtrade::select(
            'rg_desc',
            'partner_id',
            DB::raw('sum(trade_value) as turnover'),
            DB::raw("round(100.0 * SUM(trade_value) / (SELECT SUM(trade_value) FROM comtrades  WHERE country_id = {$id} {$from_where} {$to_where} AND rg_desc = 'Exports'), 2) as percent")
        )
            ->where('country_id', $id)
            ->where('rg_desc', 'Exports')
            ->when($from, function ($q, $from) {
                return $q->where('year', '>=', $from);
            })
            ->when($to, function ($q, $to) {
                return $q->where('year', '<=', $to);
            })
            ->groupBy('rg_desc')
            ->groupBy('partner_id')
            ->orderByDesc('turnover')
            ->limit(5)
            ->with(['partner'])
            ->get(), 10080);
        $comtrades_export = Cache::get($name_export);
//        }

        return response()->json([
            'import' => $comtrades_import,
            'export' => $comtrades_export
        ]);
    }
}