<?php

namespace App\Http\Controllers\Filters;

use JWTAuth;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FiltersController extends Controller {

    /**
     * @SWG\Get(
     *     path="/api/{module_name}/filters",
     *     tags={"filter"},
     *     operationId="getList",
     *     description="Get filter list",
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Get filter list",
     *          @SWG\Schema(
     *              type="array",
     *              @SWG\Items(
     *                  ref="#/definitions/Filter"
     *              ),
     *          )
     *     ),
     *     security={{"Bearer":{}}}
     * )
     *
     * @param string $module_name
     * @return \Illuminate\Http\JsonResponse
     */
    public function getList(string $module_name) {
        return response()->json($this->getModel($module_name)::get());
    }

    /**
     * @SWG\Put(
     *     path="/api/{module_name}/filters",
     *     tags={"filter"},
     *     operationId="create",
     *     description="Create filter",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *          in="body",
     *          name="body",
     *          required=true,
     *          @SWG\Schema(
     *              ref="#/definitions/newFilter"
     *          )
     *     ),
     *     @SWG\Response(
     *         response=201,
     *         description="Create filter",
     *          @SWG\Schema(ref="#/definitions/Filter")
     *     ),
     *     security={{"Bearer":{}}}
     * )
     * @param $module_name
     * @param Request $request
     * @return $this|\Illuminate\Http\JsonResponse
     */
    public function create(string $module_name, Request $request) {

        $validator = Validator::make(
            $request->only('name', 'filter'),
            [
                'name' => 'required',
                'filter' => 'required'
            ]
        );

        if ($validator->fails()) {
            return response()->json(['success' => false, 'errors' => $validator->messages()]);
        }

        return response()->json($this->getModel($module_name)::create($request->only('name', 'filter')), 201);
    }

    /**
     * @SWG\Get(
     *     path="/api/v1/{module_name}/filters/{filter_id}",
     *     tags={"filter"},
     *     operationId="view",
     *     description="Get filter view",
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Get filter view",
     *          @SWG\Schema(
     *              ref="#/definitions/Filter"
     *          )
     *     ),
     *     security={{"Bearer":{}}}
     * )
     *
     * @param $module_name
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function view(string $module_name, int $id) {
        return response()->json($this->getModel($module_name)::find($id));
    }

    /**
     * @SWG\Put(
     *     path="/api/v1/{module_name}/filters/{filter_id}",
     *     tags={"filter"},
     *     operationId="edit",
     *     description="Edit filter",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *          in="body",
     *          name="body",
     *          required=true,
     *          @SWG\Schema(
     *              ref="#/definitions/newFilter"
     *          )
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Edit filter",
     *          @SWG\Schema(ref="#/definitions/Filter")
     *     ),
     *     security={{"Bearer":{}}}
     * )
     *
     * @param $module_name
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit(string $module_name, int $id, Request $request) {

        $validator = Validator::make(
            $request->only('name', 'filter'),
            [
                'name' => 'required',
                'filter' => 'required'
            ]
        );

        if ($validator->fails()) {
            return response()->json(['success' => false, 'errors' => $validator->messages()]);
        }

        $filter = $this->getModel($module_name)::find($id);
        $filter->update($request->only('name', 'filter'));

        return response()->json($filter);
    }

    /**
     * @SWG\Delete(
     *     path="/api/v1/{module_name}/filters/{filter_id}",
     *     tags={"filter"},
     *     operationId="delete",
     *     description="Delete filter",
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=204,
     *         description="Delete filter"
     *     ),
     *     security={{"Bearer":{}}}
     * )
     *
     * @param string $module_name
     * @param int $id
     * @return $this
     */
    public function delete(string $module_name, int $id) {
        return response()->json($this->getModel($module_name)::find($id)->delete(), 204);
    }

    /**
     * Get model namespace
     *
     * @param string $model
     * @return string
     */
    private function getModel(string $model): string {
        return "App\Filters\Models\\". ucwords($model) . "Filter";
    }
}
