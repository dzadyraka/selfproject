<?php

namespace App\Http\Controllers\Language;


use App\Http\Controllers\Controller;
use App\Language\Models\Language;
use Illuminate\Http\Request;

class LanguageController extends Controller {
	public function getInfoByCode(string $code, Request $request) {
		return response()->mjson(Language::whereCode($code)->firstOrFail());
	}

	public function getInfoById(int $id, Request $request) {
		return response()->mjson(Language::findOrFail($id));
	}

	public function getList(Request $request) {
		return response()->mjson(Language::get());
	}
}