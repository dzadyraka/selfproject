<?php

namespace App\Http\Controllers\Classifiers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Classifiers\Models\ClassifiersHs;
use App\Classifiers\Models\ClassifiersCpv;
use App\Classifiers\Models\ClassifiersIsic;
use App\Classifiers\Models\ClassifiersUspsc;

class ClassifiersController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllFirstLevel() {
        return response()->mjson([
            'cpv' => (new ClassifiersCpv())->firstLevel(),
            'hs' => (new ClassifiersHs())->firstLevel(),
            'isic' => (new ClassifiersIsic())->firstLevel(),
            'uspsc' => (new ClassifiersUspsc())->firstLevel()
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function search(Request $request) {
        $grouped['cpv'] = ClassifiersCpv::search($request->input('q'))->toArray();
        $grouped['uspsc'] = ClassifiersUspsc::search($request->input('q'))->toArray();
        $grouped['hs'] = ClassifiersHs::search($request->input('q'))->toArray();
        $grouped['isic'] = ClassifiersIsic::search($request->input('q'))->toArray();
        return response()->mjson(
            $this->_groupData($grouped)
        );
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getById(Request $request) {
        $data = [];
        if(!empty($request->input('uspsc'))){
            $data['uspsc'] = (new ClassifiersUspsc())->byId($request->input('uspsc'));
        }
        if(!empty($request->input('hs'))){
            $data['hs'] = (new ClassifiersHs())->byId($request->input('hs'));
        }
        if(!empty($request->input('isic'))){
            $data['isic'] = (new ClassifiersIsic())->byId($request->input('isic'));
        }
        if(!empty($request->input('cpv'))){
            $data['cpv'] = (new ClassifiersCpv())->byId($request->input('cpv'));
        }

        return response()->mjson($data);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function reindex() {
        ClassifiersHs::deleteIndex();
        set_time_limit(0);
        $hs = new ClassifiersHs();
        $cpv = new ClassifiersCpv();
        $isic = new ClassifiersIsic();
        $uspsc = new ClassifiersUspsc();

        $hs = $hs->reindex();
        $cpv = $cpv->reindex();
        $isic = $isic->reindex();
        $uspsc = $uspsc->reindex();

        return response()->json([
            'hs' => $hs,
            'cpv' => $cpv,
            'isic' => $isic,
            'uspsc' => $uspsc
        ]);
    }

    private function _groupData($grouped) {
        $data = [];

        foreach ($grouped as $key => $items) {
            foreach ($items as $item) {
                if (empty($data[$key][$item['id']])) {
                    $item['children'] = [ $item['children'] ];
                    $data[$key][$item['id']] = $item;
                } else {
                    $this->rec($data[$key][$item['id']]['children'], $item['children']);
                }
            }
        }

        foreach ($data as $key => &$items) {

            foreach ($items as &$item) {
                $this->normalize($item);
            }

            $data[$key] = array_values($items);
        }

        return $data;
    }

    protected function normalize(&$tree) {
        if (empty($tree['children'])) {
            return ;
        }

        if (!empty($tree['children']['id'])) {
            $tree['children'] = [ $tree['children'] ];
        }

        foreach ($tree['children'] as &$child) {
            $this->normalize($child);
        }
    }

    protected function rec(array &$parent, $item) {
        foreach ($parent as $key => $p_item) {
            if ($p_item['id'] == $item['id']) {
                if(!empty($parent[$key]['children']['id'])) {
                    $parent[$key]['children'] = [ $parent[$key]['children'] ];
                }
                $this->rec($parent[$key]['children'], $item['children']);
                return ;
            }
        }

        $parent[] = $item;
    }
}
