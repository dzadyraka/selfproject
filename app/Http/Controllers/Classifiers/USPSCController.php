<?php

namespace App\Http\Controllers\Classifiers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Classifiers\Models\ClassifiersUspsc;

class USPSCController extends Controller
{
    /**
     * @SWG\Get(
     *     path="/api/classifiers/uspsc",
     *     tags={"classifiers"},
     *     operationId="getList",
     *     description="Get uspsc list",
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Get uspsc list",
     *          @SWG\Schema(
     *              type="array",
     *              @SWG\Items(
     *                  ref="#/definitions/ClassifiersUspsc"
     *              ),
     *          )
     *     ),
     *     security={{"Bearer":{}}}
     * )
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getList() {
        return response()->json(
            (new ClassifiersUspsc())->hierarchy()
        );
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getChildrenByParentId($id) {
        return response()->mjson(
            (new ClassifiersUspsc())->children($id)
        );
    }
}
