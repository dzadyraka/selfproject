<?php

namespace App\Http\Controllers\Classifiers;

use Validator;
use Illuminate\Http\Request;
use App\Comtrade\Models\Comtrade;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use App\Classifiers\Models\ClassifiersHs;

class HSController extends Controller
{
    /**
     * @SWG\Get(
     *     path="/api/classifiers/hs",
     *     tags={"classifiers"},
     *     operationId="getList",
     *     description="Get hs list",
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Get cpv list",
     *          @SWG\Schema(
     *              type="array",
     *              @SWG\Items(
     *                  ref="#/definitions/ClassifiersCpv"
     *              ),
     *          )
     *     ),
     *     security={{"Bearer":{}}}
     * )
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getList() {
        set_time_limit(0);
        return response()->json(
            (new ClassifiersHs())->hierarchy()
        );
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getChildrenByParentId($id) {
        return response()->mjson(
            (new ClassifiersHs())->children($id)
        );
    }

    /**
     * Get first level
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getFistLevel() {
        return response()->mjson(
            (new ClassifiersHs())->firstLevel()
        );
    }

    /**
     * Search by name
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function searchByName(Request $request) {
        $q = strtolower($request->input('q'));
        return response()->mjson(
            (new ClassifiersHs())->where(function ($query) use ($q) {
                $query->whereTranslationInsensitiveLike('name', "%$q%");
            })->get()
        );
    }

    public function getExportImportList($type_name, Request $request) {
        $type = $type_name == 'export' ? 'Exports' : 'Imports';
        $from = $request->input('year_from');
        $to = $request->input('year_to');
        $country_id = $request->input('country_id');
        $partner_id = $request->input('partner_id');
        $rules = ['country_id' => 'required', 'partner_id' => 'required'];
        $validator = Validator::make($request->only('country_id', 'partner_id'), $rules);

        if ($validator->fails()) {
            return response()->json(['success' => false, 'errors' => $validator->messages()], 404);
        }
        $hs_ids = [];
        $hs_percent = [];

        $from_where = !empty($from) ? "AND year >= {$from}" : '';
        $to_where = !empty($to) ? "AND year <= {$to}" : '';

        $name_export = "hs_{$type_name}_{$country_id}_{$partner_id}_{$from}_{$to}";
//        $hs_export = Cache::get($name_export);

//        if (empty($hs_export)) {
//            Comtrade::select(
//                'classifiers_hs_id',
//                DB::raw("round(100.0 * SUM(trade_value) / (SELECT SUM(trade_value) FROM comtrades WHERE country_id = {$country_id} AND partner_id = {$partner_id} {$from_where} {$to_where} AND rg_desc = '{$type}'), 2) as percent")
//            )
//                ->where('rg_desc', $type)
//                ->when($country_id, function ($q, $country_id) {
//                    return $q->where('country_id', $country_id);
//                })
//                ->when($partner_id, function ($q, $partner_id) {
//                    return $q->where('partner_id', $partner_id);
//                })
//                ->when($from, function ($q, $from) {
//                    return $q->where('year', '>=', $from);
//                })
//                ->when($to, function ($q, $to) {
//                    return $q->where('year', '<=', $to);
//                })
//                ->groupBy('classifiers_hs_id')
//                ->get()->map(function ($hs) use (&$hs_ids, &$hs_percent) {
//                    $hs_ids[] = $hs->classifiers_hs_id;
//                    $hs_percent[$hs->classifiers_hs_id]['percent'] = $hs->percent;
//                });
//
//            Cache::put($name_export, $hs_ids, 10080);
//            $hs_export = Cache::get($name_export);
//        }

        $name_export_list = "hs_{$type_name}_list_{$country_id}_{$partner_id}_{$from}_{$to}";
//        $hs_export_list = Cache::get($name_export_list);

//        if(empty($hs_export_list)){
//            $hs_export_list = ClassifiersHs::whereIn('id', $hs_export)->whereLevel(1)->get()->map(function ($hs) use ($hs_percent) {
//                $percent = !empty($hs_percent[$hs->id])? $hs_percent[$hs->id]: ['percent' => 0];
//                return array_merge(['id' => $hs->id, 'name' => $hs->name], $percent);
//            });
//            Cache::put($name_export_list, $hs_export_list, 10080);
//            $hs_export_list = Cache::get($name_export_list);
//        }

        $hs_export_select = ClassifiersHs::getParentValues($country_id, $partner_id, $type, $from_where, $to_where);

        $hs_export_list = ClassifiersHs::whereIn('code', array_column($hs_export_select, 'code'))->orderBy('id')
            ->get()->map(function ($hs) use ($hs_export_select) {
                $key = array_search($hs->code, array_column($hs_export_select, 'code'));
                $total_sum =  array_sum(array_column($hs_export_select, 'value'));

                return [
                    'id' => $hs->id,
                    'name' => $hs->name,
                    'code' => $hs_export_select[$key]->code,
                    'value' => number_format(((int)$hs_export_select[$key]->value / $total_sum) * 100, 2, '.', ''),
                    'has_child' => $hs->has_child,
                    'parent_id' => $hs->parent_id,
                    'level' => $hs->level
                ];
            });

        return response()->json($hs_export_list);
    }

    public function getImportExportParentList(int $parent_id, $type_name, Request $request) {
        $type = $type_name == 'export' ? 'Exports' : 'Imports';
        $from = $request->input('year_from');
        $to = $request->input('year_to');
        $country_id = $request->input('country_id');
        $partner_id = $request->input('partner_id');
        $rules = ['country_id' => 'required', 'partner_id' => 'required'];
        $validator = Validator::make($request->only('country_id', 'partner_id'), $rules);

        if ($validator->fails()) {
            return response()->json(['success' => false, 'errors' => $validator->messages()], 404);
        }
        $hs_ids = [];
        $hs_percent = [];

        $from_where = !empty($from) ? "AND year >= {$from}" : '';
        $to_where = !empty($to) ? "AND year <= {$to}" : '';

        $name_export = "hs_{$type_name}_{$country_id}_{$partner_id}_{$from}_{$to}";
//        $hs_export = Cache::get($name_export);

//        if (empty($hs_export)) {
            Comtrade::select(
                'classifiers_hs_id',
                DB::raw("round(100.0 * SUM(trade_value) / (SELECT SUM(trade_value) FROM comtrades WHERE country_id = {$country_id} AND partner_id = {$partner_id} {$from_where} {$to_where} AND rg_desc = '{$type}'), 2) as percent")
            )
                ->where('rg_desc', $type)
                ->when($country_id, function ($q, $country_id) {
                    return $q->where('country_id', $country_id);
                })
                ->when($partner_id, function ($q, $partner_id) {
                    return $q->where('partner_id', $partner_id);
                })
                ->when($from, function ($q, $from) {
                    return $q->where('year', '>=', $from);
                })
                ->when($to, function ($q, $to) {
                    return $q->where('year', '<=', $to);
                })
                ->groupBy('classifiers_hs_id')
                ->get()->map(function ($hs) use (&$hs_ids, &$hs_percent) {
                    $hs_ids[] = $hs->classifiers_hs_id;
                    $hs_percent[$hs->classifiers_hs_id]['value'] = $hs->percent;
                });

            Cache::put($name_export, $hs_ids, 10080);
            $hs_export = Cache::get($name_export);
//        }

        $name_export_list = "hs_{$type_name}_list_{$country_id}_{$partner_id}_{$from}_{$to}";
//        $hs_export_list = Cache::get($name_export_list);

//        if(empty($hs_export_list)){
            $hs_export_list = ClassifiersHs::where('parent_id', $parent_id)->get()->map(function ($hs) use ($hs_percent) {
                $percent = !empty($hs_percent[$hs->id])? $hs_percent[$hs->id]: ['value' => 0];
                return array_merge([
                    'id' => $hs->id,
                    'name' => $hs->name,
                    'has_child' => $hs->has_child,
                    'parent_id' => $hs->parent_id,
                    'level' => $hs->level
                ], $percent);
            });
//            Cache::put($name_export_list, $hs_export_list, 10080);
//            $hs_export_list = Cache::get($name_export_list);
//        }

        return response()->json($hs_export_list);
    }
}
