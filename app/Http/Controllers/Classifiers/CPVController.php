<?php

namespace App\Http\Controllers\Classifiers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Classifiers\Models\ClassifiersCpv;

class CPVController extends Controller
{
    /**
     * @SWG\Get(
     *     path="/api/classifiers/cpv",
     *     tags={"classifiers"},
     *     operationId="getList",
     *     description="Get cpv list",
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Get cpv list",
     *          @SWG\Schema(
     *              type="array",
     *              @SWG\Items(
     *                  ref="#/definitions/ClassifiersCpv"
     *              ),
     *          )
     *     ),
     *     security={{"Bearer":{}}}
     * )
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getList(Request $request) {

        $cpv = new ClassifiersCpv();

        if (!empty($request->input('q'))) {
            $cpv->search($request->input('q'));
        }

        if (!empty($request->input('codes'))) {
            $cpv->setCodes(explode(',', $request->input('codes')));
        }

        return response()->mjson(
            $cpv->hierarchy()
        );
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getChildrenByParentId($id) {
        return response()->mjson(
            (new ClassifiersCpv())->children($id)
        );
    }
}
