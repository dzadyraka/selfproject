<?php

namespace App\Http\Controllers\Classifiers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Classifiers\Models\ClassifiersIsic;
use Sunra\PhpSimple\HtmlDomParser;

class ISICController extends Controller
{
    private $string_url = 'https://unstats.un.org/unsd/cr/registry/';

    public function parseSitePage() {

        $string = $this->string_url . 'regcst.asp?Cl=27';
        $dom = HtmlDomParser::file_get_html($string);

        $arr = [];
        $key = '';
        $key_name = '';
        $elements_a = $dom->find('.content > ul > li');

        foreach ($elements_a as $element) {

            $el_number = $element->find('a', 0);
            $el_links = $element->find('a', 0)->href;

            if (ctype_alpha($el_number->plaintext)) {

                $key = $el_number->plaintext;

                $key_name = explode(' - ', strip_tags(substr($element->outertext, 0, strpos($element->outertext, '<ul>'))))[1];

                $check_level = new ClassifiersIsic();
                $check_level->where('code', $key)->first();
                if (!empty($check_level)) {
                    $id = $check_level['id'];
                    $first_level = $check_level;
                    $first_level->update([
                        'name' => $key_name,
                        'link' => $this->string_url . htmlspecialchars_decode($el_links),
                    ]);
                } else {
                    $id = (new ClassifiersIsic())::create([
                        'code' => $key,
                        'name' => $key_name,
                        'link' => $this->string_url . htmlspecialchars_decode($el_links),
                        'level' => 0,
                        'parent_id' => 0
                    ])->get('id');
                }
            }
            if (is_numeric($el_number->plaintext)) {
                $check_level = new ClassifiersIsic();
                $check_level->where('code', $el_number->plaintext)->first();
                if (!empty($check_level)) {
                    $second_level_id = $check_level['id'];
                    $second_level = $check_level;
                    $second_level->update([
                        'name' => ltrim(ltrim($element->plaintext, $el_number), '- '),
                        'link' => $this->string_url . htmlspecialchars_decode($el_links),
                        'level' => 1
                    ]);
                } else {
                    $second_level_id = (new ClassifiersIsic())::create([
                        'code' => $el_number->plaintext,
                        'name' => ltrim(ltrim($element->plaintext, $el_number), '- '),
                        'link' => $this->string_url . htmlspecialchars_decode($el_links),
                        'level' => 1,
                        'parent_id' => $id
                    ])->get('id');
                }
                $children = $this->children($el_links, $second_level_id, 2);
            }

        }

    }

    private function children($link, $parent_id, $level) {

        $el = htmlspecialchars_decode($link);
        $link1 = $this->string_url . $el;

        $dom_child = HtmlDomParser::file_get_html($link1);
        $elements_child = $dom_child->find('.content > ul');

        if (isset($elements_child[1]) && $elements_child[1]->find('li', 0)->tag == 'li') {
            foreach ($elements_child[1]->find('li') as $element_c) {
                $el_number_c = $element_c->find('a', 0)->plaintext;
                $el_links_c = $element_c->find('a', 0)->href;

                $check_level = new ClassifiersIsic();
                $check_level->where('code', $el_number_c)->first();
                if (!empty($check_level)) {
                    $id = $check_level['id'];
                    $second_level = (new ClassifiersIsic())::find($id);
                    $second_level->update([
                        'name' => ltrim(ltrim($element_c->plaintext, $el_number_c), '- '),
                        'link' => $this->string_url . htmlspecialchars_decode($el_links_c),
                        'level' => $level
                    ]);
                } else {
                    $id = (new ClassifiersIsic())::create([
                        'code' => $el_number_c,
                        'name' => ltrim(ltrim($element_c->plaintext, $el_number_c), '- '),
                        'link' => $this->string_url . htmlspecialchars_decode($el_links_c),
                        'parent_id' => $parent_id,
                        'level' => $level,
                    ])->get('id');
                }
                $this->children($el_links_c, $id, 3);
            }
            return true;
        }
        return [];
    }

    /**
     * @SWG\Get(
     *     path="/api/classifiers/isic",
     *     tags={"classifiers"},
     *     operationId="getList",
     *     description="Get isic list",
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Get isic list",
     *          @SWG\Schema(
     *              type="array",
     *              @SWG\Items(
     *                  ref="#/definitions/ClassifiersIsic"
     *              ),
     *          )
     *     ),
     *     security={{"Bearer":{}}}
     * )
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getList() {
        return response()->json(
            (new ClassifiersIsic())->hierarchy()
        );
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getChildrenByParentId($id) {
        return response()->mjson(
            (new ClassifiersIsic())->children($id)
        );
    }
}
