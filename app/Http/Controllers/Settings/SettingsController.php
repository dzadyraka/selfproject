<?php

namespace App\Http\Controllers\Settings;

use JWTAuth;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SettingsController extends Controller
{

    /**
     * @SWG\Put(
     *     path="/api/settings/products",
     *     tags={"settings"},
     *     operationId="create",
     *     description="Save integration settings",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *          in="body",
     *          name="body",
     *          required=true,
     *          @SWG\Schema(
     *              ref="#/definitions/newSettings"
     *          )
     *     ),
     *     @SWG\Response(
     *         response=201,
     *         description="Save integration settings",
     *          @SWG\Schema(ref="#/definitions/Settings")
     *     ),
     *     security={{"Bearer":{}}}
     * )
     * @param string $module_name
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(string $module_name, Request $request) {

        $validator = Validator::make(
            $request->only('type', 'company_id', 'site', 'fields', 'language'),
            [
                'type' => 'required',
                'company_id' => 'required',
                'site' => 'required',
                'fields' => 'required',
                'language' => 'required',
            ]
        );

        if ($validator->fails()) {
            return response()->json(['success' => false, 'errors' => $validator->messages()]);
        }

        return response()->json($this->getModel($module_name)::create($request->only('type', 'company_id', 'site', 'fields', 'active', 'language', 'keys')), 201);
    }

    /**
     * @SWG\Get(
     *     path="/api/settings/products",
     *     tags={"settings"},
     *     operationId="getList",
     *     description="Get list integration settings",
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Get list integration settings",
     *          @SWG\Schema(
     *              type="array",
     *              @SWG\Items(
     *                  ref="#/definitions/Settings"
     *              ),
     *          )
     *     ),
     *     security={{"Bearer":{}}}
     * )
     * @param string $module_name
     * @return \Illuminate\Http\JsonResponse
     */
    public function getList(string $module_name) {
        return response()->json($this->getModel($module_name)::get());
    }

    /**
     * @SWG\Get(
     *     path="/api/settings/products/{setting_id}",
     *     tags={"settings"},
     *     operationId="view",
     *     description="View integration settings",
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="View integration settings",
     *          @SWG\Schema(
     *              ref="#/definitions/Settings"
     *          )
     *     ),
     *     security={{"Bearer":{}}}
     * )
     * @param string $module_name
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function view(string $module_name, int $id) {
        return response()->json($this->getModel($module_name)::find($id));
    }

    /**
     * @SWG\Post(
     *     path="/api/settings/products/{setting_id}",
     *     tags={"settings"},
     *     operationId="edit",
     *     description="Edit integration settings",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *          in="body",
     *          name="body",
     *          required=true,
     *          @SWG\Schema(
     *              ref="#/definitions/newSettings"
     *          )
     *     ),
     *     @SWG\Response(
     *         response=201,
     *         description="Edit integration settings",
     *          @SWG\Schema(ref="#/definitions/Settings")
     *     ),
     *     security={{"Bearer":{}}}
     * )
     * @param string $module_name
     * @param int $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit(string $module_name, int $id, Request $request) {

        $validator = Validator::make(
            $request->only('type', 'companies_id', 'site', 'fields', 'active', 'language'),
            [
                'type' => 'required',
                'companies_id' => 'required',
                'site' => 'required',
                'fields' => 'required',
                'active' => 'required',
                'language' => 'required',
            ]
        );

        if ($validator->fails()) {
            return response()->json(['success' => false, 'errors' => $validator->messages()]);
        }

        $filter = $this->getModel($module_name)::find($id);
        $filter->update($request->only('type', 'companies_id', 'site', 'fields', 'active', 'language', 'keys'));

        return response()->json($filter);
    }

    /**
     * @SWG\Delete(
     *     path="/api/settings/products/{setting_id}",
     *     tags={"settings"},
     *     operationId="delete",
     *     description="Delete integration settings",
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=204,
     *         description="Delete integration settings"
     *     ),
     *     security={{"Bearer":{}}}
     * )
     * @param string $module_name
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(string $module_name, int $id) {
        return response()->json($this->getModel($module_name)::find($id)->delete(), 204);
    }

    /**
     * Get model namespace
     *
     * @param string $model
     * @return string
     */
    private function getModel(string $model): string {
        return "App\Settings\Models\\". ucwords($model) . "Setting";
    }
}
