<?php

namespace App\Http\Controllers\Categories;

use App\Categoties\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;

class CategoriesController extends Controller {
    /**
     * @SWG\Post(
     *     path="/api/v1/categories",
     *     tags={"categories"},
     *     @SWG\Parameter(
     *         name="hierarchy",
     *         in="query",
     *         description="Hierarchy can be true or false",
     *         type="string"
     *     ),
     *     @SWG\Response(
     *          response="200",
     *          description="Get parent list category",
     *          @SWG\Schema(
     *              @SWG\Items(ref="#/definitions/Category")
     *          )
     *     )
     * )
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function categoriesList(Request $request) {
        if (!empty($request->input('q'))) {
            $categories = new Category();
            $q = strtolower($request->input('q'));
            $categories->where(function ($query) use ($q) {
                $query->whereTranslationInsensitiveLike('name', "%$q%");
            })->get();
        } elseif ($request->input('hierarchy')) {
            if (empty($categories = Cache::get('categories'))) {
                $categories = Category::with([
                    'children' => function ($children) {
                        return $children
                            ->with([
                                'children' => function ($children) {
                                    return $children->where('level', '=', 2);
                                }
                            ])
                            ->where('level', '=', 1);
                    }
                ])
                    ->where('level', '=', 0);

                Cache::put('categories', $categories->get(), 15);
                $categories = Cache::get('categories');
            }
        } else {
            $categories = Category::get();
        }

        return response()->mjson(
            $categories
        );
    }

    /**
     * Get category with children
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCategory($id) {
        return response()->json(
            $categories = Category::with([
                'children' => function ($children) {
                    return $children
                        ->with([
                            'children' => function ($children) {
                                return $children->where('level', '=', 2);
                            }
                        ])
                        ->where('level', '=', 1);
                }
            ])
                ->where('level', '=', 0)->findOrFail($id)
        );
    }

    /**
     * Save category
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request) {
        return response()->mjson(
            Category::create($request->input()),
            201
        );
    }

    /**
     * Update category
     *
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id, Request $request) {
        $category = Category::findOrFail($id);
        $category->update($request->input());
        return response()->mjson($category);
    }

    public function viewCategory($id) {
        return response()->mjson(Category::findOrFail($id));
    }

    /**
     * Delete category
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id) {
        return response()->json(Category::findOrFail($id)->delete(), 204);
    }
}
