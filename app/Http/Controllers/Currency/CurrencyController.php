<?php

namespace App\Http\Controllers\Currency;


use App\Currency\Currency;
use App\Currency\CurrencyRate;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CurrencyController extends Controller {
	public function getCurrenciesList(Request $request) {
		return response()->mjson(Currency::whereActive(true)->get());
	}

	public function getRate(Request $request) {
		return response()->json(
			array_values(CurrencyRate::currencies())
		);
	}
}