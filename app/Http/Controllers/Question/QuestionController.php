<?php

namespace App\Http\Controllers\Question;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class QuestionController extends Controller {
	public function getList($id, string $module_name, Request $request) {
		return response()->json(
			$this->getModel($module_name)::with('user', 'company')
				->get()
		);
	}

	/**
	 * Get model namespace
	 *
	 * @param string $model
	 * @return string
	 */
	private function getModel(string $model): string {
		return "App\Questions\Models\\" . ucwords($model) . "Question";
	}

	public function create(int $id, string $module_name, Request $request) {
		$validator = Validator::make(
			$request->only('content'),
			[
				'content' => 'required'
			]
		);

		if ($validator->fails()) {
			return response()->json(['success' => false, 'errors' => $validator->messages()]);
		}

		$request->merge(['product_id' => $id]);

		return response()->json(
			$this->getModel($module_name)::create($request->all())->fresh(['user', 'company']),
			201
		);
	}

	public function view(int $id, string $module_name, int $question_id) {
		return response()->json($this->getModel($module_name)::with('user', 'company')->findOrFail($question_id));
	}

	public function edit(int $id, string $module_name, int $question_id, Request $request) {
		$validator = Validator::make(
			$request->only('content'),
			[
				'content' => 'required'
			]
		);

		if ($validator->fails()) {
			return response()->json(['success' => false, 'errors' => $validator->messages()]);
		}

        $request->merge(['product_id' => $id]);

		$question = $this->getModel($module_name)::find($question_id);
		$question->update($request->all());

		return response()->json($question->fresh(['user', 'company']));
	}

	public function delete(int $id, string $module_name, int $question_id) {
		return response()->json($this->getModel($module_name)::find($question_id)->delete(), 204);
	}
}
