<?php

namespace App\Http\Controllers\Auth;

use App\Mail\ResetPassword;
use App\Users\Models\PasswordReset;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use JWTAuth;
use Validator;
use App\Mail\VerifyMail;
use App\Users\Models\User;
use Illuminate\Http\Request;
use PulkitJalan\GeoIP\GeoIP;
use App\Locations\Models\Country;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Users\Models\UserVerification;
use Tymon\JWTAuth\Exceptions\JWTException;

class AuthController extends Controller {
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('jwt-auth', ['except' => ['login', 'register', 'registerEmail', 'confirmEmail', 'sendResetPassword', 'resetPassword', 'resentCode']]);
    }

    /**
     * API Register
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request) {

        $user = User::create($request->input());
//		$verification_code = str_random(30); //Generate verification code
//		DB::table('user_verifications')->insert(['user_id' => $user->id, 'token' => $verification_code]);
//		$subject = "Please verify your email address.";
//		Mail::send('email.verify', ['name' => $name, 'verification_code' => $verification_code],
//			function ($mail) use ($email, $name, $subject) {
//				$mail->from(getenv('FROM_EMAIL_ADDRESS'), "From User/Company Name Goes Here");
//				$mail->to($email, $name);
//				$mail->subject($subject);
//			});

        return response()->json(['success' => true, 'message' => 'Thanks for signing up! Please check your email to complete your registration.'])->setStatusCode(201);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request) {
        $credentials = $request->only('email', 'password');
        $rules = ['email' => 'required|email', 'password' => 'required'];
        $validator = Validator::make($credentials, $rules);

        if ($validator->fails()) {
            return response()->json(['success' => false, 'errors' => $validator->messages()], 401);
        }

        //TODO: uncommented code
//		$credentials['is_verified'] = true;

        try {
            // attempt to verify the credentials and create a token for the user
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json(['success' => false, 'errors' => ['non_field_errors' => ['We cant find an account with this credentials. Please make sure you entered the right information and you have verified your email address.']]], 404);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['success' => false, 'errors' => ['non_field_errors' => ['Failed to login, please try again.']]], 400);
        }

        // all good so return the token
        return response()->json([
            'access_token' => $token,
            'user_id' => Auth::user()->id,
            'expires_in' => 60 * 60 * 24 * 3,
            'token_type' => 'bearer',
            'user' => Auth::user()->load('companies')
        ], 200);
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(Request $request) {
        $this->validate($request, ['token' => 'required']);

        try {
            JWTAuth::invalidate($request->input('token'));

            return response()->json(['success' => true, 'message' => "You have successfully logged out."]);
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['success' => false, 'error' => 'Failed to logout, please try again.'], 500);
        }
    }

    /**
     * API Verify User
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function verifyUser($verification_code) {
        $check = DB::table('user_verifications')->where('token', $verification_code)->first();
        if (!is_null($check)) {
            $user = User::find($check->user_id);
            if ($user->is_verified == 1) {
                return response()->json([
                    'success' => true,
                    'message' => 'Account already verified..'
                ]);
            }
            $user->update(['is_verified' => 1]);

            DB::table('user_verifications')->where('token', $verification_code)->delete();

            return response()->json([
                'success' => true,
                'message' => 'You have successfully verified your email address.'
            ]);
        }

        return response()->json(['success' => false, 'error' => "Verification code is invalid."]);
    }

    public function registerEmail(Request $request) {
        $credentials = $request->only('email');

        $check_user = User::where('email', $credentials['email'])->first();

        if(!empty($check_user) && !$check_user->is_verified) {
            return $this->resentCode($request);
        }

        $rules = ['email' => 'required|email|unique:users'];
        $validator = Validator::make($credentials, $rules);

        if ($validator->fails()) {
            return response()->json(['success' => false, 'errors' => $validator->messages()], 400);
        }

        $geo_ip = new GeoIP();
        $geo_ip->setIp($request->ip());
        $geo_country = $geo_ip->getCountry();

        $q = strtolower($geo_country);
        $country = Country::where(function ($query) use ($q) {
            $query->whereTranslationInsensitiveLike('name', "%$q%");
        })->first();

        $user = User::create([
            'phone' => null,
            'first_name' => null,
            'last_name' => null,
            'email' => $request->input('email'),
            'country_id' => $country->id,
            'language_id' => 3,
            'password' => "123456",
            'is_verified' => false,
            'is_active' => true
        ]);

        $verifyUser = UserVerification::create([
            'user_id' => $user->id,
//            'token' => 111111,
            'token' => rand(100000, 999999),
            'expires_at' => Carbon::now()->addHours(24)
        ]);

        Mail::to($user->email)->send(new VerifyMail($user));

        return response()->json(['success' => true, 'message' => 'User created. Email send.'], 201);
    }

    public function confirmEmail(Request $request) {
        $user = User::where('email', $request->input('email'))->with(['companies'])->first();
        $verifyUser = UserVerification::where('token', $request->input('code'))->where('user_id', $user->id)->first();

        if (!$verifyUser) {
            return response()->json(['success' => false, 'errors' => 'User not found.'], 404);
        }

        $user = $verifyUser->user;
        if (!$user->is_verified) {
            $verifyUser->user->is_verified = true;
            $verifyUser->user->is_active = true;
            if ($verifyUser->user->save()) {
                $verifyUser->delete();
            }
        } else {
            return response()->json(['success' => false, 'errors' => "Your e-mail is already verified. You can now login."], 204);
        }

        try {
            // attempt to verify the credentials and create a token for the user
            $token = JWTAuth::fromUser($user);
            if (!$token) {
                return response()->json(['success' => false, 'errors' => ['non_field_errors' => ['We cant find an account with this credentials. Please make sure you entered the right information and you have verified your email address.']]], 404);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['success' => false, 'errors' => ['non_field_errors' => ['Failed to login, please try again.']]], 400);
        }

        return response()->json([
            'access_token' => $token,
            'user_id' => $user->id,
            'expires_in' => 60 * 60 * 24 * 3,
            'token_type' => 'bearer',
            'user' => $user
        ], 200);
    }

    public function resentCode(Request $request) {
        if (empty($request->input('email'))) {
            return response()->json(['success' => false, 'errors' => 'Empty email input.'], 404);
        }

        $user = User::where('email', $request->input('email'))->first();

        UserVerification::where('user_id', $user->id)->delete();
        UserVerification::create([
            'user_id' => $user->id,
//            'token' => 111111,
            'token' => rand(100000, 999999),
            'expires_at' => Carbon::now()->addHours(24)
        ]);

        Mail::to($user->email)->send(new VerifyMail($user));

        return response()->json(['success' => true, 'message' => 'New code created. Email send.'], 201);
    }

    public function sendResetPassword(Request $request) {
        $email = $request->input('email');

        if (empty($email)) {
            return response()->json(['success' => false, 'errors' => "Empty email input."], 404);
        }

        $user = User::where('email', $email)->first();
        if (empty($user)) {
            return response()->json(['success' => false, 'errors' => "We cant find an account with this email."], 404);
        }

        PasswordReset::where('email', $request->input('email'))->delete();
        $reset_password = PasswordReset::create(['email' => $request->input('email')]);
        $user->update(['password' => Hash::make(str_random(16))]);

        Mail::to($reset_password->email)->send(new ResetPassword($reset_password));

        return response()->json(['success' => true, 'message' => 'Password reset. Email send.'], 201);
    }

    public function resetPassword(Request $request) {

        $password_fields = $request->only('token', 'password', 'confirm');

        $validator = Validator::make($password_fields, ['token' => 'required','password' => 'required', 'confirm' => 'required|same:password']);

        if ($validator->fails()) {
            return response()->json(['success' => false, 'errors' => $validator->messages()], 400);
        }

        $password_reset = PasswordReset::where('token', $request->input('token'))->where('created_at', '>=', Carbon::now()->subHour(24)->toDateTimeString())->first();

        if (empty($password_reset)) {
            return response()->json(['success' => false, 'errors' => "Recovery link not valid."], 400);
        }

        $user = User::where('email', $password_reset->email)->first();

        if ($user->update(['password' => Hash::make($request->input('password'))])) {
            return response()->json(['success' => true, 'message' => "You have password change success."]);
        }
    }
}
