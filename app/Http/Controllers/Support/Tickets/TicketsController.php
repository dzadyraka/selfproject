<?php

namespace App\Http\Controllers\Support\Tickets;

use Zendesk;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TicketsController extends Controller {

    /**
     * Create ticket fro ZenDesk
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function createTicket(Request $request) {
        try {
            Zendesk::tickets()->create([
                'type' => 'problem',
                'subject' => 'Question to support',
                'description' => $request->input('message'),
                'priority' => 'normal',
                'requester' => [
                    'name' => $request->input('name'),
                    'email' => $request->input('email')
                ],
            ]);
        } catch (\Exception $e) {
            return response(['success' => false, 'message' => 'Failed to create ticket'], 400);
        }

        return response()->json(['success' => true, 'message' => 'Ticket created.']);
    }
}
