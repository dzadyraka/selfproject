<?php

namespace App\Http\Controllers\Messages;

use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Log;
use JWTAuth;
use App\Users\Models\User;
use Illuminate\Http\Request;
use App\Messages\Models\Message;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Messages\Models\Conversation;

class MessagesController extends Controller {


    /**
     * @SWG\Put(
     *     path="/api/messages",
     *     tags={"messages"},
     *     operationId="sendMessage",
     *     description="Send message",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *          in="body",
     *          name="body",
     *          @SWG\Schema(
     *              @SWG\Property(property="title", type="string"),
     *              @SWG\Property(property="conversation_id", type="string", description="Empty if new conversation."),
     *              @SWG\Property(property="content", type="string"),
     *              @SWG\Property(property="to_user_id", type="integer"),
     *          )
     *     ),
     *     @SWG\Response(
     *         response=201,
     *         description="Send message",
     *          @SWG\Schema(ref="#/definitions/Message")
     *     ),
     *     security={{"Bearer":{}}}
     * )
     * @param Request $request
     * @return $this|\Illuminate\Http\JsonResponse
     */
    public function sendMessage(Request $request) {
        $from_user = User::find(JWTAuth::parseToken()->toUser()->id);
        $to_user = User::findOrFail($request->input('to_user_id'));
        if (empty($request->input('conversation_id'))) {

            $conversation = Conversation::create([
                'title' => $request->input('title'),
            ]);

            $conversation->users()->attach($to_user);
            $conversation->users()->attach($from_user);

            $request->merge(['conversation_id' => $conversation->id]);
        }

        Conversation::findOrFail($request->input('conversation_id'));

        return response()->json(Message::create([
            'content' => $request->input('content'),
            'conversation_id' => $request->input('conversation_id'),
            'attachments' => empty(json_encode($request->input('attachments'))) ? $request->input('attachments') : '[]'
        ]), 201);
    }

    /**
     * @SWG\Post(
     *     path="/api/messages/conversation/{id}",
     *     tags={"messages"},
     *     operationId="setSeenStatus",
     *     description="Set seen status",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *          in="body",
     *          name="body",
     *          @SWG\Schema(
     *          )
     *     ),
     *     @SWG\Response(
     *         response=201,
     *         description="Set seen status",
     *     ),
     *     security={{"Bearer":{}}}
     * )
     * @SWG\Patch(
     *     path="/api/messages/conversation/{id}",
     *     tags={"messages"},
     *     operationId="setSeenStatus",
     *     description="Set seen status",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *          in="body",
     *          name="body",
     *          @SWG\Schema(
     *          )
     *     ),
     *     @SWG\Response(
     *         response=201,
     *         description="Set seen status",
     *     ),
     *     security={{"Bearer":{}}}
     * )
     * @param int $id
     * @return $this|\Illuminate\Http\JsonResponse
     */
    public function setSeenStatus(int $id) {
        $unreadMessages = Conversation::findOrFail($id)->unreadMessages->map(
            function (Message $message) {
                return $message->id;
            }
        )->toArray();

        if (count($unreadMessages)) {
            $row = 'UPDATE messages 
			    SET seen = array_to_json(array_append(ARRAY(SELECT jsonb_array_elements_text(seen)),\'' . JWTAuth::parseToken()->toUser()->id . '\'))::jsonb 
			    WHERE id IN (' . implode(', ', $unreadMessages) . ')';

            DB::statement($row);
        }

        return response()->json([], 201);
    }

    /**
     * @SWG\Get(
     *     path="/api/messages",
     *     tags={"messages"},
     *     operationId="getConversations",
     *     description="Get conversation list",
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Get conversation list",
     *          @SWG\Schema(
     *              type="array",
     *              @SWG\Items(
     *                  ref="#/definitions/Conversations"
     *              ),
     *          )
     *     ),
     *     security={{"Bearer":{}}}
     * )
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getConversations() {
        return response()->json(
            (new Conversation)->userDialogs(JWTAuth::parseToken()->toUser()->id)
        );
    }

    /**
     * @SWG\Get(
     *     path="/api/messages/conversation/{id}",
     *     tags={"messages"},
     *     operationId="getConversation",
     *     description="Get conversation list",
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Get filter list",
     *          @SWG\Schema(
     *              type="object",
     *              ref="#/definitions/Conversation"
     *          )
     *     ),
     *     security={{"Bearer":{}}}
     * )
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getConversation(int $id) {
        return response()->json(
            Conversation::with(['messages', 'users'])->find($id)
        );

    }

    /**
     * @SWG\Get(
     *     path="/api/messages/conversation/{id}/messages",
     *     tags={"messages"},
     *     operationId="getConversationMessages",
     *     description="Get conversation messages list",
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Get filter messages list",
     *          @SWG\Schema(
     *              type="object",
     *              ref="#/definitions/Message"
     *          )
     *     ),
     *     security={{"Bearer":{}}}
     * )
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getConversationMessages(int $id) {
        return response()->json([
            Message::where('conversation_id', $id)->get()
        ]);
    }

    /**
     * @SWG\Delete(
     *     path="/api/messages/{id}",
     *     tags={"messages"},
     *     operationId="deleteMessage",
     *     description="Delete message",
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=204,
     *         description="Delete message"
     *     ),
     *     security={{"Bearer":{}}}
     * )
     *
     * @param int $id
     * @return $this
     */
    public function deleteMessage(int $id) {
        $message = Message::findOrFail($id);
        $hide = $message->hide;
        $hide[] = JWTAuth::parseToken()->toUser()->id;
        $message->hide = array_unique($hide);
        $message->save();

        return response()->json([], 204);
    }

    /**
     * @SWG\Delete(
     *     path="/api/messages/conversation/{id}",
     *     tags={"messages"},
     *     operationId="deleteConversation",
     *     description="Delete conversation",
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=204,
     *         description="Delete conversation"
     *     ),
     *     security={{"Bearer":{}}}
     * )
     *
     * @param int $id
     * @return $this
     */
    public function deleteConversation(int $id) {
        $conversation = Conversation::findOrFail($id);

        if ($conversation->users()->count() == 1) {
            $conversation->delete();
        } else {
            $user = User::findOrFail(JWTAuth::parseToken()->toUser()->id);
            $conversation->users()->detach($user);
        }
        return response()->json([], 204);
    }
}
