<?php

namespace App\Http\Controllers\Companies\Payment;

use App\Helpers\ArrayCopy;
use App\Helpers\CryptAlgorithm;
use GuzzleHttp\Client;
use http\Env\Response;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use JWTAuth;
use Stripe\Account;
use Validator;
use Stripe\Stripe;
use Stripe\Customer;
use Cloudipsp\Verification;
use Cloudipsp\Configuration;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Companies\Models\CompanyPayment;

class PaymentController extends Controller {

    public function createPayment(int $id, string $payment_type, Request $request) {
        switch ($payment_type) {
            case 'fondy':
                return $this->_createFondyPayment($id, $payment_type, $request);
            case 'stripe':
                return $this->getToken($id, $payment_type, $request);
        }
    }

    private function _createStripePayment(int $id, string $payment_type, $request) {
        $credentials = $request->only('content');

        $rules = ['content' => 'required'];
        $validator = Validator::make($credentials, $rules);
        $content = $request->input('content');

        if ($validator->fails()) {
            return response()->json(['success' => false, 'errors' => $validator->messages()], 401);
        }

        Stripe::setApiKey($content['access_token']);

        try {
            $account_info = Account::retrieve($content['stripe_user_id']);
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'errors' => $e->getMessage()], 401);
        }

        $credentials['user_info'] = $account_info;

        $request->merge(['company_id' => $id, 'credentials' => $credentials, 'provider' => $payment_type]);

        return CompanyPayment::create($request->input());
    }

    public function _createFondyPayment(int $id, string $payment_type, $request) {
        $credentials = $request->input('credentials');

        $rules = ['merchant_id' => 'required', 'secret_key' => 'required'];
        $validator = Validator::make($credentials, $rules);

        if ($validator->fails()) {
            return response()->json(['success' => false, 'errors' => $validator->messages()], 401);
        }

        Configuration::setMerchantId($credentials['merchant_id']);
        Configuration::setSecretKey($credentials['secret_key']);

        $data = [
            'verification_type' => 'code', //default - amount
            'currency' => 'USD',
            'amount' => 1
        ];

        try {
            $url = Verification::url($data);
            $url->getData();
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'errors' => $e->getMessage()], 401);
        }

        $request->merge(['company_id' => $id, 'credentials' => $credentials, 'provider' => $payment_type]);

        return response()->json(CompanyPayment::create($request->input()), 201);
    }

    public function paymentList(int $id) {
        return response()->json(CompanyPayment::where('company_id', $id)->with(['company'])->get());
    }

    public function companiesPaymentList() {
        return response()->json(CompanyPayment::whereHas('companies', function ($companies) {
            return $companies->whereHas(
                'users',
                function ($users) {
                    return $users->where('id', JWTAuth::parseToken()->toUser()->id);
                }
            );
        })->get());
    }

    public function getPayment(int $id, string $payment_type) {
        $payment = CompanyPayment::where('company_id', $id)->where('provider', $payment_type)->first();
        $data = $payment->toArray();
        $credential = $payment->credentials;
        $decode  = CryptAlgorithm::decrypt($credential);

        $data['credentials'] = CryptAlgorithm::decrypt($decode);

        return response()->json($data);
    }

    public function getToken(int $id, string $payment_type, $request) {
        $code = $request->input('code');
        $client_secret = config('services.stripe.client_secret');
        $grant_type = config('services.stripe.grant_type');

        $url = 'https://connect.stripe.com';

        $client = new Client(['base_uri' => $url]);

        try {
            $response = $client->post('/oauth/token', [
                'form_params' => [
                    'client_secret' => $client_secret,
                    'code' => $code,
                    'grant_type' => $grant_type
                ]
            ]);
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'errors' => $e->getMessage()], 401);
        }


        $content = (array)json_decode($response->getBody()->getContents());

        $request->merge(['content' => $content]);

        return response()->json($this->_createStripePayment($id, $payment_type, $request), 201);
    }

    /**
     * Delete company payment
     *
     * @param int $id
     * @param string $payment_type
     * @return \Illuminate\Http\JsonResponse
     */
    public function deletePayment(int $id, string $payment_type) {
        $payment = CompanyPayment::where('company_id', $id)->where('provider', $payment_type)->first();

        if(empty($payment)) {
            return response()->json(['success' => false, 'errors' => 'Record not found'], 404);
        }

        return response()->json(
            $payment->delete(), 204
        );
    }

    /**
     * Edit company payment
     *
     * @param int $id
     * @param string $payment_type
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function editPayment(int $id, string $payment_type, Request $request) {
        $payment = CompanyPayment::where('company_id', $id)->where('provider', $payment_type)->first();
        $payment->update($request->input());

        return response()->json($payment->fresh());
    }
}
