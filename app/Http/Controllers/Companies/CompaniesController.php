<?php

namespace App\Http\Controllers\Companies;

use App\Companies\Repositories\CompanyRepository;
use JWTAuth;
use Validator;
use App\Users\Models\User;
use Illuminate\Http\Request;
use App\Companies\Models\Company;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Companies\Models\CompanyAddress;
use App\Companies\Models\CompanyExportInfo;
use Rossjcooper\LaravelHubSpot\Facades\HubSpot;
use App\Companies\Models\CompanyDeliveryAndPayment;

class CompaniesController extends Controller {

    /**
     * @var CompanyRepository
     */
    private $repository;

    public function __construct(CompanyRepository $companyRepository) {
        $this->repository = $companyRepository;
    }
    
    /**
     * @SWG\Get(
     *     path="/api/v1/companies",
     *     tags={"company"},
     *     summary="Returns companies list",
     *     description="Returns a map of status codes to quantities",
     *     operationId="getList",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="",
     *         required=true,
     *         @SWG\Schema(
     *              @SWG\Property(
     *                  property="limit",
     *                  type="integer"
     *              ),
     *              @SWG\Property(
     *                  property="offset",
     *                  type="integer"
     *              ),
     *              @SWG\Property(
     *                  property="is_approved",
     *                  type="boolean"
     *              ),
     *         ),
     *     ),
     *
     *  @SWG\Response(
     *     response=200,
     *     description="successful operation",
     *     @SWG\Schema(
     *      type="object"
     *     )
     *  )
     *
     * )
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getList(Request $request) {
        $companies = Company::with(['country']);

        if (!empty($request->input('is_draft'))) {
            $companies->where('is_draft', $request->input('is_draft'));
        } else {
            $companies->where('is_draft', false);
        }

        if (!empty($request->input('q'))) {
            $q = strtolower($request->input('q'));
            $companies->where(function ($query) use ($q) {
                $query->whereTranslationInsensitiveLike('name', "%$q%")
                    ->orWhereTranslationInsensitiveLike('description', "%$q%");
            })->orWhere('email', 'like', "%$q%")
                ->orWhere('phone', 'like', "%$q%")
                ->orWhere('website', 'like', "%$q%");
        }

        return response()->mjson(
            $companies->get()
        );
    }

    /**
     * @SWG\Put(
     *     path="/api/v1/companies",
     *     tags={"company"},
     *     operationId="create",
     *     summary="Add a new company",
     *     description="Create new company",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Company object that needs to be added to the DB",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/newCompany"),
     *     ),
     *     @SWG\Response(
     *         response=201,
     *         description="Created",
     *         @SWG\Schema(ref="#/definitions/Company"),
     *     ),
     *     security={{"Bearer":{}}}
     * )
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request) {
        $validator = $this->_validateInputForCreate($request->only('business_type', 'name', 'country_id'));
        $trade_address = $request->input('trade_address');
        $legal_address = $request->input('legal_address');

        if ($validator->fails()) {
            return response()->json(['success' => false, 'errors' => $validator->messages()]);
        }

        $user = User::findOrFail(JWTAuth::parseToken()->toUser()->id);

        $company = Company::create($request->input());

        if (!empty($trade_address)) {
            CompanyAddress::create([
                'type' => 'trade',
                'company_id' => $company->id,
                'country_id' => $trade_address['country_id'],
                'city_id' => $trade_address['city_id']
            ]);
        }
        if (!empty($legal_address)) {
            CompanyAddress::create([
                'type' => 'legal',
                'company_id' => $company->id,
                'country_id' => $legal_address['country_id'],
                'city_id' => $legal_address['city_id'],
                'street_name' => $legal_address['street_name']
            ]);
        }


        $company->users()->attach($user, [
            'position' => !empty($request->input('position')) ? $request->input('position') : null,
            'access' => json_encode(['messages_work', 'add_and_edit', 'balance_work', 'employees_edit', 'employees_add'])
        ]);

        return response()->mjson($company->fresh([
            'country',
            'legal_address' => function ($legal_address) {
                return $legal_address->with('country');
            },
            'trade_address' => function ($trade_address) {
                return $trade_address->with('country');
            }
        ]), 201);
    }

    /**
     * @SWG\Get(
     *     path="/api/v1/companies/{company_id}",
     *     summary="Get company by id",
     *     description="Returns a single company",
     *     operationId="get",
     *     tags={"company"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *         @SWG\Schema(ref="#/definitions/Company")
     *     ),
     *     @SWG\Response(
     *         response="404",
     *         description="Not found"
     *     ),
     *     security={{"Bearer":{}}}
     * )
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function get($id) {
        return response()->mjson(
            Company::with([
                'country',
                'legal_address' => function ($legal_address) {
                    return $legal_address->with('country');
                },
                'trade_address' => function ($trade_address) {
                    return $trade_address->with('country');
                }
            ])->findOrFail($id)
        );
    }

    /**
     * @SWG\Post(
     *     path="/api/v1/companies/{company_id}",
     *     tags={"company"},
     *     operationId="edit",
     *     summary="Update an existing company",
     *     description="",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Company object that needs to be added to the DB",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/Company"),
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Success",
     *         @SWG\Schema(ref="#/definitions/Company"),
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Not found",
     *     ),
     *     security={{"Bearer":{}}}
     * )
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit($id, Request $request) {
        $validator = $this->_validateInput($request->only('country_id', 'city_id', 'phone', 'email', 'name', 'description'));
        $trade_address = $request->input('trade_address');
        $legal_address = $request->input('legal_address');
        if ($validator->fails()) {
            return response()->json(['success' => false, 'errors' => $validator->messages()]);
        }
        $company = Company::findOrFail($id);
        $company->update($request->input());

        if (!empty($trade_address)) {
            if (!empty($trade_address['id'])) {
                CompanyAddress::find($trade_address['id'])->update([
                    'type' => 'trade',
                    'company_id' => $company->id,
                    'country_id' => $trade_address['country_id'],
                    'city_id' => $trade_address['city_id']
                ]);
            } else {
                CompanyAddress::create([
                    'type' => 'trade',
                    'company_id' => $company->id,
                    'country_id' => $trade_address['country_id'],
                    'city_id' => $trade_address['city_id']
                ]);
            }
        }
        if (!empty($legal_address)) {
            if (!empty($legal_address['id'])) {
                CompanyAddress::find($legal_address['id'])->update([
                    'type' => 'legal',
                    'company_id' => $company->id,
                    'country_id' => $legal_address['country_id'],
                    'city_id' => $legal_address['city_id'],
                    'street_name' => $legal_address['street_name']
                ]);
            } else {
                CompanyAddress::create([
                    'type' => 'legal',
                    'company_id' => $company->id,
                    'country_id' => $legal_address['country_id'],
                    'city_id' => $legal_address['city_id'],
                    'street_name' => $legal_address['street_name']
                ]);
            }
        }

        return response()->mjson($company->fresh(['country']));
    }

    /**
     * @SWG\Delete(
     *     path="/compamies/{company_id}",
     *     summary="Deletes a company",
     *     description="",
     *     operationId="delete",
     *     produces={"application/json"},
     *     tags={"company"},
     *     @SWG\Response(
     *         response=204,
     *         description="Delete success"
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Company not found"
     *     ),
     *     security={{"Bearer":{}}}
     * )
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id) {
        return response()->json(Company::findOrFail($id)->delete(), 204);
    }

    /**
     * @SWG\Put(
     *     path="/api/v1/companies/{company_id}/users",
     *     tags={"company"},
     *     operationId="addCompanyUser",
     *     summary="Add a new user to the company",
     *     description="",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *          in="body",
     *          name="body",
     *          description="List of user object",
     *          required=true,
     *          @SWG\Schema(
     *              @SWG\Property(property="user_id", type="integer"),
     *              @SWG\Property(property="position", type="string"),
     *          )
     *     ),
     *     @SWG\Response(
     *         response=201,
     *         description="Added user to company",
     *         @SWG\Schema(ref="#/definitions/CompanyUsers"),
     *     ),
     *     security={{"Bearer":{}}}
     * )
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addCompanyUser($id, Request $request) {
        $company = Company::findOrFail($id);
        $user = User::findOrFail($request->input('user_id'));

        $company->users()->attach($user, [
            'position' => !empty($request->input('position')) ? $request->input('position') : null,
        ]);

        return response()->json([], 201);
    }

    /**
     * @SWG\Delete(
     *     path="/api/v1/companies/{company_id}/users/{user_id}",
     *     tags={"company"},
     *     operationId="removeCompanyUser",
     *     summary="Remove user from company",
     *     description="",
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=201,
     *         description="Remove user from company",
     *         @SWG\Schema(ref="#/definitions/CompanyUsers"),
     *     ),
     *     security={{"Bearer":{}}}
     * )
     * @param $id
     * @param $user_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function removeCompanyUser($id, $user_id) {
        $company = Company::findOrFail($id);
        $user = User::findOrFail($user_id);

        $company->users()->detach($user);

        return response()->json([], 204);
    }

    /**
     * @SWG\Post(
     *     path="/api/v1/companies/{company_id}/users/{user_id}",
     *     tags={"company"},
     *     operationId="modifyCompanyUser",
     *     summary="Modify user in company",
     *     description="",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *          in="body",
     *          name="body",
     *          description="List of user object",
     *          required=true,
     *          @SWG\Schema(
     *              @SWG\Property(property="user_id", type="integer"),
     *              @SWG\Property(property="position", type="string"),
     *          )
     *     ),
     *     @SWG\Response(
     *         response=201,
     *         description="Modify user in company",
     *         @SWG\Schema(ref="#/definitions/CompanyUsers"),
     *     ),
     *     security={{"Bearer":{}}}
     * )
     * @param $id
     * @param $user_id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function modifyCompanyUser($id, $user_id, Request $request) {
        $company = Company::findOrFail($id);
        User::findOrFail($user_id);

        $company->users()->updateExistingPivot($user_id, [
            'position' => !empty($request->input('position')) ? $request->input('position') : null,
        ]);

        return response()->mjson([
            'users' => $company->users()->get()
        ]);
    }

    /**
     * @SWG\Get(
     *     path="/api/v1/companies/{company_id}/users",
     *     tags={"company"},
     *     operationId="getCompanyUsers",
     *     summary="Get users in company",
     *     description="",
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Get users in company",
     *         @SWG\Schema(ref="#/definitions/CompanyUsers"),
     *     ),
     *     security={{"Bearer":{}}}
     * )
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCompanyUsers($id) {
        $company = Company::findOrFail($id);
        return response()->mjson([
            'users' => $company->users()->with('companies')->get()
        ]);
    }

    public function checkSubDomainAvailability(Request $request) {
        return response()->json([
            'exists' => $this->repository->isDomainExists($request->input('domain'))
        ]);
    }

    /**
     * @SWG\Get(
     *     path="/api/v1/users/{user_id}/companies",
     *     summary="Get user company",
     *     description="Returns a single company",
     *     operationId="getUserCompanies",
     *     tags={"users"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *         @SWG\Schema(ref="#/definitions/Company")
     *     ),
     *     @SWG\Response(
     *         response="404",
     *         description="Not found"
     *     ),
     *     security={{"Bearer":{}}}
     * )
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUserCompanies($id) {
        return response()->json(User::findOrFail($id)->companiesWithOutDrafts()->with('country')->get());
    }

    /**
     * Upload company logo
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function uploadLogo($id, Request $request) {
        $file = $request->file('file');
        $upload_file = Storage::putFile('uploads', $file);

        return response()->json([
            'logo' => $upload_file,
        ], 201);
    }

    /**
     * @SWG\Put(
     *     path="/api/v1/companies/{company_id}/address",
     *     tags={"company"},
     *     operationId="addAddress",
     *     summary="Add address to company",
     *     description="",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *          in="body",
     *          name="body",
     *          required=true,
     *          @SWG\Schema(
     *              ref="#/definitions/newCompanyAddress"
     *          )
     *     ),
     *     @SWG\Response(
     *         response=201,
     *         description="List addresses in company",
     *         @SWG\Schema(ref="#/definitions/CompanyAddress"),
     *     ),
     *     security={{"Bearer":{}}}
     * )
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addAddress($id, Request $request) {
        Company::findOrFail($id);
        $request->merge(['company_id' => $id]);

        $validator = $this->_validateAddress($request->only('company_id', 'region_id', 'city_id', 'country_id', 'street_name', 'street_data', 'zip_code', 'first_name', 'last_name', 'type'));

        if ($validator->fails()) {
            return response()->json(['success' => false, 'errors' => $validator->messages()]);
        }

        return response()->json(CompanyAddress::create($request->input())->fresh(['company', 'country']), 201);
    }

    /**
     * @SWG\Get(
     *     path="/api/v1/companies/{company_id}/address",
     *     tags={"company"},
     *     operationId="listAddresses",
     *     summary="List address to company",
     *     description="",
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="List addresses in company",
     *          @SWG\Schema(
     *              type="array",
     *              @SWG\Items(
     *                  @SWG\Property(property="id", ref="#/definitions/CompanyAddress"),
     *              ),
     *          )
     *     ),
     *     security={{"Bearer":{}}}
     * )
     * @param int $company_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function listAddresses(int $company_id) {
        return response()->json(
            CompanyAddress::where('company_id', $company_id)
                ->with(['company', 'country'])
                ->get()
        );
    }

    /**
     * @SWG\Post(
     *     path="/api/v1/companies/{company_id}/address/{address_id}",
     *     tags={"company"},
     *     operationId="updateAddress",
     *     summary="Update address to company",
     *     description="",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *          in="body",
     *          name="body",
     *          required=true,
     *          @SWG\Schema(
     *              ref="#/definitions/newCompanyAddress"
     *          )
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Address in company",
     *         @SWG\Schema(ref="#/definitions/CompanyAddress"),
     *     ),
     *     security={{"Bearer":{}}}
     * )
     *
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateAddress($id, Request $request) {
        $company_address = CompanyAddress::findOrFail($id);
        $request->merge(['company_id' => $id]);

        $validator = $this->_validateAddress($request->only('company_id', 'region_id', 'city_id', 'country_id', 'street_name', 'street_data', 'zip_code', 'first_name', 'last_name', 'type'));

        if ($validator->fails()) {
            return response()->json(['success' => false, 'errors' => $validator->messages()]);
        }
        $company_address->update($request->input());

        return response()->json($company_address->fresh(['company', 'country']));
    }

    /**
     * @SWG\Delete(
     *     path="/api/v1/companies/{company_id}/address/{address_id}",
     *     tags={"company"},
     *     operationId="deleteAddress",
     *     summary="Remove address from company",
     *     description="",
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=204,
     *         description="Remove address from company"
     *     ),
     *     security={{"Bearer":{}}}
     * )
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteAddress($id) {
        $company_address = CompanyAddress::findOrFail($id);
        return response()->json($company_address->delete(), 204);
    }

    /**
     * @SWG\Get(
     *     path="/api/v1/companies/{company_id}/delivery-and-payment",
     *     tags={"company"},
     *     operationId="listDeliveryPayment",
     *     summary="List delivery and payment to company",
     *     description="",
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="List delivery and payment to company",
     *          @SWG\Schema(
     *              type="array",
     *              @SWG\Items(
     *                  @SWG\Property(property="id", ref="#/definitions/DeliveryAndPayment"),
     *              ),
     *          )
     *     ),
     *     security={{"Bearer":{}}}
     * )
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function listDeliveryPayment($id) {
        return response()->json(
            CompanyDeliveryAndPayment::where('company_id', $id)
                ->with(['company'])
                ->get()
        );
    }

    /**
     * @SWG\Put(
     *     path="/api/v1/companies/{company_id}/delivery-and-payment",
     *     tags={"company"},
     *     operationId="createDeliveryPayment",
     *     summary="Create company delivery and payment info",
     *     description="",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *          in="body",
     *          name="body",
     *          required=true,
     *          @SWG\Schema(
     *              ref="#/definitions/newDeliveryAndPayment"
     *          )
     *     ),
     *     @SWG\Response(
     *         response=201,
     *         description="Create company delivery and payment info",
     *          @SWG\Schema(ref="#/definitions/DeliveryAndPayment")
     *     ),
     *     security={{"Bearer":{}}}
     * )
     *
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createDeliveryPayment($id, Request $request) {
        Company::findOrFail($id);
        $request->merge(['company_id' => $id]);

        $validator = $this->_validateDeliveryAndPayment($request->only('company_id', 'delivery_type', 'shipping_services', 'payment_terms'));

        if ($validator->fails()) {
            return response()->json(['success' => false, 'errors' => $validator->messages()]);
        }

        return response()->json(
            CompanyDeliveryAndPayment::create($request->input())
                ->fresh(['company']),
            201);
    }

    /**
     * @SWG\Get(
     *     path="/api/v1/companies/{company_id}/delivery-and-payment/{delivery_and_payment_id}",
     *     tags={"company"},
     *     operationId="viewDeliveryPayment",
     *     summary="View company delivery and payment info",
     *     description="",
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="View company delivery and payment info",
     *          @SWG\Schema(ref="#/definitions/DeliveryAndPayment")
     *     ),
     *     security={{"Bearer":{}}}
     * )
     *
     * @param $id
     * @param $delivery_and_payment_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function viewDeliveryPayment($id, $delivery_and_payment_id) {
        return response()->json(
            CompanyDeliveryAndPayment::with(['company'])->find($delivery_and_payment_id)
        );
    }

    /**
     * @SWG\Post(
     *     path="/api/v1/companies/{company_id}/delivery-and-payment/{delivery_and_payment_id}",
     *     tags={"company"},
     *     operationId="editDeliveryPayment",
     *     summary="Edit company delivery and payment info",
     *     description="",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *          in="body",
     *          name="body",
     *          required=true,
     *          @SWG\Schema(
     *              ref="#/definitions/newDeliveryAndPayment"
     *          )
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Edit company delivery and payment info",
     *          @SWG\Schema(ref="#/definitions/DeliveryAndPayment")
     *     ),
     *     security={{"Bearer":{}}}
     * )
     *
     * @param $id
     * @param $delivery_and_payment_id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function editDeliveryPayment($id, $delivery_and_payment_id, Request $request) {
        $delivery_and_payment = CompanyDeliveryAndPayment::findOrFail($delivery_and_payment_id);
        $request->merge(['company_id' => $id]);
        $validator = $this->_validateDeliveryAndPayment($request->only('company_id', 'delivery_type', 'shipping_services', 'payment_terms'));

        if ($validator->fails()) {
            return response()->json(['success' => false, 'errors' => $validator->messages()]);
        }

        $delivery_and_payment->update($request->input());

        return response()->json(
            $delivery_and_payment
                ->fresh(['company'])
        );
    }

    /**
     * @SWG\Delete(
     *     path="/api/v1/companies/{company_id}/delivery-and-payment/{delivery_and_payment_id}",
     *     tags={"company"},
     *     operationId="deleteDeliveryPayment",
     *     summary="Delete company delivery and payment info",
     *     description="",
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=204,
     *         description="Delete company delivery and payment info"
     *     ),
     *     security={{"Bearer":{}}}
     * )
     *
     * @param $id
     * @param $delivery_and_payment_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteDeliveryPayment($id, $delivery_and_payment_id) {
        $delivery_and_payment = CompanyDeliveryAndPayment::findOrFail($delivery_and_payment_id);

        return response()->json(
            $delivery_and_payment
                ->delete(),
            204
        );
    }

    /**
     * @SWG\Get(
     *     path="/api/v1/companies/{company_id}/export-info",
     *     tags={"company"},
     *     operationId="listDeliveryPayment",
     *     summary="Get company export info",
     *     description="",
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Get company export info",
     *          @SWG\Schema(
     *              type="array",
     *              @SWG\Items(
     *                  @SWG\Property(property="id", ref="#/definitions/ExportInfo"),
     *              ),
     *          )
     *     ),
     *     security={{"Bearer":{}}}
     * )
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function listExportInfo($id) {
        return response()->json(
            CompanyExportInfo::where('company_id', $id)
                ->with(['company', 'country'])
                ->get()
        );
    }

    /**
     * @SWG\Put(
     *     path="/api/v1/companies/{company_id}/export-info",
     *     tags={"company"},
     *     operationId="createExportInfo",
     *     summary="Create company export info",
     *     description="",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *          in="body",
     *          name="body",
     *          required=true,
     *          @SWG\Schema(
     *              ref="#/definitions/newExportInfo"
     *          )
     *     ),
     *     @SWG\Response(
     *         response=201,
     *         description="Create company export info",
     *          @SWG\Schema(ref="#/definitions/ExportInfo")
     *     ),
     *     security={{"Bearer":{}}}
     * )
     *
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createExportInfo($id, Request $request) {
        Company::findOrFail($id);
        $request->merge(['company_id' => $id]);

        $validator = $this->_validateExportInfo($request->only('title', 'company_id', 'country_id', 'city_id', 'port'));

        if ($validator->fails()) {
            return response()->json(['success' => false, 'errors' => $validator->messages()]);
        }

        return response()->json(
            CompanyExportInfo::create($request->input())
                ->fresh(['company', 'country']),
            201);
    }

    /**
     * @SWG\Get(
     *     path="/api/v1/companies/{company_id}/export-info/{export_and_info_id}",
     *     tags={"company"},
     *     operationId="viewExportInfo",
     *     summary="View company export info",
     *     description="",
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="View company export info",
     *          @SWG\Schema(ref="#/definitions/ExportInfo")
     *     ),
     *     security={{"Bearer":{}}}
     * )
     *
     * @param $id
     * @param $export_info
     * @return \Illuminate\Http\JsonResponse
     */
    public function viewExportInfo($id, $export_info) {
        return response()->json(
            CompanyExportInfo::with(['company', 'country'])->find($export_info)
        );
    }

    /**
     * @SWG\Post(
     *     path="/api/v1/companies/{company_id}/export-info/{export_and_info_id}",
     *     tags={"company"},
     *     operationId="editExportInfo",
     *     summary="Edit company export info",
     *     description="",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *          in="body",
     *          name="body",
     *          required=true,
     *          @SWG\Schema(
     *              ref="#/definitions/newExportInfo"
     *          )
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Edit company export info",
     *          @SWG\Schema(ref="#/definitions/ExportInfo")
     *     ),
     *     security={{"Bearer":{}}}
     * )
     *
     * @param $id
     * @param $export_info
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function editExportInfo($id, $export_info, Request $request) {
        $export_info = CompanyExportInfo::findOrfail($export_info);
        $request->merge(['company_id' => $id]);

        $validator = $this->_validateExportInfo($request->only('title', 'company_id', 'country_id', 'city_id', 'port'));

        if ($validator->fails()) {
            return response()->json(['success' => false, 'errors' => $validator->messages()]);
        }
        $export_info->update($request->input());
        return response()->json(
            $export_info
                ->fresh(['company', 'country'])
        );
    }

    public function getCustomerPeopleList(Request $request, int $user_id) {
        $companies = User::with('companies')->findOrFail($user_id)->companies->pluck('id');

        $users = User::whereHas('companies', function ($query) use ($companies) {
            return $query->whereHas('orders_created', function ($query) use ($companies) {
                return $query->whereIn('company_id', $companies);
            })->whereNotIn('id', $companies);
        })->with(['country', 'companies' => function ($query) use ($request, $companies) {
            return $query->whereHas('orders_created', function ($query) use ($companies) {
                return $query->whereIn('company_id', $companies);
            });
        }]);

        $users->when($request->input('company_id'), function ($query) use ($request) {
            return $query->whereHas('companies', function ($companies) use ($request) {
                return $companies->where('id', $request->input('company_id'));
            });
        });

        $users->when($request->input('type'), function ($query) use ($request) {
            return $query;
        });

        $users->when($request->input('country_id'), function ($query) use ($request) {
            return $query->where('country_id', $request->input('country_id'));
        });

        $users->selectRaw('users.*, count(orders.id) as orders_count, sum(orders.total) as total_spent, \'marketplace\' as type')
            ->join('company_user', 'company_user.user_id', '=', 'users.id')
            ->join('orders', 'company_user.company_id', '=', 'orders.customer_company_id')
            ->groupBy('users.id');

        return response()->mjson($users->paginate());
    }

    public function getCustomerPeopleInfo(Request $request, int $user_id, int $customer_id) {
        $companies = User::with('companies')->findOrFail($user_id)->companies->pluck('id');
        $customer = User::with(['country', 'companies' => function ($query) use ($companies) {
            return $query->whereHas('orders_created', function ($query) use ($companies) {
                return $query->whereIn('company_id', $companies)->with('creator');
            })->with('orders_created', 'country', 'city', 'region');
        }])->findOrFail($customer_id);

        return response()->mjson($customer);
    }

    public function getCustomerCompaniesList(Request $request, int $user_id) {
        $company_id = User::with('companies')->findOrFail($user_id)->companies->pluck('id');
        $companies = Company::whereHas('orders_created', function ($request) use ($company_id) {
            return $request->whereIn('company_id', $company_id);
        })->whereNotIn('companies.id', $company_id)->with('country', 'city', 'region');

        $companies->when($request->input('country_id'), function ($query) use ($request) {
            return $query->where('country_id', $request->input('country_id'));
        });

        $companies->selectRaw('companies.*, count(orders.id) as orders_count, sum(orders.total) as total_spent, \'marketplace\' as type')
            ->join('orders', 'companies.id', '=', 'orders.customer_company_id')
            ->groupBy('companies.id');

        return response()->mjson($companies->paginate());
    }

    public function getCustomerCompanyInfo(Request $request, int $user_id, int $customer_id) {
        $companies = User::with('companies')->findOrFail($user_id)->companies->pluck('id');
        $customer = Company::with(['country', 'city', 'region', 'users' => function ($query) {
            return $query->with('country');
        }, 'orders_created' => function ($query) use ($companies) {
            return $query->whereIn('company_id', $companies)->with('creator');
        }])->findOrFail($customer_id);

        return response()->mjson($customer);
    }

    /**
     * @SWG\Delete(
     *     path="/api/v1/companies/{company_id}/export-info/{export_and_info_id}",
     *     tags={"company"},
     *     operationId="deleteExportInfo",
     *     summary="Delete company export info",
     *     description="",
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=204,
     *         description="Delete company export info"
     *     ),
     *     security={{"Bearer":{}}}
     * )
     *
     * @param $id
     * @param $export_info
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteExportInfo($id, $export_info) {
        $export_info = CompanyExportInfo::findOrfail($export_info);

        return response()->json(
            $export_info->delete(),
            204
        );
    }

    public function getFirstDraftCompany() {
        return response()->json(Company::where('is_draft', true)
            ->whereHas('users', function ($users) {
                return $users->where('user_id', JWTAuth::parseToken()->toUser()->id);
            })->with('country')->first()
        );
    }

    /**
     * Validation for create and edit company
     *
     * @param $input
     * @return mixed
     */
    private function _validateInput($input) {
        return Validator::make(
            $input,
            [
//                'country_id' => 'required',
//                'city_id' => 'required',
//                'phone' => 'required',
//                'email' => 'required|email',
                'name' => 'required',
//                'description' => 'required',
            ]
        );
    }

    /**
     * Validation for create and edit company
     *
     * @param $input
     * @return mixed
     */
    private function _validateInputForCreate($input) {
        return Validator::make(
            $input,
            [
                'business_type' => 'required',
                'name' => 'required',
                'country_id' => 'required',
                'sub_domain' => 'unique'
            ]
        );
    }

    /**
     * Validaion fro create and edit address
     *
     * @param $input
     * @return mixed
     */
    private function _validateAddress($input) {
        return Validator::make(
            $input,
            [
                'company_id' => 'required',
                'region_id' => 'required',
                'city_id' => 'required',
                'country_id' => 'required',
                'street_name' => 'required',
                'street_data' => 'required',
                'zip_code' => 'required',
                'first_name' => 'required',
                'last_name' => 'required',
                'type' => 'required',
            ]
        );
    }

    /**
     * Validation for create and edit delivery and payment
     *
     * @param $input
     * @return mixed
     */
    private function _validateDeliveryAndPayment($input) {
        return Validator::make(
            $input,
            [
                'company_id' => 'required',
                'delivery_type' => 'required',
                'shipping_services' => 'required',
                'payment_terms' => 'required',
            ]
        );
    }

    /**
     * Validation for create and edit export info
     *
     * @param $input
     * @return mixed
     */
    private function _validateExportInfo($input) {
        return Validator::make(
            $input,
            [
                'title' => 'required',
                'company_id' => 'required',
                'country_id' => 'required',
                'city_id' => 'required',
                'port' => 'required',
            ]
        );
    }

}
