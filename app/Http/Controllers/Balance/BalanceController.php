<?php

namespace App\Http\Controllers\Balance;

use App\Balance\Models\Transaction;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Facades\JWTAuth;


class BalanceController extends Controller {
	public function getTransactionsList(Request $request) {
		return response()->json(
			Transaction::where('user_id', JWTAuth::parseToken()->toUser()->id)->get()
		);
	}

	/**
	 * Creates transaction.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @return mixed
	 */
	public function createTransaction(Request $request) {
		$this->middleware('transaction');

		$validator = Validator::make(
			$request->only('value', 'currency_id', 'comment'),
			[
				'value' => 'required',
				'currency_id' => 'required',
//				'currency_code' => 'required',
				'comment' => 'required'
			]
		);

		if ($validator->fails()) {
			return response()->json(['success' => false, 'errors' => $validator->messages()]);
		}

		return response()->json(Transaction::create($request->post()), 201);
	}
}