<?php

namespace App\Http\Controllers\Ports;


use App\Http\Controllers\Controller;
use App\Locations\Models\City;
use App\Locations\Services\CityService;
use App\Ports\Repositories\PortRepository;
use App\Ports\Services\PortService;
use Illuminate\Http\Request;

class PortsController extends Controller {
    /**
     * @var PortRepository
     */
    private $repository;
    /**
     * @var CityService
     */
    private $cityService;
    /**
     * @var PortService
     */
    private $portService;

    public function __construct(
        PortRepository $portRepository,
        CityService $cityService,
        PortService $portService
    ) {
        $this->repository = $portRepository;
        $this->cityService = $cityService;
        $this->portService = $portService;
    }

    public function getContainerTerminals(int $countryId, Request $request) {
        return response()->json(
            $this->repository->getCountryContainerTerminals($countryId)
        );
    }

    public function getClosestPortsForCity(int $countryId, int $cityId, Request $request) {
        return $this->portService->getClosestCityPorts(City::findOrFail($cityId));
    }
}