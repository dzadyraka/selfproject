<?php

namespace App\Http\Controllers\Comtrade;

use Validator;
use Illuminate\Http\Request;
use App\Comtrade\Models\Comtrade;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class ComtradeController extends Controller {

    public function trends(Request $request) {
        $country_id = $request->input('country_id');
        $partner_id = $request->input('partner_id');

        $rules = ['country_id' => 'required', 'partner_id' => 'required'];
        $validator = Validator::make($request->only('country_id', 'partner_id'), $rules);

        if ($validator->fails()) {
            return response()->json(['success' => false, 'errors' => $validator->messages()], 400);
        }

        $comtrade = Comtrade::select(
            'year',
            'rg_desc',
            DB::raw("SUM(trade_value) AS total")
        )
            ->when($country_id, function ($q, $country_id) {
                return $q->where('country_id', $country_id);
            })
            ->when($partner_id, function ($q, $partner_id) {
                return $q->where('partner_id', $partner_id);
            })
            ->groupBy('rg_desc')
            ->groupBy('year')
            ->orderBy('year', 'asc')
            ->get()->map(function ($data) {
                return [
                    'type' => $data->rg_desc,
                    'year' => $data->year,
                    'value' => (int)$data->total
                ];
            });

        return response()->json($comtrade);
    }
}
