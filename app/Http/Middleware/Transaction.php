<?php

namespace App\Http\Middleware;


use Closure;
use Illuminate\Support\Facades\DB;

class Transaction {
	public function handle($request, Closure $next) {
		return DB::transaction(function () use ($request, $next) {
			return $next($request);
		});
	}
}