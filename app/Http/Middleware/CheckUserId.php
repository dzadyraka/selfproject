<?php

namespace App\Http\Middleware;

use JWTAuth;
use Closure;

class CheckUserId {
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        if (!empty($request->bearerToken())) {
            $request->merge(['user_id' => JWTAuth::parseToken()->toUser()->id]);
        } elseif (empty($request->input('user_id'))) {
            $request->merge(['user_id' => hexdec(uniqid())]);
        }

        return $next($request);
    }
}
