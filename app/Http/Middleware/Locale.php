<?php

namespace App\Http\Middleware;


use Closure;
use Teto\HTTP\AcceptLanguage;

class Locale {
	/**
	 * Handle an incoming request.
	 * TODO: Do wildcart
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  \Closure $next
	 * @param  string|null $guard
	 * @return mixed
	 */
	public function handle($request, Closure $next, $guard = null) {
		list($_, $lang) = AcceptLanguage::parse($request->header('Accept-Language', 'en'));

		if ($lang['language'] != '*') { // It's not wildcart
			$languages = explode(',', $lang['language']);
			$locale = array_shift($languages);
			if (in_array($locale, app()->make('config')->get('translatable.locales'))) {
				app()->setLocale($locale);
			}
		}

		return $next($request);
	}
}