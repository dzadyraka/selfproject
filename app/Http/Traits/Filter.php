<?php

namespace App\Http\Traits;

use JWTAuth;
use Illuminate\Database\Eloquent\Builder;

trait Filter {

    private static $_resolver;

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    public static function bootFilter() {
        static::addGlobalScope(function (Builder $builder) {
            if (static::$_resolver instanceOf \Closure) {
                call_user_func(static::$_resolver, $builder);
            }
        });
    }

    /**
     * Set limit and offset
     *
     * @param \Closure $resolver
     */
    public static function resolver(\Closure $resolver) {
        static::$_resolver = $resolver;
    }
}