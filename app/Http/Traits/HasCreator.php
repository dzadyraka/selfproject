<?php

namespace App\Http\Traits;

use App\Users\Models\User;
use Illuminate\Database\Eloquent\Model;
use Tymon\JWTAuth\Facades\JWTAuth;

trait HasCreator {
    static $_creator_id_field = 'creator_id';

    /**
     * Add user id to field
     */
    public static function bootHasCreator() {
        static::saving(function (Model $model) {
            $model->{self::$_creator_id_field} = JWTAuth::parseToken()->toUser()->id;
        });

        static::creating(function (Model $model) {
            $model->{self::$_creator_id_field} = JWTAuth::parseToken()->toUser()->id;
        });
    }

    public function creator() {
        return $this->belongsTo(User::class);
    }
}