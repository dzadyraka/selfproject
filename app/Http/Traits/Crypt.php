<?php

namespace App\Http\Traits;

use App\Helpers\CryptAlgorithm;
use Illuminate\Database\Eloquent\Model;

trait Crypt {

    /**
     * Add user id to field
     */
    public static function bootCrypt() {

        static::saving(function (Model $model) {
            $encrypts = $model->encrypt();
            foreach($encrypts as $encrypt) {
                $value = $model->$encrypt;
                $model->$encrypt = CryptAlgorithm::encrypt($value);
            }
        });

        static::creating(function (Model $model) {
            $encrypts = $model->encrypt();
            foreach($encrypts as $encrypt) {
                $value = $model->$encrypt;
                $model->$encrypt = CryptAlgorithm::encrypt($value);
            }
        });

    }
}