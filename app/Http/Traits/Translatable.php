<?php

namespace App\Http\Traits;


use Dimsav\Translatable\Translatable as BaseTranslatable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Trait Translatable
 * @package App\Http\Traits
 *
 * @method Builder whereTranslationInsensitiveLike(string $column, string $value);
 * @method Builder orWhereTranslationInsensitiveLike(string $column, string $value);
 */
trait Translatable {
    use BaseTranslatable;

    public static function bootTranslatable() {
        static::retrieved(function (Model $model) {
            $model->setHidden(array_merge($model->getHidden(), ['translations']));
        });

        static::created(function (Model $model) {
            $model->setHidden(array_merge($model->getHidden(), ['translations']));
        });
    }

    /**
     * This scope filters results by checking the translation fields.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string $key
     * @param string $value
     * @param string $locale
     *
     * @return \Illuminate\Database\Eloquent\Builder|static
     */
    public function scopeWhereTranslationInsensitiveLike(Builder $query, $key, $value, $locale = null) {
        return $query->whereHas('translations', function (Builder $query) use ($key, $value, $locale) {
            $query->whereRaw("LOWER({$this->getTranslationsTable()}.$key) LIKE '$value'");
            if ($locale) {
                $query->whereRaw("LOWER({$this->getTranslationsTable()}.{$this->getLocaleKey()}) LIKE '$locale'");
            }
        });
    }

    /**
     * This scope filters results by checking the translation fields.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string $key
     * @param string $value
     * @param string $locale
     *
     * @return \Illuminate\Database\Eloquent\Builder|static
     */
    public function scopeOrWhereTranslationInsensitiveLike(Builder $query, $key, $value, $locale = null) {
        return $query->orWhereHas('translations', function (Builder $query) use ($key, $value, $locale) {
            $query->whereRaw("LOWER({$this->getTranslationsTable()}.$key) LIKE '$value'");
            if ($locale) {
                $query->whereRaw("LOWER({$this->getTranslationsTable()}.{$this->getLocaleKey()}) LIKE '$locale'");
            }
        });
    }

    /**
     * @param array $attributes
     *
     * @throws \Illuminate\Database\Eloquent\MassAssignmentException
     * @return $this
     * @throws \Dimsav\Translatable\Exception\LocalesNotDefinedException
     */
    public function fill(array $attributes) {
        foreach ($attributes as $key => $values) {
            if ($this->isKeyALocale($key)) {
                $this->getTranslationOrNew($key)->fill($values);
                foreach ($values as $key_2 => $value) {
                    if (!in_array($key_2, $this->translatedAttributes)) {
                        $attributes[$key_2] = $value;
                    }
                }
                unset($attributes[$key]);
            } else {
                list($attribute, $locale) = $this->getAttributeAndLocale($key);
                if ($this->isTranslationAttribute($attribute) and $this->isKeyALocale($locale)) {
                    $this->getTranslationOrNew($locale)->fill([$attribute => $values]);
                    unset($attributes[$key]);
                }
            }
        }

        return parent::fill($attributes);
    }
}