<?php

namespace App\Http\Traits;

use App\Users\Models\User;
use Illuminate\Database\Eloquent\Model;
use Tymon\JWTAuth\Facades\JWTAuth;

trait HasEditor {
    static $_editor_id_field = 'editor_id';

    /**
     * Add user id to field
     */
    public static function bootHasEditor() {
        static::saving(function (Model $model) {
            $model->{self::$_editor_id_field} = JWTAuth::parseToken()->toUser()->id;
        });

        static::creating(function (Model $model) {
            $model->{self::$_editor_id_field} = JWTAuth::parseToken()->toUser()->id;
        });
    }

    public function editor() {
        return $this->belongsTo(User::class);
    }
}