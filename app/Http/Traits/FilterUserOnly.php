<?php

namespace App\Http\Traits;

use Illuminate\Database\Eloquent\Builder;
use Tymon\JWTAuth\Facades\JWTAuth;

trait FilterUserOnly {

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    public static function bootFilterUserOnly() {
        static::addGlobalScope('user_id', function (Builder $builder) {
            $builder->where('user_id', '=', JWTAuth::parseToken()->toUser()->id);
        });
    }
}