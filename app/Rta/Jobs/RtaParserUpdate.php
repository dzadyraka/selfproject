<?php

namespace App\Rta\Jobs;

use App\Locations\Models\Country;
use App\Rta\Models\Rta;
use GuzzleHttp\Client;
use GuzzleHttp\Cookie\FileCookieJar;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Sunra\PhpSimple\HtmlDomParser;

class RtaParserUpdate implements ShouldQueue {

    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public function handle() {

        $filename = dirname(__FILE__) . '/1vsM.csv';
        $data = [];

        $lines = explode( "\n", file_get_contents( $filename ) );
        $headers = str_getcsv( array_shift( $lines ) );
        $data = [];
        foreach ( $lines as $line ) {
            $row = [];
            $partners = [];
            foreach ( str_getcsv( $line ) as $key => $value ) {
                $row[ $headers[ $key ] ] = $value;
                if ($key >= 8) {
                    if ($value) {
                        $partners[] = trim($value);
                    }
                }
            }
            Rta::where('name', $row['RTA Name'])->update([
                'countries_first' => json_encode([trim($row['Country'])]),
                'countries_second' => json_encode($partners)
            ]);
        }

//        var_dump($data);

//        collect($this->parseList())->each(function ($item, $key) {
//            $this->writeToDb($item);
//        });
//
//        collect($this->parseEarlyRta())->each(function ($item, $key) {
//            $this->writeToDb($item);
//        });
    }

    public function writeToDb($item) {

        $rta = Rta::updateOrCreate([
            'rta_id' => $item['rta_id']
        ], $item);

        $current_signatories = isset($item['current_signatories']) ? $item['current_signatories'] : "[]";

        foreach (json_decode($current_signatories) as $country) {
            $q = strtolower($country);
            $c = Country::where(function ($query) use ($q) {
                $query->whereTranslationInsensitiveLike('name', str_replace("'", "`", $q));
            })->first();
            if(!empty($c) && !$rta->countries()->exists($c->id)) {
                $rta->countries()->attach($c);
            }
        }
    }

    public function parseList(): array {

        $page_index = 1;

        $parsed_items = [];
        $items_count = $this->itemsCount();

        while ($page_index <= 10000) {
            if ($item = $this->parseAllPage($page_index)) {
                array_push($parsed_items, $item);
            }
            $page_index++;
        }

        return $parsed_items;
    }

    public function parseEarlyRta() {
        $page_index = 1;

        $parsed_items = [];
        $items_count = $this->earlyCount();

        while ($page_index <= 10000) {
            if ($item = $this->parseEarlyPage($page_index)) {
                array_push($parsed_items, $item);
            }
            $page_index++;
        }

        return $parsed_items;
    }

    public function earlyCount() {
        $url = 'http://rtais.wto.org/ui/PublicEARTAList.aspx';
        $count = 38;
        return $count;
    }

    public function itemsCount(): int {

        $url = 'http://rtais.wto.org/ui/PublicAllRTAList.aspx';
        $count = 308;

//        $dom = $this->connect($url);

//        $data = $dom->find('#ContentPlaceHolder1_showRelAggreement_RTAIdCardTabContainer_BasicInfoTbPanel_txtEngAgrName');

//        $count = explode(" ", $data)[2];
//        $count = intval(substr($count, 1, strlen($count) - 2));

        return $count;
    }

    public function connect($url, $id) {
        $user_agent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36';

        $tmpfname = dirname(__FILE__).'/cookie.txt';

        $jar = new FileCookieJar($tmpfname, TRUE);

        $client = new Client([
            'headers' => [
                'User-Agent' => $user_agent
            ],
            'allow_redirects' => [
                'max' => 100,
            ],
            'cookies' => $jar
        ]);

        try {
            $request = $client->get($url);
        } catch (\Exception $exception) {
            return false;
        }
//        var_dump($request->getStatusCode(), $id);
        if (in_array($request->getStatusCode(), [200, 201, 203])) {
            $content = $request->getBody();
            $dom = HtmlDomParser::str_get_html($content);
            return $dom;
        } else {
            return false;
        }
    }

    public function parseAllPage(int $id): ?array {
        $url = 'http://rtais.wto.org/UI/PublicShowRTAIDCard.aspx?rtaid=' . $id;
        return $this->parsePage($url, $id);
    }

    public function parseEarlyPage(int $id): ?array {
        $url = 'http://rtais.wto.org/ui/PublicShowEARTAIDCard.aspx?rtaid=' . $id;
        return $this->parsePage($url, $id);
    }

    public function parsePage(string $url, int $id): ?array {

        $dom = $this->connect($url, $id);

        $result = [];

        if ($dom && $dom->find('#ContentPlaceHolder1_showRelAggreement_RTAIdCardTabContainer_BasicInfoTbPanel_txtEngAgrName')) {

            $signature_date = trim($dom->find('#ContentPlaceHolder1_showRelAggreement_RTAIdCardTabContainer_BasicInfoTbPanel_txtGSD')[0]->plaintext);
            $notification_date = trim($dom->find('#ContentPlaceHolder1_showRelAggreement_RTAIdCardTabContainer_BasicInfoTbPanel_txtDON')[0]->plaintext);
            $entry_force_date = trim($dom->find('#ContentPlaceHolder1_showRelAggreement_RTAIdCardTabContainer_BasicInfoTbPanel_txtDOE')[0]->plaintext);
            $status = trim($dom->find('#ContentPlaceHolder1_showRelAggreement_RTAIdCardTabContainer_BasicInfoTbPanel_selStatus')[0]->plaintext);

            $result = [
                'rta_id' => $id,
                'name' => trim($dom->find('#ContentPlaceHolder1_showRelAggreement_RTAIdCardTabContainer_BasicInfoTbPanel_txtEngAgrName')[0]->plaintext),
                'coverage' => trim($dom->find('#ContentPlaceHolder1_showRelAggreement_RTAIdCardTabContainer_BasicInfoTbPanel_selCoverage')[0]->plaintext),
                'status' => $status,
                'notification_under' => trim($dom->find('#ContentPlaceHolder1_showRelAggreement_RTAIdCardTabContainer_BasicInfoTbPanel_selLegalcover')[0]->plaintext),
                'signature_date' => !empty($signature_date) ? $signature_date : null,
                'notification_date' => !empty($notification_date) ? $notification_date : null,
                'entry_force_date' => !empty($entry_force_date) ? $entry_force_date : null,
                'wto_members' => trim($dom->find('#ContentPlaceHolder1_showRelAggreement_RTAIdCardTabContainer_BasicInfoTbPanel_txtWTOMember')[0]->plaintext) == 'Yes',
                'type' => trim($dom->find('#ContentPlaceHolder1_showRelAggreement_RTAIdCardTabContainer_BasicInfoTbPanel_selType')[0]->plaintext),
            ];

            $current_signatories = explode('; ', $dom->find('#ContentPlaceHolder1_showRelAggreement_RTAIdCardTabContainer_BasicInfoTbPanel_lbltxtCurrSign')[0]->plaintext);
            if (count($current_signatories)) {
                $current_signatories[count($current_signatories) - 1] = rtrim($current_signatories[count($current_signatories) - 1]);
            }
            $result['current_signatories'] = json_encode($current_signatories);

            $original_signatories = explode('; ', $dom->find('#ContentPlaceHolder1_showRelAggreement_RTAIdCardTabContainer_BasicInfoTbPanel_lbltxtOrigSign')[0]->plaintext);
            if (count($original_signatories)) {
                $original_signatories[count($original_signatories) - 1] = rtrim($original_signatories[count($original_signatories) - 1]);
            }
            $result['original_signatories'] = json_encode($original_signatories);

            $rta_composition = explode('; ', $dom->find('#ContentPlaceHolder1_showRelAggreement_RTAIdCardTabContainer_BasicInfoTbPanel_txtComposition')[0]->plaintext);
            if (count($rta_composition)) {
                $rta_composition[count($rta_composition) - 1] = rtrim($rta_composition[count($rta_composition) - 1]);
            }
            $result['rta_composition'] = json_encode($rta_composition);

            $region = explode('; ', $dom->find('#ContentPlaceHolder1_showRelAggreement_RTAIdCardTabContainer_BasicInfoTbPanel_txtRegion')[0]->plaintext);
            if (count($region)) {
                $region[0] = ltrim($region[0]);
                $region[count($region) - 1] = rtrim($region[count($region) - 1]);
            }
            $result['region'] = json_encode($region);

            $web = $dom->find('#ContentPlaceHolder1_showRelAggreement_RTAIdCardTabContainer_BasicInfoTbPanel_fldOffWebAdd a');
            $web_addresses = [];
            foreach($web as $item) {
                array_push($web_addresses, [
                    'name' => trim($item->plaintext),
                    'url' => $item->href
                ]);
            }
            $result['web_addresses'] = json_encode($web_addresses);

            $agreement_links = $dom->find('#ContentPlaceHolder1_showRelAggreement_RTAIdCardTabContainer_BasicInfoTbPanel_fldTOA a');
            $agreement = [];

            foreach($agreement_links as $item) {
                if ($item->href) {
                    array_push($agreement, [
                        'name' => trim($item->plaintext),
                        'url' => $item->href
                    ]);
                }
            }
            $result['agreement'] = json_encode($agreement);

            return $result;
        } else {
            return null;
        }
    }
}
