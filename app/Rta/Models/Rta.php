<?php

namespace App\Rta\Models;

use App\Locations\Models\Country;
use Illuminate\Database\Eloquent\Model;

class Rta extends Model {

    public $timestamps = false;
    public $table = 'rta';

    protected $fillable = [
        'rta_id',
        'name',
        'coverage',
        'status',
        'notification_under',
        'signature_date',
        'notification_date',
        'entry_force_date',
        'current_signatories',
        'original_signatories',
        'rta_composition',
        'region',
        'wto_members',
        'type',
        'web_addresses',
        'agreement',
        'countries_first',
        'countries_second',
    ];

    public $casts = [
        'current_signatories' => 'json',
        'original_signatories' => 'json',
        'rta_composition' => 'json',
        'region' => 'json',
        'countries_first' => 'json',
        'countries_second' => 'json',
        'web_addresses' => 'json',
        'agreement' => 'json',
    ];

    protected $appends = [
        'total_turnover',
        'total_import',
        'total_export'
    ];

    public function countries() {
        return $this->belongsToMany(Country::class, 'rta_countries');
    }

    public function countries_first_side() {
        return $this->belongsToMany(Country::class, 'rta_countries_first');
    }

    public function countries_second_side() {
        return $this->belongsToMany(Country::class, 'rta_countries_second');
    }

    public function comtrades() {
        return $this->hasMany(RtaComtrade::class);
    }

    public function getTotalTurnoverAttribute(): int {
        return $this->comtrades->sum(function (RtaComtrade $comtrade) {
            return $comtrade->trade_value;
        });
    }

    public function getTotalImportAttribute(): int {
        return $this->comtrades->sum(function (RtaComtrade $comtrade) {
            return $comtrade->rg_desc == "Imports" ? $comtrade->trade_value : 0;
        });
    }

    public function getTotalExportAttribute(): int {
        return $this->comtrades->sum(function (RtaComtrade $comtrade) {
            return $comtrade->rg_desc == "Exports" ? $comtrade->trade_value : 0;
        });
    }
}

/**
 *  @SWG\Definition(
 *   definition="Rta",
 *   type="object",
 *   allOf={
 *       @SWG\Schema(
 *           @SWG\Property(property="rta_id", type="integer"),
 *           @SWG\Property(property="name", type="string"),
 *           @SWG\Property(property="coverage", type="string"),
 *           @SWG\Property(property="status", type="string"),
 *           @SWG\Property(property="notification_under", type="string"),
 *           @SWG\Property(property="signature_date", type="string", format="date"),
 *           @SWG\Property(property="notification_date", type="string", format="date"),
 *           @SWG\Property(property="entry_force_date", type="string", format="date"),
 *           @SWG\Property(property="current_signatories", type="array", @SWG\Items(type="string")),
 *           @SWG\Property(property="original_signatories", type="array", @SWG\Items(type="string")),
 *           @SWG\Property(property="rta_composition", type="array", @SWG\Items(type="string")),
 *           @SWG\Property(property="region", type="array", @SWG\Items(type="string")),
 *           @SWG\Property(property="wto_members", type="boolean"),
 *           @SWG\Property(property="type", type="string"),
 *           @SWG\Property(property="web_addresses", type="array", @SWG\Items(type="string")),
 *           @SWG\Property(property="agreement", type="array", @SWG\Items(type="string")),
 *           @SWG\Property(property="countries_first", type="array", @SWG\Items(type="string")),
 *           @SWG\Property(property="countries_second", type="array", @SWG\Items(type="string"))
 *       )
 *   }
 * )
 */
