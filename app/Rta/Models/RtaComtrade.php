<?php

namespace App\Rta\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class RtaComtrade extends Model {
    protected $table = 'rta_countries_first';

    public function newQuery() {
        return parent::newQuery()->from('comtrades')->select([
                'rta_countries_first.rta_id',
                'comtrades.country_id',
                'partner_id',
                'rg_desc',
                DB::raw('sum(trade_value) as trade_value')
            ])
            ->leftJoin('rta_countries_first', 'comtrades.country_id', '=', 'rta_countries_first.country_id')
            ->leftJoin('rta_countries_second', 'comtrades.partner_id', '=', 'rta_countries_second.country_id')
            ->whereRaw('rta_countries_first.rta_id = rta_countries_second.rta_id')
            ->groupBy(['rta_countries_first.rta_id', 'comtrades.country_id', 'partner_id', 'rg_desc']);
    }

}
