<?php

namespace App\Comtrade\Commands;


use App\Classifiers\Models\ClassifiersHs;
use GuzzleHttp\Client;
use Illuminate\Console\Command;

class HsSynchronize extends Command {
    protected $signature = 'hs:sync';
    protected $description = 'Sync HS classifiers with Comtrade database';

    const URL = 'https://comtrade.un.org/data/cache/classificationHS.json';

    private $_excepts = ['ALL', 'TOTAL', 'AG2', 'AG4', 'AG6'];

    public function handle() {
        $classifiers = json_decode((new Client())->get(self::URL)->getBody()->getContents(), true);

        foreach ($classifiers['results'] as $classifier) {
            if (in_array($classifier['id'], $this->_excepts)) {
                continue;
            }

            $classifierModel = ClassifiersHs::whereCode($classifier['id'])->first();
            if ($classifier['parent'] != 'TOTAL') {
                $parent = ClassifiersHs::whereCode($classifier['parent'])->firstOrFail();
                $parent->has_child = true;
                $parent->save();
            }

            if ($classifierModel) {
                $classifierModel->translate('en')->name = $classifier['text'];
                $classifierModel->level = strlen(trim($classifier['id'])) / 2;
                $classifierModel->parent_id = isset($parent) ? $parent->id : null;
                $classifierModel->save();
            } else {
                ClassifiersHs::create([
                    'name' => $classifier['text'],
                    'code' => $classifier['id'],
                    'level' => strlen($classifier['id']) / 2,
                    'parent_id' => isset($parent) ? $parent->id : null
                ]);
            }
        }
    }
}