<?php

namespace App\Comtrade\Models;

use App\Classifiers\Models\ClassifiersHs;
use App\Locations\Models\Country;
use Illuminate\Database\Eloquent\Model;

class Comtrade extends Model {

    protected $fillable = [
        'year','period','period_desc','aggr_level','is_leaf',
        'rg_code','rg_desc','country_id',
        'partner_id','classifiers_hs_id','cst_code',
        'cst_desc','mot_code','mot_desc',
        'qt_code','qt_desc','qt_alt_code',
        'qt_alt_desc','trade_quantity','alt_quantity',
        'net_weight','gross_weight','trade_value',
        'cif_value','fob_value','est_code', 'type',
        'hs_level', 'hs_code'
    ];

    protected $casts = [
        'turnover' => 'integer',
        'percent' => 'float',
    ];

    public function country() {
        return $this->hasOne(Country::class, 'id', 'country_id');
    }

    public function partner() {
        return $this->hasOne(Country::class, 'id', 'partner_id');
    }

    public function classifiersHs() {
        return $this->hasOne(ClassifiersHs::class, 'id', 'classifiers_hs_id');
    }
}
