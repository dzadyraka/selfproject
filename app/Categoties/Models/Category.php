<?php

namespace App\Categoties\Models;

use App\Http\Traits\Translatable;
use App\Products\Models\Product;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class Category extends Model {
    use Translatable;

    protected $fillable = [
        'slug', 'parent_id', 'level', 'position'
    ];

    public $translatedAttributes = ['name'];

    public function children() {
        return $this->hasMany(Category::class, 'parent_id', 'id');
    }

    public function products() {
        return $this->hasMany(Product::class);
    }

    public static function getList($ids = []) {
        $query = static::query();
        if (!empty($ids)) {
            $query->whereIn('id', $ids);
        }
        $data = $query->orderBy('position')->get()->toArray();

        $category = static::createHierarchy(
            static::_toParents(static::_toLevels($data)),
            0
        );

        return $category[0];
    }

    /**
     * Transforming data to levels.
     *
     * @param array $data
     * @return array
     */
    private static function _toLevels(array $data = []) {
        $levels = [];
        foreach ($data as $key => $datum) {
            $levels[$datum['level']][] = $datum;
        }
        return $levels;
    }

    /**
     * Creating hash map with parent_id => children.
     *
     * @param array $levels
     *
     * @return array
     */
    private static function _toParents(array $levels = []) {
        $data = [];
        foreach ($levels as $index => $level) {
            $data[$index] = [];
            foreach ($level as $key => $item) {
                $parent_id = isset($item['parent_id']) ? $item['parent_id'] : 0;
                if (empty($data[$index][$parent_id])) {
                    $data[$index][$parent_id] = [];
                }
                $data[$index][$parent_id][] = $item;
            }
        }
        return $data;
    }

    /**
     * Create child hierarchy uspsc classifiers
     *
     * @param $levels
     * @param int $level
     *
     * @return mixed
     */
    private static function createHierarchy($levels, $level = 0) {
        foreach ($levels[$level][0] as $key => $item) {
            $levels[$level][0][$key]['children'] = [];
            if (!empty($levels[$level + 1][$item['id']])) {
                $levels[$level][0][$key]['children'] = static::fetchHierarchyChildren($levels[1 + $level][$item['id']], $levels, 2 + $level);
            }
        }
        return $levels[$level];
    }

    /**
     * Adding children to hierarchy
     *
     * @param array $items
     * @param array $levels
     * @param int $level
     *
     * @return array
     */
    private static function fetchHierarchyChildren(array $items = [], array $levels = [], $level = 0) {
        foreach ($items as $key => $item) {
            $items[$key]['children'] = [];
            if (!empty($levels[$level][$item['id']])) {
                $items[$key]['children'] = static::fetchHierarchyChildren($levels[$level][$item['id']], $levels, $level + 1);
            }
        }
        return $items;
    }
}

/**
 * @SWG\Definition(
 *   definition="Category",
 *   type="object",
 *   allOf={
 *       @SWG\Schema(
 *           @SWG\Property(property="name", type="string"),
 *           @SWG\Property(property="slug", type="string"),
 *           @SWG\Property(property="parent_id", type="integer"),
 *           @SWG\Property(property="level", type="integer"),
 *           @SWG\Property(property="position", type="integer"),
 *           @SWG\Property(property="categories_id", type="integer"),
 *           @SWG\Property(property="language_id", type="integer"),
 *           @SWG\Property(
 *              property="children",
 *              type="array",
 *              @SWG\Items(ref="#/definitions/CategoryChildren")
 *          ),
 *       )
 *   }
 * )
 */

/**
 * @SWG\Definition(
 *   definition="CategoryChildren",
 *   type="object",
 *   allOf={
 *       @SWG\Schema(
 *           @SWG\Property(property="name", type="string"),
 *           @SWG\Property(property="slug", type="string"),
 *           @SWG\Property(property="parent_id", type="integer"),
 *           @SWG\Property(property="level", type="integer"),
 *           @SWG\Property(property="position", type="integer"),
 *           @SWG\Property(property="categories_id", type="integer"),
 *           @SWG\Property(property="language_id", type="integer")
 *       )
 *   }
 * )
 */