<?php

namespace App\Categoties\Models;

use Illuminate\Database\Eloquent\Model;

class CategoryTranslation extends Model {
    public $timestamps = false;
    protected $fillable = ['name'];
}
