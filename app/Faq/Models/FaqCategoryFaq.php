<?php

namespace App\Faq\Models;


use Illuminate\Database\Eloquent\Model;

class FaqCategoryFaq extends Model {
	public $timestamps = false;
}