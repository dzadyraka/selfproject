<?php

namespace App\Faq\Models;

use App\Http\Traits\Translatable;
use Illuminate\Database\Eloquent\Model;

class Faq extends Model {
	use Translatable;

	const CREATED_AT = 'created_date';
	const UPDATED_AT = 'edited_date';

	public $translatedAttributes = ['name', 'content', 'description', 'meta_title', 'meta_keywords', 'meta_description'];

	protected $fillable = ['is_published', 'is_featured'];

	public function sections() {
		return $this->belongsToMany(FaqCategory::class, 'faq_category_faq');
	}
}
