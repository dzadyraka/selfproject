<?php

namespace App\Faq\Models;


use App\Http\Traits\Translatable;
use Illuminate\Database\Eloquent\Model;

class FaqCategory extends Model {
	use Translatable;

	const CREATED_AT = 'created_date';
	const UPDATED_AT = 'edited_date';

	public $translatedAttributes = ['name'];

	protected $fillable = ['alias', 'position', 'is_active'];
}