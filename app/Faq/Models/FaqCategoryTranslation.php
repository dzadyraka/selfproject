<?php

namespace App\Faq\Models;


use Illuminate\Database\Eloquent\Model;

class FaqCategoryTranslation extends Model {
	public $timestamps = false;

	protected $fillable = ['name'];
}