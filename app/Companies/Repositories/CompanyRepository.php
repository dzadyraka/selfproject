<?php

namespace App\Companies\Repositories;


use App\Companies\Models\Company;
use App\Repositories\Repository;

class CompanyRepository extends Repository {
    public function __construct(Company $model) {
        parent::__construct($model);
    }

    public function isDomainExists(string $domain): bool {
        return $this->getModel()->whereSubDomain($domain)->exists();
    }
}