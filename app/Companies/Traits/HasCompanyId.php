<?php

namespace App\Companies\Traits;

use App\Companies\Models\Company;

trait HasCompanyId {
	public function company() {
		return $this->belongsTo(Company::class);
	}
}