<?php

namespace App\Companies\Models;

use Illuminate\Database\Eloquent\Model;

class CompanyTranslation extends Model {
    public $timestamps = false;
    protected $fillable = ['name', 'description'];
}
