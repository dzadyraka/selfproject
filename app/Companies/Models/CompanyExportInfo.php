<?php

namespace App\Companies\Models;

use App\Companies\Traits\HasCompanyId;
use App\Locations\Traits\HasCountryId;
use Illuminate\Database\Eloquent\Model;

class CompanyExportInfo extends Model {
    use HasCompanyId;
    use HasCountryId;

    public $table = 'company_export_info';
    public $timestamps = false;

    protected $fillable = [
        'title', 'company_id', 'country_id', 'city_id', 'port'
    ];
}

/**
 *  @SWG\Definition(
 *   definition="newExportInfo",
 *   type="object",
 *   required={"title", "company_id", "country_id", "city_id", "port"},
 *   allOf={
 *       @SWG\Schema(
 *           @SWG\Property(property="title", type="string"),
 *           @SWG\Property(property="company_id", type="integer"),
 *           @SWG\Property(property="country_id", type="integer"),
 *           @SWG\Property(property="city_id", type="integer"),
 *           @SWG\Property(property="port", type="string"),
 *       )
 *   }
 * )
 */

/**
 *  @SWG\Definition(
 *   definition="ExportInfo",
 *   type="object",
 *   allOf={
 *       @SWG\Schema(
 *           @SWG\Property(property="title", type="string"),
 *           @SWG\Property(property="company_id", type="integer"),
 *           @SWG\Property(property="country_id", type="integer"),
 *           @SWG\Property(property="city_id", type="integer"),
 *           @SWG\Property(property="port", type="string"),
 *           @SWG\Property(property="country", ref="#/definitions/Country"),
 *           @SWG\Property(property="city", ref="#/definitions/City"),
 *           @SWG\Property(property="company", ref="#/definitions/Company"),
 *       )
 *   }
 * )
 */
