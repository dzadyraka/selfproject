<?php

namespace App\Companies\Models;

use App\Companies\Traits\HasCompanyId;
use App\Locations\Traits\HasCountryId;
use Illuminate\Database\Eloquent\Model;

class CompanyAddress extends Model {

    use HasCompanyId;
    use HasCountryId;

    public $timestamps = false;

    protected $fillable = [
        'company_id', 'region_id', 'city_id', 'country_id', 'street_name',
        'street_data', 'zip_code', 'first_name', 'last_name', 'type'
    ];
}

/**
 * @SWG\Definition(
 *   definition="newCompanyAddress",
 *   type="object",
 *   required={"company_id", "city_id", "country_id", "street_name", "street_data", "zip_code", "first_name", "last_name", "type"},
 *   allOf={
 *       @SWG\Schema(
 *           @SWG\Property(property="company_id", type="integer"),
 *           @SWG\Property(property="region_id", type="integer"),
 *           @SWG\Property(property="city_id", type="integer"),
 *           @SWG\Property(property="country_id", type="integer"),
 *           @SWG\Property(property="street_name", type="string"),
 *           @SWG\Property(property="street_data", type="string"),
 *           @SWG\Property(property="zip_code", type="integer"),
 *           @SWG\Property(property="first_name", type="string"),
 *           @SWG\Property(property="last_name", type="string"),
 *           @SWG\Property(property="type", type="string", description="can be shipping or billing"),
 *       )
 *   }
 * )
 */

/**
 * @SWG\Definition(
 *   definition="CompanyAddress",
 *   type="object",
 *   allOf={
 *       @SWG\Schema(
 *           @SWG\Property(property="company_id", type="integer"),
 *           @SWG\Property(property="region_id", type="integer"),
 *           @SWG\Property(property="city_id", type="integer"),
 *           @SWG\Property(property="country_id", type="integer"),
 *           @SWG\Property(property="street_name", type="string"),
 *           @SWG\Property(property="street_data", type="string"),
 *           @SWG\Property(property="zip_code", type="integer"),
 *           @SWG\Property(property="first_name", type="string"),
 *           @SWG\Property(property="last_name", type="string"),
 *           @SWG\Property(property="type", type="string", description="can be shipping or billing"),
 *           @SWG\Property(property="country", ref="#/definitions/Country"),
 *           @SWG\Property(property="city", ref="#/definitions/City"),
 *           @SWG\Property(property="company", ref="#/definitions/Company"),
 *       )
 *   }
 * )
 */