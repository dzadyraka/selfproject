<?php

namespace App\Companies\Models;

use App\Http\Traits\Crypt;
use Illuminate\Database\Eloquent\Model;

class CompanyPayment extends Model {

    use Crypt;

    protected $fillable = [
        'provider',
        'company_id',
        'credentials',
        'active'
    ];

    protected $casts = [
        'credentials' => 'json'
    ];

    protected $hidden = [
        'credentials'
    ];

    public function encrypt(): array {
        return [ 'credentials' ];
    }

    public function company() {
        return $this->belongsTo(Company::class, 'company_id');
    }
}
