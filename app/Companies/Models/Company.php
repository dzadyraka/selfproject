<?php

namespace App\Companies\Models;

use App\Users\Models\User;
use App\Http\Traits\Filter;
use App\Orders\Models\Order;
use App\Http\Traits\Translatable;
use App\Locations\Traits\HasCityId;
use Illuminate\Support\Facades\Event;
use App\Locations\Traits\HasRegionId;
use App\Locations\Traits\HasCountryId;
use Illuminate\Database\Eloquent\Model;

class Company extends Model {

    use Filter;
    use Translatable;
    use HasCountryId;
    use HasCityId;
    use HasRegionId;

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'updated_date';

    public $translatedAttributes = ['name', 'description'];

    protected $fillable = [
        'country_id', 'region_id', 'city_id', 'phone', 'website', 'email',
        'logo', 'cpv_codes', 'is_approved', 'number_of_employees',
        'attachments', 'certificates', 'rating', 'registration_number',
        'business_type', 'year_established', 'authorized_products',
        'production_information', 'trade_capacity', 'annual_revenue', 'images',
        'import_trade_capacity', 'export_trade_capacity', 'brand', 'first_name',
        'last_name', 'interested_in', 'reseller', 'business_entity',
        'address', 'postal_code', 'number_of_employees_distribution', 'annual_turnover',
        'crm_id', 'language_id', 'is_draft', 'languages',
        'business_type_other', 'business_entity_other', 'social_links', 'authorized_persons', 'port_id',
        'sub_domain'
    ];

    protected $casts = [
        'cpv_codes' => 'json',
        'attachments' => 'json',
        'certificates' => 'json',
        'authorized_products' => 'json',
        'production_information' => 'json',
        'trade_capacity' => 'json',
        'images' => 'json',
        'import_trade_capacity' => 'json',
        'export_trade_capacity' => 'json',
        'phone' => 'json',
        'email' => 'json',
        'social_links' => 'json',
        'authorized_persons' => 'json',
        'languages' => 'json'
    ];

    public function users() {
        return $this->belongsToMany(User::class)->withPivot('position', 'access');
    }

    public function orders_created() {
        return $this->hasMany(Order::class, 'customer_company_id', 'id');
    }

    public function orders() {
        return $this->hasMany(Order::class);
    }

    public function legal_address() {
        return $this->hasOne(CompanyAddress::class)->where('type', '=', 'legal');
    }

    public function trade_address() {
        return $this->hasOne(CompanyAddress::class)->where('type', '=', 'trade');
    }

    public function payments() {
        return $this->hasMany(CompanyPayment::class);
    }

    public static function boot() {
        parent::boot();

        static::created(function ($item) {
            Event::fire('company.create', $item);
        });

        static::updated(function ($item) {
            Event::fire('company.update', $item);
        });
    }
}

/**
 * @SWG\Definition(
 *   definition="newCompany",
 *   type="object",
 *   required={"name", "description", "country_id", "city_id", "phone", "email", "business_type"},
 *   allOf={
 *       @SWG\Schema(
 *           @SWG\Property(property="name", type="string"),
 *           @SWG\Property(property="description", type="string"),
 *           @SWG\Property(property="trade_name", type="string"),
 *           @SWG\Property(property="production_description", type="string"),
 *           @SWG\Property(property="country_id", type="integer"),
 *           @SWG\Property(property="region_id", type="integer"),
 *           @SWG\Property(property="city_id", type="integer"),
 *           @SWG\Property(
 *                  property="phone",
 *                  type="array",
 *                  @SWG\Items(
 *                      @SWG\Property(property="number", type="integer"),
 *                  ),
 *           ),
 *           @SWG\Property(property="website", type="string"),
 *           @SWG\Property(property="email", type="string"),
 *           @SWG\Property(property="cpv_codes", type="string"),
 *           @SWG\Property(property="business_type", type="string"),
 *           @SWG\Property(property="created_date", type="string", format="date-time"),
 *           @SWG\Property(property="edited_date", type="string", format="date-time"),
 *           @SWG\Property(property="is_approved", type="boolean"),
 *           @SWG\Property(property="registration_number", type="string"),
 *           @SWG\Property(property="year_established", type="integer"),
 *           @SWG\Property(property="number_of_employees", type="string"),
 *           @SWG\Property(property="annual_revenue", type="string"),
 *           @SWG\Property(
 *                  property="attachments",
 *                  type="array",
 *                  @SWG\Items(
 *                      @SWG\Property(property="name", type="string"),
 *                      @SWG\Property(property="type", type="string"),
 *                      @SWG\Property(property="url", type="string"),
 *                  ),
 *           ),
 *           @SWG\Property(
 *                  property="certificates",
 *                  type="array",
 *                  @SWG\Items(
 *                      @SWG\Property(property="name", type="string"),
 *                      @SWG\Property(property="type", type="string"),
 *                      @SWG\Property(property="url", type="string"),
 *                  ),
 *           ),
 *           @SWG\Property(property="rating", type="integer"),
 *           @SWG\Property(
 *                  property="trade_capacity",
 *                  type="array",
 *                  @SWG\Items(
 *                      @SWG\Property(property="data", type="array", @SWG\Items(@SWG\Property(property="country_id", type="integer"),@SWG\Property(property="percentage", type="integer"))),
 *                      @SWG\Property(property="number_of_employees", type="integer"),
 *                  ),
 *           ),
 *           @SWG\Property(
 *                  property="production_information",
 *                  type="array",
 *                  @SWG\Items(
 *                      @SWG\Property(property="country_id", type="integer"),
 *                      @SWG\Property(property="city_id", type="integer"),
 *                      @SWG\Property(property="description", type="string"),
 *                      @SWG\Property(property="number_of_employees", type="integer"),
 *                  ),
 *           ),
 *           @SWG\Property(
 *                  property="authorized_products",
 *                  type="array",
 *                  @SWG\Items(
 *                      @SWG\Property(property="city_id", type="string"),
 *                      @SWG\Property(property="type", type="string"),
 *                      @SWG\Property(property="date", type="array", @SWG\Items(@SWG\Property(property="from", type="string", format="date-time"),@SWG\Property(property="to", type="string", format="date-time"))),
 *                      @SWG\Property(property="trade_name", type="string"),
 *                  ),
 *           )
 *       )
 *   }
 * )
 */

/**
 * @SWG\Definition(
 *   definition="Company",
 *   type="object",
 *   allOf={
 *       @SWG\Schema(
 *           @SWG\Property(property="name", type="string"),
 *           @SWG\Property(property="description", type="string"),
 *           @SWG\Property(property="trade_name", type="string"),
 *           @SWG\Property(property="production_description", type="string"),
 *           @SWG\Property(property="country_id", type="integer"),
 *           @SWG\Property(property="region_id", type="integer"),
 *           @SWG\Property(property="city_id", type="integer"),
 *           @SWG\Property(
 *                  property="phone",
 *                  type="array",
 *                  @SWG\Items(
 *                      @SWG\Property(property="number", type="integer"),
 *                  ),
 *           ),
 *           @SWG\Property(property="website", type="string"),
 *           @SWG\Property(property="email", type="string"),
 *           @SWG\Property(property="cpv_codes", type="string"),
 *           @SWG\Property(property="business_type", type="string"),
 *           @SWG\Property(property="created_date", type="datetime"),
 *           @SWG\Property(property="edited_date", type="datetime"),
 *           @SWG\Property(property="is_approved", type="boolean"),
 *           @SWG\Property(property="registration_number", type="string"),
 *           @SWG\Property(property="year_established", type="integer"),
 *           @SWG\Property(property="number_of_employees", type="string"),
 *           @SWG\Property(property="annual_revenue", type="string"),
 *           @SWG\Property(
 *                  property="attachments",
 *                  type="array",
 *                  @SWG\Items(
 *                      @SWG\Property(property="name", type="string"),
 *                      @SWG\Property(property="type", type="string"),
 *                      @SWG\Property(property="url", type="string"),
 *                  ),
 *           ),
 *           @SWG\Property(
 *                  property="certificates",
 *                  type="array",
 *                  @SWG\Items(
 *                      @SWG\Property(property="name", type="string"),
 *                      @SWG\Property(property="type", type="string"),
 *                      @SWG\Property(property="url", type="string"),
 *                  ),
 *           ),
 *           @SWG\Property(property="rating", type="integer"),
 *           @SWG\Property(
 *                  property="trade_capacity",
 *                  type="array",
 *                  @SWG\Items(
 *                      @SWG\Property(property="data", type="array", @SWG\Items(@SWG\Property(property="country_id", type="integer"),@SWG\Property(property="percentage", type="integer"))),
 *                      @SWG\Property(property="number_of_employees", type="integer"),
 *                  ),
 *           ),
 *           @SWG\Property(
 *                  property="production_information",
 *                  type="array",
 *                  @SWG\Items(
 *                      @SWG\Property(property="country_id", type="integer"),
 *                      @SWG\Property(property="city_id", type="integer"),
 *                      @SWG\Property(property="description", type="string"),
 *                      @SWG\Property(property="number_of_employees", type="integer"),
 *                  ),
 *           ),
 *           @SWG\Property(
 *                  property="authorized_products",
 *                  type="array",
 *                  @SWG\Items(
 *                      @SWG\Property(property="city_id", type="string"),
 *                      @SWG\Property(property="type", type="string"),
 *                      @SWG\Property(property="date", type="array", @SWG\Items(@SWG\Property(property="from", type="datetime"),@SWG\Property(property="to", type="datetime"))),
 *                      @SWG\Property(property="trade_name", type="string"),
 *                  ),
 *           ),
 *           @SWG\Property(
 *                  property="company_addresses",
 *                  type="array",
 *                  @SWG\Items(ref="#/definitions/CompanyAddress"),
 *           ),
 *           @SWG\Property(
 *                  property="export_info",
 *                  type="array",
 *                  @SWG\Items(ref="#/definitions/ExportInfo"),
 *           ),
 *           @SWG\Property(
 *                  property="delivery_and_payment",
 *                  type="array",
 *                  @SWG\Items(ref="#/definitions/DeliveryAndPayment"),
 *           ),
 *       )
 *   }
 * )
 */

/**
 * @SWG\Definition(
 *   definition="CompanyUsers",
 *   type="object",
 *   allOf={
 *       @SWG\Schema(
 *           @SWG\Property(
 *                  property="users",
 *                  type="array",
 *                  @SWG\Items(
 *                      @SWG\Property(property="id", type="integer"),
 *                      @SWG\Property(property="email", type="string"),
 *                      @SWG\Property(property="phone", type="integer"),
 *                      @SWG\Property(property="country_id", type="integer"),
 *                      @SWG\Property(property="language_id", type="integer"),
 *                      @SWG\Property(property="created_date", type="datetime"),
 *                      @SWG\Property(property="edited_date", type="datetime"),
 *                      @SWG\Property(property="is_active", type="boolean"),
 *                      @SWG\Property(property="is_approved", type="boolean"),
 *                      @SWG\Property(property="is_blocked", type="boolean"),
 *                      @SWG\Property(property="company_id", type="integer"),
 *                      @SWG\Property(property="user_id", type="integer"),
 *                      @SWG\Property(property="position", type="string"),
 *                      @SWG\Property(property="first_name", type="string"),
 *                      @SWG\Property(property="last_name", type="string"),
 *                  ),
 *           ),
 *       )
 *   }
 * )
 */