<?php

namespace App\Companies\Models;

use App\Companies\Traits\HasCompanyId;
use Illuminate\Database\Eloquent\Model;

class CompanyDeliveryAndPayment extends Model {
    use HasCompanyId;

    public $timestamps = false;

    protected $fillable = [
        'title', 'company_id', 'delivery_type', 'shipping_services', 'payment_terms'
    ];

    protected $casts = [
        'delivery_type' => 'array',
        'shipping_services' => 'array',
        'payment_terms' => 'array',
    ];

}

/**
 *  @SWG\Definition(
 *   definition="newDeliveryAndPayment",
 *   type="object",
 *   required={"title", "company_id", "country_id", "delivery_type", "shipping_services", "payment_terms"},
 *   allOf={
 *       @SWG\Schema(
 *           @SWG\Property(property="title", type="string"),
 *           @SWG\Property(property="company_id", type="integer"),
 *           @SWG\Property(
 *                  property="delivery_type",
 *                  type="array",
 *                  @SWG\Items(
 *                      @SWG\Property(property="delivery_name", description="Can be Sea, Air, etc."),
 *                  ),
 *           ),
 *           @SWG\Property(
 *                  property="shipping_services",
 *                  type="array",
 *                  @SWG\Items(
 *                      @SWG\Property(property="shipping_services_name", description="Can be Sea, Air, etc."),
 *                  ),
 *           ),
 *           @SWG\Property(
 *                  property="payment_terms",
 *                  type="array",
 *                  @SWG\Items(
 *                      @SWG\Property(property="payment_terms_name", description="Can be Sea, Air, etc."),
 *                  ),
 *           ),
 *       )
 *   }
 * )
 */

/**
 *  @SWG\Definition(
 *   definition="DeliveryAndPayment",
 *   type="object",
 *   allOf={
 *       @SWG\Schema(
 *           @SWG\Property(property="title", type="string"),
 *           @SWG\Property(property="company_id", type="integer"),
 *           @SWG\Property(
 *                  property="delivery_type",
 *                  type="array",
 *                  @SWG\Items(
 *                      @SWG\Property(property="delivery_name", description="Can be Sea, Air, etc."),
 *                  ),
 *           ),
 *           @SWG\Property(
 *                  property="shipping_services",
 *                  type="array",
 *                  @SWG\Items(
 *                      @SWG\Property(property="shipping_name", description="Can be DHL, Fedex, etc."),
 *                  ),
 *           ),
 *           @SWG\Property(
 *                  property="payment_terms",
 *                  type="array",
 *                  @SWG\Items(
 *                      @SWG\Property(property="payment_terms_name", description="Can be Escrow, etc."),
 *                  ),
 *           ),
 *       )
 *   }
 * )
 * )
 */