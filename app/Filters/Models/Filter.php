<?php

namespace App\Filters\Models;

use JWTAuth;
use App\Http\Traits\HasUserId;
use App\Http\Traits\FilterUserOnly;
use Illuminate\Database\Eloquent\Model;

abstract class Filter extends Model {

    use HasUserId;
    use FilterUserOnly;

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'updated_date';

    protected $fillable = [
        'name', 'user_id', 'filter'
    ];

    protected $casts = [
        'filter' => 'array'
    ];

}

/**
 *  @SWG\Definition(
 *   definition="newFilter",
 *   type="object",
 *   required={"name", "filter"},
 *   allOf={
 *       @SWG\Schema(
 *           @SWG\Property(property="title", type="string"),
 *           @SWG\Property(
 *                  property="filter",
 *                  type="object",
 *           ),
 *       )
 *   }
 * )
 */

/**
 *  @SWG\Definition(
 *   definition="Filter",
 *   type="object",
 *   allOf={
 *       @SWG\Schema(
 *           @SWG\Property(property="title", type="string"),
 *           @SWG\Property(property="user_id", type="integer"),
 *           @SWG\Property(
 *                  property="filter",
 *                  type="object",
 *           ),
 *           @SWG\Property(property="created_date", type="string", format="date-time"),
 *           @SWG\Property(property="edited_date", type="string", format="date-time"),
 *       )
 *   }
 * )
 */
