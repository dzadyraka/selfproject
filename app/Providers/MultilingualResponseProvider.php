<?php

namespace App\Providers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\ServiceProvider;
use Teto\HTTP\AcceptLanguage;

class MultilingualResponseProvider extends ServiceProvider {
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot() {
        Response::macro('mjson', function ($value, int $status = 200) {
            list($_, $lang) = AcceptLanguage::parse(request()->header('Accept-Language', 'en'));

            if ($lang['language'] == '*') { // It's not wildcart
                $response = [];
                $languages = app()->make('config')->get('translatable.locales');

                foreach ($languages as $language) {
                    if ($value instanceof Model) {
                        App::setLocale($language);
                        $response[$language] = $value->toArray();
                    } else {
                        $response[$language] = $value;
                    }
                }

                return Response::json(
                    $response,
                    $status
                );
            }

            return Response::json(
                $value,
                $status
            );
        });
    }
}
