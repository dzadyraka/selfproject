<?php

namespace App\Providers;

use App\Social\Events\SocialRegisterEvent;
use App\Social\Events\SocialValidateEvent;
use App\Social\Listeners\SocialRegisterListener;
use App\Social\Listeners\SocialValidateListener;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider {
	/**
	 * The event listener mappings for the application.
	 *
	 * @var array
	 */
	protected $listen = [
		SocialRegisterEvent::class => [
			SocialRegisterListener::class
		],
		SocialValidateEvent::class => [
			SocialValidateListener::class
		],
        'company.create' => [
            'App\Events\HubSpotEvent@hubSpotCompanyCreated'
        ],
        'contact.create' => [
            'App\Events\HubSpotEvent@hubSpotContactCreatedOrUpdate'
        ],
        'company.update' => [
            'App\Events\HubSpotEvent@hubSpotCompanyUpdate'
        ]
	];

	/**
	 * Register any events for your application.
	 *
	 * @return void
	 */
	public function boot() {
		parent::boot();

		//
	}
}
