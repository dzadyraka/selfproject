<?php

namespace App\Providers;


use Illuminate\Http\Response;
use Illuminate\Support\ServiceProvider;

// TODO: Implement

class XlsResponseProvider extends ServiceProvider {

    public function boot() {
        Response::macro('xls', function (string $file_name, array $data_items, int $status = 200) {
            $callback = function () use ($data_items) {
                $file = fopen('php://output', 'w');

                foreach ($data_items as $item) {
                    fputcsv($file, $item);
                }

                fclose($file);
            };

            return Response::stream($callback, $status, $this->getHeaders($file_name));
        });
    }

    private function getHeaders(string $file_name) {
        return [
            'Content-type' => 'text/csv',
            'Content-Disposition' => "attachment; filename={$file_name}.csv",
            'Pragma' => 'no-cache',
            'Cache-Control' => 'must-revalidate, post-check=0, pre-check=0',
            'Expires' => 0
        ];
    }
}