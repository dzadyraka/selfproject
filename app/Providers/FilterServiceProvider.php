<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Builder;
use App\Http\Traits\Filter;

class FilterServiceProvider extends ServiceProvider {
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot() {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register() {
        Filter::resolver(function (Builder $model) {
            if ($this->app['request']->input('offset')) {
                $model->skip($this->app['request']->input('offset'));
            }

            if ($this->app['request']->input('limit')) {
                $model->take($this->app['request']->input('limit'));
            }

            if($this->app['request']->input('sort')) {
                foreach ($this->app['request']->input('sort') as $col => $sort) {
                    $model->orderBy($col, $sort);
                }
            }
        });
    }
}
