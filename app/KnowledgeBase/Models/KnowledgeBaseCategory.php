<?php

namespace App\KnowledgeBase\Models;


use App\Http\Traits\Translatable;
use Illuminate\Database\Eloquent\Model;

class KnowledgeBaseCategory extends Model {
	use Translatable;

	const CREATED_AT = 'created_date';
	const UPDATED_AT = 'edited_date';

	public $translatedAttributes = ['name'];

	protected $fillable = ['alias', 'position', 'is_active'];

	public function articles() {
		return $this->hasMany(KnowledgeBaseArticle::class);
	}

	public function sub_sections() {
		return $this->hasMany(KnowledgeBaseSubCategory::class);
	}

	public function for_whom() {
		return $this->hasMany(KnowledgeBaseForWhom::class);
	}
}