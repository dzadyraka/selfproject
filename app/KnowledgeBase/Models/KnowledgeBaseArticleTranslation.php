<?php

namespace App\KnowledgeBase\Models;


use Illuminate\Database\Eloquent\Model;

class KnowledgeBaseArticleTranslation extends Model {
    public $timestamps = false;
    protected $fillable = [
        'name',
        'content',
        'description',
        'meta_keywords',
        'meta_description',
        'meta_title'
    ];

    protected $appends = ['description'];

    public function getDescriptionAttribute() {
        $value = $this->content;
        $item = '';
        if (empty($value)) {
            $item = substr(strip_tags($value), 0, 200);

            if (strlen(strip_tags($value)) > 200) {
                $item .= '...';
            }

        } elseif (strlen($value) > 200) {
            $item = preg_replace('/\s\s+/', '', substr(strip_tags($value), 0, 200) . '...');
        }

        return $item;
    }
}