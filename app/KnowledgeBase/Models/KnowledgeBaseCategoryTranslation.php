<?php

namespace App\KnowledgeBase\Models;


use Illuminate\Database\Eloquent\Model;

class KnowledgeBaseCategoryTranslation extends Model {
	public $timestamps = false;
	protected $fillable = ['name'];
}