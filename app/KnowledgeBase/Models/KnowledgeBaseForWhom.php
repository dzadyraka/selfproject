<?php

namespace App\KnowledgeBase\Models;


use App\Http\Traits\Translatable;
use Illuminate\Database\Eloquent\Model;

class KnowledgeBaseForWhom extends Model {
	use Translatable;

	const CREATED_AT = 'created_date';
	const UPDATED_AT = 'edited_date';

	public $translatedAttributes = ['name'];

	protected $fillable = ['alias', 'position', 'is_active', 'section_id'];

	public function articles() {
		return $this->belongsToMany(KnowledgeBaseArticle::class);
	}

	public function section() {
		return $this->belongsTo(KnowledgeBaseCategory::class, 'section_id');
	}
}