<?php

namespace App\KnowledgeBase\Models;


use App\Http\Traits\Translatable;
use Illuminate\Database\Eloquent\Model;

class KnowledgeBaseArticle extends Model {
	use Translatable;

	const CREATED_AT = 'created_date';
	const UPDATED_AT = 'edited_date';

	public $translatedAttributes = [
		'name',
		'content',
		'description',
		'meta_keywords',
		'meta_description',
		'meta_title'
	];

	protected $fillable = [
		'post_type',
		'section_id',
		'is_published',
		'is_featured'
	];

	public function sub_sections() {
		return $this->belongsToMany(KnowledgeBaseSubCategory::class, 'knowledge_base_sub_category_knowledge_base_article');
	}

	public function for_whom() {
		return $this->belongsToMany(KnowledgeBaseForWhom::class, 'knowledge_base_for_whom_knowledge_base_article');
	}

	public function sections() {
		return $this->belongsTo(KnowledgeBaseCategory::class, 'section_id');
	}
}