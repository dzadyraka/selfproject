<?php

namespace App\KnowledgeBase\Models;


use Illuminate\Database\Eloquent\Model;

class KnowledgeBaseForWhomTranslation extends Model {
	public $timestamps = false;
	protected $fillable = ['name'];
	public $table = 'knowledge_base_for_whoms_translations';
}