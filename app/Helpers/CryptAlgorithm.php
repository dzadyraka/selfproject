<?php

namespace App\Helpers;

use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Contracts\Encryption\EncryptException;

class CryptAlgorithm {

    /**
     * Encrypt data
     *
     * @param $value
     * @param bool $serialize
     * @return string
     * @throws \Exception
     */
    static function encrypt($value, $serialize = true) {
        $iv = random_bytes(openssl_cipher_iv_length('AES-256-CBC'));
        $key = config('app.key');

        // First we will encrypt the value using OpenSSL. After this is encrypted we
        // will proceed to calculating a MAC for the encrypted value so that this
        // value can be verified later as not having been changed by the users.
        $value = \openssl_encrypt(
            $serialize ? serialize($value) : $value,
            'AES-256-CBC', $key, 0, $iv
        );

        if ($value === false) {
            throw new EncryptException('Could not encrypt the data.');
        }

        // Once we get the encrypted value we'll go ahead and base64_encode the input
        // vector and create the MAC for the encrypted value so we can then verify
        // its authenticity. Then, we'll JSON the data into the "payload" array.
        $mac = self::hash($iv = base64_encode($iv), $value);

        $json = json_encode(compact('iv', 'value', 'mac'));

        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new EncryptException('Could not encrypt the data.');
        }

        return base64_encode($json);
    }

    /**
     * Decrypt data
     *
     * @param $payload
     * @param bool $unserialize
     * @return mixed|string
     * @throws \Exception
     */
    public static function decrypt(string $payload, $unserialize = true) {
        $payload = self::getJsonPayload($payload);

        $iv = base64_decode($payload['iv']);
        $key = config('app.key');

        // Here we will decrypt the value. If we are able to successfully decrypt it
        // we will then unserialize it and return it out to the caller. If we are
        // unable to decrypt this value we will throw out an exception message.
        $decrypted = \openssl_decrypt(
            $payload['value'], 'AES-256-CBC', $key, 0, $iv
        );

        if ($decrypted === false) {
            throw new DecryptException('Could not decrypt the data.');
        }

        return $unserialize ? unserialize($decrypted) : $decrypted;
    }

    /**
     * Create a MAC for the given value.
     *
     * @param  string  $iv
     * @param  mixed  $value
     * @return string
     */
    protected static function hash($iv, $value) {
        return hash_hmac('sha256', $iv.$value, env('APP_KEY'));
    }

    /**
     * @param $payload
     * @return mixed
     * @throws \Exception
     */
    protected static function getJsonPayload($payload) {
        $payload = json_decode(base64_decode($payload), true);

        // If the payload is not valid JSON or does not have the proper keys set we will
        // assume it is invalid and bail out of the routine since we will not be able
        // to decrypt the given value. We'll also check the MAC for this encryption.
        if (! self::validPayload($payload)) {
            throw new DecryptException('The payload is invalid.');
        }

        if (! self::validMac($payload)) {
            throw new DecryptException('The MAC is invalid.');
        }

        return $payload;
    }

    /**
     * Verify that the encryption payload is valid.
     *
     * @param  mixed  $payload
     * @return bool
     */
    protected static function validPayload(array $payload) {
        return is_array($payload) && isset($payload['iv'], $payload['value'], $payload['mac']) &&
            strlen(base64_decode($payload['iv'], true)) === openssl_cipher_iv_length('AES-256-CBC');
    }

    /**
     * Determine if the MAC for the given payload is valid.
     *
     * @param  array $payload
     * @return bool
     * @throws \Exception
     */
    protected static function validMac(array $payload) {
        $calculated = self::calculateMac($payload, $bytes = random_bytes(16));

        return hash_equals(
            hash_hmac('sha256', $payload['mac'], $bytes, true), $calculated
        );
    }
    /**
     * Calculate the hash of the given payload.
     *
     * @param  array  $payload
     * @param  string  $bytes
     * @return string
     */
    protected static function calculateMac($payload, $bytes) {
        return hash_hmac(
            'sha256', self::hash($payload['iv'], $payload['value']), $bytes, true
        );
    }
}