<?php

namespace App\Helpers;

class Background {

    static $_queue = [];

    public static function register(callable $method) {
        self::$_queue[] = $method;
    }

    public static function run() {
        foreach (self::$_queue as $item) {
            $item();
        }
    }
}