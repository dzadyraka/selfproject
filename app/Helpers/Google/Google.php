<?php

namespace App\Helpers\Google;


use Google_Client;

class Google {
    public static function getGoogleClient(): Google_Client {
        $client = new Google_Client();
        $client->setApplicationName(config('google.application_name'));
        $client->setClientId(config('google.client_id'));
        $client->setScopes([config('google.scopes')]);
        $client->setAuthConfig(config('google.service.file'));
        $client->useApplicationDefaultCredentials();

        if ($client->isAccessTokenExpired()) {
            $client->refreshTokenWithAssertion();
        }

        $service_token = $client->getAccessToken();

        return $client;
    }
}