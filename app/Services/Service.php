<?php

namespace App\Services;


use App\Repositories\RepositoryInterface;

class Service implements ServiceInterface {

    protected $repository;

    public function __construct(RepositoryInterface $repository) {
        $this->repository = $repository;
    }

    /**
     * @return RepositoryInterface
     */
    public function getRepository(): RepositoryInterface {
        return $this->repository;
    }
}