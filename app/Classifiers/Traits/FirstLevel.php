<?php

namespace App\Classifiers\Traits;


trait FirstLevel {
    public function firstLevel() {
        return $this->where('level', '=', 0)
            ->orderBy('id')
            ->get();
    }
}