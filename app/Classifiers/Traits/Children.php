<?php

namespace App\Classifiers\Traits;


trait Children {

    /**
     * Get child classifiers info by parent id
     *
     * @param $parent_id
     * @return mixed
     */
    public function children(int $parent_id) {
        return $this
            ->where('parent_id', $parent_id)
            ->orderBy('id')
            ->get();
    }
}