<?php

namespace App\Classifiers\Traits;

trait GetById {
    public function byId(array $id) {
        return $this->whereIn('level', $id)
            ->orderBy('id')
            ->get();
    }
}