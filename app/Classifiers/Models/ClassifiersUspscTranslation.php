<?php

namespace App\Classifiers\Models;

use Illuminate\Database\Eloquent\Model;

class ClassifiersUspscTranslation extends Model {
    public $timestamps = false;
    protected $fillable = ['name'];
}
