<?php

namespace App\Classifiers\Models;

use Illuminate\Database\Eloquent\Model;

class ClassifiersHsTranslation extends Model {
    public $timestamps = false;
    protected $fillable = ['name'];
}
