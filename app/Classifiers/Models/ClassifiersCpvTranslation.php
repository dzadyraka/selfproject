<?php

namespace App\Classifiers\Models;

use Illuminate\Database\Eloquent\Model;

class ClassifiersCpvTranslation extends Model {
    public $timestamps = false;
    protected $fillable = ['name'];
    protected $table = 'classifiers_cpv_translations';
}
