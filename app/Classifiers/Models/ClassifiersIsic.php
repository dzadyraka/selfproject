<?php

namespace App\Classifiers\Models;

use App\Helpers\ArrayCopy;
use App\Http\Traits\Filter;
use Elasticsearch\ClientBuilder;
use App\Http\Traits\Translatable;
use Elasticquent\ElasticquentTrait;
use App\Classifiers\Traits\Children;
use Illuminate\Support\Facades\Cache;

class ClassifiersIsic extends AbstractClassifiersModel {

    use Children;
    use Translatable;
    use ElasticquentTrait;
    use Filter;

    protected $fillable = ['code', 'level', 'parent_id', 'has_child', 'link'];

    public $translatedAttributes = ['name'];

    protected $table = 'classifiers_isic';

    function getIndexName() {
        return 'classifiers';
    }

    function getTypeName() {
        return 'isic';
    }

    /**
     * The elasticsearch settings.
     *
     * @var array
     */
    protected $indexSettings = [
        "analysis" => [
            "analyzer" => [
                "autocomplete" => [
                    "tokenizer" => "autocomplete",
                    "filter" => [
                        "lowercase"
                    ]
                ],
                "autocomplete_search" => [
                    "tokenizer" => "standard"
                ]
            ],
            "tokenizer" => [
                "autocomplete" => [
                    "type" => "edge_ngram",
                    "min_gram" => 3,
                    "max_gram" => 10,
                    "token_chars" => [
                        "letter",
                        "digit"
                    ]
                ]
            ]
        ]
    ];

    /**
     * The elasticsearch mapping.
     *
     * @var array
     */
    protected $mappingProperties = [
        "name" => [
            "type" => "text",
            "include_in_all" => true,
            "term_vector" => "yes",
        ],
        "search_string" => [
            "type" => "text",
            "analyzer" => "autocomplete",
            "search_analyzer" => "autocomplete_search"
        ],
        "code" => [
            "type" => "text",
            "include_in_all" => true,
            "term_vector" => "yes"
        ]
    ];

    public function hierarchy() {
        ini_set('memory_limit', '500M');

        if (!empty(Cache::get('isic'))) {
            return Cache::get('isic');
        }
        $items = $this->_toParents($this->_toLevels($this->orderBy('id')->get()->toArray()));

        Cache::put('isic', $items, $this->expiration(0));

        return Cache::get('isic');

    }

    /**
     * Transforming data to levels.
     *
     * @param array $data
     * @return array
     */
    private function _toLevels(array $data = []) {
        $levels = [];
        foreach ($data as $datum) {
            $levels[$datum['level']][] = $datum;
        }
        return $levels;
    }

    /**
     * Creating hash map with parent_id => children.
     *
     * @param array $levels
     *
     * @return array
     */
    private function _toParents(array $levels = []) {
        $data = [];
        foreach ($levels as $index => $level) {
            $data[$index] = [];
            foreach ($level as $key => $item) {
                if (empty($data[$index][$item['parent_id']])) {
                    $data[$index][$item['parent_id']] = [];
                }
                $data[$index][$item['parent_id']][] = $item;
            }
        }
        return $data;
    }

    public function reindex() {
        $items = $this->orderBy('id')->get()->toArray();

        $data = [];

        $levels = $this->_toLevels($items);

        foreach ($levels[0] as $item) {
            $item['__id'] = $item['id'];
            $item['search_string'] = strtolower($item['name']) . ' ' . strtolower($item['code']);
            $this->recursiveBranch($item, $levels, $item, $item, $data);
        }

        $client = ClientBuilder::create()->build();
        $items = 0;
        foreach ($data as $cl) {
            sleep(1);
            $client->index([
                'index' => $this->getIndexName(),
                'type' => $this->getTypeName(),
                'id' => $cl['__id'],
                'body' => $cl
            ]);
            $items++;
        }

        return $items;
    }

    /**
     * Get the expiration time based on the given minutes.
     *
     * @param  int $minutes
     * @return int
     */
    protected function expiration($minutes) {
        if ($minutes === 0) return 9999999999;

        return time() + ($minutes * 60);
    }

    protected function recursiveBranch($item, $levels, &$child, &$parent, &$data) {
        if (!$item['has_child']) {
            $element = ArrayCopy::copy($parent);
            $element['__id'] = $item['__id'];
            $element['search_string'] = strtolower($item['search_string']);
            $data[] = $element;
            return;
        }

        foreach ($levels[$item['level'] + 1] as $level) {
            if ($item['id'] === $level['parent_id']) {
                $level['__id'] = $item['__id'] . ':' . $level['id'];
                $level['search_string'] = $item['search_string'] . ' ' . strtolower($level['name']) . ' ' . strtolower($level['code']);
                $child['children'] = $level;
                $this->recursiveBranch($level, $levels, $child['children'], $parent, $data);
            }
        }
    }
}

/**
 * @SWG\Definition(
 *   definition="ClassifiersIsic",
 *   type="object",
 *   allOf={
 *       @SWG\Schema(
 *           @SWG\Property(property="code_id", type="string"),
 *           @SWG\Property(property="level", type="integer"),
 *           @SWG\Property(property="parent_id", type="integer"),
 *           @SWG\Property(property="name", type="string"),
 *           @SWG\Property(property="link", type="string"),
 *           @SWG\Property(
 *                  property="children",
 *                  type="array",
 *                  @SWG\Items(ref="#/definitions/ClassifiersIsicChildren")
 *           ),
 *       )
 *   }
 * )
 */

/**
 * @SWG\Definition(
 *   definition="ClassifiersIsicChildren",
 *   type="object",
 *   allOf={
 *       @SWG\Schema(
 *           @SWG\Property(property="code_id", type="string"),
 *           @SWG\Property(property="level", type="integer"),
 *           @SWG\Property(property="parent_id", type="integer"),
 *           @SWG\Property(property="name", type="string"),
 *           @SWG\Property(property="link", type="string"),
 *           @SWG\Property(
 *                  property="children",
 *                  type="array",
 *                  @SWG\Items()
 *           ),
 *       )
 *   }
 * )
 */
