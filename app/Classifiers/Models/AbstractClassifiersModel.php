<?php

namespace App\Classifiers\Models;

use Illuminate\Database\Eloquent\Model;

abstract class AbstractClassifiersModel extends Model {

    /**
     * Get first level classifiers
     *
     * @return mixed
     */
    public function firstLevel() {
        return $this->where('level', '=', 0)
            ->orderBy('id')
            ->get();
    }

    /**
     *
     *
     * @param array $id
     * @return mixed
     */
    public function byId(array $id) {
        return $this->whereIn('level', $id)
            ->orderBy('id')
            ->get();
    }
}