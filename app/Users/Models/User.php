<?php

namespace App\Users\Models;

use Exception;
use Validator;
use App\Currency\Currency;
use App\Http\Traits\Filter;
use App\Orders\Models\Order;
use App\Products\Models\Product;
use App\Companies\Models\Company;
use App\Http\Traits\Translatable;
use App\Social\Models\SocialAccount;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Event;
use App\Locations\Traits\HasCountryId;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements JWTSubject {
    use Filter;
    use Notifiable;
    use Translatable;
    use HasCountryId;

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'updated_date';

    public $translatedAttributes = ['first_name', 'last_name'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password', 'phone', 'country_id', 'language_id',
        'is_active', 'is_approved', 'is_blocked', 'avatar', 'custom_settings',
        'currency_id', 'time_zone', 'is_verified', 'has_accepted_principles',
        'google_id', 'facebook_id', 'linkedin_id', 'crm_id', 'measurement_system'
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'crm_id'
    ];

    protected $appends = [
        'position'
    ];

    public function getNameAttribute(): string {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function getPositionAttribute(): string {
        return isset($this->pivot->position) ? $this->pivot->position : '';
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier() {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims() {
        return [];
    }

    public function social_accounts() {
        return $this->hasMany(SocialAccount::class);
    }

    public function companies() {
        return $this->belongsToMany(Company::class);
    }

    public function companiesWithOutDrafts() {
        return $this->belongsToMany(Company::class)->where('is_draft', '=', false);
    }

    public function userFavoriteTenders() {
        return [];
    }

    public function userFavoriteProducts() {
        return [];
    }

    public function currency() {
        return $this->hasOne(Currency::class, 'id', 'currency_id');
    }

    public function bookmarks_products() {
        return $this->belongsToMany(Product::class, 'user_bookmarks_products');
    }

    public function createdOrders() {
        return $this->hasMany(Order::class, 'creator_id', 'id');
    }

    public function verifyUser() {
        return $this->hasOne(UserVerification::class);
    }

    public static function boot() {
        parent::boot();

        self::creating(function ($model) {
            if (!empty($model['password'])) {
                $model['password'] = Hash::make($model['password']);
            }
            $validator = Validator::make(
                [
//                    'first_name' => $model['first_name'],
//                    'last_name' => $model['last_name'],
//                    'phone' => $model['phone'],
                    'email' => $model['email'],
                    'country_id' => $model['country_id']
                ],
                [
//                    'first_name' => 'required|max:255',
//                    'last_name' => 'required|max:255',
//                    'phone' => 'required',
                    'email' => 'required|email|max:255|unique:users',
                    'country_id' => 'required'
                ]
            );

            if ($validator->fails()) {
                throw new Exception($validator->messages(), 400);
            }
            $event = Event::fire('contact.create', $model);
            $model['crm_id'] = array_first($event);
            //TODO: remove this shit
//            $model['is_active'] = true;
//            $model['is_verified'] = true;
        });

        static::updated(function ($model) {
            $event = Event::fire('contact.create', $model);
            $model['crm_id'] = array_first($event);
        });


    }
}


/**
 * @SWG\Definition(
 *   definition="User",
 *   type="object",
 *   allOf={
 *       @SWG\Schema(
 *          @SWG\Property(property="id", type="integer"),
 *          @SWG\Property(property="email", type="string"),
 *          @SWG\Property(property="phone", type="integer"),
 *          @SWG\Property(property="country_id", type="integer"),
 *          @SWG\Property(property="language_id", type="integer"),
 *          @SWG\Property(property="created_date", type="string", format="date-time"),
 *          @SWG\Property(property="edited_date", type="string", format="date-time"),
 *          @SWG\Property(property="is_active", type="boolean"),
 *          @SWG\Property(property="is_approved", type="boolean"),
 *          @SWG\Property(property="is_blocked", type="boolean"),
 *          @SWG\Property(property="balance", type="integer", format="0.00"),
 *          @SWG\Property(property="avatar", type="string"),
 *          @SWG\Property(
 *              property="custom_settings",
 *              type="array",
 *              @SWG\Items()
 *          ),
 *          @SWG\Property(property="currency_id", type="integer"),
 *          @SWG\Property(property="time_zone", type="string"),
 *          @SWG\Property(property="user_id", type="integer"),
 *          @SWG\Property(property="first_name", type="string"),
 *          @SWG\Property(property="last_name", type="string"),
 *          @SWG\Property(
 *              property="companies",
 *              type="array",
 *              @SWG\Items(ref="#/definitions/Company")
 *          ),
 *       )
 *   }
 * )
 */

/**
 * @SWG\Definition(
 *   definition="newUser",
 *   type="object",
 *   required={"name", "description", "country_id", "city_id", "phone", "email", "business_type"},
 *   allOf={
 *       @SWG\Schema(
 *          @SWG\Property(property="id", type="integer"),
 *          @SWG\Property(property="email", type="string"),
 *          @SWG\Property(property="phone", type="integer"),
 *          @SWG\Property(property="country_id", type="integer"),
 *          @SWG\Property(property="language_id", type="integer"),
 *          @SWG\Property(property="created_date", type="datetime"),
 *          @SWG\Property(property="edited_date", type="datetime"),
 *          @SWG\Property(property="is_active", type="boolean"),
 *          @SWG\Property(property="is_approved", type="boolean"),
 *          @SWG\Property(property="is_blocked", type="boolean"),
 *          @SWG\Property(property="first_name", type="string"),
 *          @SWG\Property(property="last_name", type="string"),
 *       )
 *   }
 * )
 */
