<?php

namespace App\Users\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;

class PasswordReset extends Model
{
    protected $fillable = ['email', 'token'];
    const UPDATED_AT = null;
    public $incrementing = false;

    public static function boot() {
        parent::boot();

        self::creating(function ($model) {
            $model['token'] = Hash::make($model['email']);
        });
    }
}
