<?php

namespace App\Users\Models;

use Illuminate\Database\Eloquent\Model;

class UserVerification extends Model {

    protected $fillable = ['user_id', 'token', 'expires_at'];
    public $timestamps = false;

    public function user() {
        return $this->belongsTo(User::class);
    }
}
