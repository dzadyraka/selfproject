<?php

namespace App\Users\Jobs;

use Carbon\Carbon;
use App\Mail\VerifyMail;
use App\Users\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\SerializesModels;
use App\Users\Models\UserVerification;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class CheckExpiredCode implements ShouldQueue {

    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle() {
        $codes = UserVerification::where('expires_at', '<=', Carbon::now()->toDateTimeString())->get();

        foreach ($codes as $code) {
            DB::transaction(function () use ($code) {
                $user = User::find($code['user_id']);
                UserVerification::find($code['id'])->delete();
                UserVerification::create([
                    'user_id' => $user->id,
                    'token' => 111111,
//                    'token' => rand(6, 6),
                    'expires_at' => Carbon::now()->addHours(24)
                ]);

                Mail::to($user->email)->send(new VerifyMail($user));
            });
        }
    }
}
