<?php


namespace App\Integrations\Validator;

use App\Integrations\Models\Integration;
use Exception;
use Validator;
use Illuminate\Validation\ValidationException;

class IntegrationValidator {

    /**
     * Check integration for unique
     *
     * @param $type
     * @param $company_id
     * @param $site_url
     * @throws Exception
     */
    public static function checkIntegration($type, $company_id, $site_url) {
        $integration = Integration::where('type', $type)->where('company_id', $company_id)->first();

        if ($integration['site'] == $site_url) {
            throw new Exception(json_encode("Site url already exist."), 400);
        }
    }
}