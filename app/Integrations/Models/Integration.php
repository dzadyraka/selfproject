<?php

namespace App\Integrations\Models;

use App\Companies\Models\Company;
use App\Companies\Models\CompanyDeliveryAndPayment;
use App\Companies\Models\CompanyExportInfo;
use Illuminate\Database\Eloquent\Model;

class Integration extends Model {

    protected $fillable = [
        'name',
        'company_id',
        'company_delivery_and_payment_id',
        'company_export_info_id',
        'type',
        'status',
        'settings'
    ];

    protected $casts = [
        'settings' => 'json'
    ];

    public function company() {
        return $this->belongsTo(Company::class);
    }

    public function delivery_and_payment() {
        return $this->belongsTo(CompanyDeliveryAndPayment::class);
    }

    public function export_info() {
        return $this->belongsTo(CompanyExportInfo::class);
    }
}
