<?php

namespace App\Settings\Models;


use App\Companies\Models\Company;

class ProductsSetting extends Setting {
    protected $table = 'products_settings';

    protected $fillable = [
        'type',
        'company_id',
        'user_id',
        'site',
        'active',
        'fields',
        'language',
        'keys',
    ];

    protected $casts = [
        'keys' => 'json',
        'fields' => 'json',
    ];

    public function company() {
        return $this->belongsTo(Company::class);
    }
}
