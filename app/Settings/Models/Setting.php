<?php

namespace App\Settings\Models;

use App\Http\Traits\HasUserId;
use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    use HasUserId;

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'updated_date';

    protected $fillable = [
        'type', 'user_id', 'company_id', 'site', 'fields', 'active', 'language', 'keys'
    ];

    protected $casts = [
        'fields' => 'array',
        'keys' => 'array',
    ];
}

/**
 *  @SWG\Definition(
 *   definition="newSettings",
 *   type="object",
 *   required={"type", "users_id", "companies_id", "site", "fields", "language"},
 *   allOf={
 *       @SWG\Schema(
 *           @SWG\Property(property="type", type="string"),
 *           @SWG\Property(property="companies_id", type="integer"),
 *           @SWG\Property(property="site", type="string"),
 *           @SWG\Property(
 *                  property="fields",
 *                  type="object",
 *           ),
 *           @SWG\Property(property="active", type="integer"),
 *           @SWG\Property(property="language", type="string"),
 *           @SWG\Property(
 *                  property="keys",
 *                  type="object",
 *                  @SWG\Property(property="ShopUrl", type="string"),
 *                  @SWG\Property(property="ApiKey", type="string"),
 *                  @SWG\Property(property="SharedSecret", type="string"),
 *                  @SWG\Property(property="Password", type="string"),
 *           ),
 *       )
 *   }
 * )
 */

/**
 *  @SWG\Definition(
 *   definition="Settings",
 *   type="object",
 *   allOf={
 *       @SWG\Schema(
 *           @SWG\Property(property="type", type="string"),
 *           @SWG\Property(property="users_id", type="integer"),
 *           @SWG\Property(property="companies_id", type="integer"),
 *           @SWG\Property(property="site", type="string"),
 *           @SWG\Property(
 *                  property="fields",
 *                  type="object",
 *           ),
 *           @SWG\Property(property="active", type="integer"),
 *           @SWG\Property(property="language", type="string"),
 *           @SWG\Property(
 *                  property="keys",
 *                  type="object",
 *                  @SWG\Property(property="ShopUrl", type="string"),
 *                  @SWG\Property(property="ApiKey", type="string"),
 *                  @SWG\Property(property="SharedSecret", type="string"),
 *                  @SWG\Property(property="Password", type="string"),
 *           ),
 *           @SWG\Property(property="created_date", type="string"),
 *           @SWG\Property(property="edited_date", type="string"),
 *       )
 *   }
 * )
 */