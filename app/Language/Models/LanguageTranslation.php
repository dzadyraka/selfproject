<?php

namespace App\Language\Models;


use Illuminate\Database\Eloquent\Model;

class LanguageTranslation extends Model {
	public $timestamps = false;
}