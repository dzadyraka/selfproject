<?php

namespace App\Language\Models;


use App\Http\Traits\Translatable;
use Illuminate\Database\Eloquent\Model;

class Language extends Model {
	use Translatable;

	public $timestamps = false;
	public $translatedAttributes = ['name'];

	protected $primaryKey = 'language_id';
}