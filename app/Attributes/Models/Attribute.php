<?php

namespace App\Attributes\Models;

use App\AttributeOptions\Models\AttributeOption;
use App\Categoties\Models\Category;
use App\Http\Traits\Filter;
use App\Http\Traits\Translatable;
use Illuminate\Database\Eloquent\Model;

class Attribute extends Model {

    use Translatable;
    use Filter;

    public $timestamps = false;

    public $translatedAttributes = ['name'];

    protected $fillable = ['name'];


    public function attribute_options() {
        return $this->hasMany(AttributeOption::class);
    }

    public function categories() {
        return $this->belongsToMany(Category::class, 'category_attributes');
    }
}

/**
 * @SWG\Definition(
 *   definition="Attributes",
 *   type="object",
 *   allOf={
 *       @SWG\Schema(
 *           @SWG\Property(property="name", type="string"),
 *           @SWG\Property(
 *              property="attributes_options",
 *              type="array",
 *              @SWG\Items(ref="#/definitions/AttributeOptions")
 *          ),
 *       )
 *   }
 * )
 */

/**
 * @SWG\Definition(
 *   definition="newAttribute",
 *   type="object",
 *   required={"name"},
 *   allOf={
 *       @SWG\Schema(
 *           @SWG\Property(property="name", type="string")
 *       )
 *   }
 * )
 */
