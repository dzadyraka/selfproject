<?php

namespace App\Blog\Models;


use App\Http\Traits\Filter;
use App\Http\Traits\Translatable;
use Illuminate\Database\Eloquent\Model;

class BlogCategory extends Model {
	use Translatable;
	use Filter;

	const CREATED_AT = 'created_date';
	const UPDATED_AT = 'edited_date';

	public $translatedAttributes = ['name'];

	protected $fillable = ['alias', 'is_active', 'position'];

	public function posts() {
		return $this->hasMany(BlogPost::class);
	}
}