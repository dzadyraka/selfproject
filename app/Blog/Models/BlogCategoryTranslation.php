<?php

namespace App\Blog\Models;


use Illuminate\Database\Eloquent\Model;

class BlogCategoryTranslation extends Model {
	public $timestamps = false;

	protected $fillable = ['name'];
}