<?php

namespace App\Blog\Models;


use App\Http\Traits\Filter;
use Illuminate\Database\Eloquent\Model;

class BlogCategoryBlogPost extends Model {
	public $timestamps = false;
	use Filter;
}