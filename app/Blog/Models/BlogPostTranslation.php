<?php

namespace App\Blog\Models;


use Illuminate\Database\Eloquent\Model;

class BlogPostTranslation extends Model {
    public $timestamps = false;

    protected $fillable = ['content', 'name', 'meta_title', 'meta_keywords', 'meta_description', 'description'];

    protected $appends = ['description'];

    public function getDescriptionAttribute() {
        $value = $this->content;
        $item = '';
        if (empty($value)) {
            $item = substr(strip_tags($value), 0, 200);

            if (strlen(strip_tags($value)) > 200) {
                $item .= '...';
            }

        } elseif (strlen($value) > 200) {
            $item = preg_replace('/\s\s+/', '', substr(strip_tags($value), 0, 200) . '...');
        }

        return $item;
    }
}