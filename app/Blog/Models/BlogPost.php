<?php

namespace App\Blog\Models;


use App\Http\Traits\Filter;
use App\Http\Traits\Translatable;
use Illuminate\Database\Eloquent\Model;

class BlogPost extends Model {
	use Translatable;
	use Filter;

	const CREATED_AT = 'created_date';
	const UPDATED_AT = 'edited_date';

	public $translatedAttributes = ['content', 'name', 'meta_title', 'meta_keywords', 'meta_description', 'description'];

	protected $fillable = ['image', 'is_published', 'is_featured'];

	public function sections() {
		return $this->belongsToMany(BlogCategory::class);
	}
}