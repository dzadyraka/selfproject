<?php

namespace App\Certificates\Services;


use App\Certificates\Models\CompanyCertificate;
use App\Certificates\Repositories\CertificateRepository;
use App\Services\Service;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class CertificateService extends Service {
    public function __construct(CertificateRepository $repository) {
        parent::__construct($repository);
    }

    /**
     * Method adds new certificate for the company
     *
     * @param int $companyId
     * @param array $data
     * @return CompanyCertificate
     */
    public function addCompanyCertificate(int $companyId, array $data): CompanyCertificate {
        if (!empty($data['file']) && $data['file'] instanceof UploadedFile) {
            $filename = (string)time() . '-' . $data['file']->getClientOriginalName();
            $file_path = "files/certificates/$filename";
            $data['file']->storeAs('files/certificates', $filename);
            $data['file_url'] = Storage::url($file_path);
        }

        return CompanyCertificate::create(
            array_merge($data, ['company_id' => $companyId])
        );
    }

    /**
     * Method delete certificate from company
     *
     * @param int $companyId
     * @param int $certificateId
     * @return mixed
     */
    public function deleteCompanyCertificate(int $companyId, int $certificateId) {
        return CompanyCertificate::where('company_id', $companyId)
            ->where('certificate_id', $certificateId)
            ->delete();
    }

    /**
     * Method verifies company certificate
     * TODO: Add permissions check.
     *
     * @param int $companyId
     * @param int $certificateId
     * @return mixed
     */
    public function verifyCompanyCertificate(int $companyId, int $certificateId) {
        return DB::transaction(function () use ($companyId, $certificateId) {
            $certificate = CompanyCertificate::whereCompanyId($companyId)
                ->whereCertificateId($certificateId)->firstOrFail();

            $certificate->is_verified = true;
            $certificate->save();

            return $certificate->fresh();
        });
    }

    public function getCompanyCertificates(int $companyId): Collection {
        return CompanyCertificate::whereCompanyId($companyId)
            ->with('certificate')
            ->get();
    }
}