<?php

namespace App\Certificates\Validators;


class CertificateValidator {
    public function addCompanyCertificateRules(): array {
        return [
            'certificate_id' => 'required|integer',
            'file' => 'required_without:site_url|file',
            'site_url' => 'required_without:file',
        ];
    }

    public function createCertificateRules(): array {
        return [
            'name' => 'required|string',
            'website' => 'required'
        ];
    }
}