<?php

namespace App\Certificates\Models;


use App\Companies\Models\Company;
use Illuminate\Database\Eloquent\Model;

class CompanyCertificate extends Model {
    protected $fillable = [
        'company_id', 'certificate_id', 'site_url', 'file_url'
    ];

    public function company() {
        return $this->hasOne(Company::class);
    }

    public function certificate() {
        return $this->hasOne(Certificate::class, 'id', 'certificate_id');
    }
}