<?php

namespace App\Certificates\Models;


use App\Companies\Models\Company;
use App\Locations\Models\Country;
use Illuminate\Database\Eloquent\Model;

class Certificate extends Model {
    protected $fillable = [
        'name', 'logo', 'description', 'address', 'website',
        'email', 'phone', 'responsibility', 'category', 'for_sme', 'area',
        'country_id', 'sector', 'sheet_id'
    ];

    public function country() {
        return $this->hasOne(Country::class, 'id', 'country_id');
    }

    public function companies() {
        return $this->hasManyThrough(
            Company::class,
            CompanyCertificate::class,
            'certificate_id',
            'id',
            'id',
            'company_id'
        );
    }
}