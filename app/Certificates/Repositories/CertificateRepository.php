<?php

namespace App\Certificates\Repositories;


use App\Certificates\Models\Certificate;
use App\Repositories\Repository;
use Illuminate\Database\Eloquent\Collection;

class CertificateRepository extends Repository {
    public function __construct(Certificate $model) {
        parent::__construct($model);
    }

    public function getFilteredCertificates(array $filters): Collection {
        return $this->getModel()
            ->with('country')
            ->when(!empty($filters['q']), function ($query) use ($filters) {
                $query_string = strtolower($filters['q']);
                return $query->whereRaw("LOWER(name) LIKE '%$query_string%'");
            })
            ->when(!empty($filters['responsibility']), function ($query) use ($filters) {
                return $query->whereResponsibility($filters['responsibility']);
            })
            ->when(!empty($filters['category']), function ($query) use ($filters) {
                return $query->whereCategory($filters['category']);
            })
            ->when(!empty($filters['sector']), function ($query) use ($filters) {
                return $query->whereSector($filters['sector']);
            })
            ->when(!empty($filters['area']), function ($query) use ($filters) {
                return $query->whereArea($filters['area']);
            })
            ->when(!empty($filters['country_id']), function ($query) use ($filters) {
                return $query->whereCountryId($filters['country_id']);
            })
            ->when(!empty($filters['company_id']), function ($query) use ($filters) {
                return $query->whereDoesntHave('companies', function ($companies) use ($filters) {
                    return $companies->where('company_id', $filters['company_id']);
                });
            })
            ->get();
    }
}