<?php

namespace App\Certificates\Commands;


use App\Certificates\Models\Certificate;
use App\Helpers\Google\Google;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Revolution\Google\Sheets\Sheets;

class SyncWithGoogleSheets extends Command {
    protected $signature = 'certificates:sync';
    protected $description = 'Sync certificates list with Google Sheets';

    public function handle() {
        $service = new \Google_Service_Sheets(Google::getGoogleClient());
        $sheets = new Sheets();
        $sheets->setService($service);

        $rows = $sheets->spreadsheet(env('CERTIFICATES_SHEET'))
            ->sheet('Sheet1')
            ->all();

        for ($i = 1; $i < count($rows); $i++) {
            if (!isset($rows[$i][0]) || empty($rows[$i][0])) {
                continue;
            }

            // If has image, let's download it
            if (!empty($rows[$i][2])) {
                $filename = (string)$rows[$i][0] . '-' . (string)time() . '-' . basename($rows[$i][2]);
                $file_path = "imported/certificates/$filename";

                try {
                    $contents = file_get_contents($rows[$i][2]);
                    Storage::put($file_path, $contents);

                } catch (\Exception $e) {
                    $file_path = '';
                }
            }

            $certificateData = [
                'sheet_id' => $rows[$i][0],
                'name' => $rows[$i][1],
                'description' => $rows[$i][3] ?? '',
                'logo' => isset($file_path) && !empty($file_path) ? Storage::url($file_path) : null,
                'website' => $rows[$i][8] ?? '',
                'address' => $rows[$i][6] ?? '',
                'email' => $rows[$i][9] ?? '',
                'phone' => $rows[$i][10] ?? ''
            ];

            $certificate = Certificate::whereSheetId($rows[$i][0])->first();

            if ($certificate) {
                $certificate->update($certificateData);
            } else {
                Certificate::create($certificateData);
            }
        }
    }
}