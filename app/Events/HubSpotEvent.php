<?php

namespace App\Events;

use App\Companies\Models\Company;
use App\Helpers\Background;
use Illuminate\Support\Facades\Log;
use JWTAuth;
use App\Users\Models\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Rossjcooper\LaravelHubSpot\Facades\HubSpot;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class HubSpotEvent {
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct() {
        //
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @param $company
     * @return void
     */

    public function hubSpotCompanyCreated(Company $company) {
        Background::register(function () use ($company) {
            $hubSpot = HubSpot::companies();
            $user = User::findOrFail(JWTAuth::parseToken()->toUser()->id);
            $data = [
                ['name' => 'name', 'value' => $company->name]
            ];

            if (!empty($company->phone)) {
                $data[] = ['name' => 'phone', 'value' => $company->phone[0]];
            }

            if (!empty($company->brand)) {
                $data[] = ['name' => 'brandname', 'value' => $company->brand];
            }

            if (!empty($company->business_entity)) {
                $data[] = ['name' => 'businessentity', 'value' => $company->business_entity];
            }

            if (!empty($company->description)) {
                $data[] = ['name' => 'description', 'value' => $company->description];
            }

            if (!empty($company->registration_number)) {
                $data[] = ['name' => 'registration_number', 'value' => $company->registration_number];
            }

            if (!empty($company->year_established)) {
                $data[] = ['name' => 'year_established', 'value' => $company->year_established];
            }

            if (!empty($company->country_id)) {
                $data[] = ['name' => 'country', 'value' => $company->country->name];
            }

            if (!empty($company->city_id)) {
                $data[] = ['name' => 'city', 'value' => $company->city->name];
            }

            if (!empty($company->region_id)) {
                $data[] = ['name' => 'state', 'value' => $company->region->name];
            }

            if (!empty($company->postal_code)) {
                $data[] = ['name' => 'zip', 'value' => $company->postal_code];
            }

            if (!empty($company->website)) {
                $data[] = ['name' => 'website', 'value' => $company->website];
            }

            if (!empty($company->address)) {
                $data[] = ['name' => 'address', 'value' => $company->address];
            }

            if (!empty($company->business_type)) {
                $data[] = ['name' => 'industry', 'value' => $company->business_type];
            }

            if (!empty($company->annual_turnover)) {
                $data[] = ['name' => 'annual_turnover', 'value' => $company->annual_turnover];
            }

            $hubSpot_company = $hubSpot->create(
                $data
            );

            if (empty($user->crm_id)) {
                $hubspot_user = HubSpot::contacts()->createOrUpdate($user->email, [
                    [
                        'property' => 'firstname',
                        'value' => $user->first_name
                    ],
                    [
                        'property' => 'lastname',
                        'value' => $user->last_name
                    ]
                ]);
                $user->crm_id = $hubSpot_company->companyId;
                $user->save();
            }

            if (!empty($user->crm_id) || !empty($hubSpot_company->companyId)) {
                HubSpot::companies()->addContact($user->crm_id, $hubSpot_company->companyId);
            }
            $company->crm_id = $hubSpot_company->companyId;

            $company->save();
        });
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @param $company
     * @return void
     */

    public function hubSpotCompanyUpdate(Company $company) {
        Background::register(function () use ($company) {
            $hubSpot = HubSpot::companies();
            $user = User::findOrFail(JWTAuth::parseToken()->toUser()->id);
            $data = [
                ['name' => 'name', 'value' => $company->name]
            ];

//        if(!empty($company['business_type'])){
//            $data[] = ['name' => 'type', 'value' => $company->business_type];
//        }

            if (!empty($company['phone'])) {
                $data[] = ['name' => 'phone', 'value' => $company->phone];
            }

            if (!empty($company['brand'])) {
                $data[] = ['name' => 'brandname', 'value' => $company->brand];
            }

//        if(!empty($company['business_entity'])){
//            $data[] = ['name' => 'businessentity', 'value' => $company->business_entity];
//        }

            if (!empty($company['description'])) {
                $data[] = ['name' => 'description', 'value' => $company->description];
            }

            if (!empty($company['registration_number'])) {
                $data[] = ['name' => 'registration_number', 'value' => $company->registration_number];
            }

            if (!empty($company['year_established'])) {
                $data[] = ['name' => 'year_established', 'value' => $company->year_established];
            }

            if (!empty($company['country'])) {
                $data[] = ['name' => 'country', 'value' => $company->country->name];
            }

            if (!empty($company['city_id'])) {
                $data[] = ['name' => 'city', 'value' => $company->city->name];
            }

            if (!empty($company['region_id'])) {
                $data[] = ['name' => 'state', 'value' => $company->region->name];
            }

            if (!empty($company['postal_code'])) {
                $data[] = ['name' => 'zip', 'value' => $company->postal_code];
            }

            if (!empty($company['website'])) {
                $data[] = ['name' => 'website', 'value' => $company->website];
            }

            if (!empty($company['address'])) {
                $data[] = ['name' => 'address', 'value' => $company->address];
            }

            $hubSpot_company = $hubSpot->update($company->crm_id,
                $data
            );

            if (empty($user->crm_id)) {
                $hubspot_user = HubSpot::contacts()->createOrUpdate($user->email, [
                    [
                        'property' => 'firstname',
                        'value' => $user->first_name
                    ],
                    [
                        'property' => 'lastname',
                        'value' => $user->last_name
                    ]
                ]);
                $user->crm_id = $hubSpot_company->companyId;
                $user->save();
            }

            if (!empty($user->crm_id) || !empty($hubSpot_company->companyId)) {
                HubSpot::companies()->addContact($user->crm_id, $hubSpot_company->companyId);
            }
            $company->crm_id = $hubSpot_company->companyId;

            $company->save();
        });
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @param $item
     * @return void
     */

    public function hubSpotContactCreatedOrUpdate($item) {
        $hubspot_user = HubSpot::contacts()->createOrUpdate($item->email, [
            [
                'property' => 'firstname',
                'value' => $item->first_name
            ],
            [
                'property' => 'lastname',
                'value' => $item->last_name
            ]
        ]);
        $crm_id = $hubspot_user->vid;
        return $crm_id;
    }
}
