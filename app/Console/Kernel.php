<?php

namespace App\Console;

use App\Ports\Jobs\SynchronizeSeaRatesPorts;
use App\Rta\Jobs\RtaParserUpdate;
use App\Users\Jobs\CheckExpiredCode;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel {
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Ports\Commands\GetSeaRatesPorts::class,
        \App\Certificates\Commands\SyncWithGoogleSheets::class,
        \App\Comtrade\Commands\ComtradeParse::class,
        \App\Comtrade\Commands\HsSynchronize::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule) {
//		$schedule->job(new RtaParserUpdate)->everyMinute();
        $schedule->job(new SynchronizeSeaRatesPorts)->monthly();
        $schedule->command('currencies:refresh')->dailyAt(7);
        $schedule->command('certificates:sync')->daily();
        $schedule->command('comtrade:parse', [date('Y')])->monthly();
        $schedule->call('App\Http\Controllers\Integrations\Magento\MagentoController@updateProducts')->weekly();
        $schedule->call('App\Http\Controllers\Integrations\Magento\WoocommerceController@updateProducts')->weekly();
        $schedule->job(new CheckExpiredCode)->everyMinute();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands() {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
