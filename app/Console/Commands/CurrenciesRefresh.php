<?php

namespace App\Console\Commands;

use App\Currency\API\CurrencyLayer;
use App\Currency\Currency;
use App\Currency\CurrencyRate;
use Illuminate\Console\Command;

class CurrenciesRefresh extends Command {

    protected $signature = 'currencies:refresh';

    protected $description = 'Refresh currencies rates';

    public function handle() {
        // Fetch last rates
        $currencies = CurrencyLayer::getInstance()->get();

        if (empty($currencies)) {
            return;
        }

        // Update current
        $date = date("Y-m-d H:i:s");
        CurrencyRate::where('date_from', '<=', $date)
            ->whereNull('date_to')
            ->update(['date_to' => $date]);

        // Insert new
        $activeCurrencies = [];

        foreach ($currencies as $currency) {
            $sourceId = Currency::getIdByCurrencyCode($currency['source']);
            $destinationId = Currency::getIdByCurrencyCode($currency['destination']);

            if (!$destinationId) {
                $currencyModel = Currency::whereCode($currency['destination'])
                    ->first();
                $destinationId = $currencyModel->id ?? null;
            }

            if (!$sourceId) {
                $currencyModel = Currency::whereCode($currency['source'])
                    ->first();
                $sourceId = $currencyModel->id ?? null;
            }

            if (!$sourceId || !$destinationId) {
                continue;
            }

            $activeCurrencies[] = $destinationId;

            CurrencyRate::create([
                'destination' => $currency['destination'],
                'destination_id' => $destinationId,
                'source' => $currency['source'],
                'source_id' => $sourceId,
                'rate' => $currency['rate'],
                'date_from' => $date
            ]);
        }

        Currency::whereIn('id', $activeCurrencies)
            ->update(['active' => true]);
    }
}