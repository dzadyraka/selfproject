<?php

namespace App\Locations\Helpers;


use GuzzleHttp\Client;

class GoogleGeoApi {

    const MAPS_API = 'https://maps.googleapis.com/';

    /**
     * @var string
     */
    private $api_key;
    /**
     * @var Client
     */
    private $client;

    private static $uniqueInstance = null;

    public static function getInstance(): self {
        if (self::$uniqueInstance === null) {
            self::$uniqueInstance = new self;
        }

        return self::$uniqueInstance;
    }

    protected function __construct() {
        $this->api_key = env('GOOGLE_MAPS_API_KEY');
        $this->client = new Client(['base_uri' => self::MAPS_API]);
    }

    public function getCoordinates(string $address): ?array {
        $response = json_decode($this->client->get('/maps/api/geocode/json', [
            'query' => [
                'address' => $address,
                'key' => $this->api_key
            ]
        ])->getBody()->getContents(), true);

        if (empty($response['results'])) {
            return null;
        }

        if (!empty($response['results'][0]['geometry']['location'])) {
            return $response['results'][0]['geometry']['location'];
        }

        return null;
    }

    public function searchCountryCities(string $countryIsoCode, string $query): array {
        $response = json_decode($this->client->get('/maps/api/place/autocomplete/json', [
            'query' => [
                'language' => 'en',
                'input' => $query,
                'types' => '(cities)',
                'components' => 'country:' . strtolower($countryIsoCode),
                'key' => $this->api_key
            ]
        ])->getBody()->getContents(), true);

        if (empty($response['predictions'])) {
            return [];
        }

        return array_map(function ($city) {
            return [
                'id' => $city['place_id'],
                'name' => $city['structured_formatting']['main_text'],
                'secondary_text' => $city['structured_formatting']['secondary_text'] ?? ''
            ];
        }, $response['predictions']);
    }
}