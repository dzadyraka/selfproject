<?php

namespace App\Locations\Models;


use App\Http\Traits\Filter;
use App\Http\Traits\Translatable;
use Illuminate\Database\Eloquent\Model;

class Country extends Model {
	use Translatable;
	use Filter;

	public $timestamps = false;
	public $translatedAttributes = ['name'];
}

/**
 *  @SWG\Definition(
 *   definition="Country",
 *   type="object",
 *   allOf={
 *       @SWG\Schema(
 *           @SWG\Property(property="id", type="integer"),
 *           @SWG\Property(property="code", type="string"),
 *           @SWG\Property(property="iso", type="string"),
 *           @SWG\Property(property="iso3", type="string"),
 *           @SWG\Property(property="phone_code", type="integer"),
 *           @SWG\Property(property="phone_length", type="integer"),
 *           @SWG\Property(property="countries_id", type="integer"),
 *           @SWG\Property(property="language_id", type="integer"),
 *           @SWG\Property(property="name", type="string"),
 *       )
 *   }
 * )
 */