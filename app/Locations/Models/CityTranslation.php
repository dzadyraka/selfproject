<?php

namespace App\Locations\Models;

use Illuminate\Database\Eloquent\Model;

class CityTranslation extends Model {
    public $timestamps = false;
}
