<?php

namespace App\Locations\Models;

use App\Http\Traits\Filter;
use App\Http\Traits\Translatable;
use Illuminate\Database\Eloquent\Model;

class Region extends Model {
    use Translatable;
    use Filter;

    public $timestamps = false;
    public $translatedAttributes = ['name'];
}
