<?php

namespace App\Locations\Models;


use Illuminate\Database\Eloquent\Model;

class CountryTranslation extends Model {
	public $timestamps = false;
}