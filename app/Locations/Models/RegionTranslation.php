<?php

namespace App\Locations\Models;

use Illuminate\Database\Eloquent\Model;

class RegionTranslation extends Model {
    public $timestamps = false;
}
