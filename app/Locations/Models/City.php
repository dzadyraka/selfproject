<?php

namespace App\Locations\Models;

use App\Http\Traits\Filter;
use App\Http\Traits\Translatable;
use Illuminate\Database\Eloquent\Model;

class City extends Model {
    use Filter;

    public $timestamps = false;

    protected $fillable = [
        'name',
        'country_id',
        'region_id',
        'coordinates_lat',
        'coordinates_lng',
        'secondary_text',
        'google_maps_id'
    ];

    public function country() {
        return $this->belongsTo(Country::class);
    }
}

/**
 *  @SWG\Definition(
 *   definition="City",
 *   type="object",
 *   allOf={
 *       @SWG\Schema(
 *           @SWG\Property(property="id", type="integer"),
 *           @SWG\Property(property="country_id", type="integer"),
 *           @SWG\Property(property="region_id", type="integer"),
 *           @SWG\Property(property="cities_id", type="integer"),
 *           @SWG\Property(property="language_id", type="integer"),
 *           @SWG\Property(property="name", type="string"),
 *       )
 *   }
 * )
 */