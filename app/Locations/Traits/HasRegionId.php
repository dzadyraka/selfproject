<?php

namespace App\Locations\Traits;

use App\Locations\Models\Region;

trait HasRegionId {
    public function region() {
        return $this->hasOne(Region::class, 'id', 'region_id');
    }
}