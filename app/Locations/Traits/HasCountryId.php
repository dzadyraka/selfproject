<?php

namespace App\Locations\Traits;

use App\Locations\Models\Country;

trait HasCountryId {
    public function country() {
        return $this->hasOne(Country::class, 'id', 'country_id');
    }
}