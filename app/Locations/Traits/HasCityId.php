<?php

namespace App\Locations\Traits;

use App\Locations\Models\City;

trait HasCityId {
    public function city() {
        return $this->hasOne(City::class, 'id', 'city_id');
    }
}