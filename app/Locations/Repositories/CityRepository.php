<?php

namespace App\Locations\Repositories;


use App\Locations\Models\City;
use App\Repositories\Repository;

class CityRepository extends Repository {
    public function __construct(City $city) {
        parent::__construct($city);
    }
}