<?php

namespace App\Locations\Services;


use App\Locations\Helpers\GoogleGeoApi;
use App\Locations\Models\City;
use App\Locations\Models\Country;
use App\Locations\Repositories\CityRepository;
use App\Services\Service;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class CityService extends Service {
    public function __construct(CityRepository $repository) {
        parent::__construct($repository);
    }

    public function getCityCoordinates(City $city): array {
        if ($city->coordinates_lat && $city->coordinates_lng) {
            return [
                'lat' => $city->coordinates_lat,
                'lng' => $city->coordinates_lng,
            ];
        }

        $country = $city->country->translate('en')->name;
        $query = $city->secondary_text && !empty($city->secondary_text) ? $city->secondary_text : $country->name;
        $coordinates = GoogleGeoApi::getInstance()->getCoordinates("$query, $city->name");

        $city->update([
            'coordinates_lat' => $coordinates['lat'],
            'coordinates_lng' => $coordinates['lng']
        ]);

        return $coordinates;
    }

    public function getCountryCities(int $countryId, array $filters = []): Collection {
        $cities = City::whereCountryId($countryId);

        if (!empty($filters['q'])) {
            $cities->whereRaw('LOWER(name) LIKE \'' . strtolower($filters['q']) . '%\'');
        }

        if (!$cities->count()) {
            DB::transaction(function () use ($countryId, $filters) {
                $country = Country::findOrFail($countryId);
                $citiesList = GoogleGeoApi::getInstance()
                    ->searchCountryCities($country->iso, $filters['q'] ?? '');
                foreach ($citiesList as $city) {
                    $cityModel = City::whereCountryId($countryId)
                        ->whereGoogleMapsId($city['id'])
                        ->first();

                    if (!$cityModel) {
                        City::create([
                            'country_id' => $countryId,
                            'name' => $city['name'],
                            'secondary_text' => $city['secondary_text'],
                            'google_maps_id' => $city['id']
                        ]);
                    }
                }
            });
        }

        return $cities->get();
    }
}