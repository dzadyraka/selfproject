<?php

namespace App\Repositories;


use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class Repository implements RepositoryInterface {
    protected $model;

    public function __construct(Model $model) {
        $this->model = $model;
    }

    public function all(): Collection {
        return $this->model->get();
    }

    public function create(array $data): Model {
        return $this->model->create($data);
    }

    public function update(array $data, int $id): bool {
        $record = $this->show($id);
        return $record->update($data);
    }

    public function delete(int $id): int {
        return $this->model->destroy($id);
    }

    public function show(int $id): Model {
        return $this->model->findOrFail($id);
    }

    public function getModel(): Model {
        return $this->model;
    }

    public function setModel(Model $model): self {
        $this->model = $model;
        return $this;
    }

    public function with($relations) {
        return $this->model->with($relations);
    }

    public function __call($name, $arguments) {
        return $this->getModel()->$name(...$arguments);
    }
}