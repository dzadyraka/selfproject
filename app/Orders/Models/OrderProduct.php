<?php

namespace App\Orders\Models;

use Illuminate\Database\Eloquent\Model;

class OrderProduct extends Model {

    public $timestamps = false;

    protected $fillable = [
        'order_id',
        'product_id',
        'quantity',
        'subtotal',
        'price',
    ];
}
