<?php

namespace App\Orders\Models;

use App\Http\Traits\HasUserId;
use App\Products\Models\Product;
use Illuminate\Database\Eloquent\Model;

class OrderNegotiationOfferProduct extends Model {

    public $timestamps = false;

    protected $fillable = [
        'order_negotiation_offer_id',
        'product_id',
        'price',
        'quantity',
        'subtotal',
    ];

    protected $casts = [
        'price' => 'float',
        'subtotal' => 'float'
    ];

    public function product() {
        return $this->hasOne(Product::class, 'id', 'product_id');
    }
}
