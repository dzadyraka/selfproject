<?php

namespace App\Orders\Models;

use JWTAuth;
use App\Users\Models\User;
use App\Http\Traits\HasUserId;
use App\Companies\Models\Company;
use Illuminate\Database\Eloquent\Model;

class OrderNegotiationOffer extends Model {

    const CREATED_AT = 'created_date';
    const UPDATED_AT = null;

    protected $fillable = [
        'order_id',
        'type',
        'user_id',
        'company_id',
        'message',
        'attachments',
        'is_accepted',
    ];

    protected $casts = [
        'attachments' => 'json',
    ];

    public static function boot() {
        parent::boot();
        parent::creating(function (Model $model) {
            $model->user_id = JWTAuth::parseToken()->toUser()->id;
        });
    }

    public function products() {
        return $this->hasMany(OrderNegotiationOfferProduct::class);
    }

    public function proposer() {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function company() {
        return $this->hasOne(Company::class, 'id', 'company_id');
    }
}

/**
 *  @SWG\Definition(
 *   definition="newNegotiates",
 *   type="object",
 *   allOf={
 *       @SWG\Schema(
 *           @SWG\Property(property="order_id", type="integer"),
 *           @SWG\Property(property="message", type="string"),
 *           @SWG\Property(property="user_id", type="integer"),
 *           @SWG\Property(
 *                  property="product_list",
 *                  type="array",
 *                  @SWG\Items(ref="#/definitions/Product"),
 *           ),
 *           @SWG\Property(
 *                  property="attachments",
 *                  type="array",
 *                  @SWG\Items(
 *                      @SWG\Property(property="name", type="string"),
 *                      @SWG\Property(property="type", type="string"),
 *                      @SWG\Property(property="url", type="string"),
 *                  ),
 *           ),
 *           @SWG\Property(property="subtotal", type="integer", format="0.00"),
 *           @SWG\Property(property="status", type="boolean"),
 *           @SWG\Property(property="request_user_id", type="integer")
 *       )
 *   }
 * )
 */

/**
 *  @SWG\Definition(
 *   definition="Negotiates",
 *   type="object",
 *   allOf={
 *       @SWG\Schema(
 *           @SWG\Property(property="order_id", type="integer"),
 *           @SWG\Property(property="message", type="string"),
 *           @SWG\Property(property="user_id", type="integer"),
 *           @SWG\Property(
 *                  property="product_list",
 *                  type="array",
 *                  @SWG\Items(ref="#/definitions/Product"),
 *           ),
 *           @SWG\Property(
 *                  property="attachments",
 *                  type="array",
 *                  @SWG\Items(
 *                      @SWG\Property(property="name", type="string"),
 *                      @SWG\Property(property="type", type="string"),
 *                      @SWG\Property(property="url", type="string"),
 *                  ),
 *           ),
 *           @SWG\Property(property="subtotal", type="integer", format="0.00"),
 *           @SWG\Property(property="status", type="boolean"),
 *           @SWG\Property(property="request_user_id", type="integer"),
 *           @SWG\Property(property="created_date", type="string", format="date-time"),
 *           @SWG\Property(property="edited_date", type="string", format="date-time"),
 *           @SWG\Property(property="creator", ref="#/definitions/User"),
 *       )
 *   }
 * )
 */