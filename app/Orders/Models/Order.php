<?php

namespace App\Orders\Models;

use App\Companies\Models\Company;
use App\Http\Traits\Filter;
use App\Http\Traits\HasCreator;
use App\Http\Traits\HasEditor;
use App\Products\Models\Product;
use App\Users\Models\User;
use Illuminate\Database\Eloquent\Model;

class Order extends Model {

    use Filter;
    use HasEditor;
    use HasCreator;


    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'edited_date';

    protected $fillable = [
        'description',
        'attachments',
        'subtotal',
        'creator_id',
        'customer_company_id',
        'company_id',
        'delivery_service',
        'shipping_cost',
        'order_status',
        'need_approve_request',
        'is_completed',
        'is_cancelled',
        'is_draft',
        'order_url',
        'billing_address',
        'shipping_address',
        'shipping_details',
        'editor_id',
        'shipping',
        'taxes',
        'total',
        'invoice_sent',
        'invoice_date',
    ];

    protected $casts = [
        'attachments' => 'json',
        'billing_address' => 'json',
        'shipping_address' => 'json',
        'shipping_details' => 'json',
    ];

    protected $appends = [
        'orders_request'
    ];

    public function customers() {
        return $this->belongsToMany(User::class, 'order_clients');
    }

    public function request() {
        return $this->belongsToMany(User::class, 'order_requests');
    }

    public function products() {
        return $this->belongsToMany(Product::class, 'order_products')->withPivot(['order_id', 'product_id','quantity', 'subtotal', 'price']);
    }

    public function company() {
        return $this->hasOne(Company::class, 'id', 'customer_company_id');
    }

    public function creator() {
        return $this->hasOne(User::class, 'id', 'creator_id');
    }

    public function companyCreator() {
        return $this->hasOne(Company::class, 'id', 'company_id');
    }

    public function getOrdersRequestAttribute() {
        return true;
    }

}
