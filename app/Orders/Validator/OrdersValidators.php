<?php


namespace App\Orders\Validator;

use Exception;
use Validator;
use App\Products\Models\Product;
use Illuminate\Validation\ValidationException;

class OrdersValidators {


    /**
     * @param $input
     * @throws ValidationException
     */
    public static function validateOrderInputs($input) {
        $validate_order_inputs = Validator::make(
            $input,
            [
                'currency' => 'required',
                'customers' => 'required',
                'order_status' => 'required'
            ]
        );

        if ($validate_order_inputs->fails()) {
            throw new ValidationException($validate_order_inputs);
        }
    }

    /**
     * @param $input
     * @throws ValidationException
     */
    public static function validateAddress($input) {
        $validator_billing_address = Validator::make(
            $input,
            [
                'region_id' => 'required',
                'city_id' => 'required',
                'country_id' => 'required',
                'street_name' => 'required',
                'street_data' => 'required',
                'zip_code' => 'required',
                'first_name' => 'required',
                'last_name' => 'required',
                'type' => 'required',
            ]
        );

        if ($validator_billing_address->fails()) {
            throw new ValidationException($validator_billing_address);
        }
    }

    public static function validateShippingDetails($input) {
        $validator_shipping_details = Validator::make(
            $input,
            [
                'delivery_type' => 'required',
                'shipping_service' => 'required',
                'insurance' => 'required',
            ]
        );

        if ($validator_shipping_details->fails()) {
            throw new ValidationException($validator_shipping_details);
        }
    }

    public static function validateProducts($input) {
        $validator_products = Validator::make(
            $input,
            [
                'product_id' => 'required',
                'quantity' => 'required',
            ]
        );

        if ($validator_products->fails()) {
            throw new ValidationException($validator_products);
        }
    }


    public static function validateProductsArray($products) {
        if (empty($products)) {
            return response()->json([
                'success' => false,
                'errors' => "Empty product list."
            ], 422);
        }
        return $products;
    }

    public static function checkProduct(int $product_id, $quantity, $negotiate = false) {
        $product_model = Product::findOrFail($product_id, ['price', 'quantity_available', 'minimum_ordery', 'bulk_prices']);

        if ($product_model['quantity_available'] - $quantity < 0) {
            throw new Exception(json_encode("Product quantity not available."), 400);
        }

        if ($product_model['minimum_ordery'] > $quantity) {
            throw new Exception(json_encode("Minimum product ordery {$product_model['minimum_ordery']}."), 400);
        }
        if(!$negotiate) {
            $product_model->update([
                'quantity_available' => $product_model['quantity_available'] - $quantity
            ]);
        }

        return $product_model;
    }
}