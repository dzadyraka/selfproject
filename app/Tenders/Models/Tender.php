<?php

namespace App\Tenders\Models;


use App\Http\Traits\HasCreator;
use App\Http\Traits\HasEditor;
use App\Http\Traits\Translatable;
use Illuminate\Database\Eloquent\Model;

class Tender extends Model {
    use Translatable;
    use HasCreator;
    use HasEditor;

    static $_editor_id_field = 'edited_by';
    static $_creator_id_field = 'created_by';

    public const CREATED_AT = 'created_date';
    public const UPDATED_AT = 'edited_date';

    protected $fillable = [
        'procedure_type',
        'procurement_category',
        'notice_publication_date',
        'publication_closing_period',
        'bid_security',
        'bid_security_period',
        'bid_security_sum',
        'bid_security_currency',
        'payment_security',
        'purchaser_id',
        'expected_cost',
        'reverse_auction',
        'reverse_auction_duration',
        'reverse_auction_min_step_percent',
        'reverse_auction_min_step_cash',
        'publication_prequalification_period',
        'prequalification_closing_period',
        'publication_type',
        'alternative_product_solution_allowed',
        'created_date',
        'edited_date',
        'is_published',
        'classifier_uspsc',
        'classifier_hs',
        'classifier_cpv',
        'classifier_isic',
        'stage_id',
        'stage_option',
        'stage_list',
        'reverse_auction_min_step_type',
        'change_period',
        'edit_requirements',
        'requirements',
        'is_draft'
    ];

    protected $casts = [
        'requirements' => 'json',
        'stage_option' => 'json',
        'stage_list' => 'json',
        'classifier_uspsc' => 'json',
        'classifier_hs' => 'json',
        'classifier_cpv' => 'json',
        'classifier_isic' => 'json'
    ];
}