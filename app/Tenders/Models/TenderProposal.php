<?php

namespace App\Tenders\Models;


use App\Companies\Models\Company;
use App\Currency\Currency;
use App\Http\Traits\HasUserId;
use App\Users\Models\User;
use Illuminate\Database\Eloquent\Model;

class TenderProposal extends Model {

    use HasUserId;

    protected $_user_id_field = 'creator_id';

    public const CREATED_AT = 'created_date';
    public const UPDATED_AT = 'edited_date';

    protected $casts = [
        'lots' => 'json',
        'requirements' => 'json'
    ];

    protected $fillable = [
        'tender_id', 'company_id', 'requirements', 'lots', 'is_approved', 'win'
    ];

    protected $appends = [
        'lots'
    ];

    public function company() {
        return $this->belongsTo(Company::class);
    }

    public function bidders() {
        return $this->belongsToMany(User::class, 'proposal_bidders');
    }

    public function getLotsAttribute(): array {
        if (empty($this->attributes['lots'])) {
            return [];
        }
        $lots = $this->attributes['lots'];

        foreach ($lots as $key => $datum) {
            $lots[$key]['currency_code'] = !empty($lot['currency'])
                ? Currency::getCodeByCurrencyId($lot['currency'])
                : 'USD';
        }
        return $lots;
    }
}