<?php

namespace App\Tenders\Models;

use Illuminate\Database\Eloquent\Model;

class TenderTranslation extends Model {
    public $timestamps = false;

    protected $fillable = [
        'name', 'procurement_description', 'non_price_criteria',
        'procured_item_mandatory_requirements', 'procured_item_additional_requirements',
        'bidder_mandatory_requirements', 'bidder_additional_requirements', 'lots'
    ];

    protected $casts = [
        'lots' => 'json',
        'bidder_mandatory_requirements' => 'json',
        'bidder_additional_requirements' => 'json',
        'procured_item_mandatory_requirements' => 'json',
        'procured_item_additional_requirements' => 'json',
        'non_price_criteria' => 'json'
    ];
}