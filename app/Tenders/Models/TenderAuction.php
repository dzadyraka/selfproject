<?php

namespace App\Tenders\Models;


use App\Companies\Models\Company;
use App\Currency\Currency;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class TenderAuction extends Model {

    public $timestamps = false;

    protected $fillable = [
        'tender_id', 'lot', 'company_id', 'value', 'currency',
        'rated', 'round', 'rate_start', 'rate_end'
    ];

    protected $appends = [
        'currency_code', 'is_current', 'value', 'rated'
    ];

    public function tender() {
        return $this->belongsTo(Tender::class);
    }

    public function company() {
        return $this->belongsTo(Company::class);
    }

    public function currency() {
        return $this->belongsTo(Currency::class, 'currency');
    }

    public function getCurrencyCodeAttribute(): string {
        return $this->currency()->code;
    }

    public function getIsCurrentAttribute(): bool {
        $rate_start = Carbon::parse($this->rate_start)->timestamp;
        $rate_end = Carbon::parse($this->rate_end)->timestamp;
        $now = Carbon::now()->timestamp;

        return $rate_start <= $now && $rate_end >= $now;
    }

    public function getRatedAttribute(): bool {
        $rate_end = Carbon::parse($this->rate_end)->timestamp;
        $now = Carbon::now()->timestamp;

        return $rate_end < $now;
    }

    public function getValueAttribute(): float {
        $rate_end = Carbon::parse($this->rate_end)->timestamp;
        $now = Carbon::now()->timestamp;

        if (!in_array($this->company_id, Auth::user()->companies()->pluck('id'))) {
            return $rate_end <= $now || $this->rated ? $this->attributes['value'] : null;
        }

        return $this->attributes['value'];
    }
}