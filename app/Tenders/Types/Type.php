<?php

namespace App\Tenders\Types;

use App\Tenders\Models\Tender;

abstract class Type {


    protected $_model;
    protected $_data = [];

    public function __construct(Tender $tender, array $data = []) {
        $this->_model = $tender;
        $this->_data = $data;
    }

    public function setData(array $data) {
        $this->_data = $data;
        return $this;
    }

    public function toArray() {
        return $this->_data;
    }

//    public function display($language = null) {
//        $data = $this->toArray();
//
//        $data['bid_security_currency_code'] = !empty($data['bid_security_currency'])
//            ? Currency::getCodeByCurrencyId($data['bid_security_currency'])
//            : 'USD';
//
//        $data['expected_cost'] = 0;
//
//        if (!empty($data['lots'])) {
//            $countries = new ArrayCollection((new CountriesCollection($this->_registry))->get(['id', 'name'], $language));
//
//            foreach ($data['lots'] as $i => $lot) {
//
//                if (!empty($lot['contractor_country_of_registration_countries'])) {
//                    $data['lots'][$i]['contractor_country_of_registration_countries'] = $countries->filter(function ($country) use ($lot) {
//                        return in_array($country['id'], $lot['contractor_country_of_registration_countries']);
//                    });
//                }
//
//                if (!empty($lot['expected_cost'])) {
//                    $data['expected_cost'] += $lot['expected_cost'];
//                }
//
//                if (!empty($lot['expected_cost_currency'])) {
//                    $data['lots'][$i]['expected_cost_currency_code'] = !empty($lot['expected_cost_currency'])
//                        ? Currency::getCodeByCurrencyId($lot['expected_cost_currency'])
//                        : 'USD';
//
//                    $data['expected_cost_currency_code'] = $data['lots'][$i]['expected_cost_currency_code'];
//                }
//
//                if (!empty($lot['region_of_delivery'])) {
//                    foreach ($lot['region_of_delivery'] as $j => $region) {
//                        if (!empty($region['country_id'])) {
//                            $data['lots'][$i]['region_of_delivery'][$j]['country_name'] = (new CountriesModel($this->_registry, $region['country_id']))
//                                ->get('name', $language);
//                        }
//
////						if (!empty($region['region_id'])) {
////							$data['lots'][$i]['region_of_delivery'][$j]['region_name'] = (new RegionsModel($this->_registry, $region['region_id']))
////								->get('name', $language);
////						}
////
////						if (!empty($region['city_id'])) {
////							$data['lots'][$i]['region_of_delivery'][$j]['city_name'] = (new CitiesModel($this->_registry, $region['city_id']))
////								->get('name', $language);
////						}
//                    }
//                }
//
//                if (!empty($lot['classifier_uspsc'])) {
//                    $data['lots'][$i]['classifier_uspsc_name'] = (new ClassifiersUspscModel($this->_registry, $lot['classifier_uspsc']))
//                        ->get(null, $language);
//                }
//
//                if (!empty($lot['classifier_hs'])) {
//                    $data['lots'][$i]['classifier_hs_name'] = (new ClassifiersHsModel($this->_registry, $lot['classifier_hs']))
//                        ->get(null, $language);
//                }
//
//                if (!empty($lot['classifier_cpv'])) {
//                    $data['lots'][$i]['classifier_cpv_name'] = (new ClassifiersCPVModel($this->_registry, $lot['classifier_cpv']))
//                        ->get(null, $language);
//                }
//
//                if (!empty($lot['classifier_isic'])) {
//                    $data['lots'][$i]['classifier_isic_name'] = (new ClassifiersIsicModel($this->_registry, $lot['classifier_isic']))
//                        ->get(null, $language);
//                }
//            }
//        }
//
//        return $data;
//    }

    public function validate() {

        if (empty($this->_data)) {
            throw new Exception('Data is empty.');
        }

        if (empty($this->_data['name'])) {
            throw new Exception('Name is empty.');
        }

        $this->_data['classifier_uspsc'] = [];
        $this->_data['classifier_hs'] = [];
        $this->_data['classifier_cpv'] = [];
        $this->_data['classifier_isic'] = [];
        $this->_data['expected_cost'] = 0;

//        $users_ids = array_map('intval', $this->_data['contacts']);

//        if (!(new CompaniesModel($this->_registry, $this->_data['purchaser_id']))->hasContacts($users_ids)) {
//            throw new Exception('The user is not in the contact list of the company.');
//        }

        // checking procedure type
        // 1 - Open tender, 2 - Selective tender, 3 - Limited tender, 4 - Request of Information
        if (empty($this->_data['procedure_type']) || !in_array($this->_data['procedure_type'], [1, 2, 3, 4])) {
            throw new Exception('Invalid procedure type.');
        }

        // checking procurement category
        // 1 - Goods (default), 2 - Services, 3 - Works
        if (empty($this->_data['procurement_category'])) {
            $this->_data['procurement_category'] = 1;
        } else if (!in_array($this->_data['procurement_category'], [1, 2, 3])) {
            throw new Exception('Invalid procurement category.');
        }

        switch ($this->_data['procurement_category']) {
            case 1: // Goods
                if (empty($this->_data['procurement_description'])) {
                    throw new Exception('Empty procurement description.');
                }

                if (!empty($this->_data['lots'])) {
                    foreach ($this->_data['lots'] as $i => &$lot) {
                        if (empty($lot['title'])) {
                            throw new Exception('Lot [' . ($i + 1) . ']: Empty lot title.');
                        }

                        if (empty(intval($lot['quantity']))) {
                            throw new Exception('Lot [' . ($i + 1) . ']: Empty lot quantity.');
                        }

                        $this->_data['lots'][$i]['quantity'] = intval($lot['quantity']);

                        if (empty($lot['quantity_unit']) || !in_array($lot['quantity_unit'], $this->_units)) {
                            throw new Exception('Lot [' . ($i + 1) . ']: Invalid quantity unit.');
                        }

                        if (empty($lot['expected_cost'])) {
                            throw new Exception('Lot [' . ($i + 1) . ']: Empty lot expected cost.');
                        }

                        $this->_data['expected_cost'] += $lot['expected_cost'];

//						$this->_data['lots'][$i]['id'] = uniqid();

                        if (empty($lot['classifier_uspsc'])) {
                            throw new Exception('Lot [' . ($i + 1) . ']: Empty uspsc classifier.');
                        }

                        try {
                            (new ClassifiersUspscModel($this->_registry, $lot['classifier_uspsc']));
                        } catch (\Exception $e) {
                            throw new Exception('Lot [' . ($i + 1) . ']: Invalid uspsc classifier.');
                        }

                        if (empty($lot['classifier_hs'])) {
                            throw new Exception('Lot [' . ($i + 1) . ']: Empty hs classifier.');
                        }

                        try {
                            (new ClassifiersHsModel($this->_registry, $lot['classifier_hs']));
                        } catch (\Exception $e) {
                            throw new Exception('Lot [' . ($i + 1) . ']: Invalid hs classifier.');
                        }

                        if (empty($lot['classifier_cpv'])) {
                            throw new Exception('Lot [' . ($i + 1) . ']: Empty cpv classifier.');
                        }

                        try {
                            (new ClassifiersCPVModel($this->_registry, $lot['classifier_cpv']));
                        } catch (\Exception $e) {
                            throw new Exception('Lot [' . ($i + 1) . ']: Invalid cpv classifier.');
                        }

                        if (empty($lot['period_of_delivery']) || !is_array($lot['period_of_delivery']) || count($lot['period_of_delivery']) !== 2) {
                            throw new Exception('Lot [' . ($i + 1) . ']: Invalid period of delivery.');
                        }

                        if (!in_array($lot['delivery_terms'], [1, 0])) {
                            throw new Exception('Lot [' . ($i + 1) . ']: Invalid delivery terms.');
                        }

                        if ($lot['delivery_terms'] == 1) {
                            if (empty($lot['delivery_terms_incoterms']) || !in_array($lot['delivery_terms_incoterms'], $this->_incoterms)) {
                                throw new Exception('Lot [' . ($i + 1) . ']: Invalid incoterm selected.');
                            }
                        } else {
                            if (empty($lot['delivery_terms_comment'])) {
                                throw new Exception('Lot [' . ($i + 1) . ']: Empty delivery terms comment.');
                            }
                        }
                        $this->_data['classifier_uspsc'][] = $lot['classifier_uspsc'];
                        $this->_data['classifier_hs'][] = $lot['classifier_hs'];
                        $this->_data['classifier_cpv'][] = $lot['classifier_cpv'];

                        // Request Certificate of Origin
                        if (empty($lot['request_certificate_of_origin'])) {
                            $lot['request_certificate_of_origin'] = false;
                        } else {
                            $lot['request_certificate_of_origin'] = boolval($lot['request_certificate_of_origin']);
                        }

                        if (empty($lot['procurement_item_country_of_origin'])) {
                            $lot['procurement_item_country_of_origin'] = 1; // Any country.
                        }

                        switch ($lot['procurement_item_country_of_origin']) {
                            case 1: // Any country
                            case 2: // Countries with Trade Agreements
                                break;
                            case 3: // Any country.
                                if (empty($lot['procurement_item_countries']) || !is_array($lot['procurement_item_countries'])) {
                                    throw new Exception('Lot [' . ($i + 1) . ']: Empty procurement item countries.');
                                }

                                if (count(array_diff($lot['procurement_item_countries'], (new CountriesCollection($this->_registry))->column('id')))) {
                                    throw new Exception('Lot [' . ($i + 1) . ']: Invalid procurement item countries.');
                                }

//                                foreach ($lot['procurement_item_countries'] as $region) {
//                                    if (empty($region['country_id'])) {
//                                        throw new Exception('Lot [' . ($i + 1) . ']: Empty country_id in procurement item country.');
//                                    }
//
//                                    try {
//                                        new CountriesModel($this->_registry, $region['country_id']);
//                                    } catch (\Exception $e) {
//                                        throw new Exception('Lot [' . ($i + 1) . ']: Invalid country_id in procurement item country.');
//                                    }
//                                }
                                break;
                            default:
                                throw new Exception('Lot [' . ($i + 1) . ']: Invalid procurement item country of origin.');
                        }

                        // Region of delivery
                        if (empty($lot['region_of_delivery']) || !is_array($lot['region_of_delivery'])) {
                            throw new Exception('Lot [' . ($i + 1) . ']: Invalid region of delivery selected.');
                        }

                        foreach ($lot['region_of_delivery'] as $region) {
                            if (empty($region['country_id'])) {
                                throw new Exception('Lot [' . ($i + 1) . ']: Empty country_id in region of delivery.');
                            }

                            try {
                                new CountriesModel($this->_registry, $region['country_id']);
                            } catch (\Exception $e) {
                                throw new Exception('Lot [' . ($i + 1) . ']: Invalid country_id in region of delivery.');
                            }
//
//							if (empty($region['region_id'])) {
//								throw new Exception('Lot ['.($i + 1).']: Empty region_id in region of delivery.');
//							}
//
//							try {
//								new RegionsModel($this->_registry, $region['region_id']);
//							} catch (\Exception $e) {
//								throw new Exception('Lot ['.($i + 1).']: Invalid region_id in region of delivery.');
//							}
//
//							if (empty($region['city_id'])) {
//								throw new Exception('Lot ['.($i + 1).']: Empty city_id in region of delivery.');
//							}
//
//							try {
//								new CitiesModel($this->_registry, $region['city_id']);
//							} catch (\Exception $e) {
//								throw new Exception('Lot ['.($i + 1).']: Invalid city_id in region of delivery.');
//							}
                        }
                    }
                }

                break;
            case 2:
            case 3:
                if (empty($this->_data['procurement_description'])) {
                    throw new Exception('Empty procurement description.');
                }

                if (!empty($this->_data['lots'])) {
                    foreach ($this->_data['lots'] as $i => $lot) {
                        if (empty($lot['title'])) {
                            throw new Exception('Lot [' . ($i + 1) . ']: Empty lot title.');
                        }

                        if (empty($lot['description'])) {
                            throw new Exception('Lot [' . ($i + 1) . ']: Empty lot description.');
                        }

                        if (empty($lot['expected_cost'])) {
                            throw new Exception('Lot [' . ($i + 1) . ']: Empty lot expected cost.');
                        }

                        $this->_data['expected_cost'] += $lot['expected_cost'];

                        if (empty($lot['expected_cost_currency']) || !Currency::validateCurrencyId($lot['expected_cost_currency'])) {
                            throw new Exception('Lot [' . ($i + 1) . ']: Invalid lot expected cost currency.');
                        }

                        $this->_data['lots'][$i]['expected_cost_visible_to_seller'] = !empty($lot['expected_cost_visible_to_seller']) ? boolval($lot['expected_cost_visible_to_seller']) : false;

//                        $this->_data['lots'][$i]['id'] = uniqid();

                        if (empty($lot['classifier_isic'])) {
                            throw new Exception('Lot [' . ($i + 1) . ']: Empty isic classifier.');
                        }

                        try {
                            (new ClassifiersIsicModel($this->_registry, $lot['classifier_isic']));
                        } catch (\Exception $e) {
                            throw new Exception('Lot [' . ($i + 1) . ']: Invalid isic classifier.');
                        }

                        if (empty($lot['classifier_cpv'])) {
                            throw new Exception('Lot [' . ($i + 1) . ']: Empty cpv classifier.');
                        }

                        try {
                            (new ClassifiersCPVModel($this->_registry, $lot['classifier_cpv']));
                        } catch (\Exception $e) {
                            throw new Exception('Lot [' . ($i + 1) . ']: Invalid cpv classifier.');
                        }

                        $this->_data['classifier_isic'][] = $lot['classifier_isic'];
                        $this->_data['classifier_cpv'][] = $lot['classifier_cpv'];

                        // Validate USPSC

                        if (empty($lot['contract_period']) || !is_array($lot['contract_period']) || count($lot['contract_period']) !== 2) {
                            throw new Exception('Lot [' . ($i + 1) . ']: Invalid period of contract.');
                        }

                        if (!in_array($lot['contractor_country_of_registration'], [1, 2, 3])) {
                            throw new Exception('Lot [' . ($i + 1) . ']: Invalid contractor country of registration.');
                        }

                        if ($lot['contractor_country_of_registration'] === 3) {
                            if (empty($lot['contractor_country_of_registration_countries']) || !is_array($lot['contractor_country_of_registration_countries'])) {
                                throw new Exception('Lot [' . ($i + 1) . ']: Empty contractor countries of registration.');
                            }

                            if (count(array_diff($lot['contractor_country_of_registration_countries'], (new CountriesCollection($this->_registry))->column('id')))) {
                                throw new Exception('Lot [' . ($i + 1) . ']: Invalid contractor countries of registration.');
                            }
                        }

                        // Region of delivery
                        if (empty($lot['region_of_delivery']) || !is_array($lot['region_of_delivery'])) {
                            throw new Exception('Lot [' . ($i + 1) . ']: Invalid region of delivery selected.');
                        }

                        foreach ($lot['region_of_delivery'] as $region) {
                            if (empty($region['country_id'])) {
                                throw new Exception('Lot [' . ($i + 1) . ']: Empty country_id in region of delivery.');
                            }

                            try {
                                new CountriesModel($this->_registry, $region['country_id']);
                            } catch (\Exception $e) {
                                throw new Exception('Lot [' . ($i + 1) . ']: Invalid country_id in region of delivery.');
                            }

//							if (empty($region['region_id'])) {
//								throw new Exception('Lot ['.($i + 1).']: Empty region_id in region of delivery.');
//							}
//
//							try {
//								new RegionsModel($this->_registry, $region['region_id']);
//							} catch (\Exception $e) {
//								throw new Exception('Lot ['.($i + 1).']: Invalid region_id in region of delivery.');
//							}

//							if (empty($region['city_id'])) {
//								throw new Exception('Lot ['.($i + 1).']: Empty city_id in region of delivery.');
//							}
//
//							try {
//								new CitiesModel($this->_registry, $region['city_id']);
//							} catch (\Exception $e) {
//								throw new Exception('Lot ['.($i + 1).']: Invalid city_id in region of delivery.');
//							}
                        }
                    }
                }

                break;
        }

        // notice publication
        if (!empty($this->_data['notice_publication_date']) && !Validator::datetime($this->_data['notice_publication_date'], 'Y-m-d')) {
            throw new Exception('Invalid notice publication date.');
        }

        // Bid security validation
        if (empty($this->_data['bid_security'])) {
            $this->_data['bid_security'] = 1;
        } else {
            switch ($this->_data['bid_security']) {
                case 2: // Guarantee (bond) issued by insurance company/financial institution
                case 3: // Letter of credit
                case 4: // Performance bond issued by surety
                    if (empty(intval($this->_data['bid_security_period']))) {
                        throw new Exception('Bid security: Period of validity.');
                    } else {
                        $this->_data['bid_security_period'] = intval($this->_data['bid_security_period']);
                    }

                    if (empty(floatval($this->_data['bid_security_sum']))) {
                        throw new Exception('Bid security: Empty sum.');
                    } else {
                        $this->_data['bid_security_sum'] = floatval($this->_data['bid_security_sum']);
                    }

                    if (empty($this->_data['bid_security_currency']) || !Currency::validateCurrencyId($this->_data['bid_security_currency'])) {
                        throw new Exception('Bid security: Empty or invalid currency code.');
                    }

                    break;
            }
        }

        // Payment Security
        // 1 - Advance payment
        // 2 - Escrow services - Escrow.com
        // 3 - Escrow services - Payoneer.com
        // 4 - Letter of credit
        // 5 - Other
        if (empty($this->_data['payment_security'])) {
            $this->_data['payment_security'] = 5;
        } else if (!in_array($this->_data['payment_security'], [1, 2, 3, 4, 5])) {
            throw new Exception('Invalid payment security.');
        }

        if (empty($this->_data['purchaser_id'])) {
            throw new Exception('Purchaser is empty.');
        }

        if (!empty($this->_data['reverse_auction']) && $this->_data['reverse_auction'] == true) {
            if (!empty($this->_data['reverse_auction_duration']) && !in_array($this->_data['reverse_auction_duration'], [1, 12, 24])) {
                throw new Exception('Reverse auction duration is invalid.');
            }

            if (!empty($this->_data['reverse_auction_min_step_percent'])) {
                $this->_data['reverse_auction_min_step_percent'] = floatval($this->_data['reverse_auction_min_step_percent']);

                if ($this->_data['reverse_auction_min_step_percent'] > 100) {
                    $this->_data['reverse_auction_min_step_percent'] = 100;
                }
            }

            if (!empty($this->_data['reverse_auction_min_step_cash'])) {
                $this->_data['reverse_auction_min_step_cash'] = floatval($this->_data['reverse_auction_min_step_cash']);
            }

            // lot's validation
            if (empty($this->_data['reverse_auction_min_step_type'])) {
                $this->_data['reverse_auction_min_step_type'] = false;
            } else {
                $this->_data['reverse_auction_min_step_type'] = boolval($this->_data['reverse_auction_min_step_type']);
            }
        }

        if (!empty($this->_data['non_price_criteria'])) {
            foreach ($this->_data['non_price_criteria'] as $key => $datum) {
                if (empty($datum['criteria'])) {
                    throw new Exception('Non price criteria [' . ($key + 1) . ']: Criteria field is empty.');
                }

                if (empty($datum['description'])) {
                    throw new Exception('Non price criteria [' . ($key + 1) . ']: Description field is empty.');
                }

                if (empty(floatval($datum['weight']))) {
                    throw new Exception('Non price criteria [' . ($key + 1) . ']: Weight field is empty.');
                }
            }

            if (array_sum(array_column($this->_data['non_price_criteria'], 'weight')) > 50) {
                throw new V('Maximum sum of all criterias is more then 50%.');
            }
        }

//		if (!empty($this->_data['bidder_mandatory_requirements'])) {
//			foreach ($this->_data['bidder_mandatory_requirements'] as $key => $datum) {
//				if (empty($datum['title'])) {
//					throw new Exception('Bidder mandatory requirements ['.($key + 1).']: Title field is empty.');
//				}
//
//				if (empty($datum['description'])) {
//					throw new Exception('Bidder mandatory requirements ['.($key + 1).']: Description field is empty.');
//				}
//
//				if (empty($datum['question'])) {
//					throw new Exception('Bidder mandatory requirements ['.($key + 1).']: Qualification question field is empty.');
//				}
//
//				//	Answer type (yes/no, number, date, text)
//				// TODO: Add validation of type
//				if (empty($datum['answer_type'])) {
//					throw new Exception('Bidder mandatory requirements ['.($key + 1).']: Answer type field is empty.');
//				}
//
//			}
//		}
//
//		if (!empty($this->_data['bidder_additional_requirements'])) {
//			foreach ($this->_data['bidder_additional_requirements'] as $key => $datum) {
//				if (empty($datum['title'])) {
//					throw new Exception('Bidder additional requirements ['.($key + 1).']: Title field is empty.');
//				}
//
//				if (empty($datum['description'])) {
//					throw new Exception('Bidder additional requirements ['.($key + 1).']: Description field is empty.');
//				}
//
//				if (empty($datum['question'])) {
//					throw new Exception('Bidder additional requirements ['.($key + 1).']: Qualification question field is empty.');
//				}
//
//				//	Answer type (yes/no, number, date, text)
//				// TODO: Add validation of type
//				if (empty($datum['answer_type'])) {
//					throw new Exception('Bidder additional requirements ['.($key + 1).']: Answer type field is empty.');
//				}
//
//			}
//		}
//
//		if ($this->_data['procurement_category'] == 1) {
//			if (!empty($this->_data['procured_item_mandatory_requirements'])) {
//				foreach ($this->_data['procured_item_mandatory_requirements'] as $key => $datum) {
//					if (empty($datum['title'])) {
//						throw new Exception('Procured item mandatory requirements ['.($key + 1).']: Title field is empty.');
//					}
//
//					if (empty($datum['description'])) {
//						throw new Exception('Procured item mandatory requirements ['.($key + 1).']: Description field is empty.');
//					}
//
//					if (empty($datum['question'])) {
//						throw new Exception('Procured item mandatory requirements ['.($key + 1).']: Qualification question field is empty.');
//					}
//
//					//	Answer type (yes/no, number, date, text)
//					// TODO: Add validation of type
//					if (empty($datum['answer_type'])) {
//						throw new Exception('Procured item mandatory requirements ['.($key + 1).']: Answer type field is empty.');
//					}
//
//				}
//			}
//
//			if (!empty($this->_data['procured_item_additional_requirements'])) {
//				foreach ($this->_data['procured_item_additional_requirements'] as $key => $datum) {
//					if (empty($datum['title'])) {
//						throw new Exception('Procured item additional requirements ['.($key + 1).']: Title field is empty.');
//					}
//
//					if (empty($datum['description'])) {
//						throw new Exception('Procured item additional requirements ['.($key + 1).']: Description field is empty.');
//					}
//
//					if (empty($datum['question'])) {
//						throw new Exception('Procured item additional requirements ['.($key + 1).']: Qualification question field is empty.');
//					}
//
//					//	Answer type (yes/no, number, date, text)
//					// TODO: Add validation of type
//					if (empty($datum['answer_type'])) {
//						throw new Exception('Procured item additional requirements ['.($key + 1).']: Answer type field is empty.');
//					}
//
//				}
//			}
//		}

        // Validate tender-specific properties.
        $this->_validate();

        return $this;
    }

    abstract protected function _validate();

    protected $_incoterms = [
        'EXW',
        'FCA',
        'DAT',
        'DAP',
        'DDP',
        'FAS',
        'FOB',
        'СРТ',
        'CIP',
        'CFR',
        'CIF',
    ];

    protected $_units = [
        'var',
        'kilo',
        'kilowatt-hour',
        'kilowatt',
        'kilowatt-hour',
        'megawatt-hour_per_hour',
        'tones',
        'kilogram',
        'kilometer',
        'ton-kilometer',
        'cubic_meter',
        'one_thousand_cubic_meters',
        'square_meter',
        'linear_meter',
        'meter',
        'centimeter_cubic',
        'centimeter_square',
        'centimeter',
        'hour',
        'gram',
        'hectare',
        'liter',
        'set',
        'set',
        'unit',
        'bottle',
        'dose',
        'lot',
        'service',
        'flight',
        'block',
        'box',
        'packaging',
        'couple',
        'pieces',
        'month',
        'roll',
        'spool',
        'kilocalorie',
        'gigacalorie',
        'of_people',
        'person-hour',
        'working_day',
        'bundle',
        'thousand_pieces',
        'of_work'
    ];
}