<?php

namespace App\AttributeOptions\Models;

use Illuminate\Database\Eloquent\Model;

class AttributeOptionTranslation extends Model {
    public $timestamps = false;

    public $translatedAttributes = ['name'];

    protected $fillable = ['name'];
}
