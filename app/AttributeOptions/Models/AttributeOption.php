<?php

namespace App\AttributeOptions\Models;

use App\Attributes\Models\Attribute;
use App\Http\Traits\Filter;
use App\Http\Traits\Translatable;
use Illuminate\Database\Eloquent\Model;

class AttributeOption extends Model {
    use Filter;
    use Translatable;

    public $timestamps = false;

    public $translatedAttributes = ['name'];

    protected $fillable = ['name', 'attribute_id'];

    public function attribute() {
        return $this->hasOne(Attribute::class, 'id', 'attribute_id');
    }

    protected $hidden = ['pivot'];
}

/**
 * @SWG\Definition(
 *   definition="AttributeOptions",
 *   type="object",
 *   allOf={
 *       @SWG\Schema(
 *           @SWG\Property(property="name", type="string"),
 *           @SWG\Property(property="attribute_id", type="integer"),
 *       )
 *   }
 * )
 */


/**
 * @SWG\Definition(
 *   definition="newAttributeOptions",
 *   type="object",
 *   required={"name", "attribute_id"},
 *   allOf={
 *       @SWG\Schema(
 *           @SWG\Property(property="name", type="string"),
 *           @SWG\Property(property="attribute_id", type="integer")
 *       )
 *   }
 * )
 */