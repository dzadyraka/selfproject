<?php

namespace App\Questions\Models;

use App\Companies\Traits\HasCompanyId;
use App\Http\Traits\HasUserId;
use Illuminate\Database\Eloquent\Model;

abstract class Question extends Model {
	use HasUserId;
	use HasCompanyId;

	const CREATED_AT = 'created_date';
	const UPDATED_AT = 'updated_date';

	protected $fillable = [
		'content', 'user_id', 'parent_id', 'company_id'
	];
}
