<?php

namespace App\Questions\Models;


class ProductsQuestion extends Question {
    protected $table = 'products_questions';

	protected $fillable = [
		'content', 'user_id', 'parent_id', 'company_id', 'product_id'
	];
}
