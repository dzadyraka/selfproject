<?php

namespace App\Questions\Models;


class TendersQuestion extends Question {
	protected $table = 'tenders_questions';

	protected $fillable = [
		'content', 'user_id', 'parent_id', 'company_id', 'tender_id'
	];
}
