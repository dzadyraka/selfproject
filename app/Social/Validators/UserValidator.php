<?php

namespace App\Social\Validators;


use App\Social\Integrations\SocialIntegration;
use Illuminate\Support\Facades\Validator;

class UserValidator {
	public static function validate(string $integration, array $data) {
		$validation = Validator::make($data, [
			'user_id' => 'required',
			'access_token' => 'required'
		]);

		if ($validation->fails()) {
			abort(400, $validation->errors());
		}

		$user_info = SocialIntegration::getIntegration($integration)
			->setToken($data['access_token'])
			->getUserInfo();

		if ($user_info['id'] !== $data['user_id']) {
			abort(400, "user data is invalid");
		}
	}
}