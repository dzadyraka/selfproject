<?php

namespace App\Social\Listeners;

use App\Social\Events\SocialValidateEvent;
use App\Social\Models\SocialAccount;
use App\Social\Validators\UserValidator;
use Illuminate\Support\Facades\Validator;

class SocialValidateListener {
	/**
	 * Handle the event.
	 *
	 * @param \App\Social\Events\SocialValidateEvent $event
	 * @return void
	 */
	public function handle(SocialValidateEvent $event) {
		if (!$event->data) {
			abort(400, 'there is no social object');
		}

		if (empty($event->data['user_id']) || empty($event->data['access_token']) || empty($event->data['integration'])) {
			abort(400, '');
		}

		$validator = Validator::make(
			$event->data,
			[
				'user_id' => 'required',
				'access_token' => 'required',
				'integration' => 'required'
			]
		);

		if ($validator->fails()) {
			abort(400, ['success' => false, 'errors' => $validator->errors()]);
		}

		if (SocialAccount::whereIntegration($event->data['integration'])
			->whereUserId($event->data['user_id'])->find()) {
			abort(400, 'user already exists.');
		}

		UserValidator::validate($event->data['integration'], $event->data);
	}
}
