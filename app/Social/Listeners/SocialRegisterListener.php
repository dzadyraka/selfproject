<?php

namespace App\Social\Listeners;

use App\Social\Events\SocialRegisterEvent;
use App\Social\Models\SocialAccount;

class SocialRegisterListener {
	/**
	 * Handle the event.
	 *
	 * @param \App\Social\Events\SocialRegisterEvent $event
	 * @return void
	 */
	public function handle(SocialRegisterEvent $event) {
		SocialAccount::create($event->data);
	}
}
