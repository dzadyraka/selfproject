<?php

namespace App\Social\Models;


use App\Http\Traits\HasUserId;
use App\Users\Models\User;
use Illuminate\Database\Eloquent\Model;

class SocialAccount extends Model {
	use HasUserId;

	public $timestamps = false;

	protected $fillable = ['access_token', 'user_id', 'integration_user_id', 'integration'];

    public function user() {
        return $this->hasOne(User::class, 'id', 'user_id');
	}
}