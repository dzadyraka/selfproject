<?php

namespace App\Social\Integrations;


abstract class SocialIntegration {

	protected $_token;

	static public function getIntegration(string $integration) {
		switch ($integration) {
			case 'facebook':
				return new FacebookIntegration();
			case 'google':
				return new GoogleIntegration();
			case 'linkedin':
				return new LinkedinIntegration();
		}
	}

	public function setToken(string $token): self {
		$this->_token = $token;

		return $this;
	}

	public abstract function getUserInfo(): array;

	protected function _httpRequest(string $request_url, array $headers = []): array {
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $request_url);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		if (!empty($headers)) {
			curl_setopt($ch, CURLOPT_HTTPHEADER, array_map(function ($header, $value) {
				return "$header: $value";
			}, array_keys($headers), $headers));
		}

		return json_decode(curl_exec($ch), 1);
	}
}