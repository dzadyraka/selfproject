<?php

namespace App\Social\Events;

use App\Users\Models\User;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Http\Request;
use Illuminate\Queue\SerializesModels;

class SocialRegisterEvent {
	use Dispatchable, InteractsWithSockets, SerializesModels;

	public $data;

	/**
	 * Create a new event instance.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param \App\Users\Models\User $user
	 */
	public function __construct(Request $request, User $user) {
		$this->data = $request->post('social');
		$this->data['user_id'] = $user->id;
	}
}
