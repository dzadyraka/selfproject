<?php

namespace App\Social\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Http\Request;
use Illuminate\Queue\SerializesModels;

class SocialValidateEvent {
	use Dispatchable, InteractsWithSockets, SerializesModels;

	public $data;

	/**
	 * Create a new event instance.
	 *
	 * @param \Illuminate\Http\Request $request
	 */
	public function __construct(Request $request) {
		$this->data = $request->post('social');
	}
}
