<?php

namespace App\Social\Services;


use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class LinkedInService {
    private $oauth_api_url = 'https://www.linkedin.com/oauth/v2/';
    private $api_url = 'https://api.linkedin.com/v2/';


    public function getAccessToken(string $code): array {
        try {
            $response = $this->getOAuthClient()->post('accessToken', [
                'form_params' => [
                    'grant_type' => 'authorization_code',
                    'code' => $code,
                    'redirect_uri' => $this->getRedirectUrl(),
                    'client_id' => env('LINKEDIN_CLIENT_ID'),
                    'client_secret' => env('LINKEDIN_CLIENT_SECRET')
                ]
            ]);
        } catch (RequestException $e) {
            if ($e->getResponse()->getStatusCode() == 400) {
                return [
                    'error' => 'Response code is invalid'
                ];
            }

            return [
                'error' => 'Request error'
            ];
        }

        return json_decode($response->getBody()->getContents(), true);
    }

    public function getProfileInfo(string $token): array {
        return json_decode($this->getClient($token)->get('me')->getBody()->getContents(), true);
    }

    public function getOAuthUrl(): string {
        return "{$this->oauth_api_url}authorization?{$this->getOAuthQueryParams()}";
    }

    private function getOAuthQueryParams(): string {
        return http_build_query([
            'response_type' => 'code',
            'client_id' => env('LINKEDIN_CLIENT_ID'),
            'redirect_uri' => $this->getRedirectUrl(),
            'state' => csrf_token(),
            'scope' => join(' ', ['r_basicprofile', 'r_emailaddress', /*'r_fullprofile'*/])
        ]);
    }

    private function getRedirectUrl(): string {
        return env('APP_URL') . '/api/v1/social/linkedin/oauth/callback';
    }

    private function getOAuthClient(): Client {
        return new Client(['base_uri' => $this->oauth_api_url]);
    }

    private function getClient(string $token): Client {
        return new Client(['base_uri' => $this->api_url, 'headers' => [
            'Authorization' => "Bearer $token"
        ]]);
    }
}