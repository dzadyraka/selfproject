<?php

namespace App\Messages\Models;

use JWTAuth;
use App\Users\Models\User;
use App\Http\Traits\Filter;
use Illuminate\Database\Eloquent\Model;

class Message extends Model {

    use Filter;

    const CREATED_AT = 'sending_date';

    public $timestamps = false;

    protected $fillable = [
        'content', 'status', 'from_user_id', 'conversation_id', 'seen', 'attachments', 'hide'
    ];

    protected $casts = [
        'seen' => 'array',
        'attachments' => 'array',
        'hide' => 'array',
    ];

    protected $appends = ['is_seen'];

    public static function boot() {
        parent::boot();

        static::saving(function (Model $model) {
            $model->from_user_id = JWTAuth::parseToken()->toUser()->id;
            $model->status = 'send';
            $model->seen = [JWTAuth::parseToken()->toUser()->id];
        });
    }

    public function getIsSeenAttribute(): bool {
        return in_array(JWTAuth::parseToken()->toUser()->id, $this->seen);
    }

    public function user() {
        return $this->belongsTo(User::class, 'from_user_id');
    }

    public function conversation() {
        return $this->belongsTo(Conversation::class);
    }
}


/**
 *  @SWG\Definition(
 *   definition="Message",
 *   type="object",
 *   allOf={
 *       @SWG\Schema(
 *           @SWG\Property(property="content", type="string"),
 *           @SWG\Property(property="status", type="string"),
 *           @SWG\Property(property="sending_date", type="string", format="date-time"),
 *           @SWG\Property(property="from_user_id", type="integer"),
 *           @SWG\Property(
 *                  property="seen",
 *                  type="array",
 *                  @SWG\Items(type="integer")
 *           ),
 *           @SWG\Property(property="conversation_id", type="integer"),
 *           @SWG\Property(
 *                  property="hide",
 *                  type="array",
 *                  @SWG\Items(type="integer")
 *           ),
 *           @SWG\Property(
 *                  property="attachments",
 *                  type="array",
 *                  @SWG\Items(ref="#/definitions/File")
 *           ),
 *           @SWG\Property(property="is_seen", type="boolean"),
 *       )
 *   }
 * )
 */