<?php

namespace App\Messages\Models;

use Illuminate\Database\Eloquent\Builder;
use JWTAuth;
use App\Users\Models\User;
use Illuminate\Database\Eloquent\Model;

class Conversation extends Model {

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'updated_date';

    protected $fillable = [
        'title', 'seen', 'hide'
    ];

    protected $casts = [
        'seen' => 'array',
        'hide' => 'array',
        'last_sender_id' => 'integer'
    ];

    protected $appends = [
        'is_seen', 'last_sender_id', 'first_name', 'last_name', 'avatar', 'last_message_content', 'last_message_id'
    ];

    public static function boot() {
        parent::boot();

        static::saving(function (Model $model) {
            $model->seen = [JWTAuth::parseToken()->toUser()->id];
        });
    }

    public function users() {
        return $this->belongsToMany(User::class, 'conversation_users');
    }

    public function messages() {
        return $this->hasMany(Message::class, 'conversation_id')->whereRaw("hide != ANY ('{".JWTAuth::parseToken()->toUser()->id."}')");
    }

    public function lastUser(): object {
        return $this->hasOne(Message::class, 'conversation_id')->with(['user'])->orderByDesc('sending_date')->first();
    }

    /**
     * Set seen status for user
     *
     * @return bool
     */
    public function getIsSeenAttribute(): bool {
        return in_array(JWTAuth::parseToken()->toUser()->id, $this->seen);
    }

    /**
     * Set last sender id
     *
     * @return mixed
     */
    public function getLastSenderIdAttribute(): int {
        return $this->lastUser()['from_user_id'];
    }

    /**
     * Set first name
     *
     * @return mixed
     */
    public function getFirstNameAttribute(): string {
        return isset($this->lastUser()['user']['first_name']) ? $this->lastUser()['user']['first_name'] : '';
    }

    /**
     * Set last name
     *
     * @return mixed
     */
    public function getLastNameAttribute(): string {
        return isset($this->lastUser()['user']['last_name']) ? $this->lastUser()['user']['last_name'] : '';
    }

    /**
     * Set avatar
     *
     * @return mixed
     */
    public function getAvatarAttribute(): string {
        return $this->lastUser()['user']['avatar'];
    }

    /**
     * Set last message content
     *
     * @return mixed
     */
    public function getLastMessageContentAttribute(): string {
        return $this->lastUser()['content'];
    }

    /**
     * Set last message id
     *
     * @return mixed
     */
    public function getLastMessageIdAttribute(): int {
        return $this->lastUser()['id'];
    }

    /**
     * Get all unread messages
     *
     * @return \Illuminate\Support\Collection
     */
    public function unreadMessages() {
        return $this->hasMany(Message::class, 'conversation_id')->where('from_user_id', '!=', JWTAuth::parseToken()->toUser()->id)->whereRaw("seen != ANY ('{".JWTAuth::parseToken()->toUser()->id."}')")->get();
    }

    /**
     * Get all user dialogs
     *
     * @param int $id
     * @return mixed
     */
    public function userDialogs(int $id) {
        return $this->whereHas('users', function ($participants) use ($id) {
            $participants->where('user_id', $id);
        })
            ->orderBy('updated_date', 'DESC')
            ->get();
    }
}

/**
 * @SWG\Definition(
 *   definition="Conversation",
 *   type="object",
 *   allOf={
 *       @SWG\Schema(
 *           @SWG\Property(property="title", type="string"),
 *           @SWG\Property(
 *                  property="seen",
 *                  type="array",
 *                  @SWG\Items(type="integer")
 *           ),
 *           @SWG\Property(
 *                  property="hide",
 *                  type="array",
 *                  @SWG\Items(type="integer")
 *           ),
 *           @SWG\Property(property="created_date", type="string", format="date-time"),
 *           @SWG\Property(property="updated_date", type="string", format="date-time"),
 *           @SWG\Property(property="is_seen", type="boolean"),
 *           @SWG\Property(property="last_sender_id", type="integer"),
 *           @SWG\Property(property="first_name", type="string"),
 *           @SWG\Property(property="last_name", type="string"),
 *           @SWG\Property(property="avatar", type="string"),
 *           @SWG\Property(property="last_message_content", type="string"),
 *           @SWG\Property(property="last_message_id", type="integer"),
 *           @SWG\Property(
 *                  property="messages",
 *                  type="array",
 *                  @SWG\Items(ref="#/definitions/Message"),
 *           ),
 *           @SWG\Property(
 *                  property="users",
 *                  type="array",
 *                  @SWG\Items(ref="#/definitions/User"),
 *           ),
 *       )
 *   }
 * )
 */

/**
 * @SWG\Definition(
 *   definition="Conversations",
 *   type="object",
 *   allOf={
 *       @SWG\Schema(
 *           @SWG\Property(property="title", type="string"),
 *           @SWG\Property(
 *                  property="seen",
 *                  type="array",
 *                  @SWG\Items(type="integer")
 *           ),
 *           @SWG\Property(
 *                  property="hide",
 *                  type="array",
 *                  @SWG\Items(type="integer")
 *           ),
 *           @SWG\Property(property="created_date", type="string", format="date-time"),
 *           @SWG\Property(property="updated_date", type="string", format="date-time"),
 *           @SWG\Property(property="is_seen", type="boolean"),
 *           @SWG\Property(property="last_sender_id", type="integer"),
 *           @SWG\Property(property="first_name", type="string"),
 *           @SWG\Property(property="last_name", type="string"),
 *           @SWG\Property(property="avatar", type="string"),
 *           @SWG\Property(property="last_message_content", type="string"),
 *           @SWG\Property(property="last_message_id", type="integer"),
 *       )
 *   }
 * )
 */
