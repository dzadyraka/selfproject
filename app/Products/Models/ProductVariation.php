<?php

namespace App\Products\Models;

use Illuminate\Database\Eloquent\Model;

class ProductVariation extends Model {

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'edited_date';


    protected $fillable = [
        'name', 'product_id', 'images', 'sku', 'price', 'minimum_bulk_price', 'quantity_available',
        'minimum_ordery', 'negotiate', 'bulk_prices', 'has_sample', 'sample_price'
    ];

    protected $casts = [
        'images' => 'json',
        'bulk_prices' => 'json',
        'price' => 'float',
        'sample_price' => 'float',
    ];

    public function attributesList() {
        return $this->hasMany(ProductVariationAttribute::class);
    }
}
