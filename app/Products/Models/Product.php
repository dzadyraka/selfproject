<?php

namespace App\Products\Models;

use App\Categoties\Models\Category;
use App\Classifiers\Models\ClassifiersHs;
use App\Locations\Models\Country;
use Illuminate\Support\Facades\DB;
use JWTAuth;
use App\Currency\Currency;
use App\Users\Models\User;
use App\Http\Traits\Filter;
use App\Companies\Models\Company;
use App\Http\Traits\Translatable;
use Illuminate\Database\Eloquent\Model;

class Product extends Model {
    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'edited_date';

    use Translatable;
    use Filter;

    public $translatedAttributes = ['name', 'description'];

    protected $fillable = [
        'rating',
        'price',
        'currency_id',
        'discount_percent',
        'discount_cash',
        'earliest_shipping_date',
        'quantity_available',
        'quantity_standard',
        'minimum_ordery',
        'company_id',
        'created_by',
        'unit',
        'bulk_prices',
        'certifications',
        'place_of_origin',
        'hs_classifier',
        'location_city',
        'location_country',
        'port',
        'external_id',
        'integration_name',
        'attachments',
        'negotiate',
        'images',
        'is_draft',
        'parent_id',
        'paid_order',
        'weight',
        'dim_weight',
        'width',
        'height',
        'depth',
        'shipping',
        'sku',
        'has_sample',
        'sample_price',
        'is_variable',
        'category_id',
        'quantity_standard',
        'gtin',
        'mpn',
        'sale_price',
        'sale_period',
        'brand',
        'has_sale',
        'unit_id',
        'availability',
        'availability_date',
        'sample_free',
        'sample_length',
        'sample_weight_unit',
        'sample_size_unit'
    ];

    protected $casts = [
        'rating' => 'float',
        'bulk_prices' => 'json',
        'certifications' => 'json',
        'attachments' => 'json',
        'images' => 'json',
        'shipping' => 'json',
        'price' => 'float',
        'sample_price' => 'float',
        'sale_period' => 'json',
    ];

    protected $appends = [
        'is_request_sample',
    ];

    public function getIsRequestSampleAttribute() {
        if (!empty($this->request_sample)) {
            return true;
        }
        return false;
    }

    public static function boot() {
        parent::boot();
        parent::creating(function (Model $model) {
            $model->created_by = JWTAuth::parseToken()->toUser()->id;
        });
    }

    public function creator() {
        return $this->hasOne(User::class, 'id', 'created_by');
    }

    public function company() {
        return $this->belongsTo(Company::class);
    }

    public function currency() {
        return $this->hasOne(Currency::class, 'id', 'currency_id');
    }

    public function bookmarksProducts() {
        return false;
    }

    public function categories() {
        return $this->belongsToMany(Category::class, 'product_categories');
    }

    public function request_sample() {
        return $this->hasOne(Product::class, 'parent_id', 'id');
    }

    public function attributesList() {
        return $this->hasMany(ProductAttribute::class)->with('optionsList');
    }

    public function variations() {
        return $this->hasMany(ProductVariation::class, 'product_id', 'id');
    }

    public function hs() {
        return $this->hasOne(ClassifiersHs::class, 'id', 'hs_classifier');
    }

    public function origin_place() {
        return $this->hasOne(Country::class, 'id', 'place_of_origin');
    }

    /**
     * Update product rating
     * @param int $id
     */
    public function updateRating($id) {
        $rating = DB::select(DB::raw("SELECT ROUND(AVG(rating),2) FROM product_feedbacks WHERE product_id = {$id}"))[0]->round;
        DB::table('products')->where('id', '=', $id)->update(['rating' => $rating]);
        return;
    }
}

/**
 * @SWG\Definition(
 *   definition="newProduct",
 *   type="object",
 *   required={"creator_id", "customer_company_id"},
 *   allOf={@SWG\Schema(
 *              @SWG\Property(property="name",type="string"),
 *              @SWG\Property(property="description",type="string"),
 *              @SWG\Property(property="price",type="float"),
 *              @SWG\Property(property="currency_id",type="integer"),
 *              @SWG\Property(property="discount_percent",type="integer", format="0.00"),
 *              @SWG\Property(property="earliest_shipping_date",type="string", format="date-time",),
 *              @SWG\Property(property="quantity_available",type="integer"),
 *              @SWG\Property(property="minimum_ordery",type="integer"),
 *              @SWG\Property(property="company_id",type="integer"),
 *              @SWG\Property(property="created_by",type="integer"),
 *              @SWG\Property(property="unit",type="integer"),
 *              @SWG\Property(property="bulk_prices",type="array",
 *                  @SWG\Items(
 *                       @SWG\Property(property="price",type="integer"),
 *                       @SWG\Property(property="quantity",type="integer"),
 *                  )
 *              ),
 *              @SWG\Property(
 *                  property="attributes",
 *                  type="array",
 *                  @SWG\Items(
 *                      @SWG\Property(property="name", type="string"),
 *                      @SWG\Property(property="value", type="string"),
 *                      @SWG\Property(property="attribute_id", type="integer"),
 *                      @SWG\Property(property="attribute_value_id", type="integer"),
 *                  ),
 *              ),
 *              @SWG\Property(
 *                  property="certificates",
 *                  type="array",
 *                  @SWG\Items(
 *                      @SWG\Property(property="name", type="string"),
 *                      @SWG\Property(property="type", type="string"),
 *                      @SWG\Property(property="url", type="string"),
 *                  ),
 *              ),
 *              @SWG\Property(property="negotiate",type="boolean"),
 *              @SWG\Property(property="is_draft",type="boolean"),
 *              @SWG\Property(property="categories_ids",type="array",
 *                  @SWG\Items(
 *                      @SWG\Property(property="category_id", type="integer"),
 *                  )
 *              ),
 *          )
 *   }
 * )
 */

/**
 * @SWG\Definition(
 *   definition="Product",
 *   type="object",
 *   allOf={@SWG\Schema(
 *              @SWG\Property(property="name",type="string"),
 *              @SWG\Property(property="description",type="string"),
 *              @SWG\Property(property="rating",type="integer", format="0.0"),
 *              @SWG\Property(property="price",type="integer", format="0.00"),
 *              @SWG\Property(property="currency_id",type="integer"),
 *              @SWG\Property(property="discount_percent",type="integer", format="0.00"),
 *              @SWG\Property(property="discount_cash",type="integer", format="0.00"),
 *              @SWG\Property(property="earliest_shipping_date",type="string", format="date-time"),
 *              @SWG\Property(property="quantity_available",type="integer"),
 *              @SWG\Property(property="minimum_ordery",type="integer"),
 *              @SWG\Property(property="company_id",type="integer"),
 *              @SWG\Property(property="created_by",type="integer"),
 *              @SWG\Property(property="unit",type="string"),
 *              @SWG\Property(property="bulk_prices",type="array",
 *                  @SWG\Items(
 *                       @SWG\Property(property="price",type="integer"),
 *                       @SWG\Property(property="quantity",type="integer"),
 *                  )
 *              ),
 *              @SWG\Property(
 *                  property="certificates",
 *                  type="array",
 *                  @SWG\Items(
 *                      @SWG\Property(property="name", type="string"),
 *                      @SWG\Property(property="type", type="string"),
 *                      @SWG\Property(property="url", type="string"),
 *                  ),
 *              ),
 *              @SWG\Property(
 *                  property="attributes",
 *                  type="array",
 *                  @SWG\Items(
 *                      @SWG\Property(property="name", type="string"),
 *                      @SWG\Property(property="value", type="string"),
 *                      @SWG\Property(property="attribute_id", type="integer"),
 *                      @SWG\Property(property="attribute_value_id", type="integer"),
 *                  ),
 *              ),
 *              @SWG\Property(property="place_of_origin",type="integer"),
 *              @SWG\Property(property="hs_classifier",type="integer"),
 *              @SWG\Property(property="location_city",type="integer"),
 *              @SWG\Property(property="location_country",type="integer"),
 *              @SWG\Property(property="port",type="string"),
 *              @SWG\Property(property="external_id",type="string"),
 *              @SWG\Property(property="integration_name",type="string"),
 *              @SWG\Property(property="negotiate",type="boolean"),
 *              @SWG\Property(
 *                  property="images",
 *                  type="array",
 *                  @SWG\Items(
 *                      @SWG\Property(property="name", type="string"),
 *                      @SWG\Property(property="type", type="string"),
 *                      @SWG\Property(property="url", type="string"),
 *                  ),
 *              ),
 *              @SWG\Property(property="is_draft",type="boolean"),
 *              @SWG\Property(property="parent_id",type="integer"),
 *              @SWG\Property(property="paid_order",type="integer"),
 *              @SWG\Property(property="weight",type="integer", format="0.0"),
 *              @SWG\Property(property="dim_weight",type="integer", format="0.0"),
 *              @SWG\Property(property="width",type="integer", format="0.0"),
 *              @SWG\Property(property="height",type="integer", format="0.0"),
 *              @SWG\Property(property="depth",type="integer", format="0.0"),
 *              @SWG\Property(property="shipping",type="array",
 *                  @SWG\Items(
 *                      @SWG\Property(property="shipping_name",type="string")
 *                  )
 *              ),
 *              @SWG\Property(property="categories_ids",type="array",
 *                  @SWG\Items(
 *                      @SWG\Property(property="category_id", type="integer")
 *                  )
 *              ),
 *              @SWG\Property(property="created_date",type="string", format="date-time"),
 *              @SWG\Property(property="edited_date",type="string", format="date-time"),
 *              @SWG\Property(property="company", ref="#/definitions/Company"),
 *              @SWG\Property(property="creator", ref="#/definitions/User"),
 *          )
 *   }
 * )
 */