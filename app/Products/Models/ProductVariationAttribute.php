<?php

namespace App\Products\Models;


use App\AttributeOptions\Models\AttributeOption;
use App\Attributes\Models\Attribute;
use Illuminate\Database\Eloquent\Model;

class ProductVariationAttribute extends Model {
    public $timestamps = false;

    protected $fillable = [
        'product_variation_id', 'attribute_id', 'attribute_option_id', 'product_id'
    ];

    protected $appends = [
        'attribute_name', 'attribute_option_name'
    ];

    protected $hidden = [
        'attribute', 'product_variation_id', 'id', 'option', 'product_id'
    ];

    public function getAttributeNameAttribute(): string {
        return $this->attribute->name;
    }

    public function getAttributeOptionNameAttribute(): string {
        return $this->option->name;
    }

    public function attribute() {
        return $this->belongsTo(Attribute::class);
    }

    public function option() {
        return $this->belongsTo(AttributeOption::class, 'attribute_option_id');
    }
}