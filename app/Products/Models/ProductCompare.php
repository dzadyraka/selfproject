<?php

namespace App\Products\Models;

use Illuminate\Support\Facades\Input;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class ProductCompare extends Model {
    public $timestamps = false;

    protected $fillable = ['user_id', 'product_id'];

    public static function boot() {
        parent::boot();
        static::addGlobalScope('user_id', function (Builder $builder) {
            $builder->where('user_id', '=', Input::get('user_id'));
        });
    }

    public function product() {
        return $this->hasOne(Product::class, 'id', 'product_id');
    }
}


/**
 * @SWG\Definition(
 *   definition="newProductCompare",
 *   type="object",
 *   allOf={
 *       @SWG\Schema(
 *           @SWG\Property(property="users_id", type="integer"),
 *           @SWG\Property(property="products_id", type="integer")
 *       )
 *   }
 * )
 */

/**
 * @SWG\Definition(
 *   definition="ProductCompare",
 *   type="object",
 *   allOf={
 *       @SWG\Schema(
 *           @SWG\Property(property="users_id", type="integer"),
 *           @SWG\Property(property="products_id", type="integer"),
 *           @SWG\Property(
 *              property="product",
 *              ref="#/definitions/Product"
 *          ),
 *       )
 *   }
 * )
 */