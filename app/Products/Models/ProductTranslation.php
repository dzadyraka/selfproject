<?php

namespace App\Products\Models;

use Illuminate\Database\Eloquent\Model;

class ProductTranslation extends Model {
    public $timestamps = false;
    protected $fillable = ['name', 'description', 'attributes_list'];
    protected $casts = [
        'attributes_list' => 'json'
    ];
}
