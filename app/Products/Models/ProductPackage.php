<?php

namespace App\Products\Models;


use Illuminate\Database\Eloquent\Model;

class ProductPackage extends Model {
    public $timestamps = false;

    protected $fillable = [
        'product_id', 'description', 'weight', 'weight_unit',
        'size_unit', 'length', 'width', 'height', 'quantity_in_package'
    ];
}