<?php

namespace App\Products\Models;

use App\Users\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ProductView extends Model {
    protected $fillable = ['user_id', 'product_id', 'count_views'];

    public function creator() {
        return $this->belongsTo(User::class);
    }

    public function product() {
        return $this->belongsTo(Product::class);
    }

    /**
     * Creating products recommendations by views
     *
     * @param null $product_id
     * @return mixed
     */
    public function recommendations($product_id) {

        $product_ids = DB::select('SELECT product_id FROM
        (
          SELECT views.product_id, sum(views.count_views) FROM product_views as views

          WHERE views.user_id IN

                  (SELECT view2.user_id FROM product_views as view2 WHERE view2.product_id = ' . $product_id . ')
                  
          AND views.product_id <> ' . $product_id . '

          GROUP BY views.product_id ORDER BY sum DESC LIMIT 15
        ) pv');

        $product_ids = array_map(function ($value) {
            return ((array)$value)['product_id'];
        }, $product_ids);

        return Product::whereIn('id', $product_ids)->with(['company', 'creator', 'currency'])->get();
    }
}
