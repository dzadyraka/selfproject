<?php

namespace App\Products\Models;


use App\AttributeOptions\Models\AttributeOption;
use App\Attributes\Models\Attribute;
use Illuminate\Database\Eloquent\Model;

class ProductAttribute extends Model {
    public $timestamps = false;

    protected $table = 'product_attribute';
    protected $fillable = [
        'product_id', 'attribute_id'
    ];

    protected $appends = [
        'name'
    ];

    protected $hidden = [
        'attribute', 'product_id', 'id'
    ];

    public function getNameAttribute(): string {
        return $this->attribute->name;
    }

    public function attribute() {
        return $this->belongsTo(Attribute::class);
    }

    public function optionsList() {
        return $this->belongsToMany(AttributeOption::class, 'product_attribute_options');
    }
}