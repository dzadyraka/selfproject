<?php

namespace App\Products\Models;

use App\Users\Models\User;
use App\Http\Traits\HasUserId;
use App\Companies\Models\Company;
use Illuminate\Database\Eloquent\Model;

class ProductFeedback extends Model {
    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'updated_date';

    use HasUserId;

    protected $fillable = [
        'user_id', 'product_id', 'company_id', 'content', 'rating'
    ];

    public function creator() {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function product() {
        return $this->belongsTo(Product::class);
    }

    public function company() {
        return $this->belongsTo(Company::class);
    }
}

/**
 *  @SWG\Definition(
 *   definition="newProductFeedback",
 *   type="object",
 *   allOf={
 *       @SWG\Schema(
 *           @SWG\Property(property="users_id", type="integer"),
 *           @SWG\Property(property="products_id", type="integer"),
 *           @SWG\Property(property="companies_id", type="integer"),
 *           @SWG\Property(property="content", type="string"),
 *           @SWG\Property(property="rating", type="integer"),
 *       )
 *   }
 * )
 */

/**
 *  @SWG\Definition(
 *   definition="ProductFeedback",
 *   type="object",
 *   allOf={
 *       @SWG\Schema(
 *           @SWG\Property(property="users_id", type="integer"),
 *           @SWG\Property(property="products_id", type="integer"),
 *           @SWG\Property(property="companies_id", type="integer"),
 *           @SWG\Property(property="content", type="string"),
 *           @SWG\Property(property="rating", type="integer"),
 *           @SWG\Property(property="created_date", type="integer", format="date-time"),
 *           @SWG\Property(property="edited_date", type="integer", format="date-time"),
 *           @SWG\Property(property="product", ref="#/definitions/Product"),
 *           @SWG\Property(property="creator", ref="#/definitions/User"),
 *           @SWG\Property(property="company", ref="#/definitions/Company"),
 *       )
 *   }
 * )
 */