<?php

namespace App\Products\Importers;


use Maatwebsite\Excel\Concerns\Importable;

class ProductImporter {

    use Importable;
}