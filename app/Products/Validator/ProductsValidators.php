<?php


namespace App\Products\Validator;

use Exception;
use Validator;
use Illuminate\Validation\ValidationException;

class ProductsValidators {

    /**
     * Validate product variants
     *
     * @param $inputs
     * @throws ValidationException
     */
    public static function productVariation($inputs) {
        $rules = [
            'price' => 'required',
            'quantity_available' => 'required',
            'minimum_ordery' => 'required',
        ];

        if (!empty($inputs['has_sample'])) {
            $rules['sample_price'] = 'required';
        }

        $validate = Validator::make($inputs, $rules);

        if ($validate->fails()) {
            throw new ValidationException($validate);
        }
    }

    /**
     * Validate product variant attributes
     *
     * @param $inputs
     * @throws Exception
     * @throws ValidationException
     */
    public static function productVariationAttribute($inputs) {
        $rules = [
            'attribute_id' => 'required',
//            'id' => 'required'
        ];

        $validate = Validator::make($inputs, $rules);

        if ($validate->fails()) {
            throw new ValidationException($validate);
        }
    }
}