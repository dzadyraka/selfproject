<?php

namespace App\Products\Validator;


use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ImportProductsValidator {
    public static function validateFileUpload(UploadedFile $file) {
        $validate = Validator::make(
            [
                'file' => $file,
                'extension' => strtolower($file->getClientOriginalExtension()),
            ],
            [
                'file' => 'required',
                'extension' => 'required|in:csv,xlsx,xls,tsv',
            ]
        );

        if ($validate->fails()) {
            throw new ValidationException($validate);
        }
    }
}