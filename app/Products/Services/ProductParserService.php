<?php

namespace App\Products\Services;


use App\Categoties\Models\Category;
use App\Classifiers\Models\ClassifiersHs;
use App\Currency\Currency;
use App\Products\Importers\ProductImporter;
use App\Products\Models\Product;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ProductParserService {

    /**
     * Method returns value of first row in xls/csv file for fields-comparing
     *
     * @param UploadedFile $file
     * @return array
     */
    public function getFirstRow(UploadedFile $file): array {
        return $this->getFileParser($file)->getFirstRow();
    }

    /**
     * Method parses file, and create products
     *
     * @param UploadedFile $file
     * @param array $properties
     * @return Collection
     */
    public function parseFile(UploadedFile $file, array $properties): Collection {
        return DB::transaction(function () use ($file, $properties) {
            return $this->getFileParser($file)->getCollection()
                ->slice($properties['skip_first_row'] ? 1 : 0)
                ->map(function ($item) use ($properties) {
                    $item = $item->toArray();

                    if (empty($item) || !$item[0]) {
                        return null;
                    }

                    $fields = $properties;
                    foreach ($properties['fields'] as $key => $field) {
                        if (!is_null($field) && $key) {
                            $fields[$key] = $item[(int)$field];
                        }
                    }

                    unset($fields['attachments']);
                    unset($fields['location_country']);
                    unset($fields['export_info_id']);
                    unset($fields['place_of_origin']);
                    // permanent

                    if (isset($fields['bulk_prices'])) {
                        try {
                            $fields['bulk_prices'] = $this->transformBulkPrices($fields['bulk_prices']);
                        } catch (\Exception $exception) {
                            $fields['bulk_prices'] = [];
                        }
                    } else {
                        $fields['bulk_prices'] = [];
                    }

                    $fields = $this->transformFields($fields);
                    $product = Product::create($fields);

                    return $product->fresh([
                        'company', 'creator', 'attributesList', 'currency', 'categories',
                        'variations' => function ($variations) {
                            return $variations->with('attributesList');
                        }
                    ]);
                })
                ->filter()
                ->values();
        });
    }

    /**
     * Small helper, that helps to work with files
     *
     * @param UploadedFile $file
     * @return object
     */
    public function getFileParser(UploadedFile $file): object {
        return new class ($file) {
            private $_collection;

            public function __construct(UploadedFile $file) {
                $this->_collection = (new ProductImporter)->toCollection($file)->first();
            }

            public function getFirstRow(): array {
                return array_values(array_map(function ($column) {
                    return is_null($column) ? '-' : $column;
                }, $this->_collection->first()->toArray()));
            }

            public function getCollection(): Collection {
                return $this->_collection;
            }
        };
    }

    private function transformFields(array $fields = []): array {
        foreach ($fields as $key => $field) {
            $fields[$key] = $this->transformField($key, $field);
        }

        return $fields;
    }

    private function transformField($key, $value) {
        switch ($key) {
            case "description":
            case "sample_description":
            case "name":
            case "sample_name":
            case "sku":
            case "unit":
                return (string)$value;
            case "price":
            case "sample_price":
            case "height":
            case "sample_discount":
            case "sample_height":
            case "sample_width":
            case "weight":
            case "width":
                if (is_string($value)) {
                    return floatval(str_replace(",", ".", $value));
                }
                return $value ?? 0;
            case "depth":
            case "sample_depth":
            case "minimum_ordery":
            case "quantity_available":
            case "quantity_standard":
                return $value ? intval($value) : 0;
            case "has_sample":
            case "negotiate":
                return $value && ($value == "Yes" || $value == "YES" || $value == "Y" || $value == "TRUE") ? true : false;
            case "category_id":
                $category = null; // Category::whereId($value)->first();
                return $category ? $category->id : null;
            case "currency_id":
                $currency = Currency::whereCode($value)->first();
                return $currency ? $currency->id : null;
            case "hs_classifier":
                $hs = ClassifiersHs::whereCode($value)->first();
                return $hs ? $hs->id : null;
            case "sample_image":
                if (empty($value)) {
                    return '';
                }
                $images = $this->downloadImages($value);
                return !empty($images) ? $images[0]['url'] : '';
            case "images":
                return !empty($value) ? $this->downloadImages($value) : [];
            case "certifications":
                return [];
            default:
                return $value;
        }
    }

    private function transformBulkPrices(string $prices): array {
        return array_map(function (string $price) {
            list($price, $quantity) = explode(', ', $price);

            return [
                'price' => explode(': ', $price)[1] ?? 0,
                'quantity' => explode(': ', $quantity)[1] ?? 1
            ];
        }, explode("\n", $prices));
    }

    private function downloadImages(string $images): array {
        return array_values( // Restart numeration
            array_filter( // Filter un-parsed images
                array_map(function (string $image): array { // Download image
                    $filename = (string)time() . '-' . basename($image);
                    $file_path = "imported/product_images/$filename";

                    try {
                        $contents = file_get_contents($image);
                        Storage::put($file_path, $contents);

                        return [
                            'url' => Storage::url($file_path),
                            'name' => $filename,
                            'type' => pathinfo(storage_path() . $file_path, PATHINFO_EXTENSION)
                        ];
                    } catch (\Exception $e) {
                        return null;
                    }
                }, explode("\n", $images))
            )
        );
    }
}