<?php

namespace App\Products\Services;


use App\Attributes\Models\Attribute;
use App\Categoties\Models\Category;
use App\Products\Models\Product;
use App\Products\Models\ProductAttribute;
use App\Products\Models\ProductVariation;
use App\Products\Models\ProductVariationAttribute;
use App\Products\Repositories\ProductRepository;
use App\Products\Validator\ProductsValidators;
use App\Services\Service;
use App\Users\Models\User;
use Dotenv\Exception\ValidationException;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\Facades\JWTAuth;

class ProductService extends Service {
    public function __construct(ProductRepository $repository) {
        parent::__construct($repository);
    }

    public function getFilteredProductsList(Request $request) {
        $products = $this->filterProducts($request->input('filter', []));

//        if (!empty($filter['price'])) {
//            if (!is_null(array_values($filter['price'])[0])) {
//                $products->where('price', '>=', array_values($filter['price'])[0]);
//            }
//
//            if (!is_null(array_values($filter['price'])[1])) {
//                $products->where('price', '<', array_values($filter['price'])[1]);
//            }
//        }
//
//        if (!empty($filter['quantity_available'])) {
//            $products->where('quantity_available', '>=', [$filter['quantity_available']]);
//        }
//
//        if (!empty($filter['minimum_ordery'])) {
//            $min_order = substr($filter['minimum_ordery'], 0, 6);
//            $products->where('minimum_ordery', '<=', [$min_order]);
//        }
//
//        if (!empty($filter['categories'])) {
//            $products->whereHas('categories', function ($categories) use ($filter) {
//                $categories->whereIn('category_id', $filter['categories']);
//            });
//        }
//
//        if (!empty($filter['hs'])) {
//            $products->whereIn('hs_classifier', $filter['hs']);
//        }
//
//        if (!empty($filter['request_sample'])) {
//            $products->whereHas('request_sample', function ($request_sample) {
//                return $request_sample;
//            });
//        }

//        if (!empty($filter['attributes'])) {
//            $attributes = $filter['attributes']['value'];
//
//            foreach ($attributes as $attribute => $attribute_options) {
//                $products->whereIn('id', function ($query) use ($attribute, $attribute_options) {
//                    $query->select('product_id')->from('product_attribute')->where('attribute_id', $attribute)->whereIn('attribute_option_id', $attribute_options);
//                });
//            }
//        }

//        if (!empty($filter['sort'])) {
//            if (!empty($filter['sort']['name'])) {
//                $products->where(function ($query) use ($filter) {
//                    foreach ($filter['sort'] as $col => $sort) {
//                        $query->orderBy($col, $sort);
//                    }
//                });
//            } else {
//                foreach ($filter['sort'] as $col => $sort) {
//                    $products->orderBy($col, $sort);
//                }
//            }
//        }

        $userIsAuthorised = false;

        try {
            if (JWTAuth::parseToken()) {
                $userIsAuthorised = true;
            }
        } catch (\Exception $e) {
        }

        if ($userIsAuthorised) {
            $currency = JWTAuth::parseToken()->toUser()->currency;
        } else {
            $currency = null;
        }

        $data_products = $products->get()->map(function ($product) use ($request, $userIsAuthorised, $currency) {
            if (!empty($request->input('quantity')) && $request->input('quantity') > 0) {
                if (!empty($product->bulk_prices) and $request->input('quantity') >= $product->bulk_prices[0]['quantity']) {
                    for ($i = 0; $i < count($product->bulk_prices); $i++) {
                        if ($request->input('quantity') >= $product->bulk_prices[$i]['quantity']) {
                            $product->price = $product->bulk_prices[$i]['price'];
                        } else {
                            break;
                        }
                    }
                }

                $product->price *= $request->input('quantity');
            }

//            if ($currency && $product->currency_id != $currency->id) {
//                $product->converted_price = CurrencyConverter::convert($product->price, $product->currency_id, $currency->id);
//                $product->converted_currency = $currency;
//            }

            return $product;
        })->toArray();


        // shit...
        $categories = [];
        $id = [];

        if (empty($request->input('q')) && !empty($data_products)) {
            $products_ids = array_column($data_products, 'id');
            $query_categories = DB::select(DB::raw('SELECT category_id FROM product_categories WHERE product_id IN (' . implode(', ', $products_ids) . ')'));
            $query_categories = array_map(function ($value) {
                return (array)$value;
            }, $query_categories);

            $id = array_column($query_categories, 'category_id');

            if (!empty($id)) {
                $categories = Category::getList($id);
            }
        }

        $attributes = $this->_productAttributes($request->input(), $id);

        return [
            'products' => $data_products,
            'attributes' => $attributes['attributes'],
            'categories' => !empty($attributes['categories']) ? $attributes['categories'] : $categories
        ];
    }

    public function createProduct(array $data): Product {
        return DB::transaction(function () use ($data) {
            if (!empty($data['variations'])) {
                $data['is_variable'] = true;
            }

            $product = $this->repository->create($data);

            foreach ($data['attributes_list'] ?? [] as $attribute) {
                $attributeModel = ProductAttribute::create([
                    'attribute_id' => $attribute['attribute_id'],
                    'product_id' => $product->id
                ]);

                foreach ($attribute['options_list'] as $option) {
                    $attributeModel->optionsList()->attach([
                        'attribute_option_id' => $option['id']
                    ]);
                }
            }

//            if (!empty($categories_ids)) {
//                if (!empty($request->input('attributes_list'))) {
//                    $args['attributes'] = $this->_attributesSet($request->input('attributes_list'), $categories_ids, $product);
//                }
//
//                $product->categories()->sync($categories_ids);
//            }

//            if (!empty($request->input('request_sample'))) {
//                $insert_request_sample = $request->input('request_sample');
//                Product::create([
//                    'name' => $insert_request_sample['name'],
//                    'description' => $insert_request_sample['description'],
//                    'price' => $insert_request_sample['price'],
//                    'attachments' => $insert_request_sample['attachments'],
//                    'company_id' => $insert_request_sample['company_id'],
//                    'parent_id' => $product->id
//                ]);
//            }

            if (!empty($data['variations'])) {
                foreach ($data['variations'] as $variation) {
                    $variation['product_id'] = $product->id;
                    ProductsValidators::productVariation($variation);
                    $product_variant = ProductVariation::create($variation);

                    foreach ($variation['attributes_list'] as $attribute_item) {
                        ProductsValidators::productVariationAttribute($attribute_item);

                        ProductVariationAttribute::create([
                            'product_id' => $product->id,
                            'product_variation_id' => $product_variant->id,
                            'attribute_option_id' => $attribute_item['id'],
                            'attribute_id' => $attribute_item['attribute_id']
                        ]);
                    }
                }
            }

            return $product->fresh([
                'company', 'creator', 'attributesList', 'currency', 'categories',
                'variations' => function ($variations) {
                    return $variations->with('attributesList');
                }
            ]);
        });
    }

    /**
     * Change fields in provided list of products
     *
     * @param array $productsIds
     * @param Request $request
     * @return mixed
     */
    public function bulkSetValueForProducts(array $productsIds, Request $request) {
        if (empty($productsIds)) {
            throw new ValidationException('Empty products ids list');
        }

        $user = JWTAuth::parseToken()->toUser();
        $products = $this->repository
            ->whereIn('id', $productsIds)
            ->whereIn('company_id', $user->companies->pluck('id'));

        DB::transaction(function () use ($products, $request) {
            $products->each(function (Product $product) use ($request) {
                $product->update($request->only($product->getFillable()));
                $product->variations()->each(function (ProductVariation $variation) use ($request) {
                    $variation->update($request->only($variation->getFillable()));
                });
            });
        });

        return $products->get();
    }

    public function updateProduct(int $id, array $data) {
        $product = $this->repository->show($id);

        DB::transaction(function () use ($id, $data, &$product) {
            $product->attributesList()->delete();

            foreach ($data['attributes_list'] ?? [] as $attribute) {
                $attributeModel = ProductAttribute::create([
                    'attribute_id' => $attribute['attribute_id'],
                    'product_id' => $product->id
                ]);

                foreach ($attribute['options_list'] as $option) {
                    $attributeModel->optionsList()->attach([
                        'attribute_option_id' => $option['id']
                    ]);
                }
            }

//            if (!empty($request->input('attributes_list'))) {
//                $category_id = !empty($request->input('category_id')) ? $request->input('category_id') : 0;
//                $args['attributes'] = $this->_attributesSet($request->input('attributes_list'), $category_id, $product);
//            }


//            if (!empty($args['request_sample']) && empty($args['request_sample']['id'])) {
//                $insert_request_sample = $request->input('request_sample');
//                Product::create([
//                    'name' => $insert_request_sample['name'],
//                    'description' => $insert_request_sample['description'],
//                    'price' => $insert_request_sample['price'],
//                    'attachments' => $insert_request_sample['attachments'],
//                    'company_id' => $insert_request_sample['company_id'],
//                    'parent_id' => $product->id
//                ]);
//            } elseif (!empty($args['request_sample']) && !empty($args['request_sample']['id'])) {
//                $insert_request_sample = $request->input('request_sample');
//                Product::find($insert_request_sample['id'])->update([
//                    'name' => $insert_request_sample['name'],
//                    'description' => $insert_request_sample['description'],
//                    'price' => $insert_request_sample['price'],
//                    'attachments' => $insert_request_sample['attachments'],
//                    'company_id' => $insert_request_sample['company_id'],
//                    'parent_id' => $product->id
//                ]);
//            }

            if (!empty($data['variations'])) {
                foreach ($data['variations'] as $variation) {
                    $variation['product_id'] = $id;
                    ProductsValidators::productVariation($variation);

                    if (!empty($variation['id'])) {
                        $product_variant = ProductVariation::findOrFail($variation['id']);
                        $product_variant->update($variation);
                        $product_variant->attributesList()->delete();
                    } else {
                        $product_variant = ProductVariation::create($variation);
                    }

                    foreach ($variation['attributes_list'] as $attribute_item) {
                        ProductsValidators::productVariationAttribute($attribute_item);

                        ProductVariationAttribute::create([
                            'product_id' => $product->id,
                            'product_variation_id' => $product_variant->id,
                            'attribute_option_id' => $attribute_item['id'] ?? $attribute_item['attribute_option_id'],
                            'attribute_id' => $attribute_item['attribute_id']
                        ]);
                    }
                }
            }

            $product->update($data);
        });

        return $product->fresh([
            'company', 'creator', 'currency', 'categories',
            'variations' => function ($variations) {
                return $variations->with('attributesList');
            }
        ]);
    }

    public function addProductToUserBookmarks(User $user, Product $product): void {
        $user->bookmarks_products()->attach($product);
    }

    public function removeProductFromUserBookmarks(User $user, Product $product): void {
        $user->bookmarks_products()->detach($product);
    }

    public function bulkEditProducts(array $products): Collection {
        $user = JWTAuth::parseToken()->toUser();
        $userCompanies = $user->companies->pluck('id');

        DB::transaction(function () use ($products, $userCompanies) {
            foreach ($products as $product) {
                $productModel = $this->repository->whereIn('company_id', $userCompanies)->findOrFail($product['id']);
                $this->updateProduct($productModel->id, $product);
            }
        });

        return $this->repository
            ->whereIn('company_id', $userCompanies)
            ->whereIn('id', array_column($products, 'id'))
            ->with([
                'company', 'creator', 'currency', 'categories',
                'variations' => function ($variations) {
                    return $variations->with('attributesList');
                }
            ])
            ->get();
    }

    // shit method. don't have time to refactor it.
    private function _productAttributes($filter_data = null, $cat_id = []) {
        $products_attributes = [];
        $products = [];
        $new_products = [];
        $ids = [];
        $products_ids = [];
        $query_categories = [];
        $categories = [];

        if (!empty($filter_data['q'])) {
            $q = mb_strtolower($filter_data['q']);
            $products = DB::select(DB::raw('SELECT product_id, attributes_list FROM product_translations WHERE LOWER(name) LIKE \'%' . $q . '%\''));
            foreach ($products as $product) {
                $new_products[] = (array)$product;
            }
            $products = $new_products;

            if (!empty($products)) {
                $products_ids = array_column($products, 'product_id');
                $attributes = array_reduce(array_map(function ($product) {
                    return array_column(json_decode($product['attributes_list'], true), 'attribute_id');
                }, $products), 'array_merge', []);
                $ids = array_unique(array_merge($ids, $attributes));
            }
            $products_attributes = Attribute::with(['attribute_options'])->whereIn('id', $ids)->get()->toArray();
//            $query_categories = Cache::get('query-category');

            if (!empty($products_ids)) {
                $query_categories = DB::select(DB::raw('SELECT category_id FROM product_categories WHERE product_id IN (' . implode(', ', $products_ids) . ')'));
                $query_categories = array_map(function ($value) {
                    return (array)$value;
                }, $query_categories);

//                Cache::put('query-category', $query_categories, 3600);
                $cat_id = array_column($query_categories, 'category_id');
            }
        }

        if (!empty($filter_data['categories'])) {
            $products_attributes = Attribute::with(['attribute_options'])->whereIn('id', function ($query) use ($filter_data) {
                $query->select('attribute_id')->from('category_attributes')->whereIn('category_id', $filter_data['categories']);
            })->get()->toArray();
            $cat_id = array_merge($filter_data['categories'], $cat_id);
        }

        if (empty($filter_data['categories']) && empty($filter_data['q'])) {
            $products_attributes = Attribute::with(['attribute_options'])->get();
        }

        if (!empty($cat_id)) {
            $categories = Category::getList($cat_id);
        }

        return [
            'attributes' => $products_attributes,
            'categories' => $categories
        ];
    }

    public function deleteProducts(array $products): bool {
        $user = JWTAuth::parseToken()->toUser();

        return $this->repository
            ->whereIn('company_id', $user->companies->pluck('id'))
            ->whereIn('id', $products)
            ->delete();
    }

    public function filterProducts(array $filter = []): Builder {
        $products = $this->repository->with([
            'company', 'creator', 'currency', 'categories',
            'variations' => function ($variations) {
                return $variations->with('attributesList');
            }
        ]);

        $products->when(!empty($filter['product_ids']), function ($query) use ($filter) {
            return $query->whereIn('id', $filter['product_ids']);
        });

        $products->when(!empty($filter['q']), function ($query) use ($filter) {
            $name = strtolower($filter['q']);
            return $query->where(function ($query) use ($name) {
                $query->whereTranslationInsensitiveLike('name', "%$name%");
            });
        });

        $products->when(!empty($filter['personal']) && ($filter['personal'] || $filter['personal'] === 'true'), function ($query) {
            $user = JWTAuth::parseToken()->toUser();

            return $query->whereIn('company_id', $user->companies->pluck('id'));
        });

        $products->when($filter['is_draft'] ?? false, function ($query) use ($filter) {
            $query->where('is_draft', $filter['is_draft'] === 'true');
        });

        $products->when($filter['sample'] ?? false, function ($query) {
           $query->limit(0);
        });

        $products->orderByDesc('created_date');

        return $products;
    }
}