<?php

namespace App\Products\Repositories;


use App\Products\Models\Product;
use App\Repositories\Repository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class ProductRepository extends Repository {
    public function __construct(Product $model) {
        parent::__construct($model);
    }

    public function show(int $id): Model {
        return $this->getModel()->with([
            'request_sample',
            'company' => function ($company) {
                return $company->with([
                    'country',
                    'city',
                    'region',
//                        'companies_addresses'
                ]);
            },
            'attributesList',
            'creator',
            'currency',
            'categories',
            'variations' => function ($variations) {
                return $variations->with('attributesList');
            }
        ])->findOrFail($id);
    }

    public function getCompanyProducts(int $companyId): Collection {
        return $this->with([
            'company', 'creator', 'currency', 'categories',
            'variations' => function ($variations) {
                return $variations->with('attributesList');
            }
        ])->whereCompanyId($companyId)->get();
    }
}