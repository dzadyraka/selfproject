<?php

namespace App\Products\Exporters;


use Illuminate\Database\Eloquent\Builder;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class ProductExporter implements FromQuery, WithMapping, WithHeadings {

    use Exportable;

    private $query;

    public function __construct(Builder $query, bool $sample = false) {
        $this->query = $query
            ->leftJoin('product_variations', 'products.id', '=', 'product_variations.product_id')
            ->select(
                'products.*',
                'product_variations.id as variation_id',
                'product_variations.images as variation_images',
                'product_variations.sku as variation_sku',
                'product_variations.price as variation_price',
                'product_variations.minimum_bulk_price as variation_minimum_bulk_price',
                'product_variations.quantity_available as variation_quantity_available',
                'product_variations.minimum_ordery as variation_minimum_ordery',
                'product_variations.negotiate as variation_negotiate',
                'product_variations.bulk_prices as variation_bulk_prices',
                'product_variations.has_sample as variation_has_sample',
                'product_variations.sample_price as variation_sample_price',
                'product_variations.created_date as variation_created_date',
                'product_variations.edited_date as variation_edited_date',
                'product_variations.image as variation_image',
                'product_variations.name as variation_name'
            );

        if ($sample) {
            $this->query->where('product_id', '<', 0);
        }
    }

    /**
     * @return array
     */
    public function getCsvSettings(): array {
        return [
            'delimiter' => ';'
        ];
    }

    public function query() {
        return $this->query;
    }

    /**
     * @param $product
     * @return array
     */
    public function map($product): array {
        return [
            $product->is_variable ? "$product->id-$product->variation_id" : $product->id,
            $product->is_variable ? "$product->name: $product->variation_name" : $product->name,
            $product->unit,
            $product->is_variable ? $product->variation_quantity_available : $product->quantity_available,
            $product->is_variable ? $product->variation_minimum_ordery : $product->minimum_ordery,
            $product->is_variable ? $product->variation_negotiate : $product->negotiate,
            $product->category_id,
            $product->category_id ? $product->category->name : '',
            $product->hs_classifier ? $product->hs->code : '',
            $product->hs_classifier ? $product->hs->name : '',
            $product->is_variable ? env('APP_URL') . '/' . $product->variation_image : $this->_mapImages($product->images),
            $product->is_variable ? $product->variation_sku : $product->sku,
            $product->is_variable ? $product->variation_price : $product->price,
            $product->currency_id ? $product->currency->code : '',
            $product->is_variable ? $this->_mapBulkPrices(json_decode($product->variation_bulk_prices, true)) : $this->_mapBulkPrices($product->bulk_prices),
            $product->weight,
            $product->width,
            $product->height,
            $product->depth,
            $product->quantity_standard,
            $product->description,
            $product->is_variable ? $product->variation_has_sample : $product->has_sample,
            $product->is_variable ? $product->variation_sample_price : $product->sample_price,
            $product->company_id,
            $product->company->name,
            $product->location_country,
            $product->place_of_origin ? $product->origin_place->name : '',
            $product->port
        ];
    }

    /**
     * @return array
     */
    public function headings(): array {
        return [
            'ID',
            'Title',
            'Unit',
            'Available quantity',
            'Minimum order',
            'Negotiate',
            'Category ID',
            'Category Name',
            'HS classifier: Code',
            'HS classifier: Title',
            'Image',
            'SKU',
            'Price',
            'Currency',
            'Bulk Pricing',
            'Weight',
            'Width',
            'Height',
            'Depth',
            'Quantity in standard package',
            'Description',
            'Request a sample',
            'Sample Price',
            'Company ID',
            'Company Name',
            'Place of origin',
            'Port'
        ];
    }

    private function _mapImages(array $images = []): string {
        return join("\n", array_map(function ($image): string {
            return env('APP_URL') . '/' . $image['url'];
        }, $images));
    }

    private function _mapBulkPrices(array $bulk_prices = []): string {
        if (empty($bulk_prices)) {
            return '';
        }

        return join("\n", array_map(function ($price): string {
            return "Price: $price[price], Quantity: $price[quantity]";
        }, $bulk_prices));
    }
}